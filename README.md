**项目说明** 
    数据库中新建数据库 名字随便
    在 application.yml中配置数据库连接
    然后执行以下命令 地址改成自己的
<br>
 ####淘宝的
 `mvn install:install-file -Dfile=D:\helptask\lib\taobao-sdk-java-auto_1582743968868-20200308.jar -DgroupId=com.taobao -DartifactId=kaigeyouhuigou -Dversion=20190725 -Dpackaging=jar`

**项目结构** 
```
sqx_fast
│
├─common 公共模块
│  ├─aspect 系统日志
│  ├─exception 异常处理
│  ├─validator 后台校验
│  └─xss XSS过滤
│ 
├─config 配置信息
│ 
├─modules 功能模块
│  ├─app API接口模块(APP调用)
│  ├─job 定时任务模块
│  ├─article 文章分类模块
│  ├─banner 首页分类bannr图标模块
│  ├─commodity 商品模块
│  ├─common 配置文件模块
│  ├─file 文件上传模块
│  ├─helpTask 助力任务模块
│  ├─invite 邀请好友模块
│  ├─message 消息模块
│  ├─oss 阿里云腾讯云等模块
│  ├─platformTask 平台任务模块
│  ├─oss 文件服务模块
│  └─sys 权限模块
│ 
├─sqxApplication 项目启动类
│  
├──resources 
│  ├─mapper SQL对应的XML文件
│  └─static 静态资源

```
<br> 

**技术选型：** 
- 核心框架：Spring Boot 2.1
- 安全框架：Apache Shiro 1.4
- 视图框架：Spring MVC 5.0
- 持久层框架：MyBatis 3.3
- 定时器：Quartz 2.3
- 数据库连接池：Druid 1.0
- 日志管理：SLF4J 1.7、Log4j
- 页面交互：Vue2.x 
- app：uniApp 
<br> 


 **后端部署**
- 通过git下载源码
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库helpTask_fast，数据库编码为UTF-8
- 执行helpTask.sql文件，初始化数据
- 修改application-dev.yml，更新MySQL账号和密码
- Eclipse、IDEA运行sqxApplication.java，则可启动项目
- Swagger文档路径：http://localhost:8890/sqx_fast/swagger/index.html
- Swagger注解路径：http://localhost:8890/sqx_fast/swagger-ui.html
- 接口详细文档：https://docs.apipost.cn/view/136b4a75ad1d5ca0

<br>
