package org.xqs;

/**
 * @Auther: Laban(yubo_010 @ 163.com)
 * @Date: 2020/11/11 14:45
 * @Description:
 */
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SampleTest {

    @Test
    public void Test(){
        //System.out.println("new test class");
    }
}
