package com.sqx.config;

import com.sqx.modules.sys.oauth2.OAuth2Filter;
import com.sqx.modules.sys.oauth2.OAuth2Realm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 *
 */
@Configuration
public class ShiroConfig {

    @Bean("securityManager")
    public SecurityManager securityManager(OAuth2Realm oAuth2Realm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(oAuth2Realm);
        securityManager.setRememberMeManager(null);
        return securityManager;
    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        //oauth过滤
        Map<String, Filter> filters = new HashMap<>();
        filters.put("oauth2", new OAuth2Filter());
        shiroFilter.setFilters(filters);
        Map<String, String> filterMap = new LinkedHashMap<>();
        filterMap.put("/advert/list", "anon");
        filterMap.put("/goods/recommend", "anon");
        filterMap.put("/selfActivity/state", "anon");
        filterMap.put("/goods/homeGoods", "anon");
        filterMap.put("/goods/find", "anon");
        filterMap.put("/selfActivity/list", "anon");
        filterMap.put("/alioss/**", "anon");
        filterMap.put("/pays/notifyPay", "anon");
        filterMap.put("/cash/selectCashOut", "anon");
        filterMap.put("/ActivityMessage/page/**", "anon");
        filterMap.put("/ActivityMessage/add", "anon");
        filterMap.put("/activity/state/**", "anon");
        filterMap.put("/article/selectArticleClassifyList", "anon");
        filterMap.put("/banner/user/list", "anon");
        filterMap.put("/app/selectInvite", "anon");
        filterMap.put("/helpComplaint/insertHelpComplaint", "anon");
        filterMap.put("/appinfo/", "anon");
        filterMap.put("/helpClassify/selectHelpTaskByClassify", "anon");
        filterMap.put("/helpClassify/selectHelpTaskByTitle", "anon");
        filterMap.put("/helpClassify/select", "anon");
        filterMap.put("/helpClassify/selectClassifyList", "anon");
        filterMap.put("/helpTask/selectHelpTaskDetails", "anon");
        filterMap.put("/helpTask/selectHelpTaskStatistics", "anon");
        filterMap.put("/helpTask/selectTopHelpTask", "anon");
        filterMap.put("/helpTask/selectHelpTask", "anon");
        filterMap.put("/helpTask/getHelpCode", "anon");
        filterMap.put("/helpTask/getHelpTaskId", "anon");
        filterMap.put("/websocket/**", "anon");
        filterMap.put("/selfGoodsBrand/find", "anon");
        filterMap.put("/selfGoodsComment/list", "anon");
        filterMap.put("/selfGoodsComment/count", "anon");
        filterMap.put("/selfGoodsComment/findByOrderId", "anon");
        filterMap.put("/selfGoodsComment/find", "anon");
        filterMap.put("/user/getImg", "anon");
        filterMap.put("/goods/userList", "anon");
        filterMap.put("/goods/brandList", "anon");
        filterMap.put("/article/selectArticleList", "anon");
        filterMap.put("/user/mpCreateQr", "anon");
        filterMap.put("/helpTask/selectHelpTaskV1", "anon");
        filterMap.put("/banner/selectBannerList", "anon");
        filterMap.put("/commodity/selectCommodityList", "anon");
        filterMap.put("/api/aliPay/notifyApp", "anon");
        filterMap.put("/api/aliPay/notifys", "anon");
        filterMap.put("/api/aliPay/notifyAppYunOS", "anon");
        filterMap.put("/api/order/notify", "anon");
        filterMap.put("/api/order/notifyJsApi", "anon");
        filterMap.put("/message/page/**", "anon");
        filterMap.put("/common/type/**", "anon");
        filterMap.put("/file/**", "anon");
        filterMap.put("/webjars/**", "anon");
        filterMap.put("/druid/**", "anon");
        filterMap.put("/appLogin/**", "anon");
        filterMap.put("/sys/login", "anon");
        filterMap.put("/swagger/**", "anon");
        filterMap.put("/v2/api-docs", "anon");
        filterMap.put("/swagger-ui.html", "anon");
        filterMap.put("/swagger-resources/**", "anon");
        filterMap.put("/captcha.jpg", "anon");
        filterMap.put("/aaa.txt", "anon");
        filterMap.put("/**", "oauth2");
        shiroFilter.setFilterChainDefinitionMap(filterMap);
        return shiroFilter;
    }

    @Bean("lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

}
