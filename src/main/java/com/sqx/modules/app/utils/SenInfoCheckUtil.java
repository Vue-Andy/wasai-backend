package com.sqx.modules.app.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.utils.HttpClientUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;


@Component
public class SenInfoCheckUtil {

    private static Logger logger = LoggerFactory.getLogger(SenInfoCheckUtil.class);

    private static String MpAccessToken;

    // 这里使用静态，让 service 属于类
    private static CommonInfoService commonInfoService;

    // 注入的时候，给类的 service 注入
    @Autowired
    public void setWxChatContentService(CommonInfoService commonInfoService) {
        SenInfoCheckUtil.commonInfoService = commonInfoService;
    }


    /**
     * 获取Token  小程序
     * @param
     * @param
     * @return AccessToken
     */
    public static String getMpToken(){
        if(StringUtils.isEmpty(MpAccessToken)){
            getMpAccessToken();
        }
        return MpAccessToken;
    }


    public static void getImg(String relation,String goodsId,String type, String page,HttpServletResponse response){
        String mpToken = getMpToken();
        //获取二维码数据
        String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+mpToken;
        Map<String,Object> map = Maps.newHashMap();
        map.put("scene",relation+"&"+goodsId+"&"+type);
        String value = commonInfoService.findOne(105).getValue();
        if("是".equals(value)){
            map.put("page",page);
        }
        map.put("width", 280);
        String jsonString = JSON.toJSONString(map);
        InputStream inputStream = sendPostBackStream(url, jsonString);
        //生成二维码图片
        response.setContentType("image/png");
        try{
            BufferedImage bi = ImageIO.read(inputStream);
            ImageIO.write(bi, "JPG", response.getOutputStream());
            inputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public static void getImg(String page, String scene,String width, HttpServletResponse response){
        String mpToken = getMpToken();
        //获取二维码数据
        String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+mpToken;
        Map<String,Object> map = Maps.newHashMap();
        map.put("scene",scene);
        map.put("page",page);
        map.put("width", width);
        String jsonString = JSON.toJSONString(map);
        InputStream inputStream = sendPostBackStream(url, jsonString);
        //生成二维码图片
        response.setContentType("image/png");
        try{
            BufferedImage bi = ImageIO.read(inputStream);
            ImageIO.write(bi, "JPG", response.getOutputStream());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 获取二维码图片
     */
    public static void getPoster(String invitationCode, HttpServletResponse response){
        String mpToken = getMpToken();
        //获取二维码数据
        String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+mpToken;
        Map<String,Object> map = Maps.newHashMap();
        map.put("scene",invitationCode);
        map.put("width", 280);
        String jsonString = JSON.toJSONString(map);
        InputStream inputStream = sendPostBackStream(url, jsonString);
        //生成二维码图片
        response.setContentType("image/png");
        try{
            BufferedImage bi = ImageIO.read(inputStream);
            ImageIO.write(bi, "JPG", response.getOutputStream());
            inputStream.close();
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }

    private static InputStream sendPostBackStream(String url, String param) {
        PrintWriter out = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            //解决乱码问题
            OutputStreamWriter outWriter =new OutputStreamWriter(conn.getOutputStream(), "utf-8");
            out =new PrintWriter(outWriter);
            // 发送请求参数
            if(StringUtils.isNotBlank(param)) {
                out.print(param);
            }
            // flush输出流的缓冲
            out.flush();
            return conn.getInputStream();
        } catch (Exception e) {
            logger.error("发送 POST 请求出现异常！"+e);
        } finally{
            IOUtils.closeQuietly(out);
        }
        return null;
    }


    /**
     * 获取access_token
     * 每个两个小时自动刷新AcessTocken
     */
    @Scheduled(cron = "0 0 */2 * * ?")
    public static void getMpAccessToken(){
        String appid = commonInfoService.findOne(45).getValue();
        String secret = commonInfoService.findOne(46).getValue();
        String jsonResult = HttpClientUtil.doPost("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret);
        JSONObject parseObject = JSON.parseObject(jsonResult);
        logger.info("=========accessTokenOut========="+parseObject.toJSONString());

        String errcode = parseObject.getString("errcode");
        String accessToken = parseObject.getString("access_token");
        String expiresIn = parseObject.getString("expires_in");
        MpAccessToken=accessToken;
    }


}
