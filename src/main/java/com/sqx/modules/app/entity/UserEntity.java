package com.sqx.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


/**
 * 用户
 *
 */
@Data
@TableName("tb_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@TableId
	private Long userId;
	/**
	 * 用户名
	 */
	private String username;

	private String openId;
	private String wxId;
	private String clientid;
	private String invitationCode;
	private Integer member;
	private String unionId;
	private String imageUrl;
	private Integer gender;
	private String address;
	private String nickName;
	private String zhifubao;
	private String openUid;
	private String zhifubaoName;
	private Long superior;
	private String inviterCode;
	private Integer state;

	/**
	 *
	 * 手机号
	 */
	private String phone;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 创建时间
	 */
	private String createTime;

	/**
	 * 是否可以发任务 1可以 2不可以
	 */
	private Integer isTask;

	/**
	 * 是否是系统用户 1否 2是
	 */
	private Integer isSysUser;


}
