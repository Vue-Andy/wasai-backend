package com.sqx.modules.app.entity;

import lombok.Data;

/**
 * @author fang
 * @date 2020/7/10
 */
@Data
public class LoginInfo {

    private String phone;
    private String msg;
    private String openid;
    private String pwd;
    private String invitation;
    private Long userId;
    private String imageUrl;
    private String nickName;
    private String old;
    private String grade;
    private String platform;
    private String unionid;
    private String token;
    private String invitationCode;

}