package com.sqx.modules.app.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.app.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户
 *
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

    UserEntity selectUserByOpenId(String openId);

    List<UserEntity> selectUserList();

    List<UserEntity> selectUserLists();

    UserEntity selectUserByPhone(String openId);

    Integer selectUserByMemberCount(Integer member);

    UserEntity getUserByUnionId(String unionId);

    UserEntity getUserByWxId(String openId);

    UserEntity getUserByOpenUid(String openUid);

    @Override
    int insert(UserEntity userEntity);

    UserEntity selectUserByInvitationCode(String invitationCode);

    int selectZhiUserInviteCount(String invitationCode);

    int selectFeiUserInviteCount(String invitationCode);

    Double selectZhiUserInviteMoney(String invitationCode);

    Double selectFeiUserInviteMoney(String invitationCode);

    IPage<UserEntity> selectInvitationCodeByUserIdLists(Page<UserEntity> page, @Param("invitationCode") String invitationCode);

    IPage<UserEntity> selectInviterCodeByUserIdLists(Page<UserEntity> page, @Param("invitationCode") String invitationCode);

    IPage<Map<String,Object>> selectZhiInviteByUserIdList(Page<Map<String,Object>> page, @Param("userId") Long userId);

    IPage<Map<String,Object>> selectFeiInviteByUserIdList(Page<Map<String,Object>> page, @Param("userId") Long userId);

    List<Map<String,Object>> selectRankingList();

    Map<String,Object> selectRankingByUserId(Long userId);

    List<UserEntity> findAllList();

    List<UserEntity> selectUserListByCreateTime(@Param("time") String time,@Param("flag") Integer flag);

    Integer selectUserCountByTime(@Param("time") String time);


}
