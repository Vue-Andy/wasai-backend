package com.sqx.modules.app.controller;


import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.LoginInfo;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.invite.service.InviteService;
import com.sqx.modules.sys.controller.AbstractController;
import com.sqx.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * APP登录授权
 *
 */
@RestController
@RequestMapping("/app")
@Api(value = "APP管理", tags = {"APP管理"})
public class AppController extends AbstractController {

    @Autowired
    private UserService userService;
    @Autowired
    private HelpRateService helpRateService;
    @Autowired
    private InviteService inviteService;
    @Autowired
    private CommonInfoService commonInfoService;


    @RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("用户端修改密码")
    public Result updatePwd(@RequestBody LoginInfo loginInfo) {
        UserEntity userEntity = userService.selectById(loginInfo.getUserId());
        if(userEntity==null){
            return Result.error("账号不存在！");
        }
        if(!userEntity.getPassword().equals(DigestUtils.sha256Hex(loginInfo.getOld()))){
            return Result.error("原始密码不正确！");
        }
        if(loginInfo.getPwd().equals(loginInfo.getOld())){
            return Result.error("新密码不能与旧密码相同！");
        }
        userEntity.setPassword(DigestUtils.sha256Hex(loginInfo.getPwd()));
        userService.updateById(userEntity);
        return Result.success();
    }

    @RequestMapping(value = "/updatePhone", method = RequestMethod.POST)
    @ApiOperation("用户端换绑手机号")
    @ResponseBody
    public Result updatePhone(@RequestParam String phone, @RequestParam String msg, @RequestParam String userId) {
        return userService.updatePhone(phone, msg, userId);
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ApiOperation("用户修改个人信息")
    @ResponseBody
    public Result updateUserImageUrl(UserEntity userEntity) {
        userService.updateById(userEntity);
        return Result.success();
    }


    @RequestMapping(value = "/updateUserImageUrl", method = RequestMethod.POST)
    @ApiOperation("用户修改头像")
    @ResponseBody
    public Result updateUserImageUrl(Long userId,String ImageUrl) {
        UserEntity userEntity=new UserEntity();
        userEntity.setUserId(userId);
        userEntity.setImageUrl(ImageUrl);
        userService.updateById(userEntity);
        return Result.success();
    }

    @RequestMapping(value = "/updateUserName", method = RequestMethod.POST)
    @ApiOperation("用户修改昵称")
    @ResponseBody
    public Result updateUserName(Long userId,String userName) {
        UserEntity userEntity=new UserEntity();
        userEntity.setUserId(userId);
        userEntity.setNickName(userName);
        userService.updateById(userEntity);
        return Result.success();
    }

    @RequestMapping(value = "/selectUserById", method = RequestMethod.POST)
    @ApiOperation("获取用户详细信息")
    @ResponseBody
    public Result selectUserById(Long userId) {
        SysUserEntity user = getUser();
        if(user.getUserId()==-1){
            userId=Long.parseLong(user.getUsername());
        }
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity!=null){
            userEntity.setPassword(null);
        }
        return Result.success().put("data",userEntity);
    }

    /**
     * 用户的信息查询
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/userinfo/{userId}", method = RequestMethod.GET)
    @ApiOperation("用户端用户的信息查询")
    @ResponseBody
    public Result cashUserInfo(@PathVariable Long userId) {
        SysUserEntity user = getUser();
        if(user.getUserId()==-1){
            userId=Long.parseLong(user.getUsername());
        }
        UserEntity one = userService.selectById(userId);
        if (one != null) {
            return Result.success().put("data",one);
        }
        return Result.error("账号不存在！");
    }

    @RequestMapping(value = "/insertZhiFuBao", method = RequestMethod.POST)
    @ApiOperation("保存支付宝账号")
    @ResponseBody
    public Result insertZhiFuBao(Long userId,String zhifubao,String zhifubaoName) {
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity!=null){
            if(userEntity.getPhone()==null){
                userEntity.setPhone(zhifubao);
            }
            userEntity.setZhifubao(zhifubao);
            userEntity.setZhifubaoName(zhifubaoName);
            userService.updateById(userEntity);
            return Result.success();
        }else{
            return Result.error(-100,"账号不存在");
        }
    }

    @RequestMapping(value = "/selectRankingList", method = RequestMethod.GET)
    @ApiOperation("获取排行榜信息")
    @ResponseBody
    public Result selectRankingList(Long userId) {
        return userService.selectRankingList(userId);
    }


    @RequestMapping(value = "/selectInvite", method = RequestMethod.GET)
    @ApiOperation("邀请好友界面")
    @ResponseBody
    public Result selectInvite(String userId) {
        Long userIds;
        if("undefined".equals(userId)){
            userIds=null;
        }else{
            userIds=Long.parseLong(userId);
        }
        Map<String,Object> map=new HashMap<>();
        UserEntity userEntity=null;
        Integer integer=null;
        Integer count=null;
        Double money=null;
        List<HelpRate> helpRatesInvite = helpRateService.selectRateByClassifyList(4,1);
        List<HelpRate> helpRatesPeople = helpRateService.selectRateByClassifyList(2,1);
        List<HelpRate> helpRatesRate = helpRateService.selectRateByClassifyList(1,1);
        if(userIds!=null){
            userEntity= userService.selectById(userIds);
            integer = inviteService.selectInviteCount(null, userIds);
            count= inviteService.selectInviteCount(1, userIds);
            money= inviteService.selectInviteSum(1, userIds);
        }
        if(userEntity!=null){
            //获取会员等级 1非会员 2初级 3中级  4高级
            if(userEntity.getMember()!=4){
                map.put("notInvite",helpRatesInvite.get(userEntity.getMember()-1).getRate().intValue()-integer);
                map.put("money",helpRatesPeople.get(userEntity.getMember()-1).getRate());
            }else{
                map.put("notInvite","已达到最高等级");
                map.put("money","已达到最高等级");
            }
        }else{
            map.put("notInvite","0");
            map.put("money","0");
        }
        if(integer==null){
            integer=0;
        }
        if(count==null){
            count=0;
        }
        if(money==null){
            money=0.00;
        }
        map.put("user",userEntity);
        map.put("count",count);
        map.put("moneyReward",money);
        map.put("helpRatesInvite",helpRatesInvite);
        map.put("helpRatesPeople",helpRatesPeople);
        map.put("invite",integer);
        CommonInfo one = commonInfoService.findOne(80);
        map.put("reward",one.getValue());
        List<HelpRate> zhiHelpRates = helpRateService.selectRateByClassifyList(5,1);
        List<HelpRate> feiHelpRates = helpRateService.selectRateByClassifyList(6,1);
        map.put("zhiHelpRates",zhiHelpRates);
        map.put("feiHelpRates",feiHelpRates);
        map.put("helpRatesRate",helpRatesRate);
        List<HelpRate> shopRates = helpRateService.selectRateByClassifyList(1,2);
        List<HelpRate> zhiShopRates = helpRateService.selectRateByClassifyList(5,2);
        List<HelpRate> feiShopRates = helpRateService.selectRateByClassifyList(6,2);
        map.put("zhiShopRates",zhiShopRates);
        map.put("feiShopRates",feiShopRates);
        map.put("shopRates",shopRates);
        return Result.success().put("data",map);
    }



}
