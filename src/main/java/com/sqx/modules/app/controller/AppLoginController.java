package com.sqx.modules.app.controller;


import com.sqx.common.utils.Result;
import com.sqx.common.validator.ValidatorUtils;
import com.sqx.modules.app.entity.LoginInfo;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.form.LoginForm;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.app.utils.InvitationCodeUtil;
import com.sqx.modules.app.utils.JwtUtils;
import com.sqx.modules.app.utils.UserConstantInterface;
import com.sqx.modules.app.utils.WxPhone;
import com.sqx.modules.invite.service.InviteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 注册
 *
 */
@RestController
@RequestMapping("/appLogin")
@Api(value = "APP登陆", tags = {"APP登陆"})
public class AppLoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private InviteService inviteService;
    /*@PostMapping("register")
    @ApiOperation("注册")
    public Result register(@RequestBody RegisterForm form){
        //表单校验
        ValidatorUtils.validateEntity(form);

        UserEntity user = new UserEntity();
        user.setMobile(form.getMobile());
        user.setUsername(form.getMobile());
        user.setPassword(DigestUtils.sha256Hex(form.getPassword()));
        user.setCreateTime(new Date());
        user.setIsInvitationCode(form.getIsInvitationCode());
        userService.save(user);
        return Result.success();
    }*/

    /**
     * 登录
     */
    @PostMapping("login")
    @ApiOperation("登录")
    public Result login(@RequestBody LoginForm form){
        //表单校验
        ValidatorUtils.validateEntity(form);

        //用户登录
        long userId = userService.login(form);
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity.getState()!=1){
            return Result.error("账号已被封禁，请联系客服！");
        }
        //生成token
        String token = jwtUtils.generateToken(userId);

        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("expire", jwtUtils.getExpire());
        map.put("userId",userId);
        return Result.success(map);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation("用户端 APP中手机号密码注册")
    @ResponseBody
    public Result registerUser(@RequestBody UserEntity userInfo) {
        if (userInfo.getPhone().isEmpty()) {
            return Result.error("手机号不能为空！");
        } else {
            UserEntity userByPhone = userService.queryByMobile(userInfo.getPhone());
            if (userByPhone == null) {
                userInfo.setPassword(DigestUtils.sha256Hex(userInfo.getPassword()));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                // TODO  用户默认头像 userInfo.setImageUrl();
                userInfo.setCreateTime(simpleDateFormat.format(new Date()));
                String invitationCode = userInfo.getInvitationCode();
                userInfo.setInvitationCode("");
                userService.insert(userInfo);
                //判断是否输入邀请码
                if(StringUtils.isNotBlank(invitationCode)){
                    long inviteeUserId = InvitationCodeUtil.codeToId(invitationCode);
                    UserEntity userEntity = userService.selectById(inviteeUserId);
                    if(userEntity!=null){
                        inviteService.saveBody(userInfo.getUserId(),userEntity);
                    }
                }
                userInfo.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo.getUserId()));
                userService.updateById(userInfo);
                return Result.success();
            } else {
                return Result.error("手机号已经被注册了！");
            }
        }

    }


    @ApiOperation("用户端忘记密码")
    @RequestMapping(value = "/forgetPwd", method = RequestMethod.POST)
    @ResponseBody
    public Result forgetPwd(@RequestBody LoginInfo loginInfo) {
        return userService.forgetPwd(loginInfo.getPwd(), loginInfo.getPhone(), loginInfo.getMsg());
    }


    @ApiOperation("用户端发送验证码")
    @RequestMapping(value = "/sendMsg/{phone}/{state}", method = RequestMethod.GET)
    @ResponseBody
    public Result sendMsg(@PathVariable String phone, @PathVariable String state) {
        return userService.sendMsg(phone, state);
    }

    @ApiOperation("用户端openid登录呢")
    @RequestMapping(value = "/openid/login", method = RequestMethod.GET)
    @ResponseBody
    public Result loginByOpenId(@RequestParam String openid) {
        return userService.loginByOpenId(openid);
    }

    @RequestMapping(value = "/updateClientId", method = RequestMethod.POST)
    @ApiOperation("用户端绑定消息推送clientId")
    @ResponseBody
    public Result updateClientId(@RequestParam String clientId, @RequestParam Long userId) {
        UserEntity userEntity = userService.selectById(userId);
        System.out.println(clientId);
        if (userEntity != null) {
            userEntity.setClientid(clientId);
            userService.updateById(userEntity);
            return Result.success();
        }
        return Result.error("用户不存在！");
    }


    @RequestMapping(value = "/bindOpenid", method = RequestMethod.POST)
    @ApiOperation("用户端绑定openid")
    @ResponseBody
    public Result bindOpenid(@RequestBody LoginInfo loginInfo) {
        if (loginInfo.getOpenid() != null) {
            return userService.bindOpenid(loginInfo);
        } else {
            return userService.loginCode(loginInfo);
        }
    }


    @RequestMapping(value = "/loginWeiXinApp", method = RequestMethod.POST)
    @ApiOperation("用户端微信登录绑定手机号")
    @ResponseBody
    public Result loginWeiXinApp(@RequestBody LoginInfo loginInfo) {
        return userService.loginWeiXinApp(loginInfo);
    }


    @RequestMapping(value = "/loginApp", method = RequestMethod.POST)
    @ApiOperation("用户端使用微信登录")
    @ResponseBody
    public Result loginApp(@RequestBody LoginInfo loginInfo) {
        return userService.loginApp(loginInfo);
    }


    @RequestMapping(value = "/bind/openid/{openid}/{userid}", method = RequestMethod.GET)
    @ApiOperation("用户端绑定opneid")
    @ResponseBody
    public Result bindOpenId(@PathVariable String openid, @PathVariable String userid) {
        return userService.bindOpenId(userid, openid);
    }


    @RequestMapping(value = "/bindPhone", method = RequestMethod.POST)
    @ApiOperation("用户端绑定手机号")
    @ResponseBody
    public Result bindPhone(@RequestParam String phone, @RequestParam String msg, @RequestParam String userId) {
        return userService.bindPhone(userId, phone, msg);
    }

    @ApiOperation("微信小程序登陆")
    @RequestMapping(value = "/wxLogin", method = RequestMethod.GET)
    public Result wxLogin(@ApiParam("小程序code码") String code){
        return userService.wxLogin(code);
    }


    @ApiOperation("小程序登录新增或修改个人信息")
    @RequestMapping(value = "/insertWxUser", method = RequestMethod.POST)
    public Result insertWxUser(@RequestBody UserEntity userInfo){
        return userService.wxRegister(userInfo);
    }


    @ApiOperation("解密手机号")
    @RequestMapping(value = "/selectPhone",method = RequestMethod.POST)
    public Result selectPhone(WxPhone wxPhone) {
        return UserConstantInterface.decryptS5(wxPhone.getDecryptData(), wxPhone.getKey(), wxPhone.getIv());
    }


}
