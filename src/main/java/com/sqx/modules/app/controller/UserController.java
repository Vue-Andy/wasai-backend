package com.sqx.modules.app.controller;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.app.utils.InvitationCodeUtil;
import com.sqx.modules.app.utils.SenInfoCheckUtil;
import com.sqx.modules.helpTask.entity.HelpSendOrder;
import com.sqx.modules.helpTask.entity.UserMoney;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.invite.entity.InviteMoney;
import com.sqx.modules.invite.service.InviteMoneyService;
import com.sqx.modules.invite.service.InviteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/30
 */
@RestController
@Api(value = "用户管理", tags = {"用户管理"})
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private CashOutService cashOutService;
    @Autowired
    private HelpSendOrderService helpSendOrderService;
    @Autowired
    private HelpTakeOrderService helpTakeOrderService;
    @Autowired
    private PayDetailsService payDetailsService;
    @Autowired
    private InviteService inviteService;
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;

    @ApiOperation("二维码生成")
    @GetMapping(value = "/getImg")
    public void getImg(@RequestParam String page,@RequestParam(required = false) String scene, @RequestParam String width,HttpServletResponse response) {
         SenInfoCheckUtil.getImg(page, scene, width, response);
    }


    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    @ApiOperation("获取用户详细信息")
    @ResponseBody
    public Result selectUserById(@PathVariable("userId") Long userId) {
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity!=null){
            userEntity.setPassword(null);
        }
        return Result.success().put("data",userEntity);
    }



    @RequestMapping(value = "/selectUserList", method = RequestMethod.GET)
    @ApiOperation("查询所有用户列表")
    @ResponseBody
    public Result selectUserList(int page, int limit, String member,String phone){
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("member",member);
        map.put("isSysUser","1");
        map.put("phone",phone);
        return Result.success().put("data",userService.selectUserList(map));
    }

    @RequestMapping(value = "/selectSysUserList", method = RequestMethod.GET)
    @ApiOperation("查询系统用户列表")
    @ResponseBody
    public Result selectSysUserList(int page, int limit, String member,String phone){
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("member",member);
        map.put("isSysUser","2");
        map.put("phone",phone);
        return Result.success().put("data",userService.selectUserList(map));
    }

    @RequestMapping(value = "/selectSysUserLists", method = RequestMethod.GET)
    @ApiOperation("查询系统用户列表（没有分页）")
    @ResponseBody
    public Result selectSysUserLists(){
        return Result.success().put("data",userService.selectUserLists());
    }

    @RequestMapping(value = "/insertSysUser", method = RequestMethod.POST)
    @ApiOperation("添加系统用户")
    @ResponseBody
    public Result insertSysUser(@RequestBody UserEntity userInfo){
        if (userInfo.getPhone().isEmpty()) {
            return Result.error("手机号不能为空！");
        } else {
            UserEntity userByPhone = userService.queryByMobile(userInfo.getPhone());
            if (userByPhone == null) {
                userInfo.setPassword(DigestUtils.sha256Hex(userInfo.getPassword()));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                // TODO  用户默认头像 userInfo.setImageUrl();
                userInfo.setCreateTime(simpleDateFormat.format(new Date()));
                String invitationCode = userInfo.getInvitationCode();
                userInfo.setInvitationCode("");
                userInfo.setIsSysUser(2);
                userService.insert(userInfo);
                //判断是否输入邀请码
                if(StringUtils.isNotBlank(invitationCode)){
                    long inviteeUserId = InvitationCodeUtil.codeToId(invitationCode);
                    UserEntity userEntity = userService.selectById(inviteeUserId);
                    if(userEntity!=null){
                        inviteService.saveBody(userInfo.getUserId(),userEntity);
                    }
                }
                userInfo.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo.getUserId()));
                userService.updateById(userInfo);
                return Result.success();
            } else {
                return Result.error("手机号已经被注册了！");
            }
        }
    }


    @RequestMapping(value = "/updateSysUser", method = RequestMethod.POST)
    @ApiOperation("修改系统用户")
    @ResponseBody
    public Result updateSysUser(@RequestBody UserEntity userInfo){
        if (userInfo.getPhone().isEmpty()) {
            return Result.error("手机号不能为空！");
        } else {
                if(StringUtils.isNotEmpty(userInfo.getPassword())) {
                    userInfo.setPassword(DigestUtils.sha256Hex(userInfo.getPassword()));
                }
                userService.updateById(userInfo);
                return Result.success();
        }
    }

    @RequestMapping(value = "/addMayMoney/{userId}/{money}", method = RequestMethod.POST)
    @ApiOperation("系统用户添加可提现金额")
    @ResponseBody
    public Result addMoney(@PathVariable("userId") Long userId,@PathVariable("money") Double money){
        userMoneyService.updateMayMoney(1,userId,money);
        return Result.success();
    }

    @RequestMapping(value = "/addCannotMoney/{userId}/{money}/{type}", method = RequestMethod.POST)
    @ApiOperation("系统用户添加不可提现金额")
    @ResponseBody
    public Result addCannotMoney(@PathVariable("userId") Long userId,@PathVariable("money") Double money,@PathVariable("type") Integer type){
        if(type==1){
            userMoneyService.updateCannotMoney(1,userId,money);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setMoney(money);
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setContent("管理端充值："+money);
            userMoneyDetails.setTitle("管理端充值金额");
            userMoneyDetails.setType(1);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            userMoneyDetails.setCreateTime(simpleDateFormat.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
        }else{
            userMoneyService.updateCannotMoney(2,userId,money);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setMoney(money);
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setContent("管理端减少："+money);
            userMoneyDetails.setTitle("管理端减少金额");
            userMoneyDetails.setType(2);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            userMoneyDetails.setCreateTime(simpleDateFormat.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
        }
        return Result.success();
    }



    @RequestMapping(value = "/selectUserDetails", method = RequestMethod.GET)
    @ApiOperation("查看用户详细信息")
    @ResponseBody
    public Result selectUserDetails(Long userId){
        UserEntity userEntity = userService.selectById(userId);
        //默认查询本月 获取本月的日期
        Calendar cr= Calendar.getInstance();
        Date endTime=cr.getTime();
        cr.set(cr.get(Calendar.YEAR),cr.get(Calendar.MONTH),1);
        Date startTime=cr.getTime();
        //查询接单数量
        Integer helpSendCount = helpSendOrderService.selectHelpSendOrderCount(userId, startTime, endTime);
        //查询派单数量
        Integer helpTakeCount = helpTakeOrderService.selectHelpTakeOrderCount(userId, startTime, endTime);
        //查询充值金额
        Double rechargeAmount = payDetailsService.selectSumPay(startTime.toString(), endTime.toString(), userId);
        //查询提现金额
        Double cashOutMoney = cashOutService.selectCashOutSum(userId, startTime, endTime);
        //查询邀请数量
        Integer inviteCount = inviteService.selectInviteByUserIdCount(userId, startTime, endTime);
        //查询赏金金额
        Double moneyReward = inviteService.selectInviteByUserIdSum(userId, startTime, endTime);
        //查询直属邀请人数数量
        Integer zhiUserInviteCount = userService.selectZhiUserInviteCount(userEntity.getInvitationCode());
        //查询非直属邀请人数量
        Integer feiUserInviteCount = userService.selectFeiUserInviteCount(userEntity.getInvitationCode());
        //查询直属总收益
        Double zhiUserInviteMoney = userService.selectZhiUserInviteMoney(userEntity.getInvitationCode());
        //查询非直属总收益
        Double feiUserInviteMoney = userService.selectFeiUserInviteMoney(userEntity.getInvitationCode());
        //查询用户钱包
        UserMoney userMoney = userMoneyService.selectByUserId(userId);
        Map<String,Object> map=new HashMap<>();
        map.put("user",userEntity);
        map.put("userMoney",userMoney);
        map.put("helpSendCount",helpSendCount==null?0:helpSendCount);
        map.put("helpTakeCount",helpTakeCount==null?0:helpTakeCount);
        map.put("rechargeAmount",rechargeAmount==null?0:rechargeAmount);
        map.put("cashOutMoney",cashOutMoney==null?0:cashOutMoney);
        map.put("inviteCount",inviteCount==null?0:inviteCount);
        map.put("moneyReward",moneyReward==null?0:moneyReward);
        map.put("zhiUserInviteCount", zhiUserInviteCount);
        map.put("feiUserInviteCount", feiUserInviteCount);
        map.put("zhiUserInviteMoney",zhiUserInviteMoney==null?0:zhiUserInviteMoney);
        map.put("feiUserInviteMoney",feiUserInviteMoney==null?0:feiUserInviteMoney);
        return Result.success().put("data",map);
    }

    @RequestMapping(value = "/selectInviteByUserIdLists", method = RequestMethod.GET)
    @ApiOperation("直属")
    @ResponseBody
    public Result selectInviteByUserIdList(int page,int limit,Long userId){
        UserEntity userEntity = userService.selectById(userId);
        return userService.selectInviterCodeByUserIdLists(page,limit,userEntity.getInvitationCode());
    }

    @RequestMapping(value = "/selectInvitationCodeByUserIdLists", method = RequestMethod.GET)
    @ApiOperation("非直属用户")
    @ResponseBody
    public Result selectInvitationCodeByUserIdLists(int page,int limit,String invitationCode){
        return userService.selectInvitationCodeByUserIdLists(page,limit,invitationCode);
    }

    @RequestMapping(value = "/deleteByUserId", method = RequestMethod.POST)
    @ApiOperation("删除用户")
    @ResponseBody
    public Result deleteByUserId(Long userId){
        userService.removeById(userId);
        return Result.success();
    }

    @RequestMapping(value = "/updateUserStateById", method = RequestMethod.POST)
    @ApiOperation("修改用户状态")
    @ResponseBody
    public Result updateUserStateById(Long userId){
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity.getState()==1){
            userEntity.setState(2);
        }else{
            userEntity.setState(1);
        }
        userService.updateById(userEntity);
        return Result.success();
    }

    @RequestMapping(value = "/updateUserIsTask", method = RequestMethod.POST)
    @ApiOperation("修改用户是否可以发送任务")
    @ResponseBody
    public Result updateUserIsTask(Long userId){
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity.getIsTask()==1){
            userEntity.setIsTask(2);
        }else{
            userEntity.setIsTask(1);
        }
        userService.updateById(userEntity);
        return Result.success();
    }


    @RequestMapping(value = "/selectPayDetails", method = RequestMethod.GET)
    @ApiOperation("查询提现记录列表")
    @ResponseBody
    public Result selectHelpProfit(int page,int limit,String userId,String zhifubaoName,String zhifubao){
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("zhifubaoName",zhifubaoName);
        map.put("zhifubao",zhifubao);
        map.put("userId",userId);
        PageUtils pageUtils = cashOutService.selectCashOutList(map);
        return Result.success().put("data",pageUtils);
    }


    /**
     * 获取openid
     *
     * @param code 微信code
     * @return openid
     */
    @GetMapping("/openId/{code:.+}/{userId}")
    @ApiOperation("根据code获取openid")
    public Result getOpenid(@PathVariable("code") String code,@PathVariable("userId")Long userId) {
        return userService.getOpenId(code,userId);
    }

    @GetMapping("/mpCreateQr")
    @ApiOperation("小程序推广二维码")
    public void mpCreateQr(@RequestParam String invitationCode, HttpServletResponse response) {
        SenInfoCheckUtil.getPoster(invitationCode,response);
    }


}