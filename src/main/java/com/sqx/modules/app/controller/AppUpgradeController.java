package com.sqx.modules.app.controller;


import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.App;
import com.sqx.modules.app.entity.LoginInfo;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.AppService;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.invite.service.InviteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * APP登录授权
 *
 */
@RestController
@RequestMapping("/appinfo")
@Api(value = "APP升级管理", tags = {"APP升级管理"})
public class AppUpgradeController {

    @Autowired
    private AppService iAppService;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("管理平台升级详情")
    @ResponseBody
    public Result getBanner(@PathVariable Long id) {
        return Result.success().put("data",iAppService.selectAppById(id));
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation("管理平台添加升级信息")
    @ResponseBody
    public Result addBanner(@RequestBody App app) {
        if(app.getId()!=null){
            iAppService.updateAppById(app);
        }else{
            iAppService.insertApp(app);
        }

        return Result.success();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ApiOperation("管理平台删除升级信息")
    public Result deleteBanner(@PathVariable Long id) {
        iAppService.deleteAppById(id);
        return Result.success();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation("用户端和管理平台 （用户端每次取第一个）获取app升级列表")
    @ResponseBody
    public Result getBannerList() {
        return Result.success().put("data",iAppService.selectNewApp());
    }




}
