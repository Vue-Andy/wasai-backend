package com.sqx.modules.app.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.style.Style0;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import com.sqx.common.exception.SqxException;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.common.utils.Result;
import com.sqx.common.validator.Assert;
import com.sqx.modules.app.dao.MsgDao;
import com.sqx.modules.app.dao.UserDao;
import com.sqx.modules.app.entity.AppUserInfo;
import com.sqx.modules.app.entity.LoginInfo;
import com.sqx.modules.app.entity.Msg;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.form.LoginForm;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.app.utils.HttpClient;
import com.sqx.modules.app.utils.InvitationCodeUtil;
import com.sqx.modules.app.utils.JwtUtils;
import com.sqx.modules.app.utils.UserConstantInterface;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.file.utils.Md5Utils;
import com.sqx.modules.helpTask.config.Config;
import com.sqx.modules.helpTask.utils.HttpClientUtil;
import com.sqx.modules.invite.service.InviteService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weixin.popular.api.SnsAPI;
import weixin.popular.api.UserAPI;
import weixin.popular.bean.user.User;
import weixin.popular.support.TokenManager;
import weixin.popular.util.JsonUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private MsgDao msgDao;
	@Autowired
	private CommonInfoService commonInfoService;
	private  int number = 1;
	@Autowired
	private JwtUtils jwtUtils;
	@Autowired
	private InviteService inviteService;




	@Override
	public Result selectRankingList(Long userId){
		List<Map<String, Object>> maps = userDao.selectRankingList();
		Map<String, Object> stringObjectMap = userDao.selectRankingByUserId(userId);
		Map<String,Object> map=new HashMap<>();
		String str="****";
		for(Map<String, Object> m:maps){
			String nickName = m.get("nickName").toString();
			if(nickName.length()==11){
				StringBuilder sb = new StringBuilder(nickName);
				sb.replace(3, 7, str);
				m.put("nickName",sb.toString());
			}
		}
		map.put("rankingList",maps);
		map.put("rankingByUser",stringObjectMap);
		return Result.success().put("data",map);
	}





	@Override
	public UserEntity queryByMobile(String mobile) {
		return baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("phone", mobile));
	}

	@Override
	public int selectZhiUserInviteCount(String invitationCode) {
		return baseMapper.selectZhiUserInviteCount(invitationCode);
	}

	@Override
	public int selectFeiUserInviteCount(String invitationCode) {
		return baseMapper.selectFeiUserInviteCount(invitationCode);
	}

	@Override
	public Double selectZhiUserInviteMoney(String invitationCode) {
		return baseMapper.selectZhiUserInviteMoney(invitationCode);
	}

	@Override
	public Double selectFeiUserInviteMoney(String invitationCode) {
		return baseMapper.selectFeiUserInviteMoney(invitationCode);
	}

	@Override
	public PageUtils selectUserList(Map<String,Object> params){
		String paramKey = (String)params.get("member");
		String isSysUser = (String)params.get("isSysUser");
		String phone = (String)params.get("phone");
		if(paramKey!=null && paramKey.equals("-1")){
			paramKey=null;
		}
		IPage<UserEntity> page = this.page(
				new Query<UserEntity>().getPage(params),
				new QueryWrapper<UserEntity>()
						.eq(StringUtils.isNotBlank(paramKey),"member", paramKey)
						.eq(StringUtils.isNotBlank(phone),"phone", phone)
						.eq("2".equals(isSysUser),"is_sys_user", isSysUser)
				.orderByDesc("user_id")
		);
		return new PageUtils(page);
	}


	@Override
	public List<UserEntity> selectUserList() {
		return userDao.selectUserList();
	}

	@Override
	public List<UserEntity> selectUserLists() {
		return userDao.selectUserLists();
	}

	@Override
	public Integer selectUserByMemberCount(Integer member){
		return userDao.selectUserByMemberCount(member);
	}


	@Override
	public Result getOpenId(String code,Long userId){
		try {
			//微信appid
			CommonInfo one = commonInfoService.findOne(5);
			//微信秘钥
			CommonInfo two = commonInfoService.findOne(21);
			String openid = SnsAPI.oauth2AccessToken(one.getValue(), two.getValue(), code).getOpenid();
			if(StringUtils.isNotEmpty(openid)){
				UserEntity userEntity=new UserEntity();
				userEntity.setUserId(userId);
				userEntity.setOpenId(openid);
				userDao.updateById(userEntity);
				return Result.success().put("data",openid);
			}
			return Result.error("获取失败");
		} catch (Exception e) {
			log.error("GET_OPENID_FAIL");
			return Result.error("获取失败,出错了！");
		}
	}



	@Override
	public long login(LoginForm form) {
		UserEntity userByPhone = queryByMobile(form.getMobile());
		Assert.isNull(userByPhone, "手机号未注册");
		UserEntity user = queryByMobile(form.getMobile());
		Assert.isNull(user, "手机号或密码错误");

		//密码错误
		if(!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))){
			throw new SqxException("手机号或密码错误");
		}

		return user.getUserId();
	}

	@Override
	public UserEntity selectById(Long userId){
		UserEntity userEntity = userDao.selectById(userId);
		if(userEntity==null){
			return null;
		}
		if(userEntity.getSuperior()==null){
			UserEntity userEntity1 =null;
			if(StringUtils.isNotEmpty(userEntity.getInviterCode())){
				userEntity1=userDao.selectUserByInvitationCode(userEntity.getInviterCode());
				userEntity.setSuperior(userEntity1.getUserId());
			}else{
				CommonInfo one = commonInfoService.findOne(88);
				userEntity1 = userDao.selectUserByInvitationCode(one.getValue());
				userEntity.setInviterCode(one.getValue());
			}
			userEntity.setSuperior(userEntity1.getUserId());
			userDao.updateById(userEntity);
		}
		if(StringUtils.isEmpty(userEntity.getInviterCode())){
			UserEntity userEntity1 =null;
			if(userEntity.getSuperior()!=null){
				userEntity1=userDao.selectById(userEntity.getSuperior());
			}else{
				CommonInfo one = commonInfoService.findOne(88);
				userEntity1 = userDao.selectUserByInvitationCode(one.getValue());
				userEntity.setSuperior(userEntity1.getUserId());
			}
			userEntity.setInviterCode(userEntity1.getInvitationCode());
			userDao.updateById(userEntity);
		}
		return userEntity;
	}

	@Override
	public UserEntity selectUserByInvitationCode(String invitationCode) {
		return userDao.selectUserByInvitationCode(invitationCode);
	}

	@Override
	public Result selectInvitationCodeByUserIdLists(int page, int limit, String invitationCode) {
		Page<UserEntity> pages=new Page<>(page,limit);
		IPage<UserEntity> userEntityIPage = userDao.selectInvitationCodeByUserIdLists(pages, invitationCode);
		return Result.success().put("data",userEntityIPage);
	}

	@Override
	public Result selectInviterCodeByUserIdLists(int page, int limit, String invitationCode) {
		Page<UserEntity> pages=new Page<>(page,limit);
		IPage<UserEntity> userEntityIPage = userDao.selectInviterCodeByUserIdLists(pages, invitationCode);
		return Result.success().put("data",userEntityIPage);
	}

	@Override
	public Result selectZhiInviteByUserIdList(int page, int limit, Long userId) {
		Page<Map<String,Object>> pages=new Page<>(page,limit);
		IPage<Map<String,Object>> userEntityIPage = userDao.selectZhiInviteByUserIdList(pages, userId);
		return Result.success().put("data",userEntityIPage);
	}

	@Override
	public Result selectFeiInviteByUserIdList(int page, int limit, Long userId) {
		Page<Map<String,Object>> pages=new Page<>(page,limit);
		IPage<Map<String,Object>> userEntityIPage = userDao.selectFeiInviteByUserIdList(pages, userId);
		return Result.success().put("data",userEntityIPage);
	}

	@Override
	public UserEntity selectUserByOpenId(String openId){
		return userDao.selectUserByOpenId(openId);
	}

	@Override
	public UserEntity selectUserByWxId(String wxId){
		return baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("wx_id",wxId));
	}

	@Override
	public Result forgetPwd(String pwd, String phone, String msg) {
		try {
			Msg byPhoneAndCode = msgDao.findByPhoneAndCode(phone, msg);
			//校验短信验证码
			if (byPhoneAndCode == null) {
				return Result.error("验证码不正确");
			}
			UserEntity userByPhone = queryByMobile(phone);
			userByPhone.setPassword(DigestUtils.sha256Hex(pwd));
			msgDao.deleteById(byPhoneAndCode.getId());
			userDao.updateById(userByPhone);
			return Result.success();
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error("服务器内部错误");
		}
	}

	public User getUserInfo(String openId){
		return UserAPI.userInfo(getWxToken(), openId);
	}

	private String getWxToken() {
		try {
			//微信appid
			CommonInfo one = commonInfoService.findOne(5);
			return TokenManager.getToken( one.getValue() );
		} catch (Exception e) {
			throw new RuntimeException("GET_ACCESS_TOKEN_FAIL");
		}
	}




	@Override
	public Result loginByOpenId(String openid) {
		if (!org.apache.commons.lang3.StringUtils.isEmpty(openid)) {
			CommonInfo one = commonInfoService.findOne(88);
			User wx =getUserInfo(openid);
			if (wx != null && wx.getUnionid() != null) {
				UserEntity userByUnionid = userDao.getUserByUnionId(wx.getUnionid());
				if (userByUnionid != null) {
					if (wx.getHeadimgurl() != null) {
						userByUnionid.setImageUrl(wx.getHeadimgurl());
					}
					if (wx.getSex() != null) {
						userByUnionid.setGender(wx.getSex());
					}
					if (wx.getCity() != null) {
						userByUnionid.setAddress(wx.getCity());
					}
					if (wx.getNickname() != null) {
						userByUnionid.setNickName(wx.getNickname());
					}
					userByUnionid.setOpenId(openid);
					userDao.updateById(userByUnionid);
					return Result.success();
				} else {
					return wxLoginByOpenid(openid, one);
				}
			} else {
				return wxLoginByOpenid(openid, one);
			}

		} else {
			return Result.error("当前账户受限制请联系管理员");
		}

	}

	@Override
	public Result bindPhone(String userId, String phone, String code) {
		UserEntity one = userDao.selectById(Long.valueOf(userId));
		if (one == null) {
			return Result.error("用户未注册");
		}
		UserEntity userInfo = queryByMobile(phone);
		if (userInfo != null && userInfo.getOpenId() != null) {
			return Result.error("当前手机号已经被其他微信绑定");
		}
		Msg byPhoneAndCode = msgDao.findByPhoneAndCode(phone, code);
		if (byPhoneAndCode == null) {
			return Result.error("验证码错误");
		}
		one.setPhone(phone);
		one.setZhifubao(phone);
		userDao.updateById(one);
		return Result.success();
	}


	@Override
	public Result updatePhone(String phone, String msg, String userId) {
		Msg msg1 = msgDao.findByPhoneAndCode(phone, msg);
		//校验短信验证码
		if (msg1 != null) {
			UserEntity userInfo = queryByMobile(phone);
			if (userInfo != null) {
				return Result.error("手机号已经被其他账号绑定");
			} else {
				UserEntity one = userDao.selectById(Long.valueOf(userId));
				one.setPhone(phone);
				userDao.updateById(one);
				return Result.success();
			}
		}
		return Result.error("验证码不正确");
	}

	@Override
	public int insert(UserEntity userEntity) {
		return userDao.insert(userEntity);
	}


	@Override
	public Result bindOpenId(String userId, String openId) {
		UserEntity one = userDao.selectById(Long.valueOf(userId));
		if (one != null) {
			getWx(openId, one);
			return Result.success();
		}
		return Result.error("用户未注册");
	}

	@Override
	public Result loginApp(LoginInfo loginInfo) {
		if (org.apache.commons.lang3.StringUtils.isNotBlank(loginInfo.getUnionid())) {
			UserEntity one = userDao.getUserByUnionId(loginInfo.getUnionid());
			if (one != null) {
				String s = HttpClient.doGet("https://api.weixin.qq.com/sns/userinfo?access_token=" + loginInfo.getToken() + "&openid=" + loginInfo.getOpenid());
				AppUserInfo user = JsonUtil.parseObject(s, AppUserInfo.class);
				if (user != null && user.getNickname() != null) {
					if (user.getHeadimgurl() != null) {
						one.setImageUrl(user.getHeadimgurl());
					}
					if (user.getCity() != null) {
						one.setAddress(user.getCity());
					}
					one.setGender(user.getSex());
					if (user.getNickname() != null) {
						one.setNickName(user.getNickname());
					}
				}
				userDao.updateById(one);
				return getResult(one.getUserId());
			} else {
				return Result.error(-200,"用户未注册！");
			}
		} else if (org.apache.commons.lang3.StringUtils.isNotBlank(loginInfo.getOpenid())) {
			UserEntity one = userDao.getUserByOpenUid(loginInfo.getOpenid());
			if (one != null) {
				String s = HttpClient.doGet("https://api.weixin.qq.com/sns/userinfo?access_token=" + loginInfo.getToken() + "&openid=" + loginInfo.getOpenid());
				AppUserInfo user = JsonUtil.parseObject(s, AppUserInfo.class);
				if (user != null && user.getNickname() != null) {
					if (user.getHeadimgurl() != null) {
						one.setImageUrl(user.getHeadimgurl());
					}
					if (user.getCity() != null) {
						one.setAddress(user.getCity());
					}
					one.setGender(user.getSex());
					if (user.getNickname() != null) {
						one.setNickName(user.getNickname());
					}
				}
				userDao.updateById(one);
				return getResult(one.getUserId());
			} else {
				return Result.error(-200,"用户未注册！");
			}
		} else {
			return Result.error("用户未注册");
		}
	}



	@Override
	public Result loginWeiXinApp(LoginInfo loginInfo) {
		CommonInfo one = commonInfoService.findOne(88);
		Msg msg1 = msgDao.findByPhoneAndCode(loginInfo.getPhone(), loginInfo.getMsg());
		//校验短信验证码
		if (msg1 == null) {
			return Result.error("验证码错误");
		}
		//校验手机号是否存在
		UserEntity userInfo = queryByMobile(loginInfo.getPhone());
		if (userInfo != null) {
			if (org.apache.commons.lang3.StringUtils.isNotBlank(userInfo.getOpenUid())) {
				return Result.error("手机号已经被其他账号绑定");
			} else {
				String s = HttpClient.doGet("https://api.weixin.qq.com/sns/userinfo?access_token=" + loginInfo.getToken() + "&openid=" + loginInfo.getOpenid());
				AppUserInfo user = JsonUtil.parseObject(s, AppUserInfo.class);
				if (user != null && user.getNickname() != null) {
					if (user.getHeadimgurl() != null) {
						userInfo.setImageUrl(user.getHeadimgurl());
					}
					if (user.getCity() != null) {
						userInfo.setAddress(user.getCity());
					}
					userInfo.setGender(user.getSex());
					if (user.getNickname() != null) {
						userInfo.setNickName(user.getNickname());
					}
				}
				userInfo.setOpenUid(loginInfo.getOpenid());
				userInfo.setUnionId(loginInfo.getUnionid());
				userDao.updateById(userInfo);
				msgDao.deleteById(msg1.getId());
				return getResult(userInfo.getUserId());
			}
		} else {
			if(StringUtils.isBlank(loginInfo.getInvitationCode())){
				return Result.error("请填写邀请码！");
			}
			UserEntity userEntity = userDao.selectUserByInvitationCode(loginInfo.getInvitationCode());
			if(userEntity==null){
				return Result.error("请填写正确的邀请码！");
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = simpleDateFormat.format(new Date());
			UserEntity userInfo1 = new UserEntity();
			userInfo1.setPhone(loginInfo.getPhone());
			userInfo1.setNickName(loginInfo.getPhone());
			userInfo1.setPassword(DigestUtils.sha256Hex(loginInfo.getPwd()));
			userInfo1.setUnionId(loginInfo.getUnionid());
			userInfo1.setOpenUid(loginInfo.getOpenid());
			userInfo1.setZhifubao(loginInfo.getPhone());
			userInfo1.setCreateTime(time);
			String s = HttpClient.doGet("https://api.weixin.qq.com/sns/userinfo?access_token=" + loginInfo.getToken() + "&openid=" + loginInfo.getOpenid());
			AppUserInfo user = JsonUtil.parseObject(s, AppUserInfo.class);
			if (user != null && user.getNickname() != null) {
				if (user.getHeadimgurl() != null) {
					userInfo1.setImageUrl(user.getHeadimgurl());
				}
				if (user.getCity() != null) {
					userInfo1.setAddress(user.getCity());
				}
				userInfo1.setGender(user.getSex());
				if (user.getNickname() != null) {
					userInfo1.setNickName(user.getNickname());
				}
			}
			userDao.insert(userInfo1);
			userInfo1.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo1.getUserId()));
			userDao.updateById(userInfo1);
			msgDao.deleteById(msg1.getId());
			inviteService.saveBody(userInfo1.getUserId(),userEntity);
			//判断是否输入邀请码

			return getResult(userInfo1.getUserId());
		}
	}


	@Override
	public Result loginCode(LoginInfo loginInfo) {
		Msg msg1 = msgDao.findByPhoneAndCode(loginInfo.getPhone(), loginInfo.getMsg());
		//校验短信验证码
		if (msg1 == null) {
			return Result.error("验证码不正确");
		}
		//校验手机号是否存在
		UserEntity userInfo = queryByMobile(loginInfo.getPhone());
		if (userInfo != null) {
			msgDao.deleteById(msg1.getId());
			if(userInfo.getState()!=1){
				return Result.error("账号已被封禁，请联系客服！");
			}
			return getResult(userInfo.getUserId());
		} else {
			if(StringUtils.isBlank(loginInfo.getInvitationCode())){
				return Result.error("请填写邀请码！");
			}
			UserEntity userEntity = userDao.selectUserByInvitationCode(loginInfo.getInvitationCode());
			if(userEntity==null){
				return Result.error("请填写正确的邀请码！");
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = simpleDateFormat.format(new Date());
			UserEntity userInfo1 = new UserEntity();
			userInfo1.setPhone(loginInfo.getPhone());
			userInfo1.setNickName(loginInfo.getPhone());
			userInfo1.setPassword(DigestUtils.sha256Hex(loginInfo.getPwd()));
			userInfo1.setZhifubao(loginInfo.getPhone());
			userInfo1.setGender(0);
			userInfo1.setCreateTime(time);
			userDao.insert(userInfo1);
			userInfo1.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo1.getUserId()));
			userDao.updateById(userInfo1);
			msgDao.deleteById(msg1.getId());
			inviteService.saveBody(userInfo1.getUserId(),userEntity);

			return getResult(userInfo1.getUserId());
		}
	}



	@Override
	public Result wxLogin(String code) {
		try {
			String appid = commonInfoService.findOne(45).getValue();
			String secret = commonInfoService.findOne(46).getValue();
			// 配置请求参数
			Map<String, String> param = new HashMap<>();
			param.put("appid", appid);
			param.put("secret", secret);
			param.put("js_code", code);
			param.put("grant_type", UserConstantInterface.WX_LOGIN_GRANT_TYPE);
			param.put("scope", "snsapi_userinfo");
			// 发送请求
			String wxResult = HttpClientUtil.doGet(UserConstantInterface.WX_LOGIN_URL, param);
			log.error(wxResult);
			JSONObject jsonObject = JSONObject.parseObject(wxResult);
			// 获取参数返回的
			String session_key = jsonObject.get("session_key").toString();
			String open_id = jsonObject.get("openid").toString();
			UserEntity userEntity = selectUserByWxId(open_id);
			Map<String, String> map = new HashMap<>();
			// 封装返回小程序
			map.put("session_key", session_key);
			map.put("open_id", open_id);
			if (jsonObject.get("unionid") != null) {
				String unionid = jsonObject.get("unionid").toString();
				map.put("unionid", unionid);
			} else {
				map.put("unionid", "-1");
			}
			if (userEntity == null) {
				map.put("flag", "1");
			} else {
				map.put("flag", userEntity.getPhone());
			}
			return Result.success("登陆成功").put("data", map);
		} catch (Exception e) {
			System.err.println(e.toString());
			return Result.success("登录失败！");
		}
	}


	@Override
	public Result wxRegister(UserEntity userInfo1) {
		if (StringUtils.isEmpty(userInfo1.getWxId())) {
			return Result.error("账号信息获取失败，请退出重试！");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date());
		// 根据返回的user实体类，判断用户是否是新用户，不是的话，更新最新登录时间，是的话，将用户信息存到数据库
		UserEntity userInfo = selectUserByWxId(userInfo1.getWxId());
		if (userInfo != null) {
			if (userInfo.getState().equals(2)) {
				return Result.error("账号已被封禁，请联系客服处理！");
			}
			if (StringUtils.isNotEmpty(userInfo1.getNickName())) {
				userInfo.setNickName(userInfo1.getNickName());
			}
			if (StringUtils.isNotEmpty(userInfo1.getImageUrl())) {
				userInfo.setImageUrl(userInfo1.getImageUrl());
			}
			if (StringUtils.isNotEmpty(userInfo1.getPhone())) {
				userInfo.setPhone(userInfo1.getPhone());
			}
			baseMapper.updateById(userInfo);
		} else {
			//判断是否在app登陆过  手机号是否有账号
			UserEntity userByMobile = queryByMobile(userInfo1.getPhone());
			if (userByMobile != null) {
				//有账号则绑定账号
				userByMobile.setWxId(userInfo1.getWxId());
				baseMapper.updateById(userByMobile);
				if (userByMobile.getState().equals(2)) {
					return Result.error("账号已被封禁，请联系客服处理！");
				}
			} else {
				if(StringUtils.isEmpty(userInfo1.getInvitationCode())){
					userInfo1.setInviterCode(commonInfoService.findOne(88).getValue());
				}else {
					userInfo1.setInviterCode(userInfo1.getInvitationCode());
				}
				UserEntity userEntity = selectUserByInvitationCode(userInfo1.getInviterCode());
				//没有则生成新账号
				userInfo1.setCreateTime(date);
				userInfo1.setState(1);
				if(StringUtils.isEmpty(userInfo1.getPassword())){
					userInfo1.setPassword(DigestUtils.sha256Hex(userInfo1.getPhone()));
				}
				baseMapper.insert(userInfo1);
				userInfo1.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo1.getUserId()));
				baseMapper.updateById(userInfo1);
				if(userEntity!=null){
					inviteService.saveBody(userInfo1.getUserId(),userEntity);
				}
				return getResult(userInfo1.getUserId());
			}
		}
		//返回用户信息
		UserEntity user = selectUserByWxId(userInfo1.getWxId());
		return getResult(user.getUserId());
	}





	private Result wxLoginByOpenid(String openid,  CommonInfo one) {
		UserEntity userByWxId = userDao.getUserByWxId(openid);
		User wx = getUserInfo(openid);
		if (userByWxId == null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = simpleDateFormat.format(new Date());
			UserEntity userInfo = new UserEntity();
			userInfo.setPassword(DigestUtils.sha256Hex("123456"));
			userInfo.setCreateTime(time);
			if (wx.getHeadimgurl() != null) {
				userInfo.setImageUrl(wx.getHeadimgurl());
			}
			if (wx.getSex() != null) {
				userInfo.setGender(wx.getSex());
			}
			if (wx.getCity() != null) {
				userInfo.setAddress(wx.getCity());
			}
			if (wx.getNickname() != null) {
				userInfo.setNickName(wx.getNickname());
			}
			if (wx.getUnionid() != null) {
				userInfo.setUnionId(wx.getUnionid());
			}
			userInfo.setOpenId(openid);
			userDao.insert(userInfo);
			userInfo.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo.getUserId()));
			userDao.updateById(userInfo);
			return Result.success();
		}
		getWx(openid, userByWxId);
		return Result.success();
	}


	private UserEntity getWx(String openid, UserEntity userInfo) {
		User wx = getUserInfo(openid);
		if (wx.getHeadimgurl() != null) {
			userInfo.setImageUrl(wx.getHeadimgurl());
		}
		if (wx.getSex() != null) {
			userInfo.setGender(wx.getSex());
		}
		if (wx.getCity() != null) {
			userInfo.setAddress(wx.getCity());
		}
		if (wx.getNickname() != null) {
			userInfo.setNickName(wx.getNickname());
		}
		if (wx.getNickname() != null) {
			userInfo.setNickName(wx.getNickname());
		}
		userInfo.setOpenId(openid);
		if(userInfo.getUserId()==null){
			userDao.insert(userInfo);
			userInfo.setInvitationCode(InvitationCodeUtil.toSerialCode(userInfo.getUserId()));
			userDao.updateById(userInfo);
		}else{
			userDao.updateById(userInfo);
		}
		return userInfo;
	}


	@Override
	public Result bindOpenid(LoginInfo loginInfo) {
		Msg byPhoneAndCode = msgDao.findByPhoneAndCode(loginInfo.getPhone(), loginInfo.getMsg());
		if (byPhoneAndCode == null) {
			return Result.error("验证码不正确");
		}
		//如果openid可以获取到用户信息 检查输入的手机号和openid绑定的手机号是不是一样
		UserEntity userByWxId = userDao.getUserByWxId(loginInfo.getOpenid());
		if (userByWxId != null) {
			if(userByWxId.getState()!=1){
				return Result.error("账号已被封禁，请联系客服！");
			}
			UserEntity userByPhone1 = queryByMobile(loginInfo.getPhone());
			if (userByPhone1 != null) {
				return Result.error(8, "当前手机号已注册，如需更换请联系客服");
			}
			//如果俩手机号一样 执行登录操作
			if (userByWxId.getPhone() != null && userByWxId.getPhone().equals(loginInfo.getPhone())) {
				msgDao.deleteById(byPhoneAndCode.getId());
				if(userByWxId.getInvitationCode()==null){
					if(StringUtils.isNotBlank(loginInfo.getInvitationCode())){
						UserEntity userEntity = userDao.selectUserByInvitationCode(loginInfo.getInvitationCode());
						if(userEntity!=null){
							inviteService.saveBody(userByWxId.getUserId(),userEntity);
						}
					}
				}

				return getResult(userByWxId.getUserId());
			} else {
				//没有绑定手机号 执行绑定手机号的操作
				if (userByWxId.getPhone() != null) {
					//俩手机号不一样执行 提示操作
					return Result.error(8, "当前微信已经被【" + userByWxId.getPhone() + "】账号绑定，如有疑问请联系客服");
				}
				userByWxId.setPhone(loginInfo.getPhone());
				userByWxId.setZhifubao(loginInfo.getPhone());
				msgDao.deleteById(byPhoneAndCode.getId());
				userDao.updateById(userByWxId);
				if(userByWxId.getInvitationCode()==null){
					if(StringUtils.isNotBlank(loginInfo.getInvitationCode())){
						UserEntity userEntity = userDao.selectUserByInvitationCode(loginInfo.getInvitationCode());
						if(userEntity!=null){
							inviteService.saveBody(userByWxId.getUserId(),userEntity);
						}
					}
				}
				return getResult(userByWxId.getUserId());
			}
		} else {
			//校验手机号是否注册
			UserEntity userByPhone = queryByMobile(loginInfo.getPhone());
			if (userByPhone == null) {
				return saveInfo(loginInfo.getPhone(), loginInfo.getOpenid(), byPhoneAndCode, loginInfo.getInvitationCode(), loginInfo.getPwd(), loginInfo.getPlatform());
			} else {
				if(userByPhone.getState()!=1){
					return Result.error("账号已被封禁，请联系客服！");
				}
				if(userByPhone.getInvitationCode()==null){
					if(StringUtils.isNotBlank(loginInfo.getInvitationCode())){
						UserEntity userEntity = userDao.selectUserByInvitationCode(loginInfo.getInvitationCode());
						if(userEntity!=null){
							inviteService.saveBody(userByWxId.getUserId(),userEntity);
						}
					}
				}
				return getResult(userByPhone.getUserId());
			}
		}
	}

	//保存用户信息
	private Result saveInfo(String phone, String openid, Msg byPhoneAndCode, String invitation, String pwd, String platform) {
		CommonInfo one = commonInfoService.findOne(88);
		String strDateFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strDateFormat);
		String time = simpleDateFormat.format(new Date());
		UserEntity userInfo1 = new UserEntity();
		userInfo1.setPhone(phone);
		userInfo1.setNickName(phone);
		userInfo1.setGender(0);
		userInfo1.setPassword(DigestUtils.sha256Hex(pwd));
		userInfo1.setZhifubao(phone);
		userInfo1.setCreateTime(time);
		userInfo1=getWx(openid, userInfo1);
		if(StringUtils.isNotBlank(invitation)){
			UserEntity userEntity = userDao.selectUserByInvitationCode(invitation);
			if(userEntity!=null){
				inviteService.saveBody(userInfo1.getUserId(),userEntity);
			}
		}
		msgDao.deleteById(byPhoneAndCode.getId());
		return getResult(userInfo1.getUserId());
	}


	public Result getResult(Long userId){
		//生成token
		String token = jwtUtils.generateToken(userId);

		Map<String, Object> map = new HashMap<>();
		map.put("user", selectById(userId));
		map.put("userId", userId);
		map.put("token", token);
		map.put("expire", jwtUtils.getExpire());
		return Result.success(map);
	}


	@Override
	public Result sendMsg(String phone, String state) {
		int code = (int) ((Math.random() * 9 + 1) * 100000);
		System.out.println("sendMsg code is " + code);
		SmsSingleSenderResult result = null;
		if (state.equals("forget")) {
			UserEntity userByPhone = queryByMobile(phone);
			if (userByPhone == null) {
				return Result.error("用户未注册");
			}
		}
		if (state.equals("bind")) {
			UserEntity userByPhone = queryByMobile(phone);
			if (userByPhone != null) {
				return Result.error("手机号已注册");
			}
		}
		if (state.equals("register")) {
			UserEntity userByPhone = queryByMobile(phone);
			if (userByPhone != null) {
				return Result.error("用户已经注册请前往登录");
			}
		}
		CommonInfo three = commonInfoService.findOne(163);
		//默认使用腾讯云
		if(three==null || "1".equals(three.getValue())){
			//腾讯云短信发送
			return sendMsgTencent(phone,state,code);
		}else if("2".equals(three.getValue())){
			//阿里云短信发送
			return sendMsgAlibaba(phone,state,code);
		}else{
			return sendMsgDXB(phone, state, code);
		}
	}


	private Result sendMsgAlibaba(String phone, String state,int code){

		CommonInfo three = commonInfoService.findOne(93);
		String accessKeyId = three.getValue();
		CommonInfo four = commonInfoService.findOne(94);
		String accessSecret = four.getValue();
		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
		IAcsClient client = new DefaultAcsClient(profile);
		CommonInfo name = commonInfoService.findOne(12);
		CommonRequest request = new CommonRequest();
		request.setSysMethod(MethodType.POST);
		request.setSysDomain("dysmsapi.aliyuncs.com");
		request.setSysVersion("2017-05-25");
		request.setSysAction("SendSms");
		request.putQueryParameter("RegionId", "cn-hangzhou");
		request.putQueryParameter("PhoneNumbers", phone);
		request.putQueryParameter("SignName",name.getValue() );
		String value;
		switch (state) {
			case "register":
				value=commonInfoService.findOne(90).getValue();
				break;
			case "forget":
				value=commonInfoService.findOne(91).getValue();
				break;
			case "bind":
				value=commonInfoService.findOne(92).getValue();
				break;
			default:
				value=commonInfoService.findOne(90).getValue();
				break;
		}
		request.putQueryParameter("TemplateCode", value);
		request.putQueryParameter("TemplateParam", "{\"code\":\""+code+"\"}");
		try {
			CommonResponse response = client.getCommonResponse(request);
			System.out.println(response.getData());
			String data = response.getData();
			JSONObject jsonObject = JSON.parseObject(data);
			if("OK".equals(jsonObject.get("Code"))){
				Msg byPhone = msgDao.findByPhone(phone);
				if (byPhone != null) {
					byPhone.setCode(String.valueOf(code));
					byPhone.setPhone(phone);
					msgDao.updateById(byPhone);
				} else {
					Msg msg = new Msg();
					msg.setCode(String.valueOf(code));
					msg.setPhone(phone);
					msgDao.insert(msg);
				}
				UserEntity userByPhone = queryByMobile(phone);
				if (userByPhone != null) {
					return Result.success("login");
				} else {
					return Result.success("register");
				}
			}else {
				return Result.error(6, jsonObject.get("Message").toString());
			}
		} catch (ClientException e) {
			e.printStackTrace();
		}
		return Result.error("验证码发送失败");
	}



	private Result sendMsgTencent(String phone, String state,int code){
		SmsSingleSenderResult result = null;
		try {
			CommonInfo three = commonInfoService.findOne(31);
			String clientId = three.getValue();

			CommonInfo four = commonInfoService.findOne(32);
			String clientSecret = four.getValue();
			CommonInfo name = commonInfoService.findOne(12);
			/**
			 * 发送短信验证码的状态、
			 *
			 * 在h5登录环境中 传的状态不是以下三种状态
			 */
			SmsSingleSender ssender = new SmsSingleSender(Integer.valueOf(clientId), clientSecret);
			switch (state) {
				case "register":
					result = ssender.send(0, "86", phone, "【" + name.getValue() + "】验证码: " + code + "，此验证码可用于登录或注册，10分钟内有效，如非您本人操作，可忽略本条消息", "", "");
					break;
				case "forget":
					result = ssender.send(0, "86", phone, "【" + name.getValue() + "】验证码: " + code + "，您正在执行找回密码操作，10分钟内有效，如非您本人操作，可忽略本条消息", "", "");
					break;
				case "bind":
					result = ssender.send(0, "86", phone, "【" + name.getValue() + "】验证码: " + code + "，您正在执行绑定手机号操作，10分钟内有效，如非您本人操作，可忽略本条消息", "", "");
					break;
				default:
					result = ssender.send(0, "86", phone, "【" + name.getValue() + "】验证码: " + code + "，此验证码可用于登录或注册，10分钟内有效，如非您本人操作，可忽略本条消息", "", "");
					break;
			}


			System.out.println(result);
			if (result.result == 0) {
				Msg byPhone = msgDao.findByPhone(phone);
				if (byPhone != null) {
					byPhone.setCode(String.valueOf(code));
					byPhone.setPhone(phone);
					msgDao.updateById(byPhone);
				} else {
					Msg msg = new Msg();
					msg.setCode(String.valueOf(code));
					msg.setPhone(phone);
					msgDao.insert(msg);
				}
				UserEntity userByPhone = queryByMobile(phone);
				if (userByPhone != null) {
					return Result.success("login");
				} else {
					return Result.success("register");
				}
			} else {
				return Result.error(6, result.errMsg);
			}
		} catch (HTTPException | JSONException | IOException e) {
			// HTTP 响应码错误
			e.printStackTrace();
		}
		return Result.error("验证码发送失败");
	}

	private Result sendMsgDXB(String phone, String state, int code) {
		CommonInfo three = commonInfoService.findOne(164);
		CommonInfo four = commonInfoService.findOne(165);
		CommonInfo one = commonInfoService.findOne(12);
		String testUsername = three.getValue(); //在短信宝注册的用户名
		String testPassword = four.getValue(); //在短信宝注册的密码
		String value="";
		switch (state) {
			case "register":
				value = "【" + one.getValue() + "】验证码: " + code + "，此验证码可用于登录或注册，10分钟内有效，如非您本人操作，可忽略本条消息";
				break;
			case "forget":
				value = "【" + one.getValue() + "】验证码: " + code + "，您正在执行找回密码操作，10分钟内有效，如非您本人操作，可忽略本条消息";
				break;
			case "bind":
				value = "【" + one.getValue() + "】验证码: " + code + "，您正在执行绑定手机号操作，10分钟内有效，如非您本人操作，可忽略本条消息";
				break;
			default:
				value = "【" + one.getValue() + "】验证码: " + code + "，此验证码可用于登录或注册，10分钟内有效，如非您本人操作，可忽略本条消息";
				break;
		}
		StringBuilder httpArg = new StringBuilder();
		httpArg.append("u=").append(testUsername).append("&");
		httpArg.append("p=").append(Md5Utils.md5s(testPassword)).append("&");
		httpArg.append("m=").append(phone).append("&");
		httpArg.append("c=").append(Md5Utils.encodeUrlString(value, "UTF-8"));
		String result = Md5Utils.request(Config.DXBUrl, httpArg.toString());
		log.error("短信包返回值："+result);
		if ("0".equals(result)) {
			Msg byPhone = msgDao.findByPhone(phone);
			if (byPhone != null) {
				byPhone.setCode(String.valueOf(code));
				byPhone.setPhone(phone);
				msgDao.updateById(byPhone);
			} else {
				Msg msg = new Msg();
				msg.setCode(String.valueOf(code));
				msg.setPhone(phone);
				msgDao.insert(msg);
			}
			UserEntity userByPhone = queryByMobile(phone);
			if (userByPhone != null) {
				return Result.success("login");
			} else {
				return Result.success("register");
			}
		} else {
//            return ResultUtil.error(6, result.errMsg);
			if("30".equals(result)){
				return Result.error(-200, "错误密码");
			}else if("40".equals(result)){
				return Result.error(-200, "账号不存在");
			}else if("41".equals(result)){
				return Result.error(-200, "余额不足");
			}else if("43".equals(result)){
				return Result.error(-200, "IP地址限制");
			}else if("50".equals(result)){
				return Result.error(-200, "内容含有敏感词");
			}else if("51".equals(result)){
				return Result.error(-200, "手机号码不正确");
			}
		}
		return Result.error("验证码发送失败");
	}



	@Override
	public void pushToSingle(String title, String content,String clientId) {
		String appkey = commonInfoService.findOne(60).getValue();
		String appId = commonInfoService.findOne(61).getValue();
		IGtPush push = new IGtPush(appkey,commonInfoService.findOne(62).getValue());

		NotificationTemplate template = new NotificationTemplate();

		// 设置APPID与APPKEY
		template.setAppId(appId);
		template.setAppkey(appkey);

		//设置展示样式
		Style0 style = new Style0();
		// 设置通知栏标题与内容

		style.setTitle(title);
		style.setText(content);
		// 配置通知栏图标
		style.setLogo("icon.png"); //配置通知栏图标，需要在客户端开发时嵌入，默认为push.png
		// 配置通知栏网络图标
		style.setLogoUrl(commonInfoService.findOne(19).getValue() + "/logo.png");
		// 设置通知是否响铃，震动，或者可清除
		style.setRing(true);
		style.setVibrate(true);
		style.setClearable(true);
		style.setChannel(String.valueOf(number));
		CommonInfo one = commonInfoService.findOne(12);
		String name=one==null?"助力任务":one.getValue();
		style.setChannelName(name);
		style.setChannelLevel(4); //设置通知渠道重要性
		template.setStyle(style);
		template.setTransmissionType(1);  // 透传消息设置，收到消息是否立即启动应用： 1为立即启动，2则广播等待客户端自启动
		template.setTransmissionContent(content);
		template.setNotifyid(number); // 在消息推送的时候设置notifyid。如果需要覆盖此条消息，则下次使用相同的notifyid发一条新的消息。客户端sdk会根据notifyid进行覆盖。
		number = number+1;
		// 单推消息类型
		SingleMessage message = new SingleMessage();

		message.setData(template);
		// 设置消息离线，并设置离线时间
		message.setOffline(true);
		// 离线有效时间，单位为毫秒，可选
		message.setOfflineExpireTime(72 * 3600 * 1000);
		message.setPushNetWorkType(0); // 判断客户端是否wifi环境下推送。1为仅在wifi环境下推送，0为不限制网络环境，默认不限
		// APNs下发策略；1: 个推通道优先，在线经个推通道下发，离线经APNs下发(默认);2: 在离线只经APNs下发;3: 在离线只经个推通道下发;4: 优先经APNs下发，失败后经个推通道下发;
		message.setStrategyJson("{\"ios\":4}");
		Target target = new Target();

		target.setAppId(appId);
		target.setClientId(clientId);
		IPushResult ret = null;
		try {

			ret = push.pushMessageToSingle(message, target);
		} catch (RequestException e) {
			e.printStackTrace();
			ret = push.pushMessageToSingle(message, target, e.getRequestId());
		}
		if (ret != null) {
			System.out.println(ret.getResponse().toString());
		} else {
			System.out.println("服务器响应异常");
		}

	}

	/**
	 * 统计用户：总用户数、初级会员、中级会员、高级会员
	 * @return
	 */
	@Override
	public Result statistical() {
		int sum0 = 0; //总用户数
		int sum1 = 0; //初级会员
		int sum2 = 0; //中级会员
		int sum3 = 0; //高级会员
		List<UserEntity> list = userDao.findAllList();
		for (UserEntity u : list) {
			sum0++;
			switch (u.getMember()){
				case 2 : sum1++; break;
				case 3 : sum2++; break;
				case 4 : sum3++; break;
			}
		}
		Map map = new HashMap();
		map.put("sum0", sum0);
		map.put("sum1", sum1);
		map.put("sum2", sum2);
		map.put("sum3", sum3);
		return Result.success(map);
	}

	/**
	 * 用户分析：会员数、非会员数、手机号用户、未绑定手机号
	 * @return
	 */
	@Override
	public Result userAnalysis(String time,Integer flag) {
		int sum0 = 0; //会员数
		int sum1 = 0; //非会员数
		int sum2 = 0; //手机号用户
		int sum3 = 0; //未绑定手机号
		List<UserEntity> list = userDao.selectUserListByCreateTime(time,flag);
		for (UserEntity u : list) {
			if(u.getMember()!=1){
				sum0++;
			}else{
				sum1++;
			}
			if(StringUtils.isNotBlank(u.getPhone())){
				sum2++;
			}else {
				sum3++;
			}
		}
		Map map = new HashMap();
		map.put("sum0", sum0);
		map.put("sum1", sum1);
		map.put("sum2", sum2);
		map.put("sum3", sum3);
		return Result.success(map);
	}

	@Override
	public Integer selectUserCountByTime(String time){
		return baseMapper.selectUserCountByTime(time);
	}



}
