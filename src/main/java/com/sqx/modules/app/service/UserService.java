package com.sqx.modules.app.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.LoginInfo;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.form.LoginForm;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户
 *
 */
public interface UserService extends IService<UserEntity> {

	UserEntity queryByMobile(String mobile);

	int selectZhiUserInviteCount(String invitationCode);

	int selectFeiUserInviteCount(String invitationCode);

	Double selectZhiUserInviteMoney(String invitationCode);

	Double selectFeiUserInviteMoney(String invitationCode);

	Result bindOpenid(LoginInfo loginInfo);

	Result loginCode(LoginInfo loginInfo);

	Result loginWeiXinApp(LoginInfo loginInfo);

	Result loginApp(LoginInfo loginInfo);

	Result bindOpenId(String userId, String openId);

	Result bindPhone(String userId, String phone, String code);

	Result updatePhone(String phone, String msg, String userId);

	int insert(UserEntity userEntity);

	Result selectRankingList(Long userId);

	Result userAnalysis(String time,Integer flag);

	/**
	 * 用户登录
	 * @param form    登录表单
	 * @return        返回用户ID
	 */
	long login(LoginForm form);

	Result sendMsg(String phone, String state);

	Integer selectUserByMemberCount(Integer member);

	PageUtils selectUserList(Map<String,Object> params);

	List<UserEntity> selectUserList();

	List<UserEntity> selectUserLists();

	UserEntity selectById(Long userId);

	UserEntity selectUserByInvitationCode(String invitationCode);

	Result selectInvitationCodeByUserIdLists(int page,int limit,String invitationCode);

	Result selectInviterCodeByUserIdLists(int page, int limit, String invitationCode);

	Result selectZhiInviteByUserIdList(int page,int limit,Long userId);

	Result selectFeiInviteByUserIdList(int page,int limit,Long userId);

	Result getOpenId(String code,Long userId);

	UserEntity selectUserByOpenId(String userId);

	UserEntity selectUserByWxId(String wxId);

	Result wxRegister(UserEntity userInfo1);

	Result wxLogin(String code);

	void pushToSingle(String title, String content,String clientId);

	Result forgetPwd(String pwd, String phone, String msg);

	Result loginByOpenId(String openid);

	Result statistical();

	Integer selectUserCountByTime(String time);
}
