package com.sqx.modules.article.controller;

import com.sqx.common.utils.Result;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.article.entity.ArticleBrowsingHistory;
import com.sqx.modules.article.entity.ArticleClassify;
import com.sqx.modules.article.service.ArticleBrowsingHistoryService;
import com.sqx.modules.article.service.ArticleClassifyService;
import com.sqx.modules.article.service.ArticleCollectService;
import com.sqx.modules.article.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


/**
 * @author fang
 * @date 2020/8/6
 */
@RestController
@Api(value = "文章管理", tags = {"文章管理"})
@RequestMapping(value = "/article")
public class ArticleController {

    /** 浏览记录 */
    @Autowired
    private ArticleBrowsingHistoryService articleBrowsingHistoryService;
    /** 文章分类 */
    @Autowired
    private ArticleClassifyService articleClassifyService;
    /** 收藏 */
    @Autowired
    private ArticleCollectService articleCollectService;
    /** 文章 */
    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "/addArticle", method = RequestMethod.POST)
    @ApiOperation("添加文章")
    @ResponseBody
    public Result addArticle(@RequestBody Article article) {
        articleService.insert(article);
        return Result.success();
    }

    @RequestMapping(value = "/updateArticle", method = RequestMethod.POST)
    @ApiOperation("修改文章")
    @ResponseBody
    public Result update(@RequestBody Article article) {
        articleService.update(article);
        return Result.success();
    }

    @RequestMapping(value = "/selectArticleList", method = RequestMethod.GET)
    @ApiOperation("根据分类查询文章")
    @ResponseBody
    public Result selectArticleList(Long classifyId)
    {
        return Result.success().put("data",articleService.getArticleList(classifyId));
    }

    @RequestMapping(value = "/selectArticleById", method = RequestMethod.GET)
    @ApiOperation("根据文章id查看详细信息")
    @ResponseBody
    public Result selectArticleById(Long articleId,Long userId)
    {
        Long id=articleBrowsingHistoryService.selectCount(articleId,userId);
        //判断有没有浏览记录  没有则新增 有则更新时间
        if(id==null ||id==0){
            ArticleBrowsingHistory articleBrowsingHistory=new ArticleBrowsingHistory();
            articleBrowsingHistory.setArticleId(articleId);
            articleBrowsingHistory.setCreateTime(new Date());
            articleBrowsingHistory.setUserId(userId);
            articleBrowsingHistoryService.insert(articleBrowsingHistory);
        }else{
            articleBrowsingHistoryService.updateCreateTimeById(id,new Date());
        }
        return Result.success().put("data",articleService.selectArticleById(articleId));
    }

    @RequestMapping(value = "/deleteArticle", method = RequestMethod.POST)
    @ApiOperation("删除文章")
    @ResponseBody
    public Result deleteArticle(Long articleId) {
        articleService.deleteArticleById(articleId);
        return Result.success();
    }

    @RequestMapping(value = "/deleteArticleList", method = RequestMethod.POST)
    @ApiOperation("删除多个文章")
    @ResponseBody
    public Result deleteArticleList(String articleIds) {
        articleService.updateArticleStateByIds(articleIds);
        return Result.success();
    }

    @RequestMapping(value = "/selectArticleClassifyList", method = RequestMethod.GET)
    @ApiOperation("查看所有分类")
    @ResponseBody
    public Result selectArticleClassifyList()
    {
        return Result.success().put("data",articleClassifyService.selectArticleClassifyList());
    }

    @RequestMapping(value = "/saveArticleClassify", method = RequestMethod.POST)
    @ApiOperation("添加分类")
    @ResponseBody
    public Result saveArticleClassify(@RequestBody ArticleClassify articleClassify) {
        articleClassifyService.insert(articleClassify);

        return Result.success();
    }

    @RequestMapping(value = "/deleteArticleClassifyById", method = RequestMethod.POST)
    @ApiOperation("删除分类")
    @ResponseBody
    public Result deleteArticleClassifyById(Long articleClassifyId) {
        articleClassifyService.deleteById(articleClassifyId);
//        articleClassifyService.updateStateByArticleClassifyId(articleClassifyId,2);
        return Result.success();
    }

    @RequestMapping(value = "/deleteArticleClassifyByIds", method = RequestMethod.POST)
    @ApiOperation("删除多个分类")
    @ResponseBody
    public Result deleteArticleClassifyByIds(String articleClassifyIds) {
        articleClassifyService.updateStateByArticleClassifyIds(articleClassifyIds);
        return Result.success();
    }

    @RequestMapping(value = "/updateClassifyName/{id}/{classifyName}", method = RequestMethod.POST)
    @ApiOperation("修改分类名称")
    @ResponseBody
    public Result updateClassifyName(@PathVariable("id") Long id,@PathVariable("classifyName") String classifyName) {
        articleClassifyService.updateClassifyNameByArticleClassifyId(id,classifyName);
        return Result.success();
    }

    @RequestMapping(value = "/updateClassifySort", method = RequestMethod.POST)
    @ApiOperation("修改分类顺序")
    @ResponseBody
    public Result updateClassifySort(Long articleClassifyId,Integer sort) {
        articleClassifyService.updateSortByArticleClassifyId(articleClassifyId,sort);
        return Result.success();
    }

    @RequestMapping(value = "/selectArticleBrowsingHistoryList", method = RequestMethod.GET)
    @ApiOperation("查看所有浏览记录")
    @ResponseBody
    public Result selectArticleClassifyList(Long userId,int page,int limit)
    {
        return Result.success().put("data",articleBrowsingHistoryService.selectArticleBrowsingHistoryList(userId, page, limit));
    }


    @RequestMapping(value = "/selectArticleCollectList", method = RequestMethod.GET)
    @ApiOperation("查看所有收藏记录")
    @ResponseBody
    public Result selectArticleCollectList(Long userId,int page,int limit)
    {
        return Result.success().put("data",articleCollectService.selectArticleCollectList(userId,page,limit));
    }

    @RequestMapping(value = "/updateArticleCollect", method = RequestMethod.POST)
    @ApiOperation("收藏和取消收藏")
    @ResponseBody
    public Result updateArticleCollect(Long userId,Long articleCollectId)
    {
        articleCollectService.updateArticleCollect(userId,articleCollectId);
        return Result.success();
    }




}