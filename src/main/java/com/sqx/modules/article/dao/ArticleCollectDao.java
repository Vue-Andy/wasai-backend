package com.sqx.modules.article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.article.entity.ArticleCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/8/4
 */
@Mapper
public interface ArticleCollectDao extends BaseMapper<ArticleCollect> {

    Long selectCount(@Param("articleId") Long articleId, @Param("userId") Long userId);

    IPage<ArticleCollect> selectArticleCollectList(Page<ArticleCollect> page, @Param("userId") Long userId);

}
