package com.sqx.modules.article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.article.entity.ArticleBrowsingHistory;
import com.sqx.modules.article.entity.ArticleCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author fang
 * @date 2020/8/4
 */
@Mapper
public interface ArticleBrowsingHistoryDao extends BaseMapper<ArticleBrowsingHistory> {

    Long selectCount(@Param("articleId") Long articleId, @Param("userId") Long userId);

    IPage<ArticleBrowsingHistory> selectArticleBrowsingHistoryList(Page<ArticleBrowsingHistory> page, @Param("userId") Long userId);

    Integer updateCreateTimeById(@Param("id") Long id, @Param("createTime") Date createTime);

}
