package com.sqx.modules.article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.helpTask.entity.HelpProfit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/8/4
 */
@Mapper
public interface ArticleDao extends BaseMapper<Article> {

    IPage<Article> selectArticleList(Page<Article> page, @Param("classifyId") Long classifyId, @Param("state") Integer state);

    Integer updateArticleStateById(@Param("articleId") Long articleId, @Param("state") Integer state);

    List<Article> getArticleList(@Param("classifyId") Long classifyId);

}
