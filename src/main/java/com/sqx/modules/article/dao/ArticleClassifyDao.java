package com.sqx.modules.article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.article.entity.ArticleClassify;
import com.sqx.modules.article.entity.ArticleCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/8/4
 */
@Mapper
public interface ArticleClassifyDao extends BaseMapper<ArticleClassify> {

    List<ArticleClassify> selectArticleClassifyList();

    Integer updateStateByArticleClassifyId(@Param("articleClassifyId") Long articleClassifyId, @Param("state") Integer state);

    Integer updateClassifyNameByArticleClassifyId(@Param("articleClassifyId") Long articleClassifyId, @Param("articleClassifyName") String articleClassifyName);

    Integer updateSortByArticleClassifyId(@Param("articleClassifyId") Long articleClassifyId, @Param("sort") Integer sort);

}
