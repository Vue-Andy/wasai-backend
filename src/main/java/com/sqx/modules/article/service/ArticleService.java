package com.sqx.modules.article.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.article.entity.Article;

import java.util.Date;
import java.util.List;

public interface ArticleService {

    int insert(Article article);

    int update(Article article);

    Article selectArticleById(Long articleId);

    PageUtils selectArticleList(Long classifyId, Integer state, int page, int limit);

    void deleteArticleById(Long articleId);

    void deleteArticleByIds(String articleIds);

    int updateArticleStateById(Long articleId, Integer state);

    void updateArticleStateByIds(String articleIds);

    List<Article> getArticleList(Long classifyId);

}
