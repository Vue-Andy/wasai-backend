package com.sqx.modules.article.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.article.dao.ArticleDao;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.article.service.ArticleService;
import com.sqx.modules.common.entity.CommonInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 文章
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleDao, Article> implements ArticleService {

    @Autowired
    private ArticleDao articleDao;

    @Override
    public int insert(Article article) {
        return articleDao.insert(article);
    }

    @Override
    public int update(Article article) {
        return articleDao.updateById(article);
    }

    @Override
    public Article selectArticleById(Long articleId) {
        return articleDao.selectById(articleId);
    }

    @Override
    public PageUtils selectArticleList(Long classifyId, Integer state, int page, int limit) {
        Page<Article> pages=new Page<>(page,limit);
        return new PageUtils(articleDao.selectArticleList(pages,classifyId,state));
    }

    @Override
    public void deleteArticleById(Long articleId) {
         articleDao.deleteById(articleId);
    }

    @Override
    public void deleteArticleByIds(String articleIds) {
        for(String str:articleIds.split(",")){
            articleDao.deleteById(Long.parseLong(str));
        }
    }

    @Override
    public int updateArticleStateById(Long articleId, Integer state) {
        return articleDao.updateArticleStateById(articleId,state);
    }

    @Override
    public void updateArticleStateByIds(String articleIds) {
        for(String str:articleIds.split(",")){
            articleDao.updateArticleStateById(Long.parseLong(str),2);
        }
    }

    @Override
    public List<Article> getArticleList(Long classifyId) {
        return articleDao.getArticleList(classifyId);
    }
}
