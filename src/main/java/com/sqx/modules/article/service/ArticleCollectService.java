package com.sqx.modules.article.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.article.entity.ArticleCollect;

public interface ArticleCollectService {

    int insert(ArticleCollect articleCollect);

    void deleteById(Long articleCollectId);

    void deleteByIds(String articleCollectIds);

    Long selectCount(Long articleId, Long userId);

    PageUtils selectArticleCollectList(Long userId,int page, int limit);

    public void updateArticleCollect(Long userId, Long articleCollectId);

}
