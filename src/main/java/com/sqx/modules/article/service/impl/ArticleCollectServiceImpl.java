package com.sqx.modules.article.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.article.dao.ArticleCollectDao;
import com.sqx.modules.article.dao.ArticleDao;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.article.entity.ArticleCollect;
import com.sqx.modules.article.service.ArticleCollectService;
import com.sqx.modules.article.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 收藏
 */
@Service
public class ArticleCollectServiceImpl extends ServiceImpl<ArticleCollectDao, ArticleCollect> implements ArticleCollectService {

    @Autowired
    private ArticleCollectDao articleCollectDao;

    @Override
    public int insert(ArticleCollect articleCollect) {
        return articleCollectDao.insert(articleCollect);
    }

    @Override
    public void deleteById(Long articleCollectId) {
        articleCollectDao.deleteById(articleCollectId);
    }

    @Override
    public void deleteByIds(String articleCollectIds) {
        for(String str:articleCollectIds.split(",")){
            articleCollectDao.deleteById(Long.parseLong(str));
        }
    }

    @Override
    public Long selectCount(Long articleId, Long userId) {
        return articleCollectDao.selectCount(articleId,userId);
    }

    @Override
    public PageUtils selectArticleCollectList(Long userId, int page, int limit) {
        Page<ArticleCollect> pages=new Page<>(page,limit);
        return new PageUtils(articleCollectDao.selectArticleCollectList(pages,userId));
    }

    @Override
    public void updateArticleCollect(Long userId, Long articleCollectId) {
        Long id=selectCount(userId,articleCollectId);
        if(id==null || id==0){
            ArticleCollect articleCollect=new ArticleCollect();
            articleCollect.setArticleId(articleCollectId);
            articleCollect.setCreateTime(new Date());
            articleCollect.setUserId(userId);
            insert(articleCollect);
        }else{
            deleteById(id);
        }
    }
}
