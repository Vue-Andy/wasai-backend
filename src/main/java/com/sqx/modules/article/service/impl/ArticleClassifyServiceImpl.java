package com.sqx.modules.article.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.article.dao.ArticleClassifyDao;
import com.sqx.modules.article.dao.ArticleDao;
import com.sqx.modules.article.entity.Article;
import com.sqx.modules.article.entity.ArticleClassify;
import com.sqx.modules.article.service.ArticleClassifyService;
import com.sqx.modules.article.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章分类
 */
@Service
public class ArticleClassifyServiceImpl extends ServiceImpl<ArticleClassifyDao, ArticleClassify> implements ArticleClassifyService {

    @Autowired
    private ArticleClassifyDao articleClassifyDao;

    @Override
    public int insert(ArticleClassify articleClassify) {
        return articleClassifyDao.insert(articleClassify);
    }

    @Override
    public List<ArticleClassify> selectArticleClassifyList() {
        return articleClassifyDao.selectArticleClassifyList();
    }

    @Override
    public int updateStateByArticleClassifyId(Long articleClassifyId, Integer state) {
        return articleClassifyDao.updateStateByArticleClassifyId(articleClassifyId,state);
    }

    @Override
    public void updateStateByArticleClassifyIds(String articleClassifyIds) {
        for(String str:articleClassifyIds.split(",")){
            articleClassifyDao.updateStateByArticleClassifyId(Long.parseLong(str),2);
        }
    }

    @Override
    public int updateClassifyNameByArticleClassifyId(Long articleClassifyId, String articleClassifyName) {
        return articleClassifyDao.updateClassifyNameByArticleClassifyId(articleClassifyId,articleClassifyName);
    }

    @Override
    public int updateSortByArticleClassifyId(Long articleClassifyId, Integer sort) {
        return articleClassifyDao.updateSortByArticleClassifyId(articleClassifyId,sort);
    }

    @Override
    public int deleteById(Long articleClassifyId) {
        return articleClassifyDao.deleteById(articleClassifyId);
    }
}
