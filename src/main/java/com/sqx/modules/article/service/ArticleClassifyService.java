package com.sqx.modules.article.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.article.entity.ArticleClassify;
import com.sqx.modules.article.entity.ArticleCollect;

import java.util.List;

public interface ArticleClassifyService {

    int insert(ArticleClassify articleClassify);

    List<ArticleClassify> selectArticleClassifyList();

    int updateStateByArticleClassifyId(Long articleClassifyId, Integer state);

    void updateStateByArticleClassifyIds(String articleClassifyIds);

    int updateClassifyNameByArticleClassifyId(Long articleClassifyId, String articleClassifyName);

    int updateSortByArticleClassifyId(Long articleClassifyId, Integer sort);

    int deleteById(Long articleClassifyId);

}
