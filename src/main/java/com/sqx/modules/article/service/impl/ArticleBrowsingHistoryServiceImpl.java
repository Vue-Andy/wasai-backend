package com.sqx.modules.article.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.article.dao.ArticleBrowsingHistoryDao;
import com.sqx.modules.article.entity.ArticleBrowsingHistory;
import com.sqx.modules.article.service.ArticleBrowsingHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 浏览记录
 */
@Service
public class ArticleBrowsingHistoryServiceImpl extends ServiceImpl<ArticleBrowsingHistoryDao, ArticleBrowsingHistory> implements ArticleBrowsingHistoryService {

    @Autowired
    private ArticleBrowsingHistoryDao articleBrowsingHistoryDao;

    @Override
    public PageUtils selectArticleBrowsingHistoryList(Long userId, int page, int limit) {
        Page<ArticleBrowsingHistory> pages=new Page<>(page,limit);
        return new PageUtils(articleBrowsingHistoryDao.selectArticleBrowsingHistoryList(pages,userId));
    }

    @Override
    public int insert(ArticleBrowsingHistory articleBrowsingHistory) {
        return articleBrowsingHistoryDao.insert(articleBrowsingHistory);
    }

    @Override
    public Long selectCount(Long articleId, Long userId) {
        return articleBrowsingHistoryDao.selectCount(articleId,userId);
    }

    @Override
    public int updateCreateTimeById(Long id, Date createTime) {
        return articleBrowsingHistoryDao.updateCreateTimeById(id, createTime);
    }
}
