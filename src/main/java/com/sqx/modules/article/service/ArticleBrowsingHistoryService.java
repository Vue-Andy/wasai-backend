package com.sqx.modules.article.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.article.entity.ArticleBrowsingHistory;
import com.sqx.modules.article.entity.ArticleClassify;

import java.util.Date;
import java.util.List;

public interface ArticleBrowsingHistoryService {

    PageUtils selectArticleBrowsingHistoryList(Long userId, int page, int limit);

    int insert(ArticleBrowsingHistory articleBrowsingHistory);

    Long selectCount(Long articleId, Long userId);

    int updateCreateTimeById(Long Id, Date createTime);

}
