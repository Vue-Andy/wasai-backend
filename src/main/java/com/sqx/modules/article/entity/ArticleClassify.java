package com.sqx.modules.article.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  分类
 * @author fang 2020-05-29
 */
@Data
@TableName("article_classify")
public class ArticleClassify implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */

    private Long id;

    /**
     * 分类名称
     */

    private String classifyName;

    /**
     * 状态（1正常 2删除）
     */

    private Integer state;

    /**
     * 排序
     */

    private Integer sort;

    /**
     * 创建时间
     */

    private String createTime;

    /**
     * 备注
     */

    private String remark;


}
