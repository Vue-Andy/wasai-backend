package com.sqx.modules.article.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *  收藏
 * @author fang 2020-05-29
 */
@Data
@TableName("article_collect")
public class ArticleCollect implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收藏id
     */

    private Long id;

    /**
     * 文章id
     */

    private Long articleId;

    /**
     * 用户id
     */

    private Long userId;

    /**
     * 创建时间
     */

    private Date createTime;


}
