package com.sqx.modules.article.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  文章
 * @author fang 2020-05-29
 */
@Data
@TableName("article")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文章id
     */

    private Long id;

    /**
     * 分类id
     */

    private Long classifyId;

    /**
     * 标题
     */

    private String title;

    /**
     * 内容
     */

    private String content;

    /**
     * 图片
     */

    private String picture;

    /**
     * 链接
     */

    private String articleUrl;

    /**
     * 状态（1正常 2删除）
     */

    private Integer state;

    /**
     * 创建时间
     */

    private String createTime;



}
