package com.sqx.modules.invite.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.invite.dao.InviteDao;
import com.sqx.modules.invite.entity.Invite;
import com.sqx.modules.invite.entity.InviteMoney;
import com.sqx.modules.invite.service.InviteMoneyService;
import com.sqx.modules.invite.service.InviteService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 邀请记录
 */
@Service
public class InviteServiceImpl extends ServiceImpl<InviteDao, Invite> implements InviteService {


    @Autowired
    private InviteDao inviteDao;
    @Autowired
    private HelpRateService helpRateService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private InviteMoneyService inviteMoneyService;
    /** 用户钱包 */
    @Autowired
    private UserMoneyService userMoneyService;
    /** 用户钱包明细 */
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;

    @Override
    public PageUtils selectInviteList(int page,int limit,Integer state,Long userId){
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        if(state==null || state==-1){
            state=null;
        }
        return new PageUtils(inviteDao.selectInviteList(pages,state,userId));
    }


    @Override
    public PageUtils selectInviteUser(int page,int limit,Long userId,Integer state){
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(inviteDao.selectInviteUser(pages,userId,state));
    }

    @Override
    public Integer selectInviteByUserIdCountNotTime(Long userId) {
        return inviteDao.selectInviteByUserIdCountNotTime(userId);
    }

    @Override
    public Integer selectInviteByUserIdCount(Long userId, Date startTime, Date endTime) {
        return inviteDao.selectInviteByUserIdCount(userId,startTime,endTime);
    }

    @Override
    public Double selectInviteByUserIdSum(Long userId, Date startTime, Date endTime) {
        return inviteDao.selectInviteByUserIdSum(userId,startTime,endTime);
    }

    @Override
    public Double sumInviteMoney(String time, Integer flag) {
        return inviteDao.sumInviteMoney(time,flag);
    }

    @Override
    public PageUtils inviteAnalysis(int page,int limit, String time, Integer flag) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(inviteDao.inviteAnalysis(pages,time,flag));
    }

    @Override
    public Integer selectInviteCount(Integer state,Long userId){
        if(state==null || state==-1){
            state=null;
        }
        return inviteDao.selectInviteCount(state,userId);
    }

    @Override
    public Double selectInviteSum(Integer state, Long userId) {
        if(state==null || state==-1){
            state=null;
        }
        return inviteDao.selectInviteSum(state,userId);
    }


    @Transactional
    @Override
    public int saveBody(Long userId, UserEntity userEntity){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        Invite invite=new Invite();
        invite.setState(0);
        invite.setMoney(0.00);
        invite.setUserId(userEntity.getUserId());
        invite.setInviteeUserId(userId);
        invite.setCreateTime(format);
        inviteDao.insert(invite);
        Integer integer = selectInviteCount(null, userEntity.getUserId());
        List<HelpRate> helpRates = helpRateService.selectRateByClassifyList(4,1);
        //给被邀请者增加上级id
        UserEntity user=new UserEntity();
        user.setUserId(userId);
        user.setSuperior(userEntity.getUserId());
        user.setInviterCode(userEntity.getInvitationCode());
        userService.updateById(user);
        //判断下当前邀请人数跟会员等级
        if(integer>=helpRates.get(2).getRate().intValue()){
            if(userEntity.getMember()<4){
                userEntity.setMember(4);
                userService.updateById(userEntity);
                //当成为会员后则给邀请人赏金
                if(userEntity.getSuperior()!=null){
                    updateInvite(userEntity,format);
                }
            }
        }else if(integer>=helpRates.get(1).getRate().intValue()){
            if(userEntity.getMember()<3){
                userEntity.setMember(3);
                userService.updateById(userEntity);
                //当成为会员后则给邀请人赏金
                if(userEntity.getSuperior()!=null){
                    updateInvite(userEntity,format);
                }
            }
        }
        else if(integer>=helpRates.get(0).getRate().intValue()){
            if(userEntity.getMember()<2){
                userEntity.setMember(2);
                userService.updateById(userEntity);
                //当成为会员后则给邀请人赏金
                if(userEntity.getSuperior()!=null){
                    updateInvite(userEntity,format);
                }
            }
        }
        return 1;
    }


    @Override
    public void updateInvite(UserEntity userEntity,String format){
        Invite invite1 = inviteDao.selectInviteByUser(userEntity.getSuperior(), userEntity.getUserId());
        if(invite1==null){
            Invite invite=new Invite();
            invite.setState(0);
            invite.setMoney(0.00);
            invite.setUserId(userEntity.getSuperior());
            invite.setInviteeUserId(userEntity.getUserId());
            invite.setCreateTime(format);
            inviteDao.insert(invite);
            invite1 = inviteDao.selectInviteByUser(userEntity.getSuperior(), userEntity.getUserId());
        }
        if(invite1.getState()==0){
            CommonInfo one = commonInfoService.findOne(80);
            if(one!=null && StringUtils.isNotEmpty(one.getValue()) && Integer.parseInt(one.getValue())>0){
                Integer value = Integer.parseInt(one.getValue());
                value=value*100;
                Random random=new Random();
                Integer i=random.nextInt(value)+1;
                double money = i.doubleValue();
                money=AmountCalUtils.divide(money,100);
                DecimalFormat formater = new DecimalFormat();
                formater.setMaximumFractionDigits(2);
                formater.setGroupingSize(0);
                formater.setRoundingMode(RoundingMode.FLOOR);
                money=Double.parseDouble(formater.format(money));
                invite1.setState(1);
                invite1.setMoney(money);
                inviteDao.updateById(invite1);
                inviteMoneyService.updateInviteMoneySum(money,userEntity.getSuperior());
                userMoneyService.updateMayMoney(1,userEntity.getSuperior(),money);
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userEntity.getSuperior());
                userMoneyDetails.setTitle("[邀请好友]好友名称："+userEntity.getNickName());
                userMoneyDetails.setContent("增加金额:"+money);
                userMoneyDetails.setType(1);
                userMoneyDetails.setMoney(money);
                userMoneyDetails.setCreateTime(format);
                userMoneyDetailsService.insert(userMoneyDetails);
            }
        }
    }


}
