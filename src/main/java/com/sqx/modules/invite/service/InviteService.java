package com.sqx.modules.invite.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.invite.entity.Invite;

import java.util.Date;
import java.util.Map;

public interface InviteService {

    PageUtils selectInviteList(int page, int limit, Integer state, Long userId);

    Integer selectInviteCount(Integer state,Long userId);

    Double selectInviteSum(Integer state,Long userId);

    int saveBody(Long userId, UserEntity userEntity);

    PageUtils selectInviteUser(int page,int limit,Long userId,Integer state);

    Integer selectInviteByUserIdCountNotTime(Long userId);

    Integer selectInviteByUserIdCount(Long userId, Date startTime, Date endTime);

    Double selectInviteByUserIdSum(Long userId, Date startTime,Date endTime);

    Double sumInviteMoney(String time,Integer flag);

    PageUtils inviteAnalysis(int page,int limit, String time, Integer flag);

    void updateInvite(UserEntity userEntity,String format);

}
