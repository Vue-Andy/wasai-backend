package com.sqx.modules.shopping.controller;

import com.sqx.modules.shopping.dao.OrdersRepository;
import com.sqx.modules.shopping.entity.SelfOrderRelation;
import com.sqx.modules.shopping.service.OrdersRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class OrdersTask {
    @Autowired
    private OrdersRelationService service;
    @Autowired
    private OrdersRepository ordersRepository;
    /**
     * 每天凌晨执行一次
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void scheduledTask(){
        List<SelfOrderRelation> list = service.findAllByStatus();
        for (SelfOrderRelation s : list) {
            String finishTime = s.getFinishTime();
            boolean timeOut = this.isTimeOut(finishTime, 7);
            if (timeOut){
                service.addMoney(s);
            }
        }
    }

    /**
     * 判断传入时间是否超时
     * @param finishTime 完成时间
     * @param days 天数
     * @return  是否超时 true没有过期
     */
    public static boolean isTimeOut(String finishTime, int days){
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            Date finish = sf.parse(finishTime);
            Calendar c = Calendar.getInstance();
            c.setTime(finish);
            c.add(Calendar.DATE, + days);
            finish = c.getTime(); //完成时间七天后的时间，得到过期时间
            //过期时间和现在时间比
            return finish.after(new Date());
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
