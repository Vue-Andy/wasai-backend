package com.sqx.modules.shopping.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.AliPayConstants;
import com.sqx.modules.helpTask.config.AliPayNotifyParamConstants;
import com.sqx.modules.helpTask.entity.AliPayParamModel;
import com.sqx.modules.helpTask.utils.AliPayOrderUtil;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.shopping.dao.OrdersJpaRepository;
import com.sqx.modules.shopping.entity.SelfGoods;
import com.sqx.modules.shopping.entity.SelfGoodsVirtual;
import com.sqx.modules.shopping.entity.SelfOrders;
import com.sqx.modules.shopping.service.GoodsService;
import com.sqx.modules.shopping.service.OrdersService;
import com.sqx.modules.shopping.service.SelfGoodsVirtualService;
import com.sqx.modules.shopping.utils.DateUtil;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.*;

@Api(value="自营商城支付宝支付",tags={"自营商城支付宝支付"})
@Slf4j
@RestController
@RequestMapping("/aliPay")
public class SelfAliPayController {

    @Autowired
    private CommonInfoService commonRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrdersJpaRepository ordersJpaRepository;
    @Autowired
    private SelfGoodsVirtualService selfGoodsVirtualService;
    @Autowired
    private UserService userService;
    @Autowired
    private GoodsService goodsService;


    /**
     * 创建线程池处理业务逻辑
     */
    private ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().build();
    private ExecutorService singleThreadPool = new ThreadPoolExecutor(30, 100,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    /**
     * 支付宝网页支付
     *
     * @param response
     * @throws IOException
     */
    @RequestMapping("/payment")
    public void aliPay(HttpServletResponse response) throws IOException {

        AlipayClient alipayClient = new DefaultAlipayClient(AliPayConstants.REQUEST_URL,
                commonRepository.findOne(63).getValue(), commonRepository.findOne(65).getValue(), AliPayConstants.FORMAT,
                AliPayConstants.CHARSET, commonRepository.findOne(64).getValue(), AliPayConstants.SIGNTYPE);
        //封装请求参数
        AlipayTradeWapPayRequest aliPayRequest = new AlipayTradeWapPayRequest();
        AliPayParamModel aliPayParamModel = new AliPayParamModel();
        aliPayParamModel.setOut_trade_no("2018120514559696060");
        aliPayParamModel.setSubject("支付测试");
        aliPayParamModel.setTotal_amount("0.01");
        aliPayParamModel.setProduct_code(AliPayConstants.PRODUCT_CODE);
        // TODO 同步处理业务-涉及到商户自定义页面跳转
        aliPayRequest.setReturnUrl("http://ip:port/api/aliPay/return");
        // TODO 异步处理业务-修改订单状态、校验签名是否正确
        aliPayRequest.setNotifyUrl("http://ip:port/api/aliPay/notify");
        aliPayRequest.setBizContent(JSON.toJSONString(aliPayParamModel));
        try {
            // 调用SDK生成表单
            String form = alipayClient.pageExecute(aliPayRequest).getBody();
            response.setContentType("text/html;charset=" + AliPayConstants.CHARSET);
            response.getWriter().write(form);
            response.getWriter().flush();
            response.getWriter().close();
        } catch (AlipayApiException e) {
            log.info("支付宝支付异常，异常信息为:" + e.getErrMsg());
            e.printStackTrace();
        }
    }

    /**
     * 支付宝异步通知地址
     * @param request
     * @return
     */
    @RequestMapping("notify")
    @Transactional(rollbackFor = Exception.class)
    public void returnUrl(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> params = AliPayOrderUtil.convertRequestParamsToMap(request);
        String result = "";
        //调用SDK验证签名
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV1(params, commonRepository.findOne(64).getValue(),
                    AliPayConstants.CHARSET, AliPayConstants.SIGNTYPE);
        } catch (AlipayApiException e) {
            log.info("支付宝回调验签异常：" + e.getMessage());
            e.printStackTrace();
        }
        if (signVerified) {
            this.check(params);
            singleThreadPool.execute(() -> {
                AliPayNotifyParamConstants param = AliPayOrderUtil.buildAliPayNotifyParam(params);
                String tradeStatus = param.getTradeStatus();
                //支付成功
                if (tradeStatus.equalsIgnoreCase(AliPayConstants.TRADE_SUCCESS)) {
                    // TODO 处理业务逻辑
                    String outTradeNo = params.get("out_trade_no");
                    //支付成功，处理业务
                    SelfOrders orders = ordersService.findByOrderNum(outTradeNo);
                    if (orders.getStatus() == 1){
                        orders.setPayWay(2); //支付方式为2支付宝
                        orders.setStatus(2); //2已付款
                        orders.setPayTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())); //设置支付时间
                        ordersService.updateBody(orders);
                    }
                }
            });
            result = "success";
        } else {
            result = "failure";
        }
        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(result.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            log.info("支付宝返回异常，异常信息为：" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 校验支付宝支付返回的订单信息是否正确
     *
     * @param params
     */
    private void check(Map<String, String> params) {
        for (String key : params.keySet()){
            log.info("key="+key+";  value="+params.get(key));
        }
        // TODO 判断支付订单号是否是同一个
        String outTradeNo = params.get("out_trade_no");
        // 订单支付金额是否正确
        BigDecimal totalAmount = new BigDecimal(params.get("total_amount"));
//        Assert.isTrue(!totalAmount.equals(new BigDecimal(0.2)), "支付金额错误");
        // 判断支付的商户信息是否一致
        Assert.isTrue(params.get("app_id").equals(commonRepository.findOne(63).getValue()), "支付的商户信息不正确");
    }


    @RequestMapping("/notifyApp")
    @Transactional(rollbackFor = Exception.class)
    public void notifyApp(HttpServletRequest request, HttpServletResponse response){
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        //切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        //boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset, String sign_type)
        try {
            log.info("回调成功！！！");
            boolean flag = AlipaySignature.rsaCheckV1(params, commonRepository.findOne(64).getValue(), AliPayConstants.CHARSET,"RSA2");
            log.info(flag+"回调验证信息");
            if(flag){
                String outTradeNo = params.get("out_trade_no");
                //支付成功，处理业务
                SelfOrders orders = ordersService.findByOrderNum(outTradeNo);
                if (orders.getStatus() != 1){
                    log.info("订单回调重复："+orders.toString());
                    return;
                }
                orders.setPayWay(2); //支付方式为2支付宝
                orders.setStatus(2); //已付款
                orders.setPayTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())); //设置支付时间
                //5.虚拟商品处理
                if (orders.getIsExpress() == 2){ //是否需要发货(1需要发货 2无需发货)
                    //虚拟商品发货，发货成功为确认收货，否则不做变化
                    SelfGoodsVirtual v = selfGoodsVirtualService.sendGoods(orders.getGoodsId());
                    if (v != null){
                        orders.setVirtualId(v.getId()); //虚拟商品id
                        orders.setExpressTime(DateUtil.createTime()); //发货时间
                        orders.setFinishTime(DateUtil.createTime()); //收货时间
                        orders.setStatus(4); //确认收货
                        log.info("虚拟商品发货成功，单号："+outTradeNo);
                        System.err.println("虚拟商品发货成功，单号："+outTradeNo);
                    }
                }
                ordersService.updateBody(orders);
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            log.info("回调验证失败！！！");
        }
    }



    @ApiOperation("app支付")
    @GetMapping("/payApp")
    @Transactional(rollbackFor = Exception.class)
    public Result payApp(Long ordersId){
        log.info("开始下单了！！");
        String one2 = commonRepository.findOne(19).getValue();

        String returl=one2+"/pages/zysc/my/payment?id="+ordersId;

        SelfOrders orders = ordersJpaRepository.getOne(ordersId);
        String outTradeNo=orders.getOrderNum();
        //校验金额是否一致
        SelfGoods byId = goodsService.findById(orders.getGoodsId());
        UserEntity userById = userService.selectById(orders.getUserId());
        Double aDouble=0.00;
        if(userById.getMember()==null || userById.getMember()!=1){
            aDouble = AmountCalUtils.moneyMul(byId.getPrice(), orders.getNumber());
        }else{
            aDouble = AmountCalUtils.moneyMul(byId.getMemberPrice(), orders.getNumber());
        }
        aDouble= AmountCalUtils.add(aDouble,byId.getPostagePrice());
        if(!orders.getPayMoney().equals(aDouble)){
            ordersJpaRepository.deleteById(ordersId);
            return ResultUtil.error(-100,"订单信息错误，请重新下单！");
        }
        String result="";
        try {
            String url=one2+"/tao/aliPay/notifyApp";
            log.info("回调地址:"+url);
            //实例化客户端
            AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", commonRepository.findOne(63).getValue(), commonRepository.findOne(65).getValue(), "json", AliPayConstants.CHARSET, commonRepository.findOne(64).getValue(), "RSA2");
            //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
            AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
            //SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
            model.setBody(orders.getTitle());
            model.setSubject(orders.getTitle());
            model.setOutTradeNo(outTradeNo);
            model.setTimeoutExpress("30m");
            model.setTotalAmount(orders.getPayMoney()+""); //金额
            model.setProductCode("QUICK_MSECURITY_PAY");
            request.setBizModel(model);
            request.setNotifyUrl(url);
            request.setReturnUrl(returl);
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            if(response.isSuccess()){
                result=response.getBody();
            } else {
                log.info("调用失败");
            }
            return ResultUtil.success(result);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return ResultUtil.error(-100,"获取订单失败！");

    }


    @ApiOperation("h5支付")
    @GetMapping(value = "/payH5")
    @Transactional(rollbackFor = Exception.class)
    public Result payH5(Long ordersId) {
        String one2 = commonRepository.findOne(19).getValue();

        String returl = one2+"/pages/zysc/my/payment?id="+ordersId;
        try {
            SelfOrders orders = ordersJpaRepository.getOne(ordersId);
            SelfGoods byId = goodsService.findById(orders.getGoodsId());
            UserEntity userById = userService.selectById(orders.getUserId());
            Double aDouble=0.00;
            if(userById.getMember()==null || userById.getMember()!=1){
                aDouble = AmountCalUtils.moneyMul(byId.getPrice(), orders.getNumber());
            }else{
                aDouble = AmountCalUtils.moneyMul(byId.getMemberPrice(), orders.getNumber());
            }
            aDouble= AmountCalUtils.add(aDouble,byId.getPostagePrice());
            if(!orders.getPayMoney().equals(aDouble)){
                ordersJpaRepository.deleteById(ordersId);
                return ResultUtil.error(-100,"订单信息错误，请重新下单！");
            }

            String pay = commonRepository.findOne(167).getValue();
            String result="";
            String url=one2+"/tao/aliPay/notifyApp";
            log.info("回调地址:"+url);
            AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", commonRepository.findOne(63).getValue(), commonRepository.findOne(65).getValue(), "json", AliPayConstants.CHARSET, commonRepository.findOne(64).getValue(), "RSA2");
            AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
            JSONObject order = new JSONObject();
            order.put("out_trade_no",orders.getOrderNum()); //订单号
            order.put("subject", orders.getTitle()); //商品标题
            order.put("product_code", "QUICK_WAP_WAY");
            order.put("body", orders.getTitle());//商品名称
            order.put("total_amount", orders.getPayMoney()+""); //金额
            alipayRequest.setBizContent(order.toString());
            alipayRequest.setNotifyUrl(url); //在公共参数中设置回跳和通知地址
            alipayRequest.setReturnUrl(returl); //线上通知页面地址
            result = alipayClient.pageExecute(alipayRequest).getBody();
            return ResultUtil.success(result);
        }catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return ResultUtil.error(-100,"获取订单失败！");
    }
}
