package com.sqx.modules.shopping.controller;


import com.sqx.modules.shopping.entity.SelfGoods;
import com.sqx.modules.shopping.entity.SelfGoodsAttr;
import com.sqx.modules.shopping.service.GoodsService;
import com.sqx.modules.shopping.service.SelfGoodsRuleService;
import com.sqx.modules.shopping.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="自营商城商品",tags={"自营商城商品管理"})
@RequestMapping(value = "/goods")
public class GoodsController {
    @Autowired
    private GoodsService service;
    @Autowired
    private SelfGoodsRuleService ruleService;


    @GetMapping("/list")
    @ApiOperation("列表")
    public Result findAll(Integer page, Integer size,
                          @ApiParam("商品标题：可搜索")@RequestParam(required = false) String title,
                          @ApiParam("商品类型：类型名称")@RequestParam(required = false) String type,
                          @ApiParam("商品状态：0全部 1上架 2下架")@RequestParam(required = false) Integer status,
                          @ApiParam("传值为精选")@RequestParam(required = false) Integer isSelect,
                          @ApiParam("传值为每日")@RequestParam(required = false) Integer isRecommend) {
        return service.findAll(page, size, title, type, status,isSelect,isRecommend);
    }


    @GetMapping("/goodsList")
    @ApiOperation("虚拟商品列表")
    public Result goodsVirtualList(Integer page, Integer size,
                                   @ApiParam("商品标题：可搜索")@RequestParam(required = false) String title,
                                   @ApiParam("商品类型：类型名称")@RequestParam(required = false) String type,
                                   @ApiParam("商品状态：0全部 1上架 2下架")@RequestParam(required = false) Integer status) {
        return service.goodsVirtualList(page, size, title, type, status);
    }


    @GetMapping("/find")
    @ApiOperation("查询")
    public Result findOne(Long id) {
        return service.findOne(id);
    }

    @PostMapping("/save")
    @ApiOperation("添加")
    public Result saveBody(@RequestBody SelfGoods entity) {
        return service.saveBody(entity);
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result updateBody(@RequestBody SelfGoods entity) {
        return service.updateBody(entity);
    }

    @GetMapping("/delete")
    @ApiOperation("删除")
    public Result delete(Long id) {
        return service.delete(id);
    }

    @GetMapping("/brandList")
    @ApiOperation("用户端品牌列表")
    public Result brandList(@ApiParam("搜索内容")@RequestParam(required = false) String title) {
        return service.searchBrandList(title);
    }

    @GetMapping("/userList")
    @ApiOperation("用户端商品列表")
    public Result userList(Integer page, Integer size,
                           @ApiParam("商品类型：类型名称")@RequestParam(required = false) String type,
                           @ApiParam("品牌id")@RequestParam(required = false) String brandId,
                           @ApiParam("商品标题：可搜索")@RequestParam(required = false) String title,
                           @ApiParam("排序字段 综合createAt 销量sales 佣金比例commissionPrice 超低价price")@RequestParam(required = false) String sort) {
        return service.findAllByUser(page, size, type,brandId, title, sort);
    }


    @GetMapping("/selectGoods")
    @ApiOperation("精选好物")
    public Result selectGoods(Integer page, Integer size,
                              @ApiParam("排序字段 综合createAt 销量sales 佣金比例commissionPrice 超低价price")@RequestParam(required = false) String sort) {
        return service.selectGoods(page, size, sort);
    }


    @GetMapping("/selling")
    @ApiOperation("热卖榜单")
    public Result selling(Integer page, Integer size,
                          @ApiParam("排序字段 综合createAt 销量sales 佣金比例commissionPrice 超低价price")@RequestParam(required = false) String sort) {
        return service.selling(page, size, sort);
    }


    @GetMapping("/homeGoods")
    @ApiOperation("首页商品")
    public Result homeGoods(Integer page, Integer size) {
        return service.homeGoods(page, size);
    }


    @GetMapping("/news")
    @ApiOperation("每日上新")
    public Result news(Integer page, Integer size,
                       @ApiParam("排序字段 综合createAt 销量sales 佣金比例commissionPrice 超低价price")@RequestParam(required = false) String sort) {
        return service.news(page, size, sort);
    }


    @GetMapping("/recommend")
    @ApiOperation("每日推荐商品")
    public Result recommend(Integer page, Integer size,
                            @ApiParam("排序字段 综合createAt 销量sales 佣金比例commissionPrice 超低价price")@RequestParam(required = false) String sort) {
        return service.recommend(page, size, sort);
    }

    @GetMapping("/addRecommend")
    @ApiOperation("后台管理添加每日推荐")
    public Result addRecommend(Long id) {
        return service.addRecommend(id);
    }

    @GetMapping("/deleteRecommend")
    @ApiOperation("后台管理删除每日推荐")
    public Result deleteRecommend(String ids) {
        return service.deleteRecommend(ids);
    }

    @GetMapping("/addSelectGoods")
    @ApiOperation("后台管理添加精选好物")
    public Result addSelectGoods(Long id) {
        return service.addSelectGoods(id);
    }

    @GetMapping("/deleteSelectGoods")
    @ApiOperation("后台管理删除精选好物")
    public Result addSelectGoods(String ids) {
        return service.deleteSelectGoods(ids);
    }


    @GetMapping("/info")
    @ApiOperation("商品规格选项")
    public Result info() {
        return ruleService.info();
    }

    @ApiOperation(value = "多规格生成sku")
    @PostMapping(value = "/isFormatAttr")
    public Result isFormatSku(@RequestBody SelfGoodsAttr attr,
                              @ApiParam("商品图片")@RequestParam(required = false) String coverImg,
                              @ApiParam("原价")@RequestParam(required = false) String originalPrice,
                              @ApiParam("售价")@RequestParam(required = false) String price,
                              @ApiParam("会员价")@RequestParam(required = false) String memberPrice){
        return service.isFormatAttr(attr, coverImg, originalPrice, price, memberPrice);
    }

    @ApiOperation(value = "单规格生成sku")
    @GetMapping(value = "/onlyFormatAttr")
    public Result onlyFormatSku(@ApiParam("商品图片")@RequestParam(required = false) String coverImg,
                                @ApiParam("原价")@RequestParam(required = false) String originalPrice,
                                @ApiParam("售价")@RequestParam(required = false) String price,
                                @ApiParam("会员价")@RequestParam(required = false) String memberPrice){
        return service.onlyFormatAttr(coverImg, originalPrice, price, memberPrice);
    }

    @ApiOperation(value = "回显属性")
    @GetMapping(value = "/formatAttr")
    public Result formatAttr(Long goodsId){
        return service.formatAttr(goodsId);
    }

    @ApiOperation(value = "回显规格")
    @GetMapping(value = "/findAttrValue")
    public Result findAttrValue(Long goodsId){
        return service.findAttrValue(goodsId);
    }


    @ApiOperation(value = "商品上下架")
    @GetMapping(value = "/updateStatus")
    public Result updateStatus(Long goodsId){
        return service.updateStatus(goodsId);
    }

}
