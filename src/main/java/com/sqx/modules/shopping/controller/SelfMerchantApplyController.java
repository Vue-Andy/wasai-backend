package com.sqx.modules.shopping.controller;


import com.sqx.modules.shopping.entity.SelfMerchantApply;
import com.sqx.modules.shopping.service.SelfMerchantApplyService;
import com.sqx.modules.shopping.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="自营商城多商户商户入驻申请信息",tags={"自营商城多商户商户入驻申请信息"})
@RequestMapping(value = "/self/merchantApply")
public class SelfMerchantApplyController {
    @Autowired
    private SelfMerchantApplyService service;


    @GetMapping("/list")
    @ApiOperation("列表")
    public Result findAll(Integer page, Integer size,
                          @ApiParam("商家名称") @RequestParam(required = false) String companyName,
                          @ApiParam("注册手机号") @RequestParam(required = false) String legalPhone,
                          @ApiParam("店铺类型") @RequestParam Integer storeType,
                          @ApiParam("审核状态（1待处理 2通过 3拒绝）") @RequestParam Integer status,
                          @ApiParam("申请时间开始区间") @RequestParam(required = false) String createTimeStart,
                          @ApiParam("申请时间结束区间") @RequestParam(required = false) String createTimeEnd) {
        return service.findAll(page, size, companyName, legalPhone, storeType, status, createTimeStart, createTimeEnd);
    }


    @GetMapping("/find")
    @ApiOperation("查询")
    public Result findOne(Long id) {
        return service.findOne(id);
    }


    @PostMapping("/save")
    @ApiOperation("添加")
    public Result saveBody(@RequestBody SelfMerchantApply entity) {
        return service.saveBody(entity);
    }


    @PostMapping("/update")
    @ApiOperation("修改")
    public Result updateBody(@RequestBody SelfMerchantApply entity) {
        return service.updateBody(entity);
    }

    @GetMapping("/delete")
    @ApiOperation("删除")
    public Result delete(Long id) {
        return service.delete(id);
    }


    @PostMapping("/deal")
    @ApiOperation("处理审核")
    public Result deal(@ApiParam("申请id") @RequestParam Long id,
                       @ApiParam("审核状态（1待处理 2通过 3拒绝）") @RequestParam Integer status,
                       @ApiParam("生效日期") @RequestParam(required = false) String auditTime,
                       @ApiParam("生效年限") @RequestParam String auditYears,
                       @ApiParam("拒绝原因") @RequestParam(required = false) String refundReason) {
        return service.deal(id, status, auditTime, auditYears, refundReason);
    }

}
