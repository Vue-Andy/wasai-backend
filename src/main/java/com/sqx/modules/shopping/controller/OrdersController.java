package com.sqx.modules.shopping.controller;


import com.sqx.modules.shopping.entity.SelfOrders;
import com.sqx.modules.shopping.service.OrdersService;
import com.sqx.modules.shopping.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="自营商城订单",tags={"自营商城订单管理"})
@RequestMapping(value = "/orders")
public class OrdersController {
    @Autowired
    private OrdersService service;

    @GetMapping("/list")
    @ApiOperation("后台管理列表")
    public Result findAll(Integer page, Integer size,
                          @ApiParam("订单号")@RequestParam(required = false) String orderNum,
                          @ApiParam("订单状态：1待付款 2已付款 3已发货 4已收货 5已取消 6退款中 7已退款 8拒绝退款")@RequestParam String status,
                          @ApiParam("商品名称")@RequestParam(required = false) String title,
                          @ApiParam("手机号")@RequestParam(required = false) String mobile) {
        return service.findAll(page, size, orderNum, status, title, mobile);
    }


    @GetMapping("/findMyList")
    @ApiOperation("我的订单列表")
    public Result findMyList(Integer page, Integer size,
                             @ApiParam("用户id")@RequestParam String userId,
                             @ApiParam("订单状态：1待付款 2已付款 3已发货 4已收货 5已取消 6退款中 7已退款 8拒绝退款")@RequestParam String status) {
        return service.findMyList(page, size, userId, status);
    }


    @GetMapping("/find")
    @ApiOperation("查询")
    public Result findOne(Long id) {
        return service.findOne(id);
    }

    @PostMapping("/save")
    @ApiOperation("保存订单")
    public Result saveBody(@RequestBody SelfOrders entity) {
        return service.saveBody(entity);
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result updateBody(@RequestBody SelfOrders entity) {
        return service.updateBody(entity);
    }


    @GetMapping("/delete")
    @ApiOperation("删除")
    public Result delete(Long id) {
        return service.delete(id);
    }


    @ApiOperation(value="取消订单，orderStatus=1时可取消")
    @GetMapping("/cancel")
    public Result orderCancel(@ApiParam("id") @RequestParam Long id){
        return service.orderCancel(id);
    }

    @ApiOperation("后台管理去发货")
    @GetMapping("/express")
    public Result express(Long id, String expressName, String expressNumber){
        return service.express(id, expressName, expressNumber);
    }

    @ApiOperation("后台管理虚拟去发货")
    @GetMapping("/expressVirtual")
    public Result expressVirtual(Long id){
        return service.expressVirtual(id);
    }


    @ApiOperation(value="确认收货，orderStatus=3时收货")
    @GetMapping("/confirm")
    public Result orderConfirm(@ApiParam("id") @RequestParam Long id){
        return service.orderConfirm(id);
    }



    @ApiOperation(value="收入金额统计")
    @GetMapping("/income")
    public Result income(@ApiParam("data") @RequestParam String data, @ApiParam("way： 1年 2月 3日") @RequestParam int way){
        return service.income(data, way);
    }


    @ApiOperation(value="订单成交量统计")
    @GetMapping("/statistical")
    public Result statistical(@ApiParam("data") @RequestParam String data){
        return service.statistical(data);
    }

    @ApiOperation("零钱支付")
    @GetMapping("/changePay")
    public Result changePay(String ordersId){
        return service.changePay(ordersId);
    }


    @ApiOperation("检测是否完成支付")
    @GetMapping("/wxPay")
    public Result wxPayWeb(Long ordersId){
        return service.checkPay(ordersId);
    }


    @ApiOperation("申请退款")
    @GetMapping("/refund")
    public Result refund(Long ordersId, String refund){
        return service.refund(ordersId, refund);
    }


    @ApiOperation("后台管理确认退款")
    @GetMapping("/refundMoney")
    public Result refundMoney(Long ordersId){
        return service.refundMoney(ordersId);
    }


    @ApiOperation("后台管理拒绝退款")
    @GetMapping("/refusedRefund")
    public Result refusedRefund(Long ordersId, String refusedRefund){
        return service.refusedRefund(ordersId, refusedRefund);
    }


    @ApiOperation("后台管理待处理订单")
    @GetMapping("/pendingOrder")
    public Result pendingOrder(){
        return service.pendingOrder();
    }


    @ApiOperation("用户端提醒发货")
    @GetMapping("/remind")
    public Result remind(Long ordersId){
        return service.remind(ordersId);
    }


    @ApiOperation("后台管理被提醒发货")
    @GetMapping("/remindOrder")
    public Result remindOrder(){
        return service.remindOrder();
    }


    @ApiOperation("用户端 订单统计")
    @GetMapping("/count")
    public Result count(Long userId){
        return service.count(userId);
    }


    @ApiOperation("查快递")
    @GetMapping("/findExpress")
    public Result findExpress(String expressNumber){
        return service.findExpress(expressNumber);
    }
}
