package com.sqx.modules.shopping.controller;


import com.sqx.modules.shopping.entity.Coupons;
import com.sqx.modules.shopping.service.CouponsService;
import com.sqx.modules.shopping.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="自营商城优惠券",tags={"自营商城优惠券"})
@RequestMapping(value = "/coupons")
public class CouponsController {
    @Autowired
    private CouponsService service;

    @GetMapping("/list")
    @ApiOperation("列表")
    public Result findAll(Integer page, Integer size) {
        return service.findAll(page, size);
    }


    @GetMapping("/find")
    @ApiOperation("查询")
    public Result findOne(Long id) {
        return service.findOne(id);
    }

    @PostMapping("/save")
    @ApiOperation("添加")
    public Result saveBody(@RequestBody Coupons entity) {
        return service.saveBody(entity);
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result updateBody(@RequestBody Coupons entity) {
        return service.updateBody(entity);
    }

    @GetMapping("/delete")
    @ApiOperation("删除")
    public Result delete(Long id) {
        return service.delete(id);
    }


    @GetMapping("/userGet")
    @ApiOperation("用户端领取优惠券")
    public Result userGetCoupons(Long userId, Long couponsId) {
        return service.userGetCoupons(userId, couponsId);
    }


    @GetMapping("/userCoupons")
    @ApiOperation("用户端优惠券列表")
    public Result userCoupons(Long userId) {
        return service.userCoupons(userId);
    }

}
