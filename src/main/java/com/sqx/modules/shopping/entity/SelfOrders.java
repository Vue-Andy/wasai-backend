package com.sqx.modules.shopping.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * 自营商城订单
 */
@Data
@Entity
public class SelfOrders {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //id
    @Column
    private String orderNum; //订单号
    @Column
    private String payNum; //支付单号
    @Column
    private String descrition; //订单备注
    @Column
    private Integer number; //商品个数
    @Column
    private Double payMoney; //支付金额
    @Column
    private Integer status; //订单状态（1待付款 2已付款 3已发货 4已收货 5已取消 6退款中 7已退款 8拒绝退款 9拼团中 10已评价）
    @Column
    private Integer payWay; //支付方式（1app微信 2微信公众号 3微信小程序 4app支付宝 5H5支付宝 6零钱）
    @Column
    private String createAt; //创建时间
    @Column
    private String payTime; //支付时间
    @Column
    private String finishTime; //完成时间
    /**收货信息*/
    @Column
    private String consignee; //收货人
    @Column
    private String mobile; //手机号
    @Column
    private String provinces; //省市区
    @Column
    private String detail; //详细地址
    /**用户信息*/
    @Column
    private Long userId; //用户id
    @Column
    private String relationId; //分销id(拿到返利的人)
    /**物流发货信息*/
    @Column
    private Integer isExpress; //是否需要发货(1需要发货 2无需发货)
    @Column
    private Double postagePrice; //邮费
    @Column
    private String expressName; //快递名称
    @Column
    private String expressNumber; //快递单号
    @Column
    private String expressTime; //发货时间
    @Column
    private Long virtualId; //虚拟商品id
    /**退款*/
    @Column
    private Integer isRefund; //是否可以退款（2不可退款）
    @Column
    private String refund; //退款理由
    @Column
    private String refusedRefund; //拒绝退款理由
    /**商品*/
    @Column
    private Long goodsId; //商品id
    @Column
    private Long skuId; //商品skuId
    @Column
    private String detailJson; //商品sku信息
    @Column
    private BigDecimal commissionPrice; //订单佣金
    @Column
    private Long type; //商品类型id
    @Transient
    private GoodsType goodsType;
    @Column
    private String title; //商品标题
    @Column
    private String img; //商品图片
    @Column
    private Double price; //商品价格
    @Column
    private Double memberPrice;
    /**拼团*/
    @Column
    private Integer orderType; //订单类型（1普通订单 2拼团 3秒杀）
    @Column
    private Long groupId; //拼团商品Id
    @Column
    private Long groupPinkId; //加入拼团团体id
    /**秒杀*/
    @Column
    private Long secKillId; //秒杀商品id
    /**多商户*/
    @Column
    private Long merchantId; //商户id
    /**优惠券*/
    @Column
    private Long userCouponsId; //用户优惠券id
    @Column
    private Double couponMoney; //优惠券优惠金额
    /**分销*/
    @Transient
    private List<SelfOrderRelation> relationList; //订单分销列表
}
