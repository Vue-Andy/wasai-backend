package com.sqx.modules.shopping.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 自营商城商品分类
 */
@Data
@Entity
public class GoodsType {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //id
    @Column
    private Long parentId; //父级id，父级为0时为一级
    @Column
    private String name; //分类名称
    @Column
    private String img; //图片
    @Column
    private String createAt; //创建时间
    @Column
    private Integer sort; //排序
    @Transient
    private List<GoodsType> children; //下级分类
}
