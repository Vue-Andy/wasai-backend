package com.sqx.modules.shopping.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 自营商城商家入驻申请
 */
@Data
@Entity
public class SelfMerchantApply {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //id
    //店铺信息
    @Column
    private String storeName; //店铺名称
    @Column
    private Integer storeType; //店铺类型（1个人 2个体工商户 3企业 4其他组织）
    @Column
    private String serverPhone; //客服电话
    @Column
    private String legal; //店铺负责人
    @Column
    private String legalPhone; //负责人电话
    @Column
    private String storeAddressProvince; //店铺地址省市
    @Column
    private String storeAddressDetail; //店铺地址详细
    //公司信息
    @Column
    private String companyName; //商户名称
    @Column
    private String companyCode; //营业执照编号
    @Column
    private String companyAddressProvince; //公司注册地址省
    @Column
    private String companyAddressCity; //公司注册地址市
    @Column
    private String companyAddressDistrict; //公司注册地址区
    @Column
    private String companyAddressDetail; //公司注册地址
    @Column
    private String companyTerm; //营业期限
    @Column
    private String companyLicenseImg; //营业执照照片
    //经营者个人信息
    @Column
    private String storeHead; //负责人姓名
    @Column
    private String idCardType; //证件类型(1中国大陆居民身份证 2中国香港居民来往内地通行证 3中国澳门居民来往内地通行证 4中国台湾居民来往内地通行证 5其他国家或地区居民护照)
    @Column
    private String idCardNumber; //身份证号码
    @Column
    private String idCardValidTimeStart; //身份证有效期起始时间
    @Column
    private String idCardValidTimeEnd; //身份证有效期截止时间
    @Column
    private String idCardImg1; //证件照片正面
    @Column
    private String idCardImg2; //证件照片国徽面
    @Column
    private String idCardImg3; //手持证件照片
    //授权信息
    @Column
    private String auditTime; //生效日期
    @Column
    private String auditYears; //生效年限
    //审核
    @Column
    private String createTime; //申请时间
    @Column
    private Integer status; //审核状态（1待处理 2通过 3拒绝）
    @Column
    private String refundReason; //拒绝原因
}
