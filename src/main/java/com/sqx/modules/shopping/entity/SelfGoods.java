package com.sqx.modules.shopping.entity;

import com.sqx.modules.helpTask.entity.HelpRate;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 自营商城商品分类
 */
@Data
@Entity
public class SelfGoods implements Serializable {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //id
    @Column
    private String merchants; //商户号
    @Column
    private String typeId; //商品类型id
    @Transient
    private GoodsType type; //商品类型id
    @Column
    private String createAt; //创建时间
    @Column
    private String title; //商品标题
    @Column
    private String buyReason; //必买理由
    @Column
    private String coverImg; //商品封面图片
    @Column
    private String img; //商品图片
    @Column
    private String descrition; //商品描述
    @Column
    private Double originalPrice; //商品原价
    @Column
    private Double price; //商品价格
    @Column
    private Double memberPrice; //会员价格
    @Column
    private Double commissionPrice; //商品佣金
    @Column
    private Integer sales; //商品销量
    @Column
    private Integer status; //商品状态(默认1正常 2下架)
    @Column
    private Integer isSelect; //精选好物(0默认 1精选好物)
    @Column
    private Integer homeGoods; //首页商品(0默认 1是首页商品)
    @Column
    private Integer isRecommend; //每日推荐(0默认 1推荐商品)
    @Column
    private Integer isExpress; //是否需要发货(1普通商品需要发货 2虚拟商品无需发货)
    @Column
    private Double postagePrice; //邮费

    @Transient
    private List<SelfGoodsAttr> attr; //规格
    @Transient
    private List<SelfGoodsSku> sku; //sku

    /**虚拟商品*/
    @Transient
    private Integer virtualSum; //虚拟商品总数
    @Transient
    private Integer virtualCount; //虚拟商品库存
    @Transient
    private List<HelpRate> helpRateList; //佣金比例
    @Column
    private Integer isJiFenGoods;
    @Column
    private Long brandId; //品牌id
    @Transient
    private SelfGoodsBrand brand; //品牌实体


}
