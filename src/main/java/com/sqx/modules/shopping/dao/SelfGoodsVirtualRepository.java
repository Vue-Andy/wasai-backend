package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsVirtual;
import org.springframework.data.repository.Repository;

public interface SelfGoodsVirtualRepository extends Repository<SelfGoodsVirtual, Long> {

}