package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsAttrValue;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

public interface SelfGoodsAttrValueRepository extends Repository<SelfGoodsAttrValue, Long> {

    @Transactional
    @Modifying
    @Query(value = "delete from self_goods_attr_value where goods_id=:goodsId",nativeQuery = true)
    int deleteSelfGoodsAttrValueByGoodsId(Long goodsId);


}