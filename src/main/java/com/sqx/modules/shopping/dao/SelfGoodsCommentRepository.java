package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsComment;
import org.springframework.data.repository.Repository;

public interface SelfGoodsCommentRepository extends Repository<SelfGoodsComment, Long> {

}