package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SelfActivityJpaRepository extends JpaRepository<SelfActivity, Long> {



}
