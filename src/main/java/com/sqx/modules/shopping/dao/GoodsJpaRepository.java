package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoods;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsJpaRepository extends JpaRepository<SelfGoods, Long> {

    //条件查询带分页
    Page<SelfGoods> findAll(Specification<SelfGoods> specification, Pageable pageable);

    List<SelfGoods> findAll(Specification<SelfGoods> specification);

}
