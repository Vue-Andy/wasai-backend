package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfOrders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersJpaRepository extends JpaRepository<SelfOrders, Long> {

    //条件查询带分页
    Page<SelfOrders> findAll(Specification<SelfOrders> specification, Pageable pageable);

    //条件查询
    List<SelfOrders> findAll(Specification<SelfOrders> specification);

    //根据订单号查询
    SelfOrders findByOrderNum(String orderNum);

    //根据订单号查询
    List<SelfOrders> findByPayNum(String payNum);

    //根据虚拟id查询
    SelfOrders findByVirtualId(Long virtualId);

}
