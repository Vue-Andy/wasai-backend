package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfMerchantApply;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SelfMerchantApplyJpaRepository extends JpaRepository<SelfMerchantApply, Long> {

    //条件查询带分页
    Page<SelfMerchantApply> findAll(Specification<SelfMerchantApply> specification, Pageable pageable);

    //条件查询
    List<SelfMerchantApply> findAll(Specification<SelfMerchantApply> specification);

}
