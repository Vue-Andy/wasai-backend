package com.sqx.modules.shopping.dao;

import com.sqx.modules.shopping.entity.CouponsUser;
import org.springframework.data.repository.Repository;


public interface CouponsUserRepository extends Repository<CouponsUser, Long> {

}