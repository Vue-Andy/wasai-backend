package com.sqx.modules.shopping.dao;

import com.sqx.modules.shopping.entity.SelfBanner;
import org.springframework.data.repository.Repository;


public interface SelfBannerRepository extends Repository<SelfBanner, Long> {

}