package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsSku;
import org.springframework.data.repository.Repository;

public interface SelfGoodsSkuRepository extends Repository<SelfGoodsSku, Long> {

}