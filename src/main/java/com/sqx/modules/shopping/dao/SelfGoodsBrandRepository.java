package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsBrand;
import org.springframework.data.repository.Repository;

public interface SelfGoodsBrandRepository extends Repository<SelfGoodsBrand, Long> {

}