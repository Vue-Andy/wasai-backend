package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.GoodsType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsTypeJpaRepository extends JpaRepository<GoodsType, Long> {

}
