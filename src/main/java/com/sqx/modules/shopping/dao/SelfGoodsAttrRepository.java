package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsAttr;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SelfGoodsAttrRepository extends Repository<SelfGoodsAttr, Long> {

    @Transactional
    @Modifying
    @Query(value = "delete from self_goods_attr where goods_id=:goodsId",nativeQuery = true)
    int deleteSelfGoodsAttrByGoodsId(Long goodsId);


}