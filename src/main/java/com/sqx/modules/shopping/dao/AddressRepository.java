package com.sqx.modules.shopping.dao;

import com.sqx.modules.shopping.entity.Address;
import org.springframework.data.repository.Repository;


public interface AddressRepository extends Repository<Address, Long> {

}