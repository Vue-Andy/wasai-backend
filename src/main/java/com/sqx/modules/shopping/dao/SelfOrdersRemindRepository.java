package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfOrderRemind;
import org.springframework.data.repository.Repository;

public interface SelfOrdersRemindRepository extends Repository<SelfOrderRemind, Long> {



}