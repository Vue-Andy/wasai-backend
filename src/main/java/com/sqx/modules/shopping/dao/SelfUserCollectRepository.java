package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfUserCollect;
import org.springframework.data.repository.Repository;

public interface SelfUserCollectRepository extends Repository<SelfUserCollect, Long> {

}