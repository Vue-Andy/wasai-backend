package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsRule;
import org.springframework.data.repository.Repository;

public interface SelfGoodsRuleRepository extends Repository<SelfGoodsRule, Long> {

}