package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoods;
import org.springframework.data.repository.Repository;

public interface GoodsRepository extends Repository<SelfGoods, Long> {

}