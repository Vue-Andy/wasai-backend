package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfOrders;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrdersRepository extends Repository<SelfOrders, Long> {

    /**
     * 查询待处理订单个数（1待付款 2已付款 3已发货 4已收货 5已取消 6退款中 7已退款）
     */
    @Query(value = "select count(id) from SelfOrders where status in(2,6)")
    int pendingOrder();

    //查询用户订单
    @Query(value = "from SelfOrders o where o.userId =:userId")
    List<SelfOrders> findByUserId(@Param("userId") Long userId);

//    @Query(value = "select * from self_orders where status=3 and ")


}