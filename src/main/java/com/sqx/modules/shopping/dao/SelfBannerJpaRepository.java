package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfBanner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SelfBannerJpaRepository extends JpaRepository<SelfBanner, Long> {

}
