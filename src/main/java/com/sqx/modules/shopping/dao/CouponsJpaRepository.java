package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.Coupons;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CouponsJpaRepository extends JpaRepository<Coupons, Long> {

    /**
     * 查询所有优惠券，状态为0正常
     */
    @Query(value = "from Coupons s where s.status = 0 and s.remainingNumber > 0")
    List<Coupons> findAllByStatus();

}
