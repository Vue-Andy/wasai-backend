package com.sqx.modules.shopping.dao;

import com.sqx.modules.shopping.entity.SelfMerchantApply;
import org.springframework.data.repository.Repository;

public interface SelfMerchantApplyRepository extends Repository<SelfMerchantApply, Long> {

}