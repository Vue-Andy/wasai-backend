package com.sqx.modules.shopping.dao;

import com.sqx.modules.shopping.entity.GoodsType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;


public interface GoodsTypeRepository extends Repository<GoodsType, Long> {

    /**
     * 查询所有一级分类
     */
    @Query(value = "from GoodsType s where s.parentId=0 order by s.sort asc")
    List<GoodsType> findAllStatus0();

    /**
     * 查询所有非一级分类
     */
    @Query(value = "from GoodsType s where s.parentId <> 0 order by s.sort asc")
    List<GoodsType> findAllStatus();

}