package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.CouponsUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CouponsUserJpaRepository extends JpaRepository<CouponsUser, Long> {

    /**
     * 查询用户领取的优惠券的数量
     */
    @Query(value = "select count(s.id) from CouponsUser s where s.couponsId =: couponsId and s.userId =: userId")
    int findByCouponsId(@Param("couponsId") Long couponsId, @Param("userId") Long userId);


}
