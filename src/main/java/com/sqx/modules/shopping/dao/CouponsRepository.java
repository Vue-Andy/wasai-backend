package com.sqx.modules.shopping.dao;

import com.sqx.modules.shopping.entity.Coupons;
import org.springframework.data.repository.Repository;


public interface CouponsRepository extends Repository<Coupons, Long> {

}