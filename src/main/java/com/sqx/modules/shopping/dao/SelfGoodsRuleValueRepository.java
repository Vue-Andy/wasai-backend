package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.SelfGoodsRuleValue;
import org.springframework.data.repository.Repository;

public interface SelfGoodsRuleValueRepository extends Repository<SelfGoodsRuleValue, Long> {

}