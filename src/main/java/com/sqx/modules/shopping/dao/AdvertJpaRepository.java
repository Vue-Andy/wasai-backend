package com.sqx.modules.shopping.dao;


import com.sqx.modules.shopping.entity.Adverti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertJpaRepository extends JpaRepository<Adverti, Long> {

}
