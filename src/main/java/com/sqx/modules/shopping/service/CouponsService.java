package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.Coupons;
import com.sqx.modules.shopping.utils.Result;

public interface CouponsService {
    //列表
    Result findAll(Integer page, Integer size);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(Coupons entity);

    //修改
    Result updateBody(Coupons entity);

    /**
     * 用户领取优惠券
     * @param userId 用户id
     * @return
     */
    Result userGetCoupons(Long userId, Long couponsId);

    /**
     * 用户优惠券列表
     * @param userId id
     * @return
     */
    Result userCoupons(Long userId);

}
