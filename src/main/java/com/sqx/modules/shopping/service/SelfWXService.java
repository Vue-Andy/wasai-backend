package com.sqx.modules.shopping.service;

import java.util.Map;

public interface SelfWXService {

    /**
     * 微信支付
     * @param ordersId
     * @param type
     * @return
     * @throws Exception
     */
    Map doUnifiedOrder(Long ordersId, Integer type, Long userId) throws Exception;

    /**
     * 支付回调
     * @param resXml
     * @return
     */
    String payBack(String resXml);
    String payBacks(String resXml);
    String payBackjs(String resXml);

    //退款
    boolean refund(Long ordersId);

}