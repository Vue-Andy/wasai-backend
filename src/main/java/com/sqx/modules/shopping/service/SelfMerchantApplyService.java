package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.SelfMerchantApply;
import com.sqx.modules.shopping.utils.Result;

public interface SelfMerchantApplyService {
    /**
     * 商家入驻申请列表
     * @param page 页数
     * @param size 条数
     * @param companyName 商家名称
     * @param legalPhone 注册手机号
     * @param storeType 店铺类型
     * @param status 审核状态（1待处理 2通过 3拒绝）
     * @param createTimeStart 申请时间开始区间
     * @param createTimeEnd 申请时间结束区间
     * @return
     */
    Result findAll(Integer page, Integer size, String companyName,String legalPhone, Integer storeType, Integer status, String createTimeStart, String createTimeEnd);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(SelfMerchantApply entity);

    //修改
    Result updateBody(SelfMerchantApply entity);

    /**
     * 处理审核
     * @param id
     * @param status 审核状态（1待处理 2通过 3拒绝）
     * @param auditTime 生效日期
     * @param auditYears 生效年限
     * @param refundReason 拒绝原因
     * @return
     */
    Result deal(Long id, Integer status, String auditTime, String auditYears, String refundReason);


}
