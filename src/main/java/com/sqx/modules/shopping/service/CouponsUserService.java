package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.CouponsUser;
import com.sqx.modules.shopping.utils.Result;

public interface CouponsUserService {
    //列表
    Result findAll(Integer page, Integer size);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(CouponsUser entity);

    //修改
    Result updateBody(CouponsUser entity);

}
