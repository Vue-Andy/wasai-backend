package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.GoodsType;
import com.sqx.modules.shopping.utils.Result;

public interface GoodsTypeService {
    //列表
    Result findAll();

    //数据封装
    Result findAllResult();

    //一级目录数据封装
    Result findAllResult1();

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(GoodsType entity);

    //修改
    Result updateBody(GoodsType entity);

}
