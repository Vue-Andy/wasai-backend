package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.SelfBannerJpaRepository;
import com.sqx.modules.shopping.entity.SelfBanner;
import com.sqx.modules.shopping.service.SlefBannerService;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class SelfBannerServiceImpl implements SlefBannerService {
    @Autowired
    private SelfBannerJpaRepository jpaRepository;

    @Override
    public Result findAll() {
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, "sort")); //排序升序
        return ResultUtil.success(jpaRepository.findAll(sort));
    }

    @Override
    public Result saveBody(SelfBanner entity) {
        entity.setCreateAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result updateBody(SelfBanner entity) {
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result findOne(Long id) {
        return ResultUtil.success(jpaRepository.findById(id).orElse(null));
    }

    @Override
    public Result delete(Long id) {
        jpaRepository.deleteById(id);
        return ResultUtil.success();
    }

}
