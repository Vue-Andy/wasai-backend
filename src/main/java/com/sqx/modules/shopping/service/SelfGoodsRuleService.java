package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.SelfGoodsRule;
import com.sqx.modules.shopping.utils.Result;

public interface SelfGoodsRuleService {
    //后台管理商品列表
    Result findAll(Integer page, Integer size);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(SelfGoodsRule entity);

    //修改
    Result updateBody(SelfGoodsRule entity);

    //所有商品规格模板
    Result info();

}
