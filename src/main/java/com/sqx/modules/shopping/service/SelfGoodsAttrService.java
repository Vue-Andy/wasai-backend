package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.SelfGoodsAttr;
import com.sqx.modules.shopping.utils.Result;

import java.util.List;

public interface SelfGoodsAttrService {
    //添加
    Result saveBody(SelfGoodsAttr entity);

    //修改
    Result updateBody(SelfGoodsAttr entity);

    //根据商品id查询
    List<SelfGoodsAttr> findByGoodsId(Long goodsId);

    void deleteAttrAndValue(Long goodsId);


}
