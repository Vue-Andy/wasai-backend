package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.SelfGoodsVirtual;
import com.sqx.modules.shopping.entity.SelfOrders;
import com.sqx.modules.shopping.utils.Result;

import java.util.List;

public interface OrdersService {
    //列表
    Result findAll(Integer page, Integer size, String orderNum, String status, String title,String mobile);

    //我的订单
    Result findMyList(Integer page, Integer size, String userId, String status);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(SelfOrders entity);

    //修改
    Result updateBody(SelfOrders entity);

    //收入
    Result income(String data, int way);

    //订单数统计
    Result statistical(String data);

    //取消订单
    Result orderCancel(Long id);

    //后台管理去发货
    Result express(Long id, String expressName, String expressNumber);
    //后台管理发货虚拟商品
    Result expressVirtual(Long id);

    //确认收货
    Result orderConfirm(Long id);

    Result changePay(String id);

    void fenXiao(SelfOrders o);

    //检测完成支付
    Result checkPay(Long id);

    //根据订单查询订单
    SelfOrders findByOrderNum(String orderNum);

    //用户申请退款
    Result refund(Long id, String refund);

    //退款到余额
    Result refundMoney(Long id);

    //后台管理拒绝退款
    Result refusedRefund(Long ordersId, String refusedRefund);

    //后台管理待处理订单
    Result pendingOrder();

    //提醒发货
    Result remind(Long ordersId);

    //后台管理被提醒发货
    Result remindOrder();

    //处理虚拟商品
    SelfGoodsVirtual sendExpress(SelfOrders orders);

    //根据支付单号查询多个订单
    List<SelfOrders> findByPayNum(String payNum);

    //用户端订单统计
    Result count(Long userId);

    //查快递
    Result findExpress(String expressNumber);

}
