package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.SelfMerchantApplyJpaRepository;
import com.sqx.modules.shopping.dao.SelfMerchantApplyRepository;
import com.sqx.modules.shopping.entity.SelfMerchantApply;
import com.sqx.modules.shopping.service.SelfMerchantApplyService;
import com.sqx.modules.shopping.utils.DateUtil;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class SelfMerchantApplyServiceImpl implements SelfMerchantApplyService {
    @Autowired
    private SelfMerchantApplyJpaRepository jpaRepository;
    @Autowired
    private SelfMerchantApplyRepository repository;

    /**
     * 商家入驻申请列表
     * @param page 页数
     * @param size 条数
     * @param companyName 商家名称
     * @param legalPhone 注册手机号
     * @param storeType 店铺类型
     * @param status 状态
     * @param createTimeStart 申请时间开始区间
     * @param createTimeEnd 申请时间结束区间
     * @return
     */
    @Override
    public Result findAll(Integer page, Integer size, String companyName, String legalPhone, Integer storeType, Integer status, String createTimeStart, String createTimeEnd) {
        //按照时间排序
        Pageable pageable = PageRequest.of(page, size, Sort.by(new Sort.Order(Sort.Direction.DESC, "createTime")));
        //构造自定义查询条件
        Specification<SelfMerchantApply> queryCondition = new Specification<SelfMerchantApply>() {
            @Override
            public Predicate toPredicate(Root<SelfMerchantApply> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicateList = new ArrayList<>();
                if (StringUtils.isNotEmpty(companyName)) {
                    predicateList.add(criteriaBuilder.equal(root.get("companyName"), companyName));
                }
                if (StringUtils.isNotEmpty(legalPhone)) {
                    predicateList.add(criteriaBuilder.equal(root.get("legalPhone"), legalPhone));
                }
                if (storeType != 0) {
                    predicateList.add(criteriaBuilder.equal(root.get("storeType"), storeType));
                }
                if (status != 0) {
                    predicateList.add(criteriaBuilder.equal(root.get("status"), status));
                }
                if (StringUtils.isNotEmpty(createTimeStart)) {
                    predicateList.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createTime"), status));
                }
                if (StringUtils.isNotEmpty(createTimeEnd)) {
                    predicateList.add(criteriaBuilder.lessThan(root.get("createTime"), status));
                }
                return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
        return ResultUtil.success(jpaRepository.findAll(queryCondition, pageable));
    }

    /**
     * 商户入驻申请
     * @param entity
     * @return
     */
    @Override
    public Result saveBody(SelfMerchantApply entity) {
        entity.setCreateTime(DateUtil.createTime());
        entity.setStatus(1); //审核状态（1待处理 2通过 3拒绝）
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result updateBody(SelfMerchantApply entity) {
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result findOne(Long id) {
        return ResultUtil.success(jpaRepository.getOne(id));
    }

    @Override
    public Result delete(Long id) {
        jpaRepository.deleteById(id);
        return ResultUtil.success();
    }

    /**
     * 处理审核
     * @param id
     * @param status 审核状态（1待处理 2通过 3拒绝）
     * @param auditTime 生效日期
     * @param auditYears 生效年限
     * @param refundReason 拒绝原因
     * @return
     */
    @Override
    public Result deal(Long id, Integer status, String auditTime, String auditYears, String refundReason) {
        SelfMerchantApply one = jpaRepository.getOne(id);
        one.setStatus(status);
        one.setAuditTime(auditTime);
        one.setAuditYears(auditYears);
        one.setRefundReason(refundReason);
        SelfMerchantApply update = jpaRepository.save(one);
        if (update.getStatus() == 2){
            //通过审核

        }else if(update.getStatus() == 3){
            //拒绝

        }
        return ResultUtil.success(update);
    }
}
