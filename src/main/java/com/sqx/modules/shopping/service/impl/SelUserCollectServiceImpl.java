package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.GoodsJpaRepository;
import com.sqx.modules.shopping.dao.SelfUserCollectJpaRepository;
import com.sqx.modules.shopping.entity.SelfGoods;
import com.sqx.modules.shopping.entity.SelfUserCollect;
import com.sqx.modules.shopping.service.SelfUserCollectService;
import com.sqx.modules.shopping.utils.DateUtil;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class SelUserCollectServiceImpl implements SelfUserCollectService {
    @Autowired
    private SelfUserCollectJpaRepository jpaRepository;
    @Autowired
    private GoodsJpaRepository goodsJpaRepository;


    @Override
    public Result findAll(Integer page, Integer size, Long userId) {
        Pageable pageable = PageRequest.of(page, size);
        Specification<SelfUserCollect> queryCondition = new Specification<SelfUserCollect>() {
            @Override
            public Predicate toPredicate(Root<SelfUserCollect> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicateList = new ArrayList<>();
                if (userId != null) {
                    predicateList.add(criteriaBuilder.equal(root.get("userId"), userId));
                }
                return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
        Page<SelfUserCollect> all = jpaRepository.findAll(queryCondition, pageable);
        List<SelfGoods> goodsList = goodsJpaRepository.findAll();
        for (SelfUserCollect s : all.getContent()) {
            for (SelfGoods goods : goodsList) {
                if (s.getGoodsId().equals(goods.getId())){
                    s.setGoods(goods);
                    break;
                }
            }
        }
        return ResultUtil.success(all);
    }

    @Override
    public Result saveBody(SelfUserCollect entity) {
        entity.setCreateTime(DateUtil.createTime()); //创建时间
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result updateBody(SelfUserCollect entity) {
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result findOne(Long id) {
        return ResultUtil.success(jpaRepository.findById(id).orElse(null));
    }

    @Override
    public Result delete(Long id) {
        jpaRepository.deleteById(id);
        return ResultUtil.success();
    }

    @Override
    public Result check(Long goodsId, Long userId) {
        SelfUserCollect s = jpaRepository.findByGoodsIdAndUserId(goodsId, userId);
        if (s != null && s.getId() != null){
            Result result = new Result();
            result.setData(false);
            result.setMsg(s.getId().toString());
            result.setStatus(0);
            return result;
        }
        return ResultUtil.success(true);
    }
}
