package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.SelfGoods;
import com.sqx.modules.shopping.entity.SelfGoodsAttr;
import com.sqx.modules.shopping.utils.Result;

public interface GoodsService {
    //后台管理商品列表
    Result findAll(Integer page, Integer size, String title, String type, Integer status,Integer isSelect,Integer isRecommend);

    //虚拟商品列表
    Result goodsVirtualList(Integer page, Integer size, String title, String type, Integer status);

    //用户端商品列表
    Result findAllByUser(Integer page, Integer size, String type,String brandId, String title, String sort);

    //查询
    Result findOne(Long id);

    SelfGoods findById(Long goodsId);

    //删除
    Result delete(Long id);

    Result searchBrandList(String title);

    //添加
    Result saveBody(SelfGoods entity);

    //修改
    Result updateBody(SelfGoods entity);

    //精选商品
    Result selectGoods(Integer page, Integer size, String sort);

    //热卖榜单
    Result selling(Integer page, Integer size, String sort);

    //每日上新
    Result news(Integer page, Integer size, String sort);

    //每日推荐
    Result recommend(Integer page, Integer size, String sort);

    //首页商品
    Result homeGoods(Integer page, Integer size);

    //添加精选好物
    Result addSelectGoods(Long id);

    //删除精选好物
    Result deleteSelectGoods(String ids);

    //后台管理添加每日推荐
    Result addRecommend(Long id);

    //后台管理删除每日推荐
    Result deleteRecommend(String ids);

    //商品销量+1
    void salesAddOne(Long id);

    //生成属性
    Result isFormatAttr(SelfGoodsAttr attr, String coverImg, String originalPrice, String price, String memberPrice);

    //单规格生成属性
    Result onlyFormatAttr(String coverImg, String originalPrice, String price, String memberPrice);

    //回显属性
    Result formatAttr(Long goodsId);

    //回显属性值
    Result findAttrValue(Long goodsId);

    //商品上下架
    Result updateStatus(Long goodsId);


}
