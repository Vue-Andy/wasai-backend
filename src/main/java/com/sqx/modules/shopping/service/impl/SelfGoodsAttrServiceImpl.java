package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.SelfGoodsAttrJpaRepository;
import com.sqx.modules.shopping.dao.SelfGoodsAttrValueJpaRepository;
import com.sqx.modules.shopping.entity.SelfGoodsAttr;
import com.sqx.modules.shopping.entity.SelfGoodsAttrValue;
import com.sqx.modules.shopping.service.SelfGoodsAttrService;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SelfGoodsAttrServiceImpl implements SelfGoodsAttrService {
    @Autowired
    private SelfGoodsAttrJpaRepository attrJpaRepository;
    @Autowired
    private SelfGoodsAttrValueJpaRepository attrValueJpaRepository;

    @Override
    public Result saveBody(SelfGoodsAttr e) {
        SelfGoodsAttr save = attrJpaRepository.save(e);
        List<SelfGoodsAttrValue> list = e.getAttrValue();
        for (SelfGoodsAttrValue v : list) {
            v.setGoodsId(e.getGoodsId());
            v.setAttrId(save.getId());
            attrValueJpaRepository.save(v);
        }
        return ResultUtil.success(save);
    }

    @Override
    public Result updateBody(SelfGoodsAttr e) {
        //查询所有值删掉
        List<SelfGoodsAttrValue> allRuleValue = attrValueJpaRepository.findAllByAttrId(e.getId());
        attrValueJpaRepository.deleteAll(allRuleValue);
        //放入新值
        List<SelfGoodsAttrValue> list = e.getAttrValue();
        for (SelfGoodsAttrValue v : list) {
            v.setGoodsId(e.getGoodsId());
            v.setAttrId(e.getId());
            attrValueJpaRepository.save(v);
        }
        return ResultUtil.success(attrJpaRepository.save(e));
    }

    @Override
    public List<SelfGoodsAttr> findByGoodsId(Long goodsId) {
        List<SelfGoodsAttr> list = attrJpaRepository.findAllByGoodsId(goodsId);
        for (SelfGoodsAttr s : list) {
            List<SelfGoodsAttrValue> valueList = attrValueJpaRepository.findAllByAttrId(s.getId());
            s.setAttrValue(valueList);
        }
        return list;
    }

    @Override
    public void deleteAttrAndValue(Long goodsId) {
        //删掉所有attr
        List<SelfGoodsAttr> list = attrJpaRepository.findAllByGoodsId(goodsId);
        attrJpaRepository.deleteAll(list);
        //删掉所有value
        List<SelfGoodsAttrValue> allRuleValue = attrValueJpaRepository.findAllByGoodsId(goodsId);
        attrValueJpaRepository.deleteAll(allRuleValue);
    }

}
