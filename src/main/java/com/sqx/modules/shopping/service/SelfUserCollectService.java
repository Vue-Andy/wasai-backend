package com.sqx.modules.shopping.service;


import com.sqx.modules.shopping.entity.SelfUserCollect;
import com.sqx.modules.shopping.utils.Result;

public interface SelfUserCollectService {
    //列表
    Result findAll(Integer page, Integer size, Long userId);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(SelfUserCollect entity);

    //修改
    Result updateBody(SelfUserCollect entity);

    //判断是否收藏
    Result check(Long goodsId, Long userId);

}
