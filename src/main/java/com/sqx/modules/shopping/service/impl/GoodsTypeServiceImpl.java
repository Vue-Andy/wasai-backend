package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.GoodsTypeJpaRepository;
import com.sqx.modules.shopping.dao.GoodsTypeRepository;
import com.sqx.modules.shopping.entity.GoodsType;
import com.sqx.modules.shopping.entity.GoodsTypeResult;
import com.sqx.modules.shopping.service.GoodsTypeService;
import com.sqx.modules.shopping.utils.DateUtil;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Autowired
    private GoodsTypeJpaRepository jpaRepository;
    @Autowired
    private GoodsTypeRepository repository;

    @Override
    public Result findAll() {
        List<GoodsType> type1 = repository.findAllStatus0();
        List<GoodsType> type2 = repository.findAllStatus();
        List<GoodsType> resultList = new ArrayList<>();
        for (GoodsType g : type1) {
            GoodsType result = new GoodsType();
            result.setId(g.getId());
            result.setParentId(g.getParentId());
            result.setName(g.getName());
            result.setImg(g.getImg());
            result.setSort(g.getSort());
            result.setCreateAt(g.getCreateAt());
            List<GoodsType> children = new ArrayList<>();
            for (GoodsType c : type2) {
                GoodsType r1 = new GoodsType();
                if (g.getId().equals(c.getParentId())){
                    r1.setId(c.getId());
                    r1.setParentId(c.getParentId());
                    r1.setName(c.getName());
                    r1.setImg(c.getImg());
                    r1.setSort(c.getSort());
                    r1.setCreateAt(c.getCreateAt());
                    children.add(r1);
                }
                result.setChildren(children);
            }
            resultList.add(result);
        }
        return ResultUtil.success(resultList);
    }

    @Override
    public Result findAllResult() {
        List<GoodsType> type1 = repository.findAllStatus0();
        List<GoodsType> type2 = repository.findAllStatus();
        List<GoodsTypeResult> resultList = new ArrayList<>();
        for (GoodsType g : type1) {
            GoodsTypeResult result = new GoodsTypeResult();
            result.setValue(g.getId());
            result.setLabel(g.getName());
            List<GoodsTypeResult> children = new ArrayList<>();
            for (GoodsType c : type2) {
                GoodsTypeResult r1 = new GoodsTypeResult();
                if (g.getId().equals(c.getParentId())){
                    r1.setValue(c.getId());
                    r1.setLabel(c.getName());
                    children.add(r1);
                }
                result.setChildren(children);
            }
            resultList.add(result);
        }
        return ResultUtil.success(resultList);
    }

    @Override
    public Result findAllResult1() {
        List<GoodsType> type1 = repository.findAllStatus0();
        List<GoodsTypeResult> resultList = new ArrayList<>();
        for (GoodsType g : type1) {
            GoodsTypeResult result = new GoodsTypeResult();
            if (g.getParentId().toString().equals("0")){
                result.setValue(g.getId());
                result.setLabel(g.getName());
            }
            resultList.add(result);
        }
        return ResultUtil.success(resultList);
    }

    @Override
    public Result saveBody(GoodsType entity) {
        entity.setCreateAt(DateUtil.createTime());
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result updateBody(GoodsType entity) {
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result findOne(Long id) {
        return ResultUtil.success(jpaRepository.findById(id).orElse(null));
    }

    @Override
    public Result delete(Long id) {
        jpaRepository.deleteById(id);
        return ResultUtil.success();
    }

}
