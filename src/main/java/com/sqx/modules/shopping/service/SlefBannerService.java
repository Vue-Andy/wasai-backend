package com.sqx.modules.shopping.service;

import com.sqx.modules.shopping.entity.SelfBanner;
import com.sqx.modules.shopping.utils.Result;

public interface SlefBannerService {
    //列表
    Result findAll();

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(SelfBanner entity);

    //修改
    Result updateBody(SelfBanner entity);

}
