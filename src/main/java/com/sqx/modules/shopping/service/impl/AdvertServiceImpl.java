package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.AdvertJpaRepository;
import com.sqx.modules.shopping.entity.Adverti;
import com.sqx.modules.shopping.service.AdvertService;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class AdvertServiceImpl implements AdvertService {
    @Autowired
    private AdvertJpaRepository jpaRepository;

    @Override
    public Result findAll() {
        return ResultUtil.success(jpaRepository.findAll());
    }

    @Override
    public Result saveBody(Adverti entity) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        entity.setCreateAt(sdf.format(now));
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result updateBody(Adverti entity) {
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result findOne(Long id) {
        return ResultUtil.success(jpaRepository.findById(id).orElse(null));
    }

    @Override
    public Result delete(Long id) {
        jpaRepository.deleteById(id);
        return ResultUtil.success();
    }

}
