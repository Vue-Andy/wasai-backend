package com.sqx.modules.shopping.service.impl;

import com.sqx.modules.shopping.dao.CouponsUserJpaRepository;
import com.sqx.modules.shopping.entity.CouponsUser;
import com.sqx.modules.shopping.service.CouponsUserService;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CouponsUserServiceImpl implements CouponsUserService {
    @Autowired
    private CouponsUserJpaRepository jpaRepository;

    @Override
    public Result findAll(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return ResultUtil.success(jpaRepository.findAll(pageable));
    }

    @Override
    public Result saveBody(CouponsUser entity) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        entity.setCreateAt(sdf.format(now));
        entity.setStatus(0);
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result updateBody(CouponsUser entity) {
        return ResultUtil.success(jpaRepository.save(entity));
    }

    @Override
    public Result findOne(Long id) {
        return ResultUtil.success(jpaRepository.findById(id).orElse(null));
    }

    @Override
    public Result delete(Long id) {
        jpaRepository.deleteById(id);
        return ResultUtil.success();
    }

}
