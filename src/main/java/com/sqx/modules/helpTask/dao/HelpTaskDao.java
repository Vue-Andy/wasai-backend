package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpTask;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpTaskDao extends BaseMapper<HelpTask> {

    int insertById(HelpTask helpTask);

    int updateHelpTaskState(@Param("state") Integer state,@Param("helpTaskId") Long helpTaskId);

    IPage<Map<String,Object>>  selectMyHelpTask(Page<Map<String,Object>> page, @Param("state") Integer state, @Param("userId") Long userId);

    IPage<Map<String,Object>>  selectParticipationHelpTask(Page<Map<String,Object>> page, @Param("state") Integer state, @Param("userId") Long userId);

    IPage<HelpTask>  selectHelpTaskList(Page<HelpTask> page, @Param("state") Integer state,@Param("sort") Integer sort);

    IPage<HelpTask>  selectHelpTaskListV1(Page<HelpTask> page, @Param("state") Integer state,@Param("sort") Integer sort,@Param("desc") Integer desc,@Param("classifyId") Integer classifyId,@Param("classifyDetailsId") Integer classifyDetailsId);

    IPage<HelpTask>  selectHelpTaskListByClassifyId(Page<HelpTask> page, @Param("classifyId") Long classifyId);

    IPage<HelpTask>  selectHelpTaskListByPhone(Page<HelpTask> page, @Param("state") Integer state,@Param("sort") String sort,@Param("phone") String phone);

    List<HelpTask> selectRecommendHelpTask();

    IPage<Map<String,Object>>  selectAuditHelpTask(Page<Map<String,Object>> page, @Param("state") Integer state, @Param("userId") Long userId);

    List<Long> selectAuditHelpTasks(@Param("userId") Long userId);

    Integer updateHelpTaskNum(@Param("id") Long id);

    Integer updateHelpTaskNumSub(@Param("id") Long id);

    Double sumPrice(@Param("time")String time,@Param("flag")Integer flag);

    Integer countHelpTaskByCreateTime(@Param("time")String time,@Param("flag")Integer flag);

    IPage<HelpTask> selectTopHelpTask(Page<HelpTask> page);

    Integer selectHelpTaskCountByTime(@Param("time") String time);

    Double selectHelpTaskSumPriceByTime(@Param("time") String time);

}
