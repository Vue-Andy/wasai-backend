package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpTaskDetails;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpTaskDetailsDao extends BaseMapper<HelpTaskDetails> {

    List<HelpTaskDetails> selectByHelpTaskId(Long helpTaskId);


}
