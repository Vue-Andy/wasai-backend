package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.CashClassify;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author fang
 * @date 2020/8/3
 */
@Mapper
public interface CashClassifyDao extends BaseMapper<CashClassify> {

    List<CashClassify> selectCashClassifyList();

}
