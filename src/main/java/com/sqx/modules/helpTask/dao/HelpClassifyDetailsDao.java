package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpClassifyDetails;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpClassifyDetailsDao extends BaseMapper<HelpClassifyDetails> {

    List<HelpClassifyDetails> selectListByClassifyId(Long classifyId);

    List<HelpClassifyDetails> selectListByClassifyName(String classifyName);

    List<HelpClassifyDetails> selectList();


}
