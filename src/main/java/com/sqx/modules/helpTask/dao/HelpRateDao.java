package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpRate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpRateDao extends BaseMapper<HelpRate> {

    List<HelpRate> findAll();

    HelpRate selectRateByTypeId(@Param("type") Integer type,@Param("classify") Integer classify);

    List<HelpRate> selectRateByRateList(@Param("classify") Integer classify,@Param("isShopTask") Integer isShopTask);

    List<HelpRate> selectRateList();


}
