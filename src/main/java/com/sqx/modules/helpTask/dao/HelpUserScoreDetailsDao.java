package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpUserScoreDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpUserScoreDetailsDao extends BaseMapper<HelpUserScoreDetails> {

    List<HelpUserScoreDetails> selectHelpUserScoreDetailsList(Page<HelpUserScoreDetails> page,@Param("userId") Long userId);


}
