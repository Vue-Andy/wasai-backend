package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpComplaint;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/8/3
 */
@Mapper
public interface HelpComplaintDao extends BaseMapper<HelpComplaint> {

    IPage<HelpComplaint> selectHelpComplaintList(Page<HelpComplaint> pages,@Param("content") String content,@Param("userId") Long userId);

    HelpComplaint selectHelpComplaintDetails(Long id);

}
