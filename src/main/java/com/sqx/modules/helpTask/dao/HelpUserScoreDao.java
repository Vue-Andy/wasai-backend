package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpUserScore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpUserScoreDao extends BaseMapper<HelpUserScore> {

    int updateUserScoreAdd(@Param("userId") Long userId,@Param("score")  Integer score);

    int updateUserScoreSub(@Param("userId") Long userId,@Param("score") Integer score);

    int updateUserScoreAdd2(@Param("userId") Long userId,@Param("score")  Integer score);

    int updateUserScoreSub2(@Param("userId") Long userId,@Param("score") Integer score);

    HelpUserScore  selectByUserId(Long userId);

}
