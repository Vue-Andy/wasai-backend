package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpSendOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpSendOrderDao extends BaseMapper<HelpSendOrder> {

    HelpSendOrder selectByHelpTaskIdAndUserId(@Param("helpTaskId")Long helpTaskId,@Param("userId")Long userId);

    int selectHelpTaskCountByUserIdAndTime(@Param("time")Date time,@Param("userId")Long userId);

    Integer update(@Param("id") Long id, @Param("state") Integer state);

    Integer selectResidueNum(@Param("helpTaskId") Long helpTaskId,@Param("state") Integer state);

    Integer selectEndNum(@Param("helpTaskId") Long helpTaskId);

    Integer selectCountByHelpTaskIdAndUserId(@Param("helpTaskId") Long helpTaskId,@Param("userId")  Long userId);

    Integer updateHelpSendOrder(@Param("id") Long id, @Param("state") Integer state, @Param("auditContent") String auditContent, @Param("category") Integer category);

    Integer updateHelpSendOrderByContent(@Param("id") Long id,@Param("state") Integer state,@Param("content") String content,@Param("money") Double money);

    Integer updateHelpSendOrderByContents(@Param("id") Long id,@Param("state") Integer state,@Param("content") String content,@Param("money") Double money,@Param("zhiMoney") Double zhiMoney,@Param("feiMoney") Double feiMoney,@Param("pingMoney") Double pingMoney,@Param("zhiUserId") Long zhiUserId,@Param("feiUserId") Long feiUserId);

    Integer selectHelpSendOrderCount(@Param("userId") Long userId,@Param("startTime")  Date startTime,@Param("endTime")  Date endTime);

    Integer sendOrder(@Param("createTime") Date createTime);

    Double sumMoneyBySend(@Param("time")String time,@Param("flag") Integer flag);

    Integer countBySend(@Param("time")String time,@Param("flag") Integer flag);

    IPage<HelpSendOrder> selectSendOrderByTaskId(Page<HelpSendOrder> page,@Param("helpTaskId") Long helpTaskId);

    IPage<Map<String,Object>> selectSendOrderList(Page<Map<String,Object>> page, @Param("phone") String phone, @Param("state") Integer state);

    @Override
    int insert(HelpSendOrder helpSendOrder);

    Integer selectHelpSendOrderCountByTime(@Param("time") String time);



}
