package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.Attention;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpTask;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2021/1/18
 */
@Mapper
public interface AttentionDao extends BaseMapper<Attention> {


    Attention selectAttentionByUserIdAndByUserId(@Param("userId")Long userId,@Param("byUserId")Long byUserId);

    IPage<Map<String,Object>> selectAttentionList(Page<Map<String,Object>> pages, @Param("userId") Long userId);

    IPage<HelpTask> selectHelpTaskListByAttention(Page<HelpTask> pages, @Param("userId") Long userId);

    IPage<HelpTask> selectHelpTaskListByUserId(Page<HelpTask> pages, @Param("userId") Long userId);


}
