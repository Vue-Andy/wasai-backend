package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpSendOrderDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpSendOrderDetailsDao extends BaseMapper<HelpSendOrderDetails> {

    List<HelpSendOrderDetails> selectByHelpSendOrderId(@Param("helpSendOrderId") Long helpSendOrderId);


}
