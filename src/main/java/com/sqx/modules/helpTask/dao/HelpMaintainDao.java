package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpMaintain;
import com.sqx.modules.helpTask.entity.HelpMaintainModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpMaintainDao extends BaseMapper<HelpMaintain> {


    @Override
    int insert(HelpMaintain helpMaintain);

    Integer updateState(@Param("helpMaintainId") Long helpMaintainId, @Param("state") Integer state, @Param("content") String content);

    IPage<HelpMaintainModel> selectHelpMaintainList(Page<HelpMaintainModel> page);

    IPage<Map<String, Object>> selectHelpMaintainListBySendOrder(Page<Map<String, Object>> page,@Param("sendOrderUserId") Long sendOrderUserId,@Param("state") Integer state);

    IPage<Map<String, Object>> selectHelpMaintainListByTakeOrder(Page<Map<String, Object>> page,@Param("takeOrderUserId") Long takeOrderUserId,@Param("state") Integer state);

    Integer selectCount(@Param("helpTaskId") Long helpTaskId,@Param("state") Integer state);

}
