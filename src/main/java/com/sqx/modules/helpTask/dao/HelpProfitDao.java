package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpProfit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/7
 */
@Mapper
public interface HelpProfitDao extends BaseMapper<HelpProfit> {

    IPage<HelpProfit> selectHelpProfitList(Page<HelpProfit> page, @Param("createTime") String createTime,@Param("endTime") String endTime);

    Double selectSumProfit(@Param("createTime") String createTime,@Param("endTime") String endTime);

    //统计当月收入
    Double statisticalMonth(@Param("createTime") Date createTime);
    //统计当日收入
    Double statisticalDay(@Param("createTime") Date createTime);

    Double sumMoneyByProfit(@Param("time") String time,@Param("flag")Integer flag);

    Double sumMemberMoneyByProfit(@Param("time") String time,@Param("flag")Integer flag);



    IPage<Map<String,Object>> incomeAnalysis(Page<Map<String,Object>> page, @Param("time") String time,@Param("flag") Integer flag);

}
