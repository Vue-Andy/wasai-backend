package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.UserMoney;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface UserMoneyDao extends BaseMapper<UserMoney> {

    int updateCannotMoneyAdd(@Param("userId") Long userId,@Param("money")  Double money);

    int updateCannotMoneySub(@Param("userId") Long userId,@Param("money") Double money);

    int updateMayMoneyMoneyAdd(@Param("userId") Long userId,@Param("money") Double money);

    int updateMayMoneyMoneySub(@Param("userId") Long userId,@Param("money") Double money);

    UserMoney selectByUserId(Long userId);


}
