package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpTaskRefresh;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/9/23
 */
@Mapper
public interface HelpTaskRefreshDao extends BaseMapper<HelpTaskRefresh> {

    /***
     * 查询当日用户刷新次数
     * @param userId 用户id
     * @param time 当日时间
     * @return 刷新总次数
     */
    int selectHelpTaskRefreshCount(@Param("userId") Long userId,@Param("time") String time);

    int selectHelpTaskRefreshByHelpTaskId(@Param("helpTaskId") Long helpTaskId,@Param("time") String time);

}
