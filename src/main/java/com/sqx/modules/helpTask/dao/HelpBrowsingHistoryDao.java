package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpBrowsingHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpBrowsingHistoryDao extends BaseMapper<HelpBrowsingHistory> {

    IPage<Map<String, Object>> selectHelpBrowsingHistoryList(Page<Map<String,Object>> page,@Param("userId") Long userId);

    Long selectHelpBrowsingHistoryByUserId(@Param("userId") Long userId,@Param("helpTaskId") Long helpTaskId);

    int updateCreateTimeByUserId(@Param("helpBrowsingId") Long helpBrowsingId,@Param("createTime") Date createTime);



}
