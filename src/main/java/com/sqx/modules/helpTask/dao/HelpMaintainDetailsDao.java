package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpMaintainDetails;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpMaintainDetailsDao extends BaseMapper<HelpMaintainDetails> {

    List<HelpMaintainDetails> selectByHelpMaintainId(Long helpMaintainId);

}
