package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.HelpTakeOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface HelpTakeOrderDao extends BaseMapper<HelpTakeOrder> {

    int updateHelpTakeOrder(@Param("state") Integer state, @Param("id") Long id, @Param("auditContent") String auditContent, @Param("category") Integer category);

    HelpTakeOrder selectHelpTakeOrderByHelpTaskId(Long helpTaskId);

    Integer selectCountByHelpTaskIdAndUserId(@Param("helpTaskId") Long helpTaskId,@Param("userId")  Long userId);

    Integer selectHelpTakeOrderCount(@Param("userId") Long userId,@Param("startTime") Date startTime,@Param("endTime") Date endTime);

    Integer takeOrder(@Param("createTime") Date createTime);

}
