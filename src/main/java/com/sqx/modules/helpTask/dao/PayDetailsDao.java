package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.PayDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface PayDetailsDao extends BaseMapper<PayDetails> {

    PayDetails selectById(@Param("id") Long id);

    PayDetails selectByOrderId(@Param("orderId") String orderId);

    int updateState(@Param("id") Long id, @Param("state") Integer state,@Param("time") Date time);

    IPage<Map<String,Object>> selectPayDetails(Page<Map<String,Object>> page,@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("userId") Long userId,@Param("state") Integer state);

    Double selectSumPay(@Param("createTime") String createTime,@Param("endTime") String endTime,@Param("userId") Long userId);

    Double selectSumMember(@Param("time") String time,@Param("flag") Integer flag);

    IPage<Map<String,Object>> payMemberAnalysis(Page<Map<String,Object>> page,@Param("time") String time,@Param("flag") Integer flag);

    Double selectSumPayByState(@Param("time") String time,@Param("flag") Integer flag,@Param("state") Integer state);

    Double selectSumPayByClassify(@Param("time") String time,@Param("flag") Integer flag,@Param("classify") Integer classify);

    IPage<Map<String,Object>> selectUserMemberList(Page<Map<String,Object>> page,@Param("phone") String phone);


}
