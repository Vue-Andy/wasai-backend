package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface UserMoneyDetailsDao extends BaseMapper<UserMoneyDetails> {

}
