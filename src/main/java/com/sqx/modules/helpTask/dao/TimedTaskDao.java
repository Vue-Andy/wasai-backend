package com.sqx.modules.helpTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.helpTask.entity.TimedTask;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/1
 */
@Mapper
public interface TimedTaskDao extends BaseMapper<TimedTask> {

    Integer deleteByObjectId(@Param("objectId") Long objectId, @Param("type")  Integer type);

    Integer deleteByIds(@Param("ids") String ids);

    List<TimedTask> selectEndTime();


}
