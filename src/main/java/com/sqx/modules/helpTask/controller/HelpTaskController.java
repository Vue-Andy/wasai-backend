package com.sqx.modules.helpTask.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.app.utils.InvitationCodeUtil;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.helpTask.utils.HelpTaskCodeUtil;
import com.sqx.modules.invite.service.InviteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author fang
 * @date 2020/5/14
 */
@RestController
@Api(value = "助力活动", tags = {"助力活动"})
@RequestMapping(value = "/helpTask")
public class HelpTaskController {

    /** 助力信息 */
    @Autowired
    private HelpTaskService helpTaskService;
    /** 助力详细信息 */
    @Autowired
    private  HelpTaskDetailsService helpTaskDetailsService;
    /** 派单信息 */
    @Autowired
    private  HelpTakeOrderService helpTakeOrderService;
    /** 接单信息 */
    @Autowired
    private  HelpSendOrderService helpSendOrderService;
    /** 接单详细步骤信息 */
    @Autowired
    private  HelpSendOrderDetailsService helpSendOrderDetailsService;
    /** 信誉分 */
    @Autowired
    private  HelpUserScoreService helpUserScoreService;
    /** 信誉分明细 */
    @Autowired
    private  HelpUserScoreDetailsService helpUserScoreDetailsService;
    /** 维权信息 */
    @Autowired
    private  HelpMaintainService helpMaintainService;
    /** 维权详细信息 */
    @Autowired
    private  HelpMaintainDetailsService helpMaintainDetailsService;
    /** 浏览记录 */
    @Autowired
    private  HelpBrowsingHistoryService helpBrowsingHistoryService;
    /** 任务刷新 */
    @Autowired
    private  HelpTaskRefreshService helpTaskRefreshService;
    @Autowired
    private  UserService userService;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private AttentionService attentionService;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @ApiOperation("关注或取消关注用户")
    @GetMapping("/insertAttention")
    @ResponseBody
    public Result insertAttention(Long userId,Long byUserId){
       return  attentionService.insertAttention(userId, byUserId);
    }

    @ApiOperation("我关注的用户列表")
    @GetMapping("/selectAttentionList")
    @ResponseBody
    public Result selectAttentionList(Integer page,Integer limit,Long userId){
        return  attentionService.selectAttentionList(page,limit, userId);
    }


    @ApiOperation("我关注的用户发布的任务")
    @GetMapping("/selectHelpTaskListByAttention")
    @ResponseBody
    public Result selectHelpTaskListByAttention(Integer page,Integer limit,Long userId){
        return  attentionService.selectHelpTaskListByAttention(page,limit, userId);
    }

    @ApiOperation("根据用户id查询正常任务")
    @GetMapping("/selectHelpTaskListByUserId")
    @ResponseBody
    public Result selectHelpTaskListByUserId(Integer page,Integer limit,Long userId,Long byUserId){
        return  attentionService.selectHelpTaskListByUserId(page,limit, userId,byUserId);
    }


    @ApiOperation("创建任务口令")
    @GetMapping("/getHelpCode")
    @ResponseBody
    public Result getHelpCode(Long helpTaskId){
        HelpTask helpTask = helpTaskService.selectById(helpTaskId);
        if(helpTask!=null){
            String s = HelpTaskCodeUtil.toSerialCode(helpTaskId);
            CommonInfo one = commonInfoService.findOne(118);
            //获取文案 替换文案中字符
            String value = one.getValue();
            String replace = value.replace("{code}", s);
            String replace1 = replace.replace("{title}", helpTask.getTitle());
            Double taskOriginalPrice = helpTask.getTaskOriginalPrice();
            String replace2 = replace1.replace("{money}", taskOriginalPrice+"");
            return Result.success(replace2);
        }
        return Result.error("任务不存在，请刷新后重试！");
    }


    @ApiOperation("解析任务口令")
    @GetMapping("/getHelpTaskId")
    @ResponseBody
    public Result getHelpTaskId(String content){
        CommonInfo ze = commonInfoService.findOne(119);
        String pattern = ze.getValue();
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(content);
        String group ="";
        if (m.find()) {
            group=m.group();
            System.out.println("match: " + group);
        }
        if(StringUtils.isEmpty(group)){
            return Result.error("口令解析失败！");
        }
        long l = HelpTaskCodeUtil.serialCodeToId(group);
        HelpTask helpTask = helpTaskService.selectById(l);
        if(helpTask==null){
            return Result.error("任务不存在！");
        }
        if(helpTask.getState()!=1){
            return Result.error("该任务已经结束了！");
        }
        return Result.success().put("data",String.valueOf(l));
    }




    @RequestMapping(value = "/saveMyHelpTask", method = RequestMethod.POST)
    @ApiOperation("新建派单")
    @ResponseBody
    public Result addHelpTask(@RequestBody HelpTask helpTask ) {
        Long userId=helpTask.getUserId();
        UserEntity userEntity = userService.selectById(userId);
        if(userEntity.getIsTask()==2){
            return Result.error("当前账号不可以发送任务！");
        }
        String helpTaskDetailss=helpTask.getHelpTaskDetailss();
        //计算该派单需要多少钱
        Double money= AmountCalUtils.mul(helpTask.getTaskNum(),helpTask.getTaskOriginalPrice());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, helpTask.getEndTimes());
        helpTask.setEndTime(sdf.format(cal.getTime()));
        return helpTaskService.saveHelpTask(helpTask, helpTaskDetailss, userId, money);
    }


    @RequestMapping(value = "/updateHelpTask/{helpTakeId}/{state}/{content}", method = RequestMethod.POST)
    @ApiOperation("审核派单")
    @ResponseBody
    public Result updateHelpTask(@PathVariable("helpTakeId") String helpTakeId,@PathVariable("state") Integer state,@PathVariable("content") String content) {
        helpTaskService.updateHelpTask(helpTakeId,state,content);
        return Result.success();
    }

    @RequestMapping(value = "/cancellation/{helpTaskId}", method = RequestMethod.POST)
    @ApiOperation("作废")
    @ResponseBody
    public Result cancellation(@PathVariable("helpTaskId") Long helpTakeId) {
        return helpTaskService.cancellation(helpTakeId);
    }

    @RequestMapping(value = "/cancellations", method = RequestMethod.GET)
    @ApiOperation("下架")
    @ResponseBody
    public Result cancellations(Long helpTakeId,String content) {
        return helpTaskService.cancellations(helpTakeId,content);
    }

    @RequestMapping(value = "/quicknessHelpTask/{helpTakeId}", method = RequestMethod.POST)
    @ApiOperation("急速审核")
    @ResponseBody
    public Result quicknessHelpTask(@PathVariable("helpTakeId") Long helpTakeId) {
        return helpTaskService.quicknessHelpTask(helpTakeId);
    }

    @RequestMapping(value = "/helpTaskAddNum/{helpTakeId}/{num}", method = RequestMethod.POST)
    @ApiOperation("任务加量")
    @ResponseBody
    public Result helpTaskAddNum(@PathVariable("helpTakeId") Long helpTakeId,@PathVariable("num") Integer num) {
        return helpTaskService.helpTaskAddNum(helpTakeId,num);
    }

    @RequestMapping(value = "/helpTaskAddPrice/{helpTakeId}/{price}", method = RequestMethod.POST)
    @ApiOperation("任务加价")
    @ResponseBody
    public Result helpTaskAddPrice(@PathVariable("helpTakeId") Long helpTakeId,@PathVariable("price") Double price) {
        return helpTaskService.helpTaskAddPrice(helpTakeId,price);
    }



    @RequestMapping(value = "/selectTakeOrderAuditHelpTask", method = RequestMethod.GET)
    @ApiOperation("查询派单待审核助力活动")
    @ResponseBody
    public Result selectAuditHelpTask(int page,int limit,String phone,Integer state)
    {
        PageUtils pageUtils=helpTaskService.selectHelpTaskListByPhone(page,limit,state,"id",phone);
        List<HelpTask> list= (List<HelpTask>) pageUtils.getList();
        for(HelpTask helpTask:list){
            List<HelpTaskDetails> helpTaskDetailsList = helpTaskDetailsService.selectByHelpTaskId(helpTask.getId());
            helpTask.setHelpTaskDetailsList(helpTaskDetailsList);
        }
        return Result.success().put("data",pageUtils);
    }

    @RequestMapping(value = "/selectTakeOrderDetails", method = RequestMethod.GET)
    @ApiOperation("管理平台派单任务详细信息")
    @ResponseBody
    public Result selectTakeOrderDetails(Long id)
    {
        HelpTask helpTask = helpTaskService.selectById(id);
        HelpTakeOrder helpTakeOrder = helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpTask.getId());
        UserEntity userEntity = userService.selectById(helpTakeOrder.getUserId());
        if(userEntity!=null){
            helpTask.setNickName(userEntity.getNickName());
        }
        return Result.success().put("data",helpTask);
    }

    @RequestMapping(value = "/selectSendOrderByTaskList", method = RequestMethod.GET)
    @ApiOperation("管理平台派单任务接单人列表")
    @ResponseBody
    public Result selectSendOrderByTaskList(Long id,int page,int limit )
    {
        PageUtils pageUtils = helpSendOrderService.selectSendOrderByTaskId(id, page, limit);
        return Result.success().put("data",pageUtils);
    }

    @RequestMapping(value = "/selectSendOrderList", method = RequestMethod.GET)
    @ApiOperation("管理平台查询接单列表")
    @ResponseBody
    public Result selectSendOrderList(int page,int limit,String phone,Integer state)
    {
        return Result.success().put("data",helpSendOrderService.selectSendOrderList(page,limit,phone,state));
    }

    @RequestMapping(value = "/selectSendOrderDetailsById", method = RequestMethod.GET)
    @ApiOperation("管理平台派单接单人详细信息")
    @ResponseBody
    public Result selectSendOrderDetailsById(Long id)
    {
        List<HelpSendOrderDetails> helpSendOrderDetails = helpSendOrderDetailsService.selectByHelpSendOrderId(id);
        return Result.success().put("data",helpSendOrderDetails);
    }

    @RequestMapping(value = "/selectRecommendHelpTask", method = RequestMethod.GET)
    @ApiOperation("查询推荐助力活动")
    @ResponseBody
    public Result selectRecommendHelpTask(int page,int limit,Long id,@RequestParam(required = false) Long userId)
    {
        PageUtils pageUtils = helpTaskService.selectHelpTaskListByClassifyId(page, limit, id);
        Map<String, Object> rate = helpTaskService.getRate(userId);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }


    @RequestMapping(value = "/selectHelpTask", method = RequestMethod.GET)
    @ApiOperation("查询正常列表助力活动")
    @ResponseBody
    public Result selectHelpTask(int page, int limit,Integer sort,@RequestParam(required = false) Long userId)
    {
        return helpTaskService.selectHelpTaskList(page, limit, 1, sort,userId);
    }

    @RequestMapping(value = "/selectHelpTaskV1", method = RequestMethod.GET)
    @ApiOperation("查询正常列表助力活动V1(修改版 增加上下排序)")
    @ResponseBody
    public Result selectHelpTaskV1(int page, int limit,Integer sort,@RequestParam(required = false) Long userId,@RequestParam(required = false) Integer desc,@RequestParam(required = false) Integer classifyId,@RequestParam(required = false) Integer classifyDetailsId)
    {
        return helpTaskService.selectHelpTaskListV1(page, limit, 1, sort,userId,desc,classifyId,classifyDetailsId);
    }

    @RequestMapping(value = "/selectMyHelpTask", method = RequestMethod.GET)
    @ApiOperation("查询我派发的助力活动")
    @ResponseBody
    public Result selectMyHelpTask( int page, int limit,Integer state,Long userId)
    {
        PageUtils pageList= helpTaskService.selectMyHelpTask(page,limit,state,userId);
        Map<String, Object> rate = helpTaskService.getRate(userId);
        rate.put("pageUtils",pageList);
        return Result.success().put("data",rate);
    }

    @RequestMapping(value = "/selectParticipationHelpTask", method = RequestMethod.GET)
    @ApiOperation("查询我接单的助力活动")
    @ResponseBody
    public Result selectParticipationHelpTask( int page, int limit,Integer state,Long userId)
    {
        return Result.success().put("data",helpTaskService.selectParticipationHelpTask(page,limit,state,userId));
    }

    @RequestMapping(value = "/selectAuditHelpTask", method = RequestMethod.GET)
    @ApiOperation("查询接单待审核的助力活动")
    @ResponseBody
    public Result selectAuditHelpTask( int page, int limit,Integer state,Long userId)
    {
        PageUtils pageUtils = helpTaskService.selectAuditHelpTask(page, limit, state, userId);
        Map<String, Object> rate = helpTaskService.getRate(userId);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }


    @RequestMapping(value = "/selectHelpTaskDetails", method = RequestMethod.GET)
    @ApiOperation("查看助力活动详细界面")
    @ResponseBody
    public Result selectHelpTaskDetails(Long id,@RequestParam(required = false) Long userId)
    {
        return helpTaskService.selectHelpTaskDetails(id,userId);
    }


    @RequestMapping(value = "/saveHelpTask", method = RequestMethod.POST)
    @ApiOperation("参与活动")
    @ResponseBody
    public Result saveHelpSendOrder(@RequestBody HelpSendOrder helpSendOrder) {
        UserEntity userEntity = userService.selectById(helpSendOrder.getUserId());
        if(userEntity==null){
            return Result.error(-100,"账号信息错误，请退出后重新登陆！");
        }
        //获取今日接单数量
        int count = helpSendOrderService.selectHelpTaskCountByUserIdAndTime(new Date(), userEntity.getUserId());
        CommonInfo one = commonInfoService.findOne(106 + userEntity.getMember());
        int i = Integer.parseInt(one.getValue());
        if(count>=i){
            return Result.error(-100,"今日接单数量已达上限，请提升会员等级或明日再来！");
        }
        HelpUserScore helpUserScore = helpUserScoreService.selectByUserId(helpSendOrder.getUserId());
        if(helpUserScore.getScore()<=0){
            return Result.error(-100,"当前信誉分太低无法接单，请派单提高信誉分后尝试！");
        }
        Integer exits=helpSendOrderService.selectCountByHelpTaskIdAndUserId(helpSendOrder.getHelpTaskId(),helpSendOrder.getUserId());
        if(exits!=null){
            return Result.error(-100,"您已报名，请不要重复点击！");
        }
        HelpTask helpTask=helpTaskService.selectById(helpSendOrder.getHelpTaskId());
        if(helpTask.getTaskNum().compareTo(helpTask.getEndNum())<=0){
            return Result.error(-100,"报名人数已满！");
        }
        try {
            if(System.currentTimeMillis()>sdf.parse(helpTask.getEndTime()).getTime()){
                return Result.error(-100,"活动已经过期！");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Integer num=helpTakeOrderService.selectCountByHelpTaskIdAndUserId(helpSendOrder.getHelpTaskId(),helpSendOrder.getUserId());
        if(num!=null && num>0){
            return Result.error(-100,"不能参加自己的活动！");
        }
        helpSendOrderService.savebody(helpSendOrder);
        return Result.success();
    }




    @RequestMapping(value = "/endHelpSendOrder", method = RequestMethod.POST)
    @ApiOperation("放弃活动")
    @ResponseBody
    public Result endHelpSendOrder(Long helpSendOrderId) {
        helpSendOrderService.endHelpSendOrder(helpSendOrderId);
        return Result.success();
    }


    @RequestMapping(value = "/saveHelpSendOrderDetails", method = RequestMethod.POST)
    @ApiOperation("提交活动")
    @ResponseBody
    public Result saveHelpSendOrderDetails(@RequestBody String jsonData) {
        JSONObject job = JSONObject.parseObject(jsonData);
        String helpSendOrderDetailss=job.get("helpSendOrderDetailss").toString();
        Long helpSendOrderId=Long.parseLong(job.get("helpSendOrderId").toString());
        Long helpTaskId=Long.parseLong(job.get("helpTaskId").toString());
        String content=job.get("content").toString();

        List<HelpSendOrderDetails> list= JSONArray.parseArray(helpSendOrderDetailss,HelpSendOrderDetails.class);
        /*if(list.size()==0){判断是否有提交内容
            return ResultUtil.error(-100,"请提交内容！");
        }*/
        HelpTask helpTask=helpTaskService.selectById(helpTaskId);
        HelpSendOrder helpSendOrder=helpSendOrderService.selectById(helpSendOrderId);
        //计算时间 从接单开始到任务结束时间
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(helpSendOrder.getCreateTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal.add(Calendar.MINUTE,helpTask.getRestrictTime());
        //计算时间是否超时
        if(System.currentTimeMillis()>cal.getTime().getTime()){
            return Result.error(-100,"活动已经过期！");
        }
        helpSendOrder.setContent(content);
        helpSendOrderDetailsService.saveAll(list,helpTask,helpSendOrder);
        return Result.success();
    }

    @RequestMapping(value = "/saveHelpSendOrderDetailss", method = RequestMethod.POST)
    @ApiOperation("再次提交")
    @ResponseBody
    public Result saveHelpSendOrderDetailss(@RequestBody String jsonData) {
        JSONObject job = JSONObject.parseObject(jsonData);
        String helpSendOrderDetailss=job.get("helpSendOrderDetailss").toString();
        Long helpSendOrderId=Long.parseLong(job.get("helpSendOrderId").toString());
        Long helpTaskId=Long.parseLong(job.get("helpTaskId").toString());
        String content=job.get("content").toString();

        List<HelpSendOrderDetails> list= JSONArray.parseArray(helpSendOrderDetailss,HelpSendOrderDetails.class);
        HelpTask helpTask=helpTaskService.selectById(helpTaskId);
        HelpSendOrder helpSendOrder=helpSendOrderService.selectById(helpSendOrderId);
        helpSendOrder.setContent(content);
        helpSendOrderDetailsService.saveAll(list,helpTask,helpSendOrder);
        return Result.success();
    }


    @RequestMapping(value = "/auditHelpTask", method = RequestMethod.POST)
    @ApiOperation("审核接单")
    @ResponseBody
    public Result auditHelpTask(@RequestBody HelpSendOrder helpSendOrder) {
        HelpSendOrder oldHelpSendOrder=helpSendOrderService.selectById(helpSendOrder.getId());
        if(oldHelpSendOrder.getState()!=1){
            return Result.error(-100,"派单已经审核过了！");
        }
        helpSendOrderService.updateHelpSendOrder(helpSendOrder,oldHelpSendOrder);
        return Result.success();
    }


    @RequestMapping(value = "/updateHelpSendOrders/{userId}/{status}/{category}", method = RequestMethod.POST)
    @ApiOperation("批量审核接单")
    @ResponseBody
    public Result updateHelpSendOrders(@PathVariable("userId") Long userId,@PathVariable("status") Integer status,@PathVariable("category") Integer category) {
        return helpSendOrderService.updateHelpSendOrders(userId, status,category);
    }


    @RequestMapping(value = "/selectUserScore", method = RequestMethod.GET)
    @ApiOperation("助力活动信誉分查询")
    @ResponseBody
    public Result selectUserScore(Long userId)
    {
        return Result.success().put("data",helpUserScoreService.selectByUserId(userId));
    }

    @RequestMapping(value = "/selectUserScoreDetails", method = RequestMethod.GET)
    @ApiOperation("助力活动信誉分明细查询")
    @ResponseBody
    public Result selectUserScoreDetails(int page,int limit,Long userId)
    {
        return Result.success().put("data",helpUserScoreDetailsService.selectHelpUserScoreDetailsList(page,limit,userId));
    }

    @RequestMapping(value = "/saveHelpMaintain", method = RequestMethod.POST)
    @ApiOperation("申请维权")
    @ResponseBody
    public Result saveHelpMaintain(Long helpTaskId,Long sendOrderUserId) {
        HelpMaintain helpMaintain=new HelpMaintain();
        helpMaintain.setHelpTaskId(helpTaskId);
        helpMaintain.setSendOrderUserId(sendOrderUserId);
        HelpSendOrder helpSendOrder=helpSendOrderService.selectByHelpTaskId(helpMaintain.getHelpTaskId(),helpMaintain.getSendOrderUserId());
        if(helpSendOrder.getState()!=3){
            return Result.error(-100,"当前状态不可以维权！");
        }
        HelpTask helpTask=helpTaskService.selectById(helpMaintain.getHelpTaskId());
        if(helpTask.getState()==4){
            return Result.error(-100,"活动已经结束！");
        }
        HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpMaintain.getHelpTaskId());
        if(helpTakeOrder==null){
            return Result.error(-100,"活动已经结束！");
        }
        helpMaintain.setHelpTakeOrderId(helpTakeOrder.getId());
        helpMaintain.setHelpSendOrderId(helpSendOrder.getId());
        helpMaintain.setTakeOrderUserId(helpTakeOrder.getUserId());
        helpMaintainService.insert(helpMaintain,helpSendOrder);
        return Result.success();
    }



    @RequestMapping(value = "/auditHelpMaintain", method = RequestMethod.POST)
    @ApiOperation("审核维权")
    @ResponseBody
    public Result auditHelpMaintain( String helpMaintainId,Integer state,String content) {
        helpMaintainService.auditHelpMaintain(helpMaintainId,state,content);
        return Result.success();
    }

    @RequestMapping(value = "/selectAuditHelpMaintainList", method = RequestMethod.GET)
    @ApiOperation("平台审核维权列表")
    @ResponseBody
    public Result selectAuditHelpMaintainList(int page,int limit)
    {
        IPage<HelpMaintainModel> map=helpMaintainService.selectHelpMaintainList(page,limit);
        List<HelpMaintainModel> list=map.getRecords();
        for(HelpMaintainModel helpMaintainModel:list){
            Long helpMaintainId=helpMaintainModel.getId();
            List<HelpMaintainDetails> helpMaintainDetailsList= helpMaintainDetailsService.selectByHelpMaintainId(helpMaintainId);
            for(HelpMaintainDetails helpMaintainDetails:helpMaintainDetailsList){
                if(helpMaintainDetails.getClassify()==1){
                    helpMaintainDetails.setHelpTaskDetailsList(helpTaskDetailsService.selectByHelpTaskId(helpMaintainModel.getHelpTaskId()));
                }else{
                    helpMaintainDetails.setHelpSendOrderDetailsList(helpSendOrderDetailsService.selectByHelpSendOrderId(helpMaintainModel.getHelpSendOrderId()));
                }
            }
            helpMaintainModel.setHelpMaintainDetailsList(helpMaintainDetailsList);
        }
        return Result.success().put("data",new PageUtils(map));
    }


    @RequestMapping(value = "/selectHelpMaintainListBySendOrder", method = RequestMethod.GET)
    @ApiOperation("接单维权列表")
    @ResponseBody
    public Result selectHelpMaintainListBySendOrder(int page,int limit,Long userId,Integer state)
    {
        return Result.success().put("data",helpMaintainService.selectHelpMaintainListBySendOrder(page,limit,userId,state));
    }


    @RequestMapping(value = "/selectHelpMaintainListByTakeOrder", method = RequestMethod.GET)
    @ApiOperation("派单维权列表")
    @ResponseBody
    public Result selectHelpMaintainListByTakeOrder(int page,int limit,Long userId,Integer state)
    {
        PageUtils pageUtils = helpMaintainService.selectHelpMaintainListByTakeOrder(page, limit, userId, state);
        Map<String,Object> map=new HashMap<>();
        map.put("pageUtils",pageUtils);
        return Result.success().put("data",map);
    }

    @RequestMapping(value = "/selectHelpMaintainDetails", method = RequestMethod.GET)
    @ApiOperation("维权详细信息")
    @ResponseBody
    public Result selectHelpMaintainDetails(Long helpMaintainId)
    {
        HashMap<String,Object> map=new HashMap<String,Object>();
        HelpMaintain helpMaintain=helpMaintainService.selectById(helpMaintainId);
        List<HelpMaintainDetails> helpMaintainDetailsList= helpMaintainDetailsService.selectByHelpMaintainId(helpMaintainId);
        map.put("helpMaintain",helpMaintain);
        map.put("helpMaintainDetailsList",helpMaintainDetailsList);
        return Result.success(map);
    }


    @RequestMapping(value = "/finishHelpTask", method = RequestMethod.POST)
    @ApiOperation("结算")
    @ResponseBody
    public Result finishHelpTask(Long helpTaskId) {
        HelpTask helpTask=helpTaskService.selectById(helpTaskId);
        if(helpTask.getState()!=2){
            return Result.error(-100,"活动没有结束不可以结算！");
        }
        Integer count=helpMaintainService.selectCount(helpTaskId,0);
        if(count!=null && count>0){
            return Result.error(-100,"活动有维权没有处理完，请处理完后再进行结算！");
        }
        helpTaskService.finishHelpTask(helpTask);
        return Result.success();
    }

    @RequestMapping(value = "/selectHelpBrowsingHistoryList", method = RequestMethod.GET)
    @ApiOperation("查询浏览记录")
    @ResponseBody
    public Result selectHelpBrowsingHistoryList(Long userId,int page,int limit)
    {
        PageUtils pageUtils = helpBrowsingHistoryService.selectHelpBrowsingHistoryList(page, limit, userId);
        Map<String, Object> map = helpTaskService.getRate(userId);
        map.put("pageUtils",pageUtils);
        return Result.success().put("data",map);
    }


    @RequestMapping(value = "/refreshTask", method = RequestMethod.GET)
    @ApiOperation("刷新任务")
    @ResponseBody
    public Result refreshTask(Long userId,Long helpTaskId)
    {
        return helpTaskRefreshService.refreshTask(userId, helpTaskId);
    }

    @RequestMapping(value = "/refreshCount", method = RequestMethod.GET)
    @ApiOperation("获取每日刷新任务的次数")
    @ResponseBody
    public Result refreshCount(Long userId)
    {
        return Result.success().put("data",helpTaskRefreshService.selectHelpTaskRefreshCount(userId,sdf.format(new Date())));
    }

    @RequestMapping(value = "/orderCountByDay", method = RequestMethod.GET)
    @ApiOperation("今日可接单数量")
    @ResponseBody
    public Result orderCountByDay(Long userId)
    {
        UserEntity userEntity = userService.selectById(userId);
        int count = helpSendOrderService.selectHelpTaskCountByUserIdAndTime(new Date(), userEntity.getUserId());
        CommonInfo one = commonInfoService.findOne(106 + userEntity.getMember());
        int i = Integer.parseInt(one.getValue());
        return Result.success().put("data",i-count);
    }

    @RequestMapping(value = "/refreshTaskMoney", method = RequestMethod.GET)
    @ApiOperation("付费刷新")
    @ResponseBody
    public Result refreshTaskMoney(Long userId,Long helpTaskId)
    {
        return helpTaskRefreshService.refreshTaskMoney(userId, helpTaskId);
    }

    @RequestMapping(value = "/topHelpTask", method = RequestMethod.GET)
    @ApiOperation("置顶任务")
    @ResponseBody
    public Result topHelpTask(Long userId,Long helpTaskId,Integer time)
    {
        return helpTaskRefreshService.topHelpTask(userId, helpTaskId,time);
    }

    @GetMapping("/selectTopHelpTask")
    @ApiOperation("查询置顶任务")
    @ResponseBody
    public Result selectTopHelpTask(Integer page,Integer limit){
        return helpTaskService.selectTopHelpTask(page, limit);
    }

    @GetMapping("/selectHelpTaskStatistics")
    @ApiOperation("任务首页统计")
    @ResponseBody
    public Result selectHelpTaskStatistics(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar=Calendar.getInstance();
        String today=sdf.format(calendar.getTime());
        calendar.add(Calendar.DATE,-1);
        String yesterday=sdf.format(calendar.getTime());
        //相比较昨日百分比 计算公式 相比较昨日百分比＝(今日-昨日)÷昨日×100%
        //今日任务数  昨日数量
        Integer todayHelpTaskCount = helpTaskService.selectHelpTaskCountByTime(today);
        Integer yesterdayHelpTaskCount = helpTaskService.selectHelpTaskCountByTime(today);
        int i = todayHelpTaskCount - yesterdayHelpTaskCount;
        int helpTaskPercent=0;
        if(i!=0 && yesterdayHelpTaskCount!=0){
            helpTaskPercent=(i/yesterdayHelpTaskCount)*100;
        }
        //今日用户数  昨日数量
        Integer todayUserCount = userService.selectUserCountByTime(today);
        Integer yesterdayUserCount = userService.selectUserCountByTime(yesterday);
        i = todayUserCount - yesterdayUserCount;
        int userPercent=0;
        if(i!=0 && yesterdayUserCount!=0){
            userPercent=(i/yesterdayUserCount)*100;
        }
        //今日金额数  昨日数量
        Double todayHelpTaskPrice = helpTaskService.selectHelpTaskSumPriceByTime(today);
        Double yesterdayHelpTaskPrice = helpTaskService.selectHelpTaskSumPriceByTime(yesterday);
        Double i1 = todayHelpTaskPrice - yesterdayHelpTaskPrice;
        Double HelpTaskPricePercent=0.00;
        if(i1!=0 && yesterdayHelpTaskPrice!=0){
            HelpTaskPricePercent=(i1/yesterdayHelpTaskPrice)*100;
        }
        //今日成交数  昨日数量
        Integer todayOrderCount = helpSendOrderService.selectHelpSendOrderCountByTime(today);
        Integer yesterdayOrderCount = helpSendOrderService.selectHelpSendOrderCountByTime(yesterday);
        i = todayOrderCount - yesterdayOrderCount;
        int orderCountPercent=0;
        if(i!=0 && yesterdayOrderCount!=0){
            orderCountPercent=(i/yesterdayOrderCount)*100;
        }
        //返回数据
        Map<String,Object> result=new HashMap<>();
        result.put("todayHelpTaskCount",helpTaskPercent);result.put("helpTaskPercent",helpTaskPercent);
        result.put("todayUserCount",todayUserCount);result.put("userPercent",userPercent);
        result.put("todayHelpTaskPrice",todayHelpTaskPrice);result.put("helpTaskPricePercent",HelpTaskPricePercent);
        result.put("todayOrderCount",todayOrderCount);result.put("orderCountPercent",orderCountPercent);
        return Result.success().put("data",result);
    }


}