package com.sqx.modules.helpTask.controller;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.helpTask.entity.HelpClassifyDetails;
import com.sqx.modules.helpTask.entity.HelpComplaint;
import com.sqx.modules.helpTask.service.HelpClassifyDetailsService;
import com.sqx.modules.helpTask.service.HelpClassifyService;
import com.sqx.modules.helpTask.service.HelpComplaintService;
import com.sqx.modules.helpTask.service.HelpTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/12/8
 */
@RestController
@Api(value = "任务投诉", tags = {"任务投诉"})
@RequestMapping(value = "/helpComplaint")
public class HelpComplaintController {

    @Autowired
    private HelpComplaintService helpComplaintService;


    @RequestMapping(value = "/insertHelpComplaint", method = RequestMethod.POST)
    @ApiOperation("添加投诉")
    @ResponseBody
    public Result insertHelpComplaint(@RequestBody HelpComplaint helpComplaint) {
        helpComplaintService.insertHelpComplaint(helpComplaint);
        return Result.success();
    }


    @RequestMapping(value = "/updateHelpComplaint", method = RequestMethod.POST)
    @ApiOperation("修改投诉")
    @ResponseBody
    public Result updateHelpComplaint(@RequestBody HelpComplaint helpComplaint) {
        helpComplaintService.updateHelpComplaint(helpComplaint);
        return Result.success();
    }

    @RequestMapping(value = "/solveHelpComplaint/{ids}/{state}", method = RequestMethod.POST)
    @ApiOperation("处理投诉")
    @ResponseBody
    public Result solveHelpComplaint(@ApiParam("投诉id") @PathVariable("ids") String ids,@ApiParam("状态 2下架  3 不下架")@PathVariable("state")int state) {
        helpComplaintService.solveHelpComplaint(ids,state);
        return Result.success();
    }


    @RequestMapping(value = "/deleteHelpComplaint/{ids}", method = RequestMethod.POST)
    @ApiOperation("删除投诉")
    @ResponseBody
    public Result deleteClassifyById(@PathVariable("ids") String ids) {
        helpComplaintService.deleteHelpComplaintByIds(ids);
        return Result.success();
    }


    @RequestMapping(value = "/selectHelpComplaintList", method = RequestMethod.GET)
    @ApiOperation("查看投诉记录列表")
    @ResponseBody
    public Result selectHelpComplaintList(int limit,int page,@ApiParam("用户id 可传可不传") @RequestParam(required = false) Long userId,@ApiParam("根据内容查找 可传可不传") @RequestParam(required = false) String content) {
        return Result.success().put("data",helpComplaintService.selectHelpComplaint(page, limit, content, userId));
    }

    @RequestMapping(value = "/selectHelpComplaintDetails", method = RequestMethod.GET)
    @ApiOperation("查看投诉详细信息")
    @ResponseBody
    public Result selectHelpComplaintDetails(Long id) {
        return Result.success().put("data",helpComplaintService.selectHelpComplaintDetails(id));
    }





}