package com.sqx.modules.helpTask.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeOrderinfoSyncRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.google.common.base.Charsets;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.AliPayConstants;
import com.sqx.modules.helpTask.config.AliPayNotifyParamConstants;
import com.sqx.modules.helpTask.config.Config;
import com.sqx.modules.helpTask.config.WXConfig;
import com.sqx.modules.helpTask.dao.PayDetailsDao;
import com.sqx.modules.helpTask.entity.AliPayParamModel;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.entity.PayDetails;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.AliPayOrderUtil;
import com.sqx.modules.helpTask.utils.HttpClientUtil;
import com.sqx.modules.helpTask.utils.MD5Utils;
import com.sqx.modules.helpTask.utils.StringUtils;
import com.sqx.modules.invite.service.InviteService;
import com.yungouos.pay.alipay.AliPay;
import com.yungouos.pay.entity.AliPayH5Biz;
import com.yungouos.pay.util.PaySignUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

/**
 * 支付宝支付处理--暂不做同步处理、回调方式使用异步
 */
@Slf4j
@RestController
@Api(value = "支付宝支付", tags = {"支付宝支付"})
@RequestMapping("/api/aliPay")
public class AliPayController {

    private final PayDetailsDao payDetailsDao;
    private final UserMoneyService userMoneyService;
    private final UserMoneyDetailsService userMoneyDetailsService;
    private final CommonInfoService commonInfoService;
    private final HelpRateService helpRateService;
    @Autowired
    private UserService userService;
    @Autowired
    private InviteService inviteService;
    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 创建线程池处理业务逻辑
     */
    private ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().build();
    private ExecutorService singleThreadPool = new ThreadPoolExecutor(30, 100,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    @Autowired
    public AliPayController(PayDetailsDao payDetailsDao, UserMoneyService userMoneyService, UserMoneyDetailsService userMoneyDetailsService, CommonInfoService commonInfoService, HelpRateService helpRateService) {
        this.payDetailsDao = payDetailsDao;
        this.userMoneyService = userMoneyService;
        this.userMoneyDetailsService = userMoneyDetailsService;
        this.commonInfoService = commonInfoService;
        this.helpRateService = helpRateService;
    }

    /**
     * 支付宝网页支付
     *
     * @param response
     * @throws IOException
     */
    @RequestMapping("/payment")
    public void aliPay(HttpServletResponse response) throws IOException {

        AlipayClient alipayClient = new DefaultAlipayClient(AliPayConstants.REQUEST_URL,
                commonInfoService.findOne(63).getValue(), commonInfoService.findOne(65).getValue(), AliPayConstants.FORMAT,
                AliPayConstants.CHARSET, commonInfoService.findOne(64).getValue(), AliPayConstants.SIGNTYPE);
        //封装请求参数
        AlipayTradeWapPayRequest aliPayRequest = new AlipayTradeWapPayRequest();
        AliPayParamModel aliPayParamModel = new AliPayParamModel();
        aliPayParamModel.setOut_trade_no("2018120514559696060");
        aliPayParamModel.setSubject("支付测试");
        aliPayParamModel.setTotal_amount("0.01");
        aliPayParamModel.setProduct_code(AliPayConstants.PRODUCT_CODE);
        // TODO 同步处理业务-涉及到商户自定义页面跳转
        aliPayRequest.setReturnUrl("http://ip:port/api/aliPay/return");
        // TODO 异步处理业务-修改订单状态、校验签名是否正确
        aliPayRequest.setNotifyUrl("http://ip:port/api/aliPay/notify");
        aliPayRequest.setBizContent(JSON.toJSONString(aliPayParamModel));
        try {
            // 调用SDK生成表单
            String form = alipayClient.pageExecute(aliPayRequest).getBody();
            response.setContentType("text/html;charset=" + AliPayConstants.CHARSET);
            response.getWriter().write(form);
            response.getWriter().flush();
            response.getWriter().close();
        } catch (AlipayApiException e) {
            log.info("支付宝支付异常，异常信息为:" + e.getErrMsg());
            e.printStackTrace();
        }
    }

//    /**
//     * 支付宝提现
//     */
//    @RequestMapping("/withdraw")
//    public void withdraw() {
//        val aliPayWithdrawModel = AliPayWithdrawModel.builder()
//                .out_biz_no("20181206145569697")
//                .amount(new BigDecimal(0.2))
//                .payee_account("支付宝账号")
//                .payee_real_name("真实姓名")
//                .payee_type(AliPayConstants.PAY_TYPE)
//                .remark("测试支付宝提现")
//                .build();
//        String json = JSON.toJSONString(aliPayWithdrawModel);
//        //实例化连接对象
//        AlipayFundTransToaccountTransferRequest withdrawRequest = new AlipayFundTransToaccountTransferRequest();
//        withdrawRequest.setBizContent(json);
//        try {
//            AlipayFundTransToaccountTransferResponse response = alipayClient.execute(withdrawRequest);
//            if (AliPayConstants.SUCCESS_CODE.equalsIgnoreCase(response.getCode())) {
//                // TODO 处理业务逻辑
//            }
//        } catch (AlipayApiException e) {
//            log.info("零钱提现异常原因:" + e.getMessage());
//            e.printStackTrace();
//        }
//    }

    /**
     * 支付宝异步通知地址
     *
     * @param request
     * @return
     */
    @RequestMapping("/notify")
    @Transactional(rollbackFor = Exception.class)
    public void returnUrl(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> params = AliPayOrderUtil.convertRequestParamsToMap(request);
        String result = "";
        //调用SDK验证签名
        boolean signVerified = false;
        try {
            signVerified = AlipaySignature.rsaCheckV1(params, commonInfoService.findOne(64).getValue(),
                    AliPayConstants.CHARSET, AliPayConstants.SIGNTYPE);
        } catch (AlipayApiException e) {
            log.info("支付宝回调验签异常：" + e.getMessage());
            e.printStackTrace();
        }
        if (signVerified) {
            this.check(params);
            singleThreadPool.execute(() -> {
                AliPayNotifyParamConstants param = AliPayOrderUtil.buildAliPayNotifyParam(params);
            });
            result = "success";
        } else {
            result = "failure";
        }
        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(result.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            log.info("支付宝返回异常，异常信息为：" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 校验支付宝支付返回的订单信息是否正确
     *
     * @param params
     */
    private void check(Map<String, String> params) {
        for (String key : params.keySet()){
            log.info("key="+key+";  value="+params.get(key));
        }
            // TODO 判断支付订单号是否是同一个
        String outTradeNo = params.get("out_trade_no");
        // 订单支付金额是否正确
        BigDecimal totalAmount = new BigDecimal(params.get("total_amount"));
//        Assert.isTrue(!totalAmount.equals(new BigDecimal(0.2)), "支付金额错误");
        // 判断支付的商户信息是否一致
        Assert.isTrue(params.get("app_id").equals(commonInfoService.findOne(63).getValue()), "支付的商户信息不正确");
    }


    @RequestMapping(value = "/notifyApp",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public void notifyApp(HttpServletRequest request, HttpServletResponse response){
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        try {
            log.info("回调成功！！！");
            boolean flag = AlipaySignature.rsaCheckV1(params, commonInfoService.findOne(64).getValue(), AliPayConstants.CHARSET,"RSA2");
            log.info(flag+"回调验证信息");
            if(flag){
                String tradeStatus = params.get("trade_status");
                if("TRADE_SUCCESS".equals(tradeStatus) || "TRADE_FINISHED".equals(tradeStatus)){
                    String outTradeNo = params.get("out_trade_no");
                    PayDetails payDetails=payDetailsDao.selectByOrderId(outTradeNo);
                    if(payDetails.getState()==0){
                        payDetailsDao.updateState(payDetails.getId(),1,new Date());
                        if(payDetails.getType()==1){
                            userMoneyService.updateCannotMoney(1,payDetails.getUserId(),payDetails.getMoney());
                            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                            userMoneyDetails.setUserId(payDetails.getUserId());
                            userMoneyDetails.setTitle("充值："+payDetails.getMoney());
                            userMoneyDetails.setContent("支付宝充值金额："+payDetails.getMoney());
                            userMoneyDetails.setType(0);
                            userMoneyDetails.setMoney(payDetails.getMoney());
                            userMoneyDetails.setCreateTime(sdf.format(new Date()));
                            userMoneyDetailsService.insert(userMoneyDetails);
                        }else{
                            UserEntity user=new UserEntity();
                            user.setUserId(payDetails.getUserId());
                            user.setMember(Integer.parseInt(payDetails.getRemark()));
                            userService.updateById(user);
                            UserEntity userEntity = userService.selectById(user.getUserId());
                            //开通会员同样给赏金
                            inviteService.updateInvite(userEntity,sdf.format(new Date()));
                        }
                    }
                }
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            log.info("回调验证失败！！！");
        }
    }

    @ApiOperation("支付宝回调")
    @RequestMapping("/notifyAppYunOS")
    @Transactional(rollbackFor = Exception.class)
    public String notifyAppYunOS(HttpServletRequest request, HttpServletResponse response){
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        String outTradeNo = params.get("outTradeNo");
        String code = params.get("code");
        String key = commonInfoService.findOne(169).getValue();
        try {
            boolean flag = PaySignUtil.checkNotifySign(request, key);
            if(flag){
                if("1".equals(code)){
                    PayDetails payDetails=payDetailsDao.selectByOrderId(outTradeNo);
                    if(payDetails.getState()==0) {
                        payDetailsDao.updateState(payDetails.getId(),1,new Date());
                        if(payDetails.getType()==1){
                            userMoneyService.updateCannotMoney(1,payDetails.getUserId(),payDetails.getMoney());
                            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                            userMoneyDetails.setUserId(payDetails.getUserId());
                            userMoneyDetails.setTitle("充值："+payDetails.getMoney());
                            userMoneyDetails.setContent("支付宝充值金额："+payDetails.getMoney());
                            userMoneyDetails.setType(0);
                            userMoneyDetails.setMoney(payDetails.getMoney());
                            userMoneyDetails.setCreateTime(sdf.format(new Date()));
                            userMoneyDetailsService.insert(userMoneyDetails);
                        }else{
                            UserEntity user=new UserEntity();
                            user.setUserId(payDetails.getUserId());
                            user.setMember(Integer.parseInt(payDetails.getRemark()));
                            userService.updateById(user);
                            UserEntity userEntity = userService.selectById(user.getUserId());
                            //开通会员同样给赏金
                            inviteService.updateInvite(userEntity,sdf.format(new Date()));
                        }
                    }
                }
                return "SUCCESS";
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("云购os支付报错！"+e.getMessage());
        }
        return null;
    }




    @RequestMapping(value = "/notifys",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public Result notifys(String orderNo,String secretKey,String goodsName,String price,String realPrice,String key){
            log.info("回调成功！！！");
        String newKey = getKey(orderNo, secretKey, goodsName, price, realPrice, null);
        if(newKey.equals(key)){
            CommonInfo one1 = commonInfoService.findOne(99);
            if(one1.getValue().equals(secretKey)){
                PayDetails payDetails=payDetailsDao.selectByOrderId(orderNo);
                if(payDetails.getState()==0){
                    payDetailsDao.updateState(payDetails.getId(),1,new Date());
                    if(payDetails.getType()==1){
                        userMoneyService.updateCannotMoney(1,payDetails.getUserId(),payDetails.getMoney());
                        UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                        userMoneyDetails.setUserId(payDetails.getUserId());
                        userMoneyDetails.setTitle("充值："+payDetails.getMoney());
                        userMoneyDetails.setContent("支付宝充值金额："+payDetails.getMoney());
                        userMoneyDetails.setType(0);
                        userMoneyDetails.setMoney(payDetails.getMoney());
                        userMoneyDetails.setCreateTime(sdf.format(new Date()));
                        userMoneyDetailsService.insert(userMoneyDetails);
                    }else{
                        UserEntity user=new UserEntity();
                        user.setUserId(payDetails.getUserId());
                        user.setMember(Integer.parseInt(payDetails.getRemark()));
                        userService.updateById(user);
                        UserEntity userEntity = userService.selectById(user.getUserId());
                        //开通会员同样给赏金
                        inviteService.updateInvite(userEntity,sdf.format(new Date()));
                    }
                }
            }
        }else{
            //秘钥不一致
        }
        return Result.success("回调成功");
    }



    @ApiOperation("h5支付")
    @RequestMapping(value = "/payH5",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public Result payH5( Long userId,String money) {
        //通知页面地址
        CommonInfo one = commonInfoService.findOne(19);
        String returnUrl=one.getValue()+"/#/pages/task/recharge";
        CommonInfo one3 = commonInfoService.findOne(12);
        String name=one3==null?"助力任务":one3.getValue();
        String outTradeNo=getGeneralOrder();
        CommonInfo one2 = commonInfoService.findOne(97);
        if(one2!=null){
            if("2".equals(one2.getValue())){
                CommonInfo one1 = commonInfoService.findOne(99);
                if(one1==null || StringUtils.isBlank(one1.getValue())){
                    return Result.error("请设置转账秘钥");
                }
                Map<String,String> map=new HashMap<>();
                String notifyUrl=one.getValue()+"/sqx_fast/api/aliPay/notifys";
                map.put("notifyUrl",notifyUrl);
                map.put("goodsName",name);
                map.put("price",money);
                map.put("orderNo",outTradeNo);
                map.put("secretKey",one1.getValue());
                map.put("returnUrl",returnUrl);
                map.put("key",getKey(name,outTradeNo,notifyUrl,one1.getValue(),money,returnUrl));
                String body = HttpClientUtil.doPost("https://money.xiansqx.com/sqx_fast/api/payH5", map);
                JSONObject jsonObject = JSON.parseObject(body);
                Integer code = jsonObject.getInteger("code");
                if(code==0){
                    String data = jsonObject.getString("data");
                    PayDetails payDetails=new PayDetails();
                    payDetails.setState(0);
                    payDetails.setCreateTime(sdf.format(new Date()));
                    payDetails.setOrderId(outTradeNo);
                    payDetails.setUserId(userId);
                    payDetails.setMoney(Double.parseDouble(money));
                    payDetails.setClassify(2);
                    payDetails.setType(1);
                    payDetailsDao.insert(payDetails);
                    return Result.success().put("data",data);
                }else{
                    return Result.error("获取订单信息错误！"+jsonObject.getString("msg"));
                }
            }
        }
        String result="";
        try {
            String pay = commonInfoService.findOne(167).getValue();
            if("1".equals(pay)){
                String url=one.getValue()+"/sqx_fast/api/aliPay/notifyApp";
                log.info("回调地址:"+url);
                AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", commonInfoService.findOne(63).getValue(), commonInfoService.findOne(65).getValue(), "json", AliPayConstants.CHARSET, commonInfoService.findOne(64).getValue(), "RSA2");
                AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();
                JSONObject order = new JSONObject();
                order.put("out_trade_no",outTradeNo); //订单号
                order.put("subject", name); //商品标题
                order.put("product_code", "QUICK_WAP_WAY");
                order.put("body", name);//商品名称
                order.put("total_amount", money+".00"); //金额
                alipayRequest.setBizContent(order.toString());
                //在公共参数中设置回跳和通知地址
                alipayRequest.setNotifyUrl(url);
                //通知页面地址
                alipayRequest.setReturnUrl(returnUrl);
                result = alipayClient.pageExecute(alipayRequest).getBody();
            }else{
                String url=one.getValue()+"/sqx_fast/api/aliPay/notifyAppYunOS";
                log.info("回调地址:"+url);
                String mchId = commonInfoService.findOne(168).getValue();
                String key = commonInfoService.findOne(169).getValue();
                AliPayH5Biz aliPayH5Biz = AliPay.h5Pay(outTradeNo, money, mchId, name, null, url, returnUrl, null, null, null,null,key);
                result = aliPayH5Biz.getForm();
            }
            //创建订单信息
            PayDetails payDetails=new PayDetails();
            payDetails.setState(0);
            payDetails.setCreateTime(sdf.format(new Date()));
            payDetails.setOrderId(outTradeNo);
            payDetails.setUserId(userId);
            payDetails.setMoney(Double.parseDouble(money));
            payDetails.setClassify(2);
            payDetails.setType(1);
            payDetailsDao.insert(payDetails);
            return Result.success().put("data",result);
        } catch (AlipayApiException e) {
            log.error("CreatPayOrderForH5", e);
        }
        return Result.error("获取订单信息错误！");
    }

    @ApiOperation("充值")
    @RequestMapping(value = "/payApp",method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public Result payApp(Long userId, String money){
        CommonInfo one = commonInfoService.findOne(19);
        CommonInfo one3 = commonInfoService.findOne(12);
        String name=one3==null?"助力任务":one3.getValue();
        String outTradeNo=getGeneralOrder();
        CommonInfo one2 = commonInfoService.findOne(97);
        if(one2!=null){
            if("2".equals(one2.getValue())){
                CommonInfo one1 = commonInfoService.findOne(99);
                if(one1==null || StringUtils.isBlank(one1.getValue())){
                    return Result.error("请设置转账秘钥");
                }
                Map<String,String> map=new HashMap<>();
                String notifyUrl=one.getValue()+"/sqx_fast/api/aliPay/notifys";
                map.put("notifyUrl",notifyUrl);
                map.put("goodsName",name);
                map.put("price",money);
                map.put("orderNo",outTradeNo);
                map.put("secretKey",one1.getValue());
                map.put("key",getKey(name,outTradeNo,notifyUrl,one1.getValue(),money,null));
                String body = HttpClientUtil.doPost("https://money.xiansqx.com/sqx_fast/api/payApp", map);
                JSONObject jsonObject = JSON.parseObject(body);
                Integer code = jsonObject.getInteger("code");
                if(code==0){
                    String data = jsonObject.getString("data");
                    PayDetails payDetails=new PayDetails();
                    payDetails.setState(0);
                    payDetails.setCreateTime(sdf.format(new Date()));
                    payDetails.setOrderId(outTradeNo);
                    payDetails.setUserId(userId);
                    payDetails.setMoney(Double.parseDouble(money));
                    payDetails.setClassify(2);
                    payDetails.setType(1);
                    payDetailsDao.insert(payDetails);
                    return Result.success().put("data",data);
                }else{
                    return Result.error("获取订单信息错误！"+jsonObject.getString("msg"));
                }
            }
        }
        String pay = commonInfoService.findOne(167).getValue();
        String result="";
        try {
            if("1".equals(pay)){
                String url=one.getValue()+"/sqx_fast/api/aliPay/notifyApp";
                //实例化客户端
                AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", commonInfoService.findOne(63).getValue(), commonInfoService.findOne(65).getValue(), "json", AliPayConstants.CHARSET, commonInfoService.findOne(64).getValue(), "RSA2");
                //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
                AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
                //SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
                AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
                model.setBody(name);
                model.setSubject(name);
                model.setOutTradeNo(outTradeNo);
                model.setTimeoutExpress("30m");
                model.setTotalAmount(money+".00");
                model.setProductCode("QUICK_MSECURITY_PAY");
                request.setBizModel(model);
                request.setNotifyUrl(url);
                AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
                if(response.isSuccess()){
                    result=response.getBody();
                } else {
                    log.info("调用失败");
                    result=null;
                }
            }else{
                String url=one.getValue()+"/sqx_fast/api/aliPay/notifyAppYunOS";
                log.info("回调地址:"+url);
                String mchId = commonInfoService.findOne(168).getValue();
                String key = commonInfoService.findOne(169).getValue();
                result = AliPay.appPay(outTradeNo, money, mchId, name ,null, url, null, null, null, null,key);
            }
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //创建订单信息
            PayDetails payDetails=new PayDetails();
            payDetails.setState(0);
            payDetails.setCreateTime(sdf.format(new Date()));
            payDetails.setOrderId(outTradeNo);
            payDetails.setUserId(userId);
            payDetails.setMoney(Double.parseDouble(money));
            payDetails.setClassify(2);
            payDetails.setType(1);
            payDetailsDao.insert(payDetails);
            return Result.success().put("data",result);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return Result.error(-100,"获取订单失败！");
    }



    public String getGeneralOrder(){
        Date date=new Date();
        String newString = String.format("%0"+4+"d", (int)((Math.random()*9+1)*1000));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String format = sdf.format(date);
        return format+newString;
    }

    private static String getKey(String goodsName,String orderNo,String notifyUrl,String secretKey,String price,String returnUrl) {
        StringBuffer sb=new StringBuffer();
        String key = "";
        if (org.apache.commons.lang3.StringUtils.isNotBlank(goodsName)) {
            sb.append(goodsName);
        }
        if (org.apache.commons.lang3.StringUtils.isNotBlank(orderNo)) {
            sb.append(orderNo);
        }
        if (org.apache.commons.lang3.StringUtils.isNotBlank(notifyUrl)) {
            sb.append(notifyUrl);
        }
        if (org.apache.commons.lang3.StringUtils.isNotBlank(secretKey)) {
            sb.append(secretKey);
        }
        if (org.apache.commons.lang3.StringUtils.isNotBlank(price)) {
            sb.append(price);
        }
        if (org.apache.commons.lang3.StringUtils.isNotBlank(returnUrl)) {
            sb.append(returnUrl);
        }
        return MD5Utils.encryption(key);
    }



}
