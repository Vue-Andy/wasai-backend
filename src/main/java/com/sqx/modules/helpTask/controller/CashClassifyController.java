package com.sqx.modules.helpTask.controller;


import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayFundTransToaccountTransferRequest;
import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.AliPayConstants;
import com.sqx.modules.helpTask.config.Config;
import com.sqx.modules.helpTask.entity.AliPayWithdrawModel;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.CashOut;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fang
 * @date 2020/8/3
 */
@Slf4j
@RestController
@Api(value = "app提现金额分类", tags = {"app提现金额分类"})
@RequestMapping(value = "/cashClassify")
public class CashClassifyController {

    @Autowired
    private CashClassifyService cashClassifyService;

    @RequestMapping(value = "/saveBody", method = RequestMethod.POST)
    @ApiOperation("添加")
    @ResponseBody
    public Result saveBody(@RequestBody CashClassify cashClassify){
        cashClassifyService.saveBody(cashClassify);
        return Result.success();
    }

    @RequestMapping(value = "/updateClassify", method = RequestMethod.POST)
    @ApiOperation("修改")
    @ResponseBody
    public Result updateClassify(@RequestBody CashClassify cashClassify){
        cashClassifyService.updateClassify(cashClassify);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyById", method = RequestMethod.POST)
    @ApiOperation("删除")
    @ResponseBody
    public Result deleteClassifyById(Long id){
        cashClassifyService.deleteClassifyById(id);
        return Result.success();
    }


    @RequestMapping(value = "/deleteClassifyByIds" , method = RequestMethod.POST)
    @ApiOperation("删除多个")
    @ResponseBody
    public Result deleteClassifyByIds(String  ids){
        cashClassifyService.deleteClassifyByIds(ids);
        return Result.success();
    }

    @RequestMapping(value = "/selectCashClassifyList" , method = RequestMethod.GET)
    @ApiOperation("查询所有")
    @ResponseBody
    public Result selectCashClassifyList(){
        return Result.success().put("data", cashClassifyService.selectCashClassifyList());
    }



}