package com.sqx.modules.helpTask.controller;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.helpTask.entity.HelpClassifyDetails;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.service.HelpClassifyDetailsService;
import com.sqx.modules.helpTask.service.HelpClassifyService;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.HelpTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/6/11
 */
@RestController
@Api(value = "助力分类", tags = {"助力分类"})
@RequestMapping(value = "/helpClassify")
public class HelpClassifyController {

    @Autowired
    private HelpClassifyService helpClassifyService;
    @Autowired
    private HelpClassifyDetailsService helpClassifyDetailsService;
    @Autowired
    private HelpTaskService helpTaskService;



    @RequestMapping(value = "/saveHelpClassify", method = RequestMethod.POST)
    @ApiOperation("新建一级分类")
    @ResponseBody
    public Result saveHelpClassify(@RequestBody HelpClassify helpClassify) {
        helpClassifyService.insert(helpClassify);
        return Result.success();
    }

    @RequestMapping(value = "/saveHelpClassifyDetails", method = RequestMethod.POST)
    @ApiOperation("新建二级分类")
    @ResponseBody
    public Result saveHelpClassifyDetails(@RequestBody HelpClassifyDetails helpClassifyDetails) {
        helpClassifyDetailsService.insert(helpClassifyDetails);
        return Result.success();
    }

    @RequestMapping(value = "/updateHelpClassify", method = RequestMethod.POST)
    @ApiOperation("修改一级分类")
    @ResponseBody
    public Result updateHelpClassify(@RequestBody HelpClassify helpClassify) {
        helpClassifyService.update(helpClassify);
        return Result.success();
    }

    @RequestMapping(value = "/updateClassifyDetails", method = RequestMethod.POST)
    @ApiOperation("修改二级分类")
    @ResponseBody
    public Result updateClassifyDetails(@RequestBody HelpClassifyDetails helpClassifyDetails) {
        helpClassifyDetailsService.updateHelpClassifyDetails(helpClassifyDetails);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyById", method = RequestMethod.POST)
    @ApiOperation("删除一级分类")
    @ResponseBody
    public Result deleteClassifyById(Long id) {
        helpClassifyService.deleteById(id);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyDetailsById", method = RequestMethod.POST)
    @ApiOperation("删除二级分类")
    @ResponseBody
    public Result deleteClassifyDetailsById(Long id) {
        helpClassifyDetailsService.deleteById(id);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyByIds", method = RequestMethod.POST)
    @ApiOperation("删除多个一级分类")
    @ResponseBody
    public Result deleteClassifyByIds(String ids) {
        helpClassifyService.deleteByIdList(ids);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyDetailsByIds", method = RequestMethod.POST)
    @ApiOperation("删除多个二级分类")
    @ResponseBody
    public Result deleteClassifyDetailsByIds(String ids) {
        helpClassifyDetailsService.deleteByIdList(ids);
        return Result.success();
    }

    @RequestMapping(value = "/selectClassifyById", method = RequestMethod.GET)
    @ApiOperation("根据id查看一级分类详细信息")
    @ResponseBody
    public Result selectClassifyById(Long id) {
        return Result.success().put("data",helpClassifyService.selectById(id));
    }

    @RequestMapping(value = "/selectClassifyDetailsById", method = RequestMethod.GET)
    @ApiOperation("根据id查看二级分类详细信息")
    @ResponseBody
    public Result selectClassifyDetailsById(Long id) {
        return Result.success().put("data",helpClassifyDetailsService.selectById(id));
    }

    @RequestMapping(value = "/selectClassifyList", method = RequestMethod.GET)
    @ApiOperation("查看一级分类")
    @ResponseBody
    public Result selectClassifyList() {
        return Result.success().put("data",helpClassifyService.selectList());
    }

    @RequestMapping(value = "/selectClassifyDetailsListByClassifyId", method = RequestMethod.GET)
    @ApiOperation("根据一级分类id查看二级分类")
    @ResponseBody
    public Result selectClassifyDetailsListByClassifyId(Long id) {
        HelpClassify helpClassify = helpClassifyService.selectById(id);
        List<HelpClassifyDetails> helpClassifyDetails = helpClassifyDetailsService.selectList(id);
        Map<String,Object> map=new HashMap<>(2);
        map.put("helpClassify",helpClassify);
        map.put("HelpClassifyDetailsList",helpClassifyDetails);
        return Result.success().put("data",map);
    }

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    @ApiOperation("查看一二级分类列表")
    @ResponseBody
    public Result select() {
        return Result.success().put("data",helpClassifyService.getHelpClassifyModel());
    }

    @RequestMapping(value = "/selectHelpTaskByClassify", method = RequestMethod.GET)
    @ApiOperation("根据分类查找任务")
    @ResponseBody
    public Result selectHelpTaskByClassify(String classifyDetailsId,int limit,int page,@RequestParam(required = false) Long userId) {
        Map<String, Object> rate = helpTaskService.getRate(userId);
        Map<String,Object> map=new HashMap<>(3);
        map.put("limit",limit);
        map.put("page",page);
        map.put("classifyDetailsId",classifyDetailsId);
        PageUtils pageUtils = helpTaskService.selectHelpTaskByClassify(map);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }

    @RequestMapping(value = "/selectHelpTaskByTitle", method = RequestMethod.GET)
    @ApiOperation("根据名称模糊查找任务")
    @ResponseBody
    public Result selectHelpTaskByTitle(String title,int limit,int page,@RequestParam(required = false) Long userId) {
        Map<String,Object> map=new HashMap<>(3);
        map.put("limit",limit);
        map.put("page",page);
        map.put("title",title);
        Map<String, Object> rate = helpTaskService.getRate(userId);
        PageUtils pageUtils = helpTaskService.selectByTitle(map);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }





}