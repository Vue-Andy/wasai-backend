package com.sqx.modules.helpTask.controller;

import com.sqx.common.utils.Result;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.invite.service.InviteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据中心
 */
@Slf4j
@RestController
@Api(value = "数据中心", tags = {"数据中心"})
@RequestMapping(value = "/statistical")
public class StatisticalController {

    //用户
    @Autowired
    private UserService userService;
    //发单
    @Autowired
    private HelpTakeOrderService helpTakeOrderService;
    //接单
    @Autowired
    private HelpSendOrderService helpSendOrderService;
    //收入
    @Autowired
    private HelpProfitService helpProfitService;
    //提现
    @Autowired
    private CashOutService cashOutService;
    //任务
    @Autowired
    private HelpTaskService helpTaskService;
    //邀请好友
    @Autowired
    private InviteService inviteService;
    //支付订单
    @Autowired
    private PayDetailsService payDetailsService;


    @ApiOperation("用户统计")
    @GetMapping("/user")
    public Result user(){
        return userService.statistical();
    }

    @ApiOperation("今日发单数")
    @GetMapping("/takeOrder")
    public Result takeOrder(){
        return helpTakeOrderService.takeOrder();
    }

    @ApiOperation("今日接单数")
    @GetMapping("/sendOrder")
    public Result sendOrder(){
        return helpSendOrderService.sendOrder();
    }


    @ApiOperation("收入")
    @GetMapping("/income")
    public Result income(){
        return helpProfitService.statistical();
    }

    @ApiOperation("财务提现统计")
    @GetMapping("/statisticsCashMoney")
    public Result statisticsMoney(String time,Integer flag){
        Double sumMoney = cashOutService.sumMoney(time, flag);
        Integer countMoney = cashOutService.countMoney(time, flag);
        Integer stayMoney = cashOutService.stayMoney(time, flag);
        Map<String,Object> map=new HashMap<>();
        map.put("sumMoney",sumMoney==null?0.00:sumMoney);
        map.put("countMoney",countMoney==null?0:countMoney);
        map.put("stayMoney",stayMoney==null?0:stayMoney);
        return Result.success().put("data",map);
    }

    @ApiOperation("充值统计")
    @GetMapping("/payMember")
    public Result payMember(String time,Integer flag){
        Double sumMoney = payDetailsService.selectSumPayByClassify(time, flag, null);
        Double weixinMoney = payDetailsService.selectSumPayByClassify(time, flag, 1);
        Double zhifubaoMoney = payDetailsService.selectSumPayByClassify(time, flag, 2);
        Map<String,Object> map=new HashMap<>();
        map.put("sumMoney",sumMoney==null?0.00:sumMoney);
        map.put("weixinMoney",weixinMoney==null?0.00:weixinMoney);
        map.put("zhifubaoMoney",zhifubaoMoney==null?0.00:zhifubaoMoney);
        return Result.success().put("data",map);
    }

    @ApiOperation("财务收入统计")
    @GetMapping("/statisticsIncomeMoney")
    public Result stayMoney(String time,Integer flag){
        Double sumPrice = helpTaskService.sumPrice(time, flag);
        Double sumMoneyBySend = helpSendOrderService.sumMoneyBySend(time, flag);
        Double sumMoneyByProfit = helpProfitService.sumMoneyByProfit(time, flag);
        Double sumInviteMoney = inviteService.sumInviteMoney(time, flag);
        Double sumMember = helpProfitService.sumMemberMoneyByProfit(time, flag);
        Map<String,Object> map=new HashMap<>();
        map.put("sumMember",sumMember==null?0.00:sumMember);
        map.put("sumMoneyByProfit",sumMoneyByProfit==null?0.00:sumMoneyByProfit);
        map.put("sumMoneyBySend",sumMoneyBySend==null?0.00:sumMoneyBySend);
        map.put("sumPrice",sumPrice==null?0.00:sumPrice);
        map.put("sumInviteMoney",sumInviteMoney==null?0.00:sumInviteMoney);
        return Result.success().put("data",map);
    }

    @ApiOperation("会员开通记录")
    @GetMapping("/selectUserMemberList")
    public Result selectUserMemberList(int page,int limit,String phone){
        return Result.success().put("data",payDetailsService.selectUserMemberList(page,limit,phone));
    }



    @ApiOperation("用户分析")
    @GetMapping("/userAnalysis")
    public Result userAnalysis(String time,Integer flag){
        return Result.success().put("data",userService.userAnalysis(time,flag));
    }

    @ApiOperation("任务分析")
    @GetMapping("/taskAnalysis")
    public Result taskAnalysis(String time,Integer flag){
        Integer takeOrderCount = helpTaskService.countHelpTaskByCreateTime(time, flag);
        Double takeOrderSum = helpTaskService.sumPrice(time, flag);
        Double sendOrderSum = helpSendOrderService.sumMoneyBySend(time, flag);
        Integer sendOrderCount = helpSendOrderService.countBySend(time, flag);
        Map<String,Object> map=new HashMap<>();
        map.put("takeOrderSum",takeOrderSum==null?0.00:takeOrderSum);//发单总金额
        map.put("sendOrderSum",sendOrderSum==null?0.00:sendOrderSum);//接单总金额
        map.put("takeOrderCount",takeOrderCount==null?0:takeOrderCount);//发单数量
        map.put("sendOrderCount",sendOrderCount==null?0:sendOrderCount);//接单数量
        return Result.success().put("data",map);
    }

    @ApiOperation("邀请人分析")
    @GetMapping("/inviteAnalysis")
    public Result inviteAnalysis(String time,Integer flag,int page,int limit){
        return Result.success().put("data",inviteService.inviteAnalysis(page,limit,time,flag));
    }

    @ApiOperation("付费会员分析")
    @GetMapping("/payMemberAnalysis")
    public Result payMemberAnalysis(String time,Integer flag,int page,int limit){
        return Result.success().put("data",payDetailsService.payMemberAnalysis(page,limit,time,flag));
    }

    @ApiOperation("任务收入分析")
    @GetMapping("/incomeAnalysis")
    public Result incomeAnalysis(String time,Integer flag,int page,int limit){
        return Result.success().put("data",helpProfitService.incomeAnalysis(page,limit,time,flag));
    }





}
