package com.sqx.modules.helpTask.controller;

import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.WxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * @author fang
 * @date 2020/2/26
 */
@RestController
@Api(value = "微信支付", tags = {"微信支付"})
@RequestMapping("/api/order")
public class ApiWeiXinPayController {

    private final Logger log = LoggerFactory.getLogger(ApiWeiXinPayController.class);

    private final WxService wxService;
    private final HelpRateService helpRateService;

    @Autowired
    public ApiWeiXinPayController(WxService wxService, HelpRateService helpRateService) {
        this.wxService = wxService;
        this.helpRateService = helpRateService;
    }

    @ApiOperation("微信app支付")
    @PostMapping("/wxPayApp")
    public Result wxPayApp(Long userId,String money) throws Exception {
        return Result.success().put("data",wxService.doUnifiedOrder(money,userId,1,1,""));
    }

    @ApiOperation("微信公众号支付")
    @PostMapping("/wxPayJsApi")
    public Map wxPayH5(Long userId,String money) throws Exception {
        return wxService.doUnifiedOrder(money,userId,2,1,"");
    }

    @ApiOperation("微信小程序支付")
    @PostMapping("/wxMpPay")
    public Map wxMpPay(Long userId,String money) throws Exception {
        return wxService.doUnifiedOrder(money,userId,3,1,"");
    }


    @ApiOperation("app开通会员支付")
    @ResponseBody
    @PostMapping("/wxPayMember")
    public Result wxPayApp(Long userId, Integer member) throws Exception {
        HelpRate helpRate = helpRateService.selectRateByTypeId(member + 1, 2);
        if(helpRate!=null){
            Double money=(helpRate.getRate()*100);
            return Result.success().put("data",wxService.doUnifiedOrder(money.intValue()+"",userId,1,2,member+""));
        }
        return Result.error("获取价格信息失败！");
    }



    @ApiOperation("小程序开通会员支付")
    @ResponseBody
    @PostMapping("/wxMpPayMember")
    public Result wxMpPayMember(Long userId, Integer member) throws Exception {
        HelpRate helpRate = helpRateService.selectRateByTypeId(member + 1, 2);
        if(helpRate!=null){
            Double money=(helpRate.getRate()*100);
            return Result.success().put("data",wxService.doUnifiedOrder(money.intValue()+"",userId,3,2,member+""));
        }
        return Result.error("获取价格信息失败！");
    }


    @PostMapping("/notify")
    @ApiOperation("微信app回调")
    public String wxPayNotify(HttpServletRequest request) {
        return notify(request,1);
    }


    @PostMapping("/notifyJsApi")
    @ApiOperation("微信公众号回调")
    public String notifyJsApi(HttpServletRequest request) {
        return notify(request,2);
    }


    @PostMapping("/notifyMp")
    @ApiOperation("微信小程序回调")
    public String notifyMp(HttpServletRequest request) {
        return notify(request,3);
    }


    private String notify(HttpServletRequest request,Integer type){
        String resXml = "";
        try {
            InputStream inputStream = request.getInputStream();
            //将InputStream转换成xmlString
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                log.info(e.getMessage());
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            resXml = sb.toString();
            String result = wxService.payBack(resXml,type);
            log.info("成功");
            log.info(result);
            return result;
        } catch (Exception e) {
            log.info("微信手机支付失败:" + e.getMessage());
            String result = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            log.info("失败");
            log.info(result);
            return result;
        }
    }


}
