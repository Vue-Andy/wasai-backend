package com.sqx.modules.helpTask.controller;


import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.entity.HelpUserScore;
import com.sqx.modules.helpTask.entity.UserMoney;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.HelpUserScoreService;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.invite.service.InviteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fang
 * @date 2020/5/15
 */
@RestController
@Api(value = "用户可消费金额", tags = {"用户可消费金额"})
@RequestMapping(value = "/userMoney")
public class UserMoneyController {

    /** 用户金额信息 */
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    /** 用户金额明细 */
    @Autowired
    private UserMoneyService userMoneyService;
    /** 信誉分 */
    @Autowired
    private HelpUserScoreService helpUserScoreService;

    @RequestMapping(value = "/sendMoney", method = RequestMethod.POST)
    @ApiOperation("根据积分发送余额")
    @ResponseBody
    public Result sendMoney(int integralNum,double money)
    {
        return userMoneyService.sendMoney(integralNum, money);
    }



    @RequestMapping(value = "/selectUserMoney", method = RequestMethod.GET)
    @ApiOperation("查询用户金额信息")
    @ResponseBody
    public Result selectUserMoney(Long userId)
    {
        return Result.success().put("data", userMoneyService.selectByUserId(userId));
    }

    @RequestMapping(value = "/selectUserMoneyDetails", method = RequestMethod.GET)
    @ApiOperation("查询用户金额明细信息")
    @ResponseBody
    public Result selectUserMoneyDetails(int page,int limit,String userId)
    {
        Map<String,Object> map=new HashMap<>(3);
        map.put("limit",limit);
        map.put("page",page);
        map.put("userId",userId);
        return Result.success().put("data",userMoneyDetailsService.selectUserMoneyDetailsList(map));
    }

    @RequestMapping(value = "/cashMoney", method = RequestMethod.POST)
    @ApiOperation("发起提现")
    @ResponseBody
    public Result cashMoney(Long userId,Double money)
    {
        HelpUserScore helpUserScore = helpUserScoreService.selectByUserId(userId);
        if(helpUserScore.getScore()<=0){
            return Result.error(-100,"当前信誉分太低无法提现，请派单提高信誉分后尝试！");
        }
        return userMoneyService.cashMoney(userId,money);
    }


    @RequestMapping(value = "/openMember", method = RequestMethod.POST)
    @ApiOperation("开通会员")
    @ResponseBody
    public Result openMember(@ApiParam("用户id")Long userId, @ApiParam("会员  1初级 2中级 3高级")Integer member)
    {
        return userMoneyService.openMember(userId,member);
    }







}