package com.sqx.modules.helpTask.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sqx.modules.helpTask.dao.PayDetailsDao;
import com.sqx.modules.helpTask.entity.PayDetails;
import com.sqx.modules.helpTask.entity.PaySaPi;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.PayUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/pays")
@Api(value = "个人支付", tags = {"个人支付"})
public class PayController {

	@Autowired
	private PayDetailsDao payDetailsDao;
	@Autowired
	private UserMoneyService userMoneyService;
	@Autowired
	private UserMoneyDetailsService userMoneyDetailsService;
	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


	@PostMapping("/pay")
	@ResponseBody
	@ApiOperation("支付")
	public Map<String, Object> pay(HttpServletRequest request,@ApiParam("价格") String price, @ApiParam("1：支付宝；2：微信支付") int istype,@ApiParam("用户id")  Long userId) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> remoteMap = new HashMap<String, Object>();
		String orderIdByUUId = PayUtil.getOrderIdByUUId();
		if(istype==1){
			istype=6;
		}else{
			istype=5;
		}
		remoteMap.put("price", price);
		remoteMap.put("istype", istype);
		remoteMap.put("orderid", orderIdByUUId);
		remoteMap.put("orderuid", getGeneralOrder());
		remoteMap.put("goodsname", "renwu");
		resultMap.put("data", PayUtil.payOrder(remoteMap));
		PayDetails payDetails=new PayDetails();
		payDetails.setState(0);
		payDetails.setCreateTime(sdf.format(new Date()));
		payDetails.setOrderId(orderIdByUUId);
		payDetails.setUserId(userId);
		payDetails.setMoney(Double.parseDouble(price));
		if(istype==6){
			payDetails.setClassify(2);
		}else{
			payDetails.setClassify(1);
		}
		payDetails.setType(1);
		payDetailsDao.insert(payDetails);
		return resultMap;
	}

	@RequestMapping("/notifyPay")
	public void notifyPay(HttpServletRequest request, HttpServletResponse response, PaySaPi paySaPi) {
		// 保证密钥一致性
		if (PayUtil.checkPayKey(paySaPi)) {
			// TODO 做自己想做的
			PayDetails payDetails=payDetailsDao.selectByOrderId(paySaPi.getOrderid());
			if(payDetails.getState()==0) {
				payDetailsDao.updateState(payDetails.getId(), 1, new Date());
				userMoneyService.updateCannotMoney(1, payDetails.getUserId(), payDetails.getMoney());
				UserMoneyDetails userMoneyDetails = new UserMoneyDetails();
				userMoneyDetails.setUserId(payDetails.getUserId());
				userMoneyDetails.setTitle("充值：" + payDetails.getMoney());
				if(payDetails.getClassify()==1){
					userMoneyDetails.setContent("微信充值金额：" + payDetails.getMoney());
				}else{
					userMoneyDetails.setContent("支付宝充值金额：" + payDetails.getMoney());
				}
				userMoneyDetails.setType(0);
				userMoneyDetails.setMoney(payDetails.getMoney());
				userMoneyDetails.setCreateTime(sdf.format(new Date()));
				userMoneyDetailsService.insert(userMoneyDetails);
			}
		} else {
			// TODO  秘钥不一致暂不做处理

		}
	}

	@RequestMapping("/returnPay")
	public ModelAndView returnPay(HttpServletRequest request, HttpServletResponse response, String orderid) {
		boolean isTrue = false;
		ModelAndView view = null;
		// 根据订单号查找相应的记录:根据结果跳转到不同的页面
		if (isTrue) {
			view = new ModelAndView("/正确的跳转地址");
		} else {
			view = new ModelAndView("/没有支付成功的地址");
		}
		return view;
	}

	public String getGeneralOrder(){
		Date date=new Date();
		String newString = String.format("%0"+4+"d", (int)((Math.random()*9+1)*1000));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String format = sdf.format(date);
		return format+newString;
	}


}
