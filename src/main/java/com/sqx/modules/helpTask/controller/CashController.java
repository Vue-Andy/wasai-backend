package com.sqx.modules.helpTask.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayFundTransToaccountTransferRequest;
import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import com.google.common.base.Charsets;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.app.utils.HttpClient;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.AliPayConstants;
import com.sqx.modules.helpTask.config.Config;
import com.sqx.modules.helpTask.entity.AliPayWithdrawModel;
import com.sqx.modules.helpTask.entity.CashOut;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.helpTask.utils.HttpClientUtil;
import com.sqx.modules.helpTask.utils.StringUtils;
import com.sqx.modules.message.entity.MessageInfo;
import com.sqx.modules.message.service.MessageService;
import com.yungouos.pay.alipay.AliPay;
import com.yungouos.pay.util.PaySignUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/5/15
 */
@Slf4j
@RestController
@Api(value = "管理平台", tags = {"管理平台"})
@RequestMapping(value = "/cash")
public class CashController {

    /** 平台收入 */
    @Autowired
    private HelpProfitService helpProfitService;
    /** 充值记录 */
    @Autowired
    private PayDetailsService payDetailsService;
    /** 提现记录 */
    @Autowired
    private CashOutService cashOutService;
    /** app用户 */
    @Autowired
    private UserService userService;
    /** 通用配置 */
    @Autowired
    private CommonInfoService commonInfoService;
    /** 会员比例和开通价格 */
    @Autowired
    private HelpRateService helpRateService;
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    @Autowired
    private MessageService messageService;


    @RequestMapping(value = "/sendMsgByUserId", method = RequestMethod.GET)
    @ApiOperation("管理平台主动推送消息(指定用户)")
    @ResponseBody
    public Result sendMsgByUserId(String title,String content,Long userId){
        UserEntity user = userService.selectById(userId);
        send(user,title,content);
        return Result.success();
    }


    @RequestMapping(value = "/sendMsg", method = RequestMethod.GET)
    @ApiOperation("管理平台主动推送消息")
    @ResponseBody
    public Result sendMsg(String title,String content,String phone,Integer flag){
        if(flag==1){
            //根据手机号推送
            UserEntity userByPhone = userService.queryByMobile(phone);
            if(null==userByPhone){
                return Result.error(-100,"手机号不存在！");
            }
            send(userByPhone,title,content);
        }else{
            //所有人推送
            List<UserEntity> userInfos = userService.selectUserList();
            //用户数量较大  使用多线程推送  根据用户数量进行拆分  同时按照3个线程进行推送
            int count = userInfos.size() / 3;
            new Thread(() -> {
                for(int i=0 ;i<count;i++){
                    send(userInfos.get(i),title,content);
                }
            }).start();
            new Thread(() -> {
                for(int i=count ;i<count*2;i++){
                    send(userInfos.get(i),title,content);
                }
            }).start();
            new Thread(() -> {
                for(int i=count*2 ;i<userInfos.size();i++){
                    send(userInfos.get(i),title,content);
                }
            }).start();
           /* for(UserInfo userByPhone:userInfos){

            }*/
        }
        return Result.success();
    }

    private void send(UserEntity userByPhone,String title,String content){
        if (userByPhone.getClientid() != null) {
            userService.pushToSingle(title, content, userByPhone.getClientid());
        }
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setContent(content);
        messageInfo.setTitle(title);
        messageInfo.setState(String.valueOf(8));
        messageInfo.setUserName(userByPhone.getNickName());
        messageInfo.setUserId(String.valueOf(userByPhone.getUserId()));
        messageService.saveBody(messageInfo);
    }


    @RequestMapping(value = "/selectCashOut", method = RequestMethod.GET)
    @ApiOperation("获取最新的提现信息")
    @ResponseBody
    public Result selectCashOut(){
        return Result.success().put("data",cashOutService.selectCashOutLimit3());
    }


    @RequestMapping(value = "/selectHelpProfit", method = RequestMethod.GET)
    @ApiOperation("查询平台收入信息列表")
    @ResponseBody
    public Result selectHelpProfit(int page,int limit,String createTime,String endTime){
        return Result.success().put("data",helpProfitService.selectHelpProfitList(page,limit,createTime,endTime));
    }

    @RequestMapping(value = "/selectHelpProfitCount", method = RequestMethod.GET)
    @ApiOperation("查询平台收入总金额")
    @ResponseBody
    public Result selectHelpProfitCount(String createTime,String endTime){
        return Result.success().put("data",helpProfitService.selectSumProfit(createTime,endTime));
    }

    @RequestMapping(value = "/selectSumPay", method = RequestMethod.GET)
    @ApiOperation("查询用户充值金额")
    @ResponseBody
    public Result selectSumPay(String createTime,String endTime,Long userId){
        return Result.success().put("data",payDetailsService.selectSumPay(createTime,endTime,userId));
    }

    @RequestMapping(value = "/selectUserRecharge", method = RequestMethod.GET)
    @ApiOperation("查询所有用户充值信息列表")
    @ResponseBody
    public Result selectUserRecharge(int page,int limit,String startTime,String endTime,Integer state){
        return Result.success().put("data",payDetailsService.selectPayDetails(page,limit,startTime,endTime,null,state));
    }

    @RequestMapping(value = "/selectUserRechargeByUserId", method = RequestMethod.GET)
    @ApiOperation("查询某个用户充值信息列表")
    @ResponseBody
    public Result selectUserRechargeByUserId(int page,int limit,String startTime,String endTime,Long userId){
        return Result.success().put("data",payDetailsService.selectPayDetails(page,limit,startTime,endTime,userId,null));
    }

    @RequestMapping(value = "/selectUserRechargeByUserIdApp", method = RequestMethod.GET)
    @ApiOperation("查询某个用户充值信息列表")
    @ResponseBody
    public Result selectUserRechargeByUserIdApp(int page,int limit,String startTime,String endTime,Long userId){
        return Result.success().put("data",payDetailsService.selectPayDetails(page,limit,startTime,endTime,userId,1));
    }

    @RequestMapping(value = "/selectPayDetails", method = RequestMethod.GET)
    @ApiOperation("查询提现记录列表")
    @ResponseBody
    public Result selectHelpProfit(int page,int limit,String zhifubaoName,String zhifubao,String userId){
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("zhifubaoName",zhifubaoName);
        map.put("zhifubao",zhifubao);
        map.put("userId",userId);
        PageUtils pageUtils = cashOutService.selectCashOutList(map);
        return Result.success().put("data",pageUtils);
    }


    @RequestMapping(value = "/selectUserCount", method = RequestMethod.GET)
    @ApiOperation("查询所有用户数量")
    @ResponseBody
    public Result selectUserCount(Integer member){
        return Result.success().put("data",userService.selectUserByMemberCount(member));
    }

    @RequestMapping(value = "/selectRateByRateList", method = RequestMethod.GET)
    @ApiOperation("查询平台扣费会员等列表")
    @ResponseBody
    public Result selectRateByRateList(Integer classify,Integer isShopTask){
        if(isShopTask==null){
            isShopTask=1;
        }
        List<HelpRate> helpRates = helpRateService.selectRateByClassifyList(classify,isShopTask);
        return Result.success().put("data",helpRates);
    }


    @RequestMapping(value = "/updateRate", method = RequestMethod.POST)
    @ApiOperation("修改平台扣费会员等级")
    @ResponseBody
    public Result updateRate(Long id,Double rate){
        helpRateService.updateByMoney(id,rate);
        return Result.success();
    }


    /*@RequestMapping(value = "/updateByMoney/{id}/{money}", method = RequestMethod.POST)
    @ApiOperation("修改平台开通价格或会员扣除比例")
    @ResponseBody
    public Result updateByMoney(@PathVariable("id") Long id,@PathVariable("money") Double money){
        helpRateService.updateByMoney(id,money);
        return Result.success();
    }

    @RequestMapping(value = "/alipay/{cashId}", method = RequestMethod.POST)
    @ApiOperation("管理平台确认提现")
    @ResponseBody
    public Result alipayPay(@PathVariable Long cashId) {
        *//**
         * 初始化客户端
         *//*
        AlipayClient alipayClient = new DefaultAlipayClient(AliPayConstants.REQUEST_URL,
                commonInfoService.findOne(63).getValue(), commonInfoService.findOne(65).getValue(), AliPayConstants.FORMAT,
                AliPayConstants.CHARSET, commonInfoService.findOne(64).getValue(), AliPayConstants.SIGNTYPE);
        CashOut one = cashOutService.selectById(cashId);


        if (one == null) {
            return Result.error("提现记录不存在！");
        }
        if (one.getState()!=0) {
            return Result.error(9999, one.getZhifubaoName() + "转账失败！原因是用户已转账");
        }
        CommonInfo commonInfo = commonInfoService.findOne(51);
        String date = String.valueOf(System.currentTimeMillis());
        CommonInfo name = commonInfoService.findOne(12);
        if (commonInfo.getValue() != null && commonInfo.getValue().equals("是")) {
            val aliPayWithdrawModel = AliPayWithdrawModel.builder()
                    .out_biz_no(date)
                    .amount(new BigDecimal(one.getMoney()))
                    .payee_account(one.getZhifubao())
                    .payee_real_name(one.getZhifubaoName())
                    .payee_type(AliPayConstants.PAY_TYPE)
                    .remark(name==null?"助力任务":name.getValue() + "佣金结算")
                    .build();
            String json = JSON.toJSONString(aliPayWithdrawModel);
            //实例化连接对象
            AlipayFundTransToaccountTransferRequest withdrawRequest = new AlipayFundTransToaccountTransferRequest();
            withdrawRequest.setBizContent(json);
            try {
                AlipayFundTransToaccountTransferResponse response = alipayClient.execute(withdrawRequest);
                if (AliPayConstants.SUCCESS_CODE.equalsIgnoreCase(response.getCode())) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    one.setState(1);
                    one.setOrderNumber(date);
                    one.setOutAt(sdf.format(new Date()));
                    cashOutService.update(one);
                    UserEntity userInfo=userService.selectById(one.getUserId());
                    if (userInfo != null && userInfo.getOpenId() != null) {
                        //提现通知消息
                        cashOutService.cashOutSuccess(userInfo.getOpenId(), one.getOutAt(), one.getMoney(), one.getZhifubao(),  commonInfoService.findOne(19).getValue());
                    }
                    return Result.success(one.getZhifubaoName() + "转账成功");
                } else {
                    return Result.error(9999, one.getZhifubaoName() + "转账失败！" + response.getSubMsg());
                }
            } catch (AlipayApiException e) {
                log.error("零钱提现异常原因:" + e.getMessage());
                e.printStackTrace();
                return Result.error(9999, one.getZhifubaoName() + "转账失败！" + e.getMessage());

            }
        } else {
            //人工转账后改变状态的
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            one.setState(1);
            one.setOrderNumber(date);
            one.setOutAt(sdf.format(now));
            cashOutService.update(one);
            UserEntity userInfo=userService.selectById(one.getUserId());
            if (userInfo != null && userInfo.getOpenId() != null) {
                //推送提现通知消息
                cashOutService.cashOutSuccess(userInfo.getOpenId(), one.getOutAt(), one.getMoney(), one.getZhifubao(),commonInfoService.findOne(19).getValue());
            }
            return Result.success(one.getZhifubaoName() + "转账成功");
        }
    }*/

    @RequestMapping(value = "/updateByMoney/{id}/{money}", method = RequestMethod.POST)
    @ApiOperation("修改平台开通价格或会员扣除比例")
    @ResponseBody
    public Result updateByMoney(@PathVariable("id") Long id,@PathVariable("money") Double money){
        helpRateService.updateByMoney(id,money);
        return Result.success();
    }

    @RequestMapping(value = "/alipay/{cashId}", method = RequestMethod.POST)
    @ApiOperation("管理平台确认提现")
    @ResponseBody
    public Result alipayPay(@PathVariable Long cashId) {
        
        
        CashOut one = cashOutService.selectById(cashId);


        if (one == null) {
            return Result.error("提现记录不存在！");
        }
        if (one.getState()!=0) {
            return Result.error(9999, one.getZhifubaoName() + "转账失败！原因是用户已转账");
        }
        CommonInfo commonInfo = commonInfoService.findOne(98);
        String date = String.valueOf(System.currentTimeMillis());
        CommonInfo name = commonInfoService.findOne(12);
        if (commonInfo.getValue() != null && commonInfo.getValue().equals("1")) {
            CommonInfo pay = commonInfoService.findOne(167);
            if("1".equals(pay.getValue())){
                AlipayClient alipayClient = new DefaultAlipayClient(AliPayConstants.REQUEST_URL,
                        commonInfoService.findOne(63).getValue(), commonInfoService.findOne(65).getValue(), AliPayConstants.FORMAT,
                        AliPayConstants.CHARSET, commonInfoService.findOne(64).getValue(), AliPayConstants.SIGNTYPE);
                val aliPayWithdrawModel = AliPayWithdrawModel.builder()
                        .out_biz_no(date)
                        .amount(new BigDecimal(one.getMoney()))
                        .payee_account(one.getZhifubao())
                        .payee_real_name(one.getZhifubaoName())
                        .payee_type(AliPayConstants.PAY_TYPE)
                        .remark(name==null?"助力任务":name.getValue() + "佣金结算")
                        .build();
                String json = JSON.toJSONString(aliPayWithdrawModel);
                //实例化连接对象
                AlipayFundTransToaccountTransferRequest withdrawRequest = new AlipayFundTransToaccountTransferRequest();
                withdrawRequest.setBizContent(json);
                try {
                    AlipayFundTransToaccountTransferResponse response = alipayClient.execute(withdrawRequest);
                    if (AliPayConstants.SUCCESS_CODE.equalsIgnoreCase(response.getCode())) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        one.setState(1);
                        one.setOrderNumber(date);
                        one.setOutAt(sdf.format(new Date()));
                        cashOutService.update(one);
                        UserEntity userInfo=userService.selectById(one.getUserId());
                        if (userInfo != null && userInfo.getOpenId() != null) {
                            //提现通知消息
                            cashOutService.cashOutSuccess(userInfo.getOpenId(), one.getOutAt(), one.getMoney(), one.getZhifubao(),  commonInfoService.findOne(19).getValue());
                        }
                        return Result.success(one.getZhifubaoName() + "转账成功");
                    } else {
                        return Result.error(9999, one.getZhifubaoName() + "转账失败！" + response.getSubMsg());
                    }
                } catch (AlipayApiException e) {
                    log.error("零钱提现异常原因:" + e.getMessage());
                    e.printStackTrace();
                    return Result.error(9999, one.getZhifubaoName() + "转账失败！" + e.getMessage());

                }
            }else{
                CommonInfo payAppId = commonInfoService.findOne(170);
                CommonInfo payKey = commonInfoService.findOne(171);
                String url="https://api.pay.yungouos.com/api/finance/repay/alipay";
                Map<String,Object> maps=new HashMap<>();
                maps.put("merchant_id",payAppId.getValue());
                maps.put("out_trade_no",date);
                maps.put("account",one.getZhifubao());
                maps.put("account_name",one.getZhifubaoName());
                maps.put("money",String.valueOf(new BigDecimal(one.getMoney())));
                maps.put("desc",name==null?"助力任务":name.getValue() + "佣金结算");
                String sign = PaySignUtil.createSign(maps, payKey.getValue());
                Map<String,String> payMap=new HashMap<>();
                payMap.put("merchant_id",payAppId.getValue());
                payMap.put("out_trade_no",date);
                payMap.put("account",one.getZhifubao());
                payMap.put("account_name",one.getZhifubaoName());
                payMap.put("money",String.valueOf(new BigDecimal(one.getMoney())));
                payMap.put("desc",name==null?"助力任务":name.getValue() + "佣金结算");
                payMap.put("sign", sign);
                String datas = HttpClientUtil.doPost(url, payMap);
                JSONObject data = JSONObject.parseObject(datas);
                String code = data.getString("code");
                if("0".equals(code)){
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    one.setState(1);
                    one.setOrderNumber(date);
                    one.setOutAt(sdf.format(new Date()));
                    cashOutService.update(one);
                    UserEntity userInfo=userService.selectById(one.getUserId());
                    if (userInfo != null && userInfo.getOpenId() != null) {
                        //提现通知消息
                        cashOutService.cashOutSuccess(userInfo.getOpenId(), one.getOutAt(), one.getMoney(), one.getZhifubao(),  commonInfoService.findOne(19).getValue());
                    }
                    return Result.success(one.getZhifubaoName() + "转账成功");
                }else{
                    return Result.error(9999, one.getZhifubaoName() + "转账失败！" + data.getString("msg"));
                }
            }
        }else if(commonInfo.getValue() != null && commonInfo.getValue().equals("2")){
            CommonInfo one1 = commonInfoService.findOne(99);
            if(one1==null || StringUtils.isBlank(one1.getValue())){
                return Result.error("请设置转账秘钥");
            }
            Map<String,String> map=new HashMap<>();
            Object o = JSONArray.toJSON(one);
            map.put("jsonCashOut",new String(o.toString().getBytes(), Charsets.UTF_8));
            map.put("secretKey",one1.getValue());
            String body = HttpClientUtil.doPost("https://money.xiansqx.com/sqx_fast/cash/alipay", map);
            JSONObject jsonObject = JSON.parseObject(body);
            Integer code = jsonObject.getInteger("code");
            if(code!=0){
                return Result.error(jsonObject.getString("msg"));
            }else{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                one.setState(1);
                one.setOrderNumber(date);
                one.setOutAt(sdf.format(new Date()));
                cashOutService.update(one);
                UserEntity userInfo=userService.selectById(one.getUserId());
                if (userInfo != null && userInfo.getOpenId() != null) {
                    //提现通知消息
                    cashOutService.cashOutSuccess(userInfo.getOpenId(), one.getOutAt(), one.getMoney(), one.getZhifubao(),  commonInfoService.findOne(19).getValue());
                }
                return Result.success(one.getZhifubaoName() + "转账成功");
            }
        } else{
            //人工转账后改变状态的
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            one.setState(1);
            one.setOrderNumber(date);
            one.setOutAt(sdf.format(now));
            cashOutService.update(one);
            UserEntity userInfo=userService.selectById(one.getUserId());
            if (userInfo != null && userInfo.getOpenId() != null) {
                //推送提现通知消息
                cashOutService.cashOutSuccess(userInfo.getOpenId(), one.getOutAt(), one.getMoney(), one.getZhifubao(),commonInfoService.findOne(19).getValue());
            }
            return Result.success(one.getZhifubaoName() + "转账成功");
        }
    }


    @RequestMapping(value = "/refund/{cashId}/{content}", method = RequestMethod.POST)
    @ApiOperation("管理平台退款")
    @ResponseBody
    public Result refund(@PathVariable("cashId") Long cashId,@PathVariable("content") String content) {
        CashOut one = cashOutService.selectById(cashId);
        if (one == null) {
            return Result.error("提现信息不存在");
        }
        if(one.getState()!=0){
            return Result.error(-100,"状态错误，已经转账或退款!");
        }
        String date = String.valueOf(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        one.setState(-1);
        one.setOrderNumber(date);
        one.setRefund(content);
        one.setOutAt(sdf.format(now));
        cashOutService.update(one);
        Long userId = one.getUserId();
        UserEntity userInfo = userService.selectById(userId);
        if(userInfo!=null){
            double v = Double.parseDouble(one.getMoney());
            if(StringUtils.isNotBlank(one.getRelationId())){
                double v1 = Double.parseDouble(one.getRelationId());
                v=AmountCalUtils.add(v,v1);
            }
            //将金额退还
            userMoneyService.updateMayMoney(1,userInfo.getUserId(),v);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userInfo.getUserId());
            userMoneyDetails.setTitle("[退款提醒]退款："+v);
            userMoneyDetails.setContent(content);
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(v);
            userMoneyDetails.setCreateTime(sdf.format(now));
            userMoneyDetailsService.insert(userMoneyDetails);
            if (userInfo.getOpenId() != null) {
                //推送提现通知消息
                cashOutService.refundSuccess(userInfo, one.getOutAt(), one.getMoney(),  commonInfoService.findOne(19).getValue(),content);
            }
        }
        return Result.success();
    }





}