package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  接单详细信息
 * @author 房 2020-05-14
 */
@Data
@TableName("help_send_order_details")
public class HelpSendOrderDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 接单详细id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 接单id
     */

    private Long helpSendOrderId;

    /**
     * 图片
     */

    private String picture;

    /**
     * 顺序
     */

    private Integer sort;


    public HelpSendOrderDetails() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getHelpSendOrderId() {
        return helpSendOrderId;
    }

    public void setHelpSendOrderId(Long helpSendOrderId) {
        this.helpSendOrderId = helpSendOrderId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

}
