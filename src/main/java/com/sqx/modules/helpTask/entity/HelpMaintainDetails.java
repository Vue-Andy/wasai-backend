package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *  help_maintain_details
 * @author 房 2020-05-18
 */
@Data
@TableName("help_maintain_details")
public class HelpMaintainDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 维权详细信息id
     */
    @TableId(type = IdType.INPUT)
    private Long id;

    /**
     * 维权id
     */

    private Long helpMaintainId;

    /**
     * 分类（1派单人发言2接单人发言）
     */

    private Integer classify;

    /**
     * 内容
     */

    private String content;

    /**
     * 图片
     */

    private String picture;

    /**
     * 创建时间
     */

    private String createTime;

    /**
     * 用户id
     */
    private Long userId;

    @TableField(exist = false)
    private String nickName;

    @TableField(exist = false)
    private List<HelpTaskDetails> helpTaskDetailsList;

    @TableField(exist = false)
    private List<HelpSendOrderDetails> helpSendOrderDetailsList;


    public List<HelpTaskDetails> getHelpTaskDetailsList() {
        return helpTaskDetailsList;
    }

    public void setHelpTaskDetailsList(List<HelpTaskDetails> helpTaskDetailsList) {
        this.helpTaskDetailsList = helpTaskDetailsList;
    }

    public List<HelpSendOrderDetails> getHelpSendOrderDetailsList() {
        return helpSendOrderDetailsList;
    }

    public void setHelpSendOrderDetailsList(List<HelpSendOrderDetails> helpSendOrderDetailsList) {
        this.helpSendOrderDetailsList = helpSendOrderDetailsList;
    }

    public HelpMaintainDetails() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHelpMaintainId() {
        return helpMaintainId;
    }

    public void setHelpMaintainId(Long helpMaintainId) {
        this.helpMaintainId = helpMaintainId;
    }

    public Integer getClassify() {
        return classify;
    }

    public void setClassify(Integer classify) {
        this.classify = classify;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
