package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  信用分
 * @author 房 2020-05-14
 */
@Data
@TableName("help_user_score")
public class HelpUserScore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 用户id
     */

    private Long userId;

    /**
     * 信用分数
     */

    private Integer score;

    /**
     * 累计增加分数
     */

    private Integer addScore;

    /**
     * 累计减少分数
     */

    private Integer reduceScore;


    public HelpUserScore() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long  getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getAddScore() {
        return addScore;
    }

    public void setAddScore(Integer addScore) {
        this.addScore = addScore;
    }

    public Integer getReduceScore() {
        return reduceScore;
    }

    public void setReduceScore(Integer reduceScore) {
        this.reduceScore = reduceScore;
    }
}
