package com.sqx.modules.helpTask.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.util.Date;
import java.util.List;

/**
 * @author fang
 * @date 2021/1/18
 */
@Data
@TableName("attention")
public class Attention implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 关注id
     */
    @TableId(type = IdType.INPUT)
    private Long attentionId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 被关注用户id
     */
    private Long byUserId;

    /**
     * 时间
     */
    private String createTime;

    /**
     * 用户名
     */
    @TableField(exist = false)
    private String nickName;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String imageUrl;



}
