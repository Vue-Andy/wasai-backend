package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  浏览记录
 * @author 房 2020-05-14
 */
@Data
@TableName("help_browsing_history")
public class HelpBrowsingHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 浏览记录id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 助力id
     */

    private Long HelpTaskId;

    /**
     * 用户id
     */

    private Long userId;

    /**
     * 创建时间
     */

    private String createTime;


    public HelpBrowsingHistory() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getHelpTaskId() {
        return HelpTaskId;
    }

    public void setHelpTaskId(Long helpTaskId) {
        HelpTaskId = helpTaskId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
