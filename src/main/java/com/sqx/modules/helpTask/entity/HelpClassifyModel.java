package com.sqx.modules.helpTask.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *  help_classify
 * @author fang 2020-06-11
 */
@Data
public class HelpClassifyModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 一级分类id
     */
    private Long id;

    /**
     * 名称
     */
    private String classifyName;

    /**
     * 图标
     */
    private String classifyIcon;

    /**
     * 地址
     */
    private String classifyUrl;

    /**
     * 排序
     */
    private String sort;

    private Integer state;

    private List<HelpClassifyDetails> list;

    /**
     * 描述
     */
    private String describes;


}
