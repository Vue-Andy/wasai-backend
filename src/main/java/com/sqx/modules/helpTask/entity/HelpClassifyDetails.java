package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  help_classify_details
 * @author fang 2020-06-11
 */
@Data
@TableName("help_classify_details")
public class HelpClassifyDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 二级分类id
     */
    @TableId(type = IdType.INPUT)
    private Long id;

    /**
     * 一级分类id
     */

    private Long classifyId;

    /**
     * 名称
     */

    private String classifyDeatilsName;

    /**
     * 图标
     */
    private String classifyIcon;

    /**
     * 地址
     */
    private String classifyUrl;

    /**
     * 描述
     */
    private String describes;

    /**
     * 状态
     */

    private Integer state;

    /**
     * 排序
     */

    private Integer sort;

    /**
     * 任务最低数量
     */
    private String taskNum;

    /**
     * 任务最低单价
     */
    private String taskMoney;

    /**
     * 备注
     */
    private String remark;


}
