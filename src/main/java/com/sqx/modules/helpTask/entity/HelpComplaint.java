package com.sqx.modules.helpTask.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.util.Date;
import java.util.List;

/**
 *  任务投诉
 * @author fang 2020-12-08
 */
@Data
@TableName("help_complaint")
public class HelpComplaint implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 任务id
     */
    private Long helpTaskId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 投诉内容
     */
    private String content;

    /**
     * 截图
     */
    private String picture;

    /**
     * 状态 1待审核 2已审核
     */
    private Integer state;

    /**
     * 创建时间
     */
    private String createTime;

    @TableField(exist = false)
    private String title;

    @TableField(exist = false)
    private String nickName;


}
