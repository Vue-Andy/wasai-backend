package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  发单
 * @author 房 2020-05-14
 */
@Data
@TableName("help_take_order")
public class HelpTakeOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 助力id
     */

    private Long helpTaskId;

    /**
     * 用户id
     */

    private Long userId;

    /**
     * 创建时间
     */

    private String createTime;

    /**
     * 派单id(页面显示)
     */

    private String sendOrdersId;

    /**
     * 状态(0待审核 1审核成功 2拒绝 3结算完成)
     */

    private Integer state;

    /**
     * 拒绝原因
     */

    private String auditContent;

    /**
     * 拒绝类型
     */

    private Integer category;

    /**
     * 图片
     */

    private String picture;

    /**
     * 审核时间
     */

    private String auditTime;



    public HelpTakeOrder() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getHelpTaskId() {
        return helpTaskId;
    }

    public void setHelpTaskId(Long helpTaskId) {
        this.helpTaskId = helpTaskId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getSendOrdersId() {
        return sendOrdersId;
    }

    public void setSendOrdersId(String sendOrdersId) {
        this.sendOrdersId = sendOrdersId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getAuditContent() {
        return auditContent;
    }

    public void setAuditContent(String auditContent) {
        this.auditContent = auditContent;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }
}
