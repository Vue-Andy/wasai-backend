package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author fang
 * @date 2020/9/22
 */
@Data
@TableName("help_task_refresh")
public class HelpTaskRefresh {

        @TableId(type = IdType.INPUT)
        private static final long serialVersionUID = 1L;

        /**
         * 任务每日刷新id
         */
        private Long id;

        /**
         * 任务id
         */
        private Long helpTaskId;

        /**
         * 用户id
         */
        private Long userId;

        /**
         * 时间
         */
        private String createTime;



}



