package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  平台抽成
 * @author 房 2020-05-14
 */
@Data
@TableName("help_rate")
public class HelpRate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 平台抽成id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 抽成比率
     */

    private Double rate;

    /**
     * 类别
     */

    private Integer type;

    private Integer isShopTask;




}
