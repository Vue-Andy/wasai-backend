package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  充值记录
 * @author fang 2020-05-14
 */
@Data
@TableName("pay_details")
public class PayDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 充值记录id
     */
    @TableId(type = IdType.INPUT)
    private Long id;

    /**
     * 分类（1微信 2支付宝）
     */
    private Integer classify;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 充值金额
     */
    private Double money;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 0待支付 1支付成功 2失败
     */
    private Integer state;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 支付时间
     */
    private String payTime;

    /**
     * 支付类型 1 充值  2开通会员
     */
    private Integer type;

    private String remark;

}
