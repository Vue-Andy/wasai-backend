package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  助力步骤
 * @author 房 2020-05-14
 */
@Data
@TableName("help_task_details")
public class HelpTaskDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 步骤id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 助力id
     */

    private Long helpTaskId;

    /**
     * 图片
     */

    private String picture;

    /**
     * 说明
     */

    private String content;

    /**
     * 是否为验证图片（0否 1是）
     */

    private Integer isVerify;

    /**
     * 顺序（1、2..）
     */

    private Integer sort;


    public HelpTaskDetails() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getHelpTaskId() {
        return helpTaskId;
    }

    public void setHelpTaskId(Long helpTaskId) {
        this.helpTaskId = helpTaskId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getIsVerify() {
        return isVerify;
    }

    public void setIsVerify(Integer isVerify) {
        this.isVerify = isVerify;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
