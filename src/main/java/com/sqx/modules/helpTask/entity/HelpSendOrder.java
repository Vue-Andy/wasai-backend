package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  接单
 * @author 房 2020-05-14
 */
@Data
@TableName("help_send_order")
public class HelpSendOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 接单id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 助力id
     */

    private Long helpTaskId;

    /**
     * 用户id
     */

    private Long userId;
    @TableField(exist = false)
    private String nickName;

    /**
     * 创建时间
     */

    private String createTime;

    /**
     * 接单id(显示在页面上)
     */

    private String orderFormId;

    /**
     * 状态（0接单成功 1提交待审核 2审核成功 3拒绝 4维权 5放弃  6维权拒绝 7超时）
     */

    private Integer state;

    /**
     * 接单对应昵称
     */

    private String content;

    /**
     * 拒绝原因
     */

    private String auditContent;

    /**
     * 拒绝类型
     */

    private Integer category;

    /**
     * 图片
     */

    private String picture;

    /**
     * 审核时间
     */

    private String auditTime;

    private Double money;

    private Double zhiMoney;

    private Double feiMoney;

    private Long zhiUserId;

    private Long feiUserId;

    private Double pingMoney;


}
