package com.sqx.modules.helpTask.entity;

import java.util.List;

/**
 * @author fang
 * @date 2020/6/29
 */
public class HelpMaintainModel {

    private Long id;


    private Long helpTaskId;


    private String title;


    private Double taskPrice;


    private Long helpSendOrderId;


    private String label;


    private String createTime;


    private Integer state;


    private String content;

    private List<HelpMaintainDetails> helpMaintainDetailsList;

    public Long getHelpSendOrderId() {
        return helpSendOrderId;
    }

    public void setHelpSendOrderId(Long helpSendOrderId) {
        this.helpSendOrderId = helpSendOrderId;
    }

    public Long getHelpTaskId() {
        return helpTaskId;
    }

    public void setHelpTaskId(Long helpTaskId) {
        this.helpTaskId = helpTaskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getTaskPrice() {
        return taskPrice;
    }

    public void setTaskPrice(Double taskPrice) {
        this.taskPrice = taskPrice;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<HelpMaintainDetails> getHelpMaintainDetailsList() {
        return helpMaintainDetailsList;
    }

    public void setHelpMaintainDetailsList(List<HelpMaintainDetails> helpMaintainDetailsList) {
        this.helpMaintainDetailsList = helpMaintainDetailsList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}