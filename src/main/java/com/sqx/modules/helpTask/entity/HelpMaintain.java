package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  维权详细信息
 * @author 房 2020-05-14
 */
@Data
@TableName("help_maintain")
public class HelpMaintain implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 维权id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 助力id
     */

    private Long helpTaskId;

    /**
     * 接单id
     */

    private Long helpSendOrderId;

    /**
     * 派单id
     */

    private Long helpTakeOrderId;

    /**
     * 接单人id
     */

    private Long sendOrderUserId;

    /**
     * 派单人id
     */

    private Long takeOrderUserId;


    /**
     * 状态（0待审核 1派单人胜 2接单人胜）
     */

    private Integer state;

    /**
     * 平台审核意见
     */

    private String content;

    /**
     * 创建时间
     */

    private String createTime;


    public HelpMaintain() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getHelpTaskId() {
        return helpTaskId;
    }

    public void setHelpTaskId(Long helpTaskId) {
        this.helpTaskId = helpTaskId;
    }

    public Long getHelpSendOrderId() {
        return helpSendOrderId;
    }

    public void setHelpSendOrderId(Long helpSendOrderId) {
        this.helpSendOrderId = helpSendOrderId;
    }

    public Long getHelpTakeOrderId() {
        return helpTakeOrderId;
    }

    public void setHelpTakeOrderId(Long helpTakeOrderId) {
        this.helpTakeOrderId = helpTakeOrderId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getSendOrderUserId() {
        return sendOrderUserId;
    }

    public void setSendOrderUserId(Long sendOrderUserId) {
        this.sendOrderUserId = sendOrderUserId;
    }

    public Long getTakeOrderUserId() {
        return takeOrderUserId;
    }

    public void setTakeOrderUserId(Long takeOrderUserId) {
        this.takeOrderUserId = takeOrderUserId;
    }
}
