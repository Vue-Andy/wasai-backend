package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  信用分详细
 * @author 房 2020-05-14
 */
@Data
@TableName("help_user_score_details")
public class HelpUserScoreDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 信用详细id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 用户id
     */

    private Long userId;

    /**
     * 标题
     */

    private String title;

    /**
     * 分数
     */

    private Integer score;

    /**
     * 类别（1收入2支出）
     */

    private Integer type;

    /**
     * 内容
     */

    private String content;

    /**
     * 创建时间
     */

    private String createTime;


    public HelpUserScoreDetails() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



}
