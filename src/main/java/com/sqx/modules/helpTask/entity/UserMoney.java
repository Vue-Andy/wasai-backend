package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  用户钱包
 * @author fang 2020-05-14
 */
@Data
@TableName("user_money")
public class UserMoney implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 钱包id（本钱包只能充值，不可提现）
     */
    @TableId(type = IdType.INPUT)
    private Integer id;

    /**
     * 钱包金额  不可提现金额
     */
    private Double cannotMoney;

    /**
     * 钱包金额 可提现金额
     */
    private Double mayMoney;

    /**
     * 用户id
     */
    private Long userId;


    public UserMoney() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getCannotMoney() {
        return cannotMoney;
    }

    public void setCannotMoney(Double cannotMoney) {
        this.cannotMoney = cannotMoney;
    }

    public Double getMayMoney() {
        return mayMoney;
    }

    public void setMayMoney(Double mayMoney) {
        this.mayMoney = mayMoney;
    }
}
