package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *  help_task
 * @author 房 2020-05-14
 */
@Data
@TableName("help_task")
public class HelpTask implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 助力id
     */
    @TableId(type = IdType.INPUT)
    private long id;

    /**
     * 任务id
     */

    private Integer taskId;

    /**
     * 二级分类id
     */
    private Long classifyDetailsId;

    /**
     * 二级分类名称
     */
    @TableField(exist = false)
    private String classifyDetailsName;

    /**
     * 标题
     */

    private String title;

    /**
     * 说明
     */

    private String content;

    /**
     * 价格（原价）
     */

    private Double taskOriginalPrice;

    /**
     * 价格（平台扣除费用后价格）
     */

    private Double taskPrice;

    /**
     * 数量
     */

    private Integer taskNum;

    /**
     * 接单人数
     */

    private Integer endNum;

    /**
     * 图标
     */

    private String label;

    /**
     * 客户端分类
     */
    private Integer classify;

    /**
     * 标签
     */
    private String tag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 留言
     */
    private String leaveWord;

    /**
     * 打开方式
     */

    private String openType;

    /**
     * 打开内容
     */

    private String openContent;

    /**
     * 打开app分类
     */

    private String openApp;

    /**
     * 验证信息
     */

    private String verifyContent;

    /**
     * 创建时间
     */

    private String createTime;


    /**
     * 审核时间(按分)
     */

    private Integer auditTime;

    /**
     * 任务限时(按分)
     */

    private Integer restrictTime;

    /**
     * 截止时间
     */

    private String endTime;

    @TableField(exist = false)
    private Integer endTimes;

    @TableField(exist = false)
    private String helpTaskDetailss;

    @TableField(exist = false)
    private Long userId;

    @TableField(exist = false)
    private String nickName;

    @TableField(exist = false)
    private String imageUrl;

    @TableField(exist = false)
    private List<HelpTaskDetails> helpTaskDetailsList;

    /**
     * 状态（0待审核 1正常 2结束 3拒绝 4结算）
     */
    private Integer state;

    /**
     * 是否置顶
     */
    private Integer isTop;

    /**
     * 排序时间（每日可刷新）
     */
    private String orderTime;

    /**
     * 是否客多次接单
     */
//    private Integer is

}
