package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  help_classify
 * @author fang 2020-06-11
 */
@Data
@TableName("help_classify")
public class HelpClassify implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 一级分类id
     */
    @TableId(type = IdType.INPUT)
    private Long id;

    /**
     * 名称
     */
    private String classifyName;

    /**
     * 图标
     */
    private String classifyIcon;

    /**
     * 地址
     */
    private String classifyUrl;

    /**
     * 描述
     */
    private String describes;

    /**
     * 状态
     */

    private Integer state;

    /**
     * 排序
     */

    private String sort;


}
