package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fang
 * @date 2020/8/3
 */
@Data
@TableName("cash_classify")
public class CashClassify implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 提现分类id
     */
    private Long id;

    /**
     * 分类额度
     */
    private Double money;

    /**
     * 创建时间
     */
    private String createTime;


}