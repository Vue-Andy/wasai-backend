package com.sqx.modules.helpTask.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fang
 * @date 2020/7/7
 */
@Data
@TableName("help_profit")
public class HelpProfit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 收入id
     */
    @TableId(type = IdType.INPUT)
    private Long id;

    /**
     * 助力任务id
     */
    private Long helpTaskId;

    /**
     * 接单id
     */
    private Long helpSendOrderId;

    /**
     * 利润
     */
    private Double profit;

    /**
     * 时间
     */
    private String createTime;



}