package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.Result;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.dao.CashClassifyDao;
import com.sqx.modules.helpTask.dao.HelpTaskDao;
import com.sqx.modules.helpTask.dao.HelpTaskRefreshDao;
import com.sqx.modules.helpTask.dao.TimedTaskDao;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.CashClassifyService;
import com.sqx.modules.helpTask.service.HelpTaskRefreshService;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 任务刷新
 */
@Service
public class HelpTaskRefreshServiceImpl extends ServiceImpl<HelpTaskRefreshDao, HelpTaskRefresh> implements HelpTaskRefreshService {

    @Autowired
    private HelpTaskRefreshDao helpTaskRefreshDao;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    @Autowired
    private HelpTaskDao helpTaskDao;
    @Autowired
    private TimedTaskDao timedTaskDao;


    @Override
    public int insertHelpTaskRefresh(HelpTaskRefresh helpTaskRefresh){
        return helpTaskRefreshDao.insert(helpTaskRefresh);
    }

    @Override
    public int selectHelpTaskRefreshCount(Long userId,String time){
        int i = helpTaskRefreshDao.selectHelpTaskRefreshCount(userId, time);
        CommonInfo one = commonInfoService.findOne(103);
        int i1 = Integer.parseInt(one.getValue()) - i;
        return i1;
    }

    @Override
    public Result refreshTask(Long userId, Long helpTaskId){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(new Date());
        int i1 = helpTaskRefreshDao.selectHelpTaskRefreshByHelpTaskId(helpTaskId, date);
        if(i1>0){
            return Result.error("该任务今日已经刷新过了！");
        }
        int i = helpTaskRefreshDao.selectHelpTaskRefreshCount(userId, date);
        CommonInfo one = commonInfoService.findOne(103);
        if(i>=Integer.parseInt(one.getValue())){
            return Result.error("今日免费刷新任务的次数已经用完了！");
        }
        HelpTaskRefresh helpTaskRefresh=new HelpTaskRefresh();
        helpTaskRefresh.setUserId(userId);
        helpTaskRefresh.setHelpTaskId(helpTaskId);
        helpTaskRefresh.setCreateTime(date);
        insertHelpTaskRefresh(helpTaskRefresh);
        HelpTask helpTask=new HelpTask();
        helpTask.setId(helpTaskId);
        helpTask.setOrderTime(date);
        helpTaskDao.updateById(helpTask);
        return Result.success();
    }

    @Override
    public Result refreshTaskMoney(Long userId, Long helpTaskId){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(new Date());
        int i1 = helpTaskRefreshDao.selectHelpTaskRefreshByHelpTaskId(helpTaskId, date);
        if(i1>0){
            return Result.error("该任务今日已经刷新过了！");
        }
        CommonInfo one = commonInfoService.findOne(105);
        UserMoney userMoney = userMoneyService.selectByUserId(userId);
        Double price = Double.parseDouble(one.getValue());
        if(userMoney.getCannotMoney()>=price){
            userMoneyService.updateCannotMoney(2,userId,price);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("[刷新任务]派单ID："+helpTaskId);
            userMoneyDetails.setContent("刷新任务扣除金额："+price);
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(price);
            userMoneyDetails.setCreateTime(date);
            userMoneyDetailsService.insert(userMoneyDetails);
        }else{
            double add = AmountCalUtils.add(userMoney.getCannotMoney(), userMoney.getMayMoney());
            if(add>=price){
                Double sub = AmountCalUtils.sub(price, userMoney.getCannotMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userId);
                userMoneyDetails.setTitle("[刷新任务]派单ID："+helpTaskId);
                if(userMoney.getCannotMoney()>0){
                    userMoneyService.updateCannotMoney(2,userId,userMoney.getCannotMoney());
                    userMoneyDetails.setContent("刷新任务扣除金额："+userMoney.getCannotMoney());
                    userMoneyDetails.setType(2);
                    userMoneyDetails.setMoney(userMoney.getCannotMoney());
                    userMoneyDetails.setCreateTime(date);
                    userMoneyDetailsService.insert(userMoneyDetails);
                }
                userMoneyService.updateMayMoney(2,userId,sub);
                userMoneyDetails.setContent("刷新任务，不可提现金额不足，可提现金额扣除金额："+sub);
                userMoneyDetails.setMoney(sub);
                userMoneyDetailsService.insert(userMoneyDetails);
            }else{
                return Result.error("账户金额不足，请充值！");
            }
        }
        HelpTaskRefresh helpTaskRefresh=new HelpTaskRefresh();
        helpTaskRefresh.setUserId(userId);
        helpTaskRefresh.setHelpTaskId(helpTaskId);
        helpTaskRefresh.setCreateTime(date);
        insertHelpTaskRefresh(helpTaskRefresh);
        HelpTask helpTask=new HelpTask();
        helpTask.setId(helpTaskId);
        helpTask.setOrderTime(date);
        helpTaskDao.updateById(helpTask);
        return Result.success();
    }

    @Override
    public Result topHelpTask(Long userId,Long helpTaskId,Integer time){
        HelpTask helpTask = helpTaskDao.selectById(helpTaskId);
        if(helpTask==null){
            return Result.error("任务失效！");
        }
        if(helpTask.getIsTop()==1){
            return Result.error("该任务现在正在置顶中！");
        }
        UserMoney userMoney = userMoneyService.selectByUserId(userId);
        CommonInfo one = commonInfoService.findOne(104);
        double v = Double.parseDouble(one.getValue());
        Double mul = AmountCalUtils.mul(v, time);
        DecimalFormat formater = new DecimalFormat();
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(0);
        formater.setRoundingMode(RoundingMode.FLOOR);
        mul=Double.parseDouble(formater.format(mul));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(new Date());
        if(userMoney.getCannotMoney()>=mul){
            userMoneyService.updateCannotMoney(2,userId,mul);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("[置顶任务]派单ID："+helpTaskId);
            userMoneyDetails.setContent("置顶任务扣除金额："+mul);
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(mul);
            userMoneyDetails.setCreateTime(date);
            userMoneyDetailsService.insert(userMoneyDetails);
        }else{
            double add = AmountCalUtils.add(userMoney.getCannotMoney(), userMoney.getMayMoney());
            if(add>=mul){
                Double sub = AmountCalUtils.sub(mul, userMoney.getCannotMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userId);
                userMoneyDetails.setTitle("[置顶任务]派单ID："+helpTaskId);
                if(userMoney.getCannotMoney()>0){
                    userMoneyService.updateCannotMoney(2,userId,userMoney.getCannotMoney());
                    userMoneyDetails.setContent("置顶任务扣除金额："+userMoney.getCannotMoney());
                    userMoneyDetails.setType(2);
                    userMoneyDetails.setMoney(userMoney.getCannotMoney());
                    userMoneyDetails.setCreateTime(date);
                    userMoneyDetailsService.insert(userMoneyDetails);
                }
                userMoneyService.updateMayMoney(2,userId,sub);
                userMoneyDetails.setContent("置顶任务，不可提现金额不足，可提现金额扣除金额："+sub);
                userMoneyDetails.setMoney(sub);
                userMoneyDetails.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetails);
            }else{
                return Result.error("账户金额不足，请充值！");
            }
        }
            helpTask.setIsTop(1);
            helpTaskDao.updateById(helpTask);
            TimedTask newTimedTask=new TimedTask();
            newTimedTask.setType(5);
            newTimedTask.setObjectId(helpTaskId);
            newTimedTask.setTitle("置顶任务结束");
            newTimedTask.setCreateTime(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR,time);
            newTimedTask.setEndTime(cal.getTime());
            timedTaskDao.insert(newTimedTask);
        return Result.success();
    }




}
