package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.HelpMaintainDetailsDao;
import com.sqx.modules.helpTask.entity.HelpMaintainDetails;
import com.sqx.modules.helpTask.service.HelpMaintainDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 维权详细信息
 */
@Service
public class HelpMaintainDetailsServiceImpl extends ServiceImpl<HelpMaintainDetailsDao, HelpMaintainDetails> implements HelpMaintainDetailsService {

    /** 维权详细信息 */
    private final HelpMaintainDetailsDao helpMaintainDetailsDao;

    @Autowired
    public HelpMaintainDetailsServiceImpl(HelpMaintainDetailsDao helpMaintainDetailsDao) {
        this.helpMaintainDetailsDao = helpMaintainDetailsDao;
    }

    @Override
    public List<HelpMaintainDetails> selectByHelpMaintainId(Long helpMaintainId){
        return helpMaintainDetailsDao.selectByHelpMaintainId(helpMaintainId);
    }


}