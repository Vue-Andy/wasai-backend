package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.UserMoney;


public interface UserMoneyService {

    Result sendMoney(int integralNum,double money);

    UserMoney selectByUserId(Long userId);

    Integer insert(UserMoney userMoney);

    Integer updateCannotMoney(int type, Long userId, Double money);

    Integer updateMayMoney(int type, Long userId, Double money);

    Result cashMoney(Long userId, Double money);

    Result openMember(Long userId,Integer member);


}
