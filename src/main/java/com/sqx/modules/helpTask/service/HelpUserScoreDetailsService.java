package com.sqx.modules.helpTask.service;

import com.sqx.modules.helpTask.entity.HelpUserScoreDetails;

import java.util.List;


public interface HelpUserScoreDetailsService {

    List<HelpUserScoreDetails> selectHelpUserScoreDetailsList(int page,int limit,Long userId);

    int insert(HelpUserScoreDetails helpUserScoreDetails);


}
