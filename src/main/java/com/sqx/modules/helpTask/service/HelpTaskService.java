package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpTask;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface HelpTaskService {

    Result quicknessHelpTask(Long helpTakeId);

    Result cancellation(Long helpTakeId);

    Result cancellations(Long helpTaskId,String content);

    Result selectHelpTaskDetails(Long id,Long userId);

    Result helpTaskAddNum(Long helpTakeId,Integer num);

    Result helpTaskAddPrice(Long helpTakeId, Double price);

    List<HelpTask> selectRecommendHelpTask();

    PageUtils selectHelpTaskListByPhone(int page,int limit,Integer state,String sort,String phone);

    PageUtils selectHelpTaskByClassify(Map<String, Object> params);

    PageUtils selectByTitle(Map<String, Object> params);

    HelpTask selectById(Long Id);

    Result saveBody(HelpTask helpTask);

    Result saveHelpTask(HelpTask helpTask, String HelpTaskDetails, Long userId, Double money);


    int updateHelpTask(String helpTakeId, Integer state, String content);

    PageUtils selectMyHelpTask(int page,int limit,Integer state, Long userId);

    PageUtils selectParticipationHelpTask(int page,int limit,Integer state, Long userId);

    Result selectHelpTaskList(int page,int limit,Integer state,Integer sort,Long userId);

    Result selectHelpTaskListV1(int page,int limit,Integer state,Integer sort,Long userId,Integer desc,Integer classifyId,Integer classifyDetailsId);

    Map<String,Object> getRate(Long userId);

    PageUtils selectHelpTaskListByClassifyId(int page,int limit,Long classifyId);

    PageUtils selectAuditHelpTask(int page,int limit,Integer state, Long userId);

    int finishHelpTask(HelpTask helpTask);

    Double sumPrice(String time, Integer flag);

    Integer countHelpTaskByCreateTime(String time, Integer flag);

    Result selectTopHelpTask(Integer page,Integer limit);

    Integer selectHelpTaskCountByTime(String time);

    Double selectHelpTaskSumPriceByTime(String time);

}
