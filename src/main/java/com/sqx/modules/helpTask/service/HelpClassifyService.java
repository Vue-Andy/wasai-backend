package com.sqx.modules.helpTask.service;


import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.helpTask.entity.HelpClassifyModel;

import java.util.List;

public interface HelpClassifyService {

    HelpClassify selectById(Long id);

    List<HelpClassify> selectList();

    int insert(HelpClassify helpClassify);

    void saveAll(List<HelpClassify> list);

    int update(HelpClassify helpClassify);

    void deleteById(Long id);

    void deleteByIdList(String ids);

    List<HelpClassifyModel> getHelpClassifyModel();

}
