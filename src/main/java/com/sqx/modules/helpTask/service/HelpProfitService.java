package com.sqx.modules.helpTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;

import java.util.Date;

public interface HelpProfitService {

    PageUtils selectHelpProfitList(int page, int limit, String createTime, String endTime);

    Double selectSumProfit(String createTime,String endTime);

    Result statistical();

    Double sumMoneyByProfit( String time, Integer flag);

    Double sumMemberMoneyByProfit(String time, Integer flag);

    PageUtils incomeAnalysis(int page, int limit, String time, Integer flag);

}
