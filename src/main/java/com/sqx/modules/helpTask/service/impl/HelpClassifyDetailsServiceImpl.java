package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.HelpClassifyDetailsDao;
import com.sqx.modules.helpTask.entity.HelpClassifyDetails;
import com.sqx.modules.helpTask.service.HelpClassifyDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 二级分类
 */
@Service
public class HelpClassifyDetailsServiceImpl extends ServiceImpl<HelpClassifyDetailsDao, HelpClassifyDetails> implements HelpClassifyDetailsService {

    private final HelpClassifyDetailsDao helpClassifyDetailsDao;

    @Autowired
    public HelpClassifyDetailsServiceImpl(HelpClassifyDetailsDao helpClassifyDetailsDao) {
        this.helpClassifyDetailsDao = helpClassifyDetailsDao;
    }


    @Override
    public List<HelpClassifyDetails> selectList(Long classifyId) {
        return helpClassifyDetailsDao.selectListByClassifyId(classifyId);
    }

    @Override
    public List<HelpClassifyDetails> selectList(String classifyName) {
        return helpClassifyDetailsDao.selectListByClassifyName(classifyName);
    }

    @Override
    public List<HelpClassifyDetails> selectList() {
        return helpClassifyDetailsDao.selectList();
    }

    @Override
    public HelpClassifyDetails selectById(Long id) {
        return helpClassifyDetailsDao.selectById(id);
    }

    @Override
    public int insert(HelpClassifyDetails helpClassifyDetails) {
        return helpClassifyDetailsDao.insert(helpClassifyDetails);
    }

    @Override
    public void saveAll(List<HelpClassifyDetails> list) {
        for(HelpClassifyDetails helpClassifyDetails:list){
            helpClassifyDetailsDao.insert(helpClassifyDetails);
        }
    }

    @Override
    public int updateHelpClassifyDetails(HelpClassifyDetails helpClassifyDetails) {
        return helpClassifyDetailsDao.updateById(helpClassifyDetails);
    }

    @Override
    public void deleteById(Long id) {
        helpClassifyDetailsDao.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByIdList(String ids) {
        String []id=ids.split(",");
        for(String i:id){
            helpClassifyDetailsDao.deleteById(Long.parseLong(i));
        }

    }

}
