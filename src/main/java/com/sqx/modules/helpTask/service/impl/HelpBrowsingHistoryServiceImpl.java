package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.helpTask.dao.HelpBrowsingHistoryDao;
import com.sqx.modules.helpTask.entity.HelpBrowsingHistory;
import com.sqx.modules.helpTask.service.HelpBrowsingHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * 浏览记录
 */
@Service
public class HelpBrowsingHistoryServiceImpl extends ServiceImpl<HelpBrowsingHistoryDao, HelpBrowsingHistory> implements HelpBrowsingHistoryService{

    /** 浏览记录 */
    private final HelpBrowsingHistoryDao helpBrowsingHistoryDao;

    @Autowired
    public HelpBrowsingHistoryServiceImpl(HelpBrowsingHistoryDao helpBrowsingHistoryDao) {
        this.helpBrowsingHistoryDao = helpBrowsingHistoryDao;
    }


    @Override
    public int insert(HelpBrowsingHistory helpBrowsingHistory) {
        return helpBrowsingHistoryDao.insert(helpBrowsingHistory);
    }

    @Override
    public PageUtils selectHelpBrowsingHistoryList(int page, int limit, Long userId) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpBrowsingHistoryDao.selectHelpBrowsingHistoryList(pages,userId));
    }

    @Override
    public Long selectHelpBrowsingHistoryByUserId(Long userId, Long helpTaskId) {
        return helpBrowsingHistoryDao.selectHelpBrowsingHistoryByUserId(userId,helpTaskId);
    }

    @Override
    public int updateCreateTimeByUserId(Long helpBrowsingId, Date createTime) {
        return helpBrowsingHistoryDao.updateCreateTimeByUserId(helpBrowsingId,createTime);
    }
}
