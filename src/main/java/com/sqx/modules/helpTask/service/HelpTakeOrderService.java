package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpTakeOrder;

import java.util.Date;

public interface HelpTakeOrderService {

    Result saveBody(HelpTakeOrder helpTakeOrder);

    HelpTakeOrder selectHelpTakeOrderByHelpTaskId(Long helpTaskId);

    Integer selectCountByHelpTaskIdAndUserId(Long helpTaskId, Long userId);

    HelpTakeOrder selectById(Long helpTakeOrderId);

    Integer selectHelpTakeOrderCount(Long userId, Date startTime, Date endTime);

    int updateHelpTakeOrder(HelpTakeOrder helpTakeOrder);

    /**
     * 今日派单数量
     * @return
     */
    Result takeOrder();
}
