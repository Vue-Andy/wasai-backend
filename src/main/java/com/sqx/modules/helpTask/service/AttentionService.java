package com.sqx.modules.helpTask.service;


import com.sqx.common.utils.Result;

public interface AttentionService {

    Result insertAttention(Long userId, Long byUserId);

    Result selectAttentionList(Integer page,Integer limit,Long userId);

    Result selectHelpTaskListByAttention(Integer page,Integer limit,Long userId);

    Result selectHelpTaskListByUserId(Integer page,Integer limit,Long userId,Long byUserId);

}
