package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.dao.HelpProfitDao;
import com.sqx.modules.helpTask.dao.HelpSendOrderDao;
import com.sqx.modules.helpTask.dao.HelpTaskDao;
import com.sqx.modules.helpTask.dao.TimedTaskDao;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.message.dao.MessageInfoDao;
import com.sqx.modules.message.entity.MessageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 接单信息
 */
@Service
public class HelpSendOrderServiceImpl extends ServiceImpl<HelpSendOrderDao, HelpSendOrder> implements HelpSendOrderService {

    /** 接单信息 */
    @Autowired
    private HelpSendOrderDao helpSendOrderDao;
    /** 助力信息自定义方法类 */
    @Autowired
    private HelpTaskDao helpTaskDao;
    /** 定时任务 */
    @Autowired
    private TimedTaskDao timedTaskDao;
    /** 用户钱包 */
    @Autowired
    private UserMoneyService userMoneyService;
    /** 用户钱包明细 */
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    /** 信誉分 */
    @Autowired
    private HelpUserScoreService helpUserScoreService;
    /** 信誉明细分 */
    @Autowired
    private HelpUserScoreDetailsService helpUserScoreDetailsService;
    @Autowired
    private HelpProfitDao helpProfitDao;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageInfoDao messageInfoDao;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



    @Override
    public HelpSendOrder selectByHelpTaskId(Long helpTaskId,Long userId) {
        return helpSendOrderDao.selectByHelpTaskIdAndUserId(helpTaskId,userId);
    }

    @Override
    public int selectHelpTaskCountByUserIdAndTime(Date time, Long userId) {
        return helpSendOrderDao.selectHelpTaskCountByUserIdAndTime(time,userId);
    }

    @Override
    public Integer selectCountByHelpTaskIdAndUserId(Long helpTaskId, Long userId) {
        return helpSendOrderDao.selectCountByHelpTaskIdAndUserId(helpTaskId,userId);
    }

    @Transactional
    @Override
    public Integer savebody(HelpSendOrder helpSendOrder) {
        helpSendOrder.setState(0);
        helpSendOrder.setCreateTime(sdf.format(new Date()));
        //添加接单信息
        helpSendOrderDao.insert(helpSendOrder);
        //助力活动报名人数加一
        helpTaskDao.updateHelpTaskNum(helpSendOrder.getHelpTaskId());
        //添加定时任务
        HelpTask helpTask=helpTaskDao.selectById(helpSendOrder.getHelpTaskId());
        TimedTask timedTask=new TimedTask();
        timedTask.setType(2);
        timedTask.setObjectId(helpSendOrder.getId());
        timedTask.setTitle("接单助力活动任务定时");
        timedTask.setCreateTime(new Date());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE,helpTask.getRestrictTime());
        timedTask.setEndTime(cal.getTime());
        timedTaskDao.insert(timedTask);
        return 1;
    }




    @Override
    public HelpSendOrder selectById(Long helpSendOrderId) {
        return helpSendOrderDao.selectById(helpSendOrderId);
    }

    @Override
    @Transactional
    public Integer updateHelpSendOrder(HelpSendOrder helpSendOrder,HelpSendOrder oldHelpSendOrder){
        String date=sdf.format(new Date());
        UserEntity userByWxId = userService.selectById(oldHelpSendOrder.getUserId());
        MessageInfo messageInfo = new MessageInfo();
        if(helpSendOrder.getState()==2){//同意
            //给接单人增加金额
            HelpTask helpTask= helpTaskDao.selectById(oldHelpSendOrder.getHelpTaskId());
            userMoneyService.updateMayMoney(1,oldHelpSendOrder.getUserId(),oldHelpSendOrder.getMoney());
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(oldHelpSendOrder.getUserId());
            userMoneyDetails.setTitle("[完成任务]任务ID："+helpTask.getId());
            userMoneyDetails.setContent("增加金额:"+oldHelpSendOrder.getMoney());
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(oldHelpSendOrder.getMoney());
            userMoneyDetails.setCreateTime(date);
            userMoneyDetailsService.insert(userMoneyDetails);

            //完成接单增加信誉分
            helpUserScoreService.updateUserScore(oldHelpSendOrder.getUserId(),1,1);
            HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
            helpUserScoreDetails.setUserId(oldHelpSendOrder.getUserId());
            helpUserScoreDetails.setTitle("[完成任务]任务Id："+oldHelpSendOrder.getHelpTaskId());
            helpUserScoreDetails.setContent("增加积分：1");
            helpUserScoreDetails.setCreateTime(date);
            helpUserScoreDetails.setScore(1);
            helpUserScoreDetails.setType(1);
            helpUserScoreDetailsService.insert(helpUserScoreDetails);
            if(oldHelpSendOrder.getZhiUserId()!=null && oldHelpSendOrder.getZhiUserId()!=0L){
                userMoneyService.updateMayMoney(1,oldHelpSendOrder.getZhiUserId(),oldHelpSendOrder.getZhiMoney());
                UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                userMoneyDetail.setUserId(oldHelpSendOrder.getZhiUserId());
                userMoneyDetail.setTitle("[直属赏金]任务ID："+helpTask.getId());
                userMoneyDetail.setContent("直属用户："+userByWxId.getNickName()+"完成任务，增加金额:"+oldHelpSendOrder.getZhiMoney());
                userMoneyDetail.setType(1);
                userMoneyDetail.setMoney(oldHelpSendOrder.getZhiMoney());
                userMoneyDetail.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetail);
            }
            if(oldHelpSendOrder.getFeiUserId()!=null && oldHelpSendOrder.getFeiUserId()!=0L){
                userMoneyService.updateMayMoney(1,oldHelpSendOrder.getFeiUserId(),oldHelpSendOrder.getFeiMoney());
                UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                userMoneyDetail.setUserId(oldHelpSendOrder.getFeiUserId());
                userMoneyDetail.setTitle("[非直属赏金]任务ID："+helpTask.getId());
                userMoneyDetail.setContent("非直属用户："+userByWxId.getNickName()+"完成任务，增加金额:"+oldHelpSendOrder.getFeiMoney());
                userMoneyDetail.setType(1);
                userMoneyDetail.setMoney(oldHelpSendOrder.getFeiMoney());
                userMoneyDetail.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetail);
            }
            //完成一单之后给平台计算利润
            HelpProfit helpProfit=new HelpProfit();
            helpProfit.setCreateTime(date);
            helpProfit.setHelpTaskId(helpTask.getId());
            helpProfit.setHelpSendOrderId(oldHelpSendOrder.getId());
            helpProfit.setProfit(oldHelpSendOrder.getPingMoney());
            helpProfitDao.insert(helpProfit);

            messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已通过！");
            if (userByWxId.getClientid() != null) {
                userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已通过！", userByWxId.getClientid());
            }
        }else{
            //拒绝原因
            if(helpSendOrder.getCategory()!=null && helpSendOrder.getCategory()==1){//错误截图  扣除2分
                //扣除信誉分
                helpUserScoreService.updateUserScore(oldHelpSendOrder.getUserId(),2,2);
                HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                helpUserScoreDetails.setUserId(oldHelpSendOrder.getUserId());
                helpUserScoreDetails.setTitle("[审核失败]任务Id："+oldHelpSendOrder.getHelpTaskId());
                helpUserScoreDetails.setContent("错误截图扣除：2");
                helpUserScoreDetails.setCreateTime(date);
                helpUserScoreDetails.setScore(2);
                helpUserScoreDetails.setType(2);
                helpUserScoreDetailsService.insert(helpUserScoreDetails);
                messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已拒绝！");
                if (userByWxId.getClientid() != null) {
                    userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已拒绝！", userByWxId.getClientid());
                }
            }else if(helpSendOrder.getCategory()!=null && helpSendOrder.getCategory()==2){//伪造截图  扣除3分
                //扣除信誉分
                helpUserScoreService.updateUserScore(oldHelpSendOrder.getUserId(),2,3);
                HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                helpUserScoreDetails.setUserId(oldHelpSendOrder.getUserId());
                helpUserScoreDetails.setTitle("[审核失败]任务Id："+oldHelpSendOrder.getHelpTaskId());
                helpUserScoreDetails.setContent("伪造截图扣除：3");
                helpUserScoreDetails.setCreateTime(date);
                helpUserScoreDetails.setScore(3);
                helpUserScoreDetails.setType(2);
                helpUserScoreDetailsService.insert(helpUserScoreDetails);
                messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已拒绝！");
                if (userByWxId.getClientid() != null) {
                    userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已拒绝！", userByWxId.getClientid());
                }
            }
        }
        messageInfo.setState(String.valueOf(6));
        messageInfo.setTitle("接单任务");
        messageInfo.setUserName(userByWxId.getNickName());
        messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        messageInfo.setCreateAt(sdf.format(now));
        messageInfoDao.insert(messageInfo);

        //删除定时任务
        timedTaskDao.deleteByObjectId(helpSendOrder.getId(),3);
        return helpSendOrderDao.updateHelpSendOrder(helpSendOrder.getId(),helpSendOrder.getState(),helpSendOrder.getAuditContent(),helpSendOrder.getCategory());
    }

    @Override
    @Transactional
    public Result updateHelpSendOrders(Long userId,Integer status,Integer category){
        List<Long> longs = helpTaskDao.selectAuditHelpTasks(userId);
        for(Long id:longs){
            MessageInfo messageInfo = new MessageInfo();
            String date=sdf.format(new Date());
            HelpSendOrder helpSendOrder = helpSendOrderDao.selectById(id);
            helpSendOrder.setState(status);
            UserEntity userByWxId = userService.selectById(helpSendOrder.getUserId());
            if(status==2){//同意
                //给接单人增加金额
                HelpTask helpTask= helpTaskDao.selectById(helpSendOrder.getHelpTaskId());
                userMoneyService.updateMayMoney(1,helpSendOrder.getUserId(),helpSendOrder.getMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(helpSendOrder.getUserId());
                userMoneyDetails.setTitle("[完成任务]任务ID："+helpTask.getId());
                userMoneyDetails.setContent("增加金额:"+helpSendOrder.getMoney());
                userMoneyDetails.setType(1);
                userMoneyDetails.setMoney(helpSendOrder.getMoney());
                userMoneyDetails.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetails);

                //完成接单增加信誉分
                helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),1,1);
                HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
                helpUserScoreDetails.setTitle("[完成任务]任务Id："+helpSendOrder.getHelpTaskId());
                helpUserScoreDetails.setContent("增加积分：1");
                helpUserScoreDetails.setCreateTime(date);
                helpUserScoreDetails.setScore(1);
                helpUserScoreDetails.setType(1);
                helpUserScoreDetailsService.insert(helpUserScoreDetails);
                if(helpSendOrder.getZhiUserId()!=null && helpSendOrder.getZhiUserId()!=0L){
                    userMoneyService.updateMayMoney(1,helpSendOrder.getZhiUserId(),helpSendOrder.getZhiMoney());
                    UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                    userMoneyDetail.setUserId(helpSendOrder.getZhiUserId());
                    userMoneyDetail.setTitle("[直属赏金]任务ID："+helpTask.getId());
                    userMoneyDetail.setContent("直属用户："+userByWxId.getNickName()+"完成任务，增加金额:"+helpSendOrder.getZhiMoney());
                    userMoneyDetail.setType(1);
                    userMoneyDetail.setMoney(helpSendOrder.getZhiMoney());
                    userMoneyDetail.setCreateTime(date);
                    userMoneyDetailsService.insert(userMoneyDetail);
                }
                if(helpSendOrder.getFeiUserId()!=null && helpSendOrder.getFeiUserId()!=0L){
                    userMoneyService.updateMayMoney(1,helpSendOrder.getFeiUserId(),helpSendOrder.getFeiMoney());
                    UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                    userMoneyDetail.setUserId(helpSendOrder.getFeiUserId());
                    userMoneyDetail.setTitle("[非直属赏金]任务ID："+helpTask.getId());
                    userMoneyDetail.setContent("非直属用户："+userByWxId.getNickName()+"完成任务，增加金额:"+helpSendOrder.getFeiMoney());
                    userMoneyDetail.setType(1);
                    userMoneyDetail.setMoney(helpSendOrder.getFeiMoney());
                    userMoneyDetail.setCreateTime(date);
                    userMoneyDetailsService.insert(userMoneyDetail);
                }
                //完成一单之后给平台计算利润
                HelpProfit helpProfit=new HelpProfit();
                helpProfit.setCreateTime(date);
                helpProfit.setHelpTaskId(helpTask.getId());
                helpProfit.setHelpSendOrderId(helpSendOrder.getId());
                helpProfit.setProfit(helpSendOrder.getPingMoney());
                helpProfitDao.insert(helpProfit);

                messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已通过！");
                if (userByWxId.getClientid() != null) {
                    userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已通过！", userByWxId.getClientid());
                }
            }else{
                helpSendOrder.setCategory(category);
                //拒绝原因
                if(category!=null && category==1){//错误截图  扣除2分
                    //扣除信誉分
                    helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),2,2);
                    HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                    helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
                    helpUserScoreDetails.setTitle("[审核失败]任务Id："+helpSendOrder.getHelpTaskId());
                    helpUserScoreDetails.setContent("错误截图扣除：2");
                    helpUserScoreDetails.setCreateTime(date);
                    helpUserScoreDetails.setScore(2);
                    helpUserScoreDetails.setType(2);
                    helpUserScoreDetailsService.insert(helpUserScoreDetails);
                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已拒绝！");
                    if (userByWxId.getClientid() != null) {
                        userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已拒绝！", userByWxId.getClientid());
                    }
                }else if(category!=null && category==2){//伪造截图  扣除3分
                    //扣除信誉分
                    helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),2,3);
                    HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                    helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
                    helpUserScoreDetails.setTitle("[审核失败]任务Id："+helpSendOrder.getHelpTaskId());
                    helpUserScoreDetails.setContent("伪造截图扣除：3");
                    helpUserScoreDetails.setCreateTime(date);
                    helpUserScoreDetails.setScore(3);
                    helpUserScoreDetails.setType(2);
                    helpUserScoreDetailsService.insert(helpUserScoreDetails);
                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已拒绝！");
                    if (userByWxId.getClientid() != null) {
                        userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已拒绝！", userByWxId.getClientid());
                    }
                }
            }
            messageInfo.setState(String.valueOf(6));
            messageInfo.setTitle("接单任务");
            messageInfo.setUserName(userByWxId.getNickName());
            messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            messageInfo.setCreateAt(sdf.format(now));
            messageInfoDao.insert(messageInfo);

            //删除定时任务
            timedTaskDao.deleteByObjectId(helpSendOrder.getId(),3);
            helpSendOrderDao.updateHelpSendOrder(helpSendOrder.getId(),helpSendOrder.getState(),helpSendOrder.getAuditContent(),helpSendOrder.getCategory());
        }
        return Result.success();
    }



    @Override
    public Integer update(Long id, Integer state){
        return helpSendOrderDao.update(id,state);
    }

    @Override
    public Integer selectResidueNum(Long helpTaskId,Integer state){
        return helpSendOrderDao.selectResidueNum(helpTaskId,state);
    }

    @Override
    public Integer endHelpSendOrder(Long helpSendOrderId){
        helpSendOrderDao.update(helpSendOrderId,5);
        timedTaskDao.deleteByObjectId(helpSendOrderId,3);
        HelpSendOrder helpSendOrder = helpSendOrderDao.selectById(helpSendOrderId);
        return helpTaskDao.updateHelpTaskNumSub(helpSendOrder.getHelpTaskId());
    }

    @Override
    public Integer selectEndNum(Long helpTaskId) {
        return helpSendOrderDao.selectEndNum(helpTaskId);
    }

    @Override
    public Integer selectHelpSendOrderCount(Long userId, Date startTime, Date endTime) {
        return helpSendOrderDao.selectHelpSendOrderCount(userId,startTime,endTime);
    }

    @Override
    public Result sendOrder() {
        Calendar cal = Calendar.getInstance();
        return Result.success().put("data", helpSendOrderDao.sendOrder(cal.getTime()));
    }



    @Override
    public Double sumMoneyBySend(String time, Integer flag) {
        return helpSendOrderDao.sumMoneyBySend(time,flag);
    }

    @Override
    public Integer countBySend(String time, Integer flag) {
        return helpSendOrderDao.countBySend(time,flag);
    }

    @Override
    public PageUtils selectSendOrderByTaskId(Long helpTaskId,int page,int limit) {
        Page<HelpSendOrder> pages=new Page<>(page,limit);
        return new PageUtils(helpSendOrderDao.selectSendOrderByTaskId(pages,helpTaskId));
    }

    @Override
    public PageUtils selectSendOrderList(int page, int limit,String phone,Integer state) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpSendOrderDao.selectSendOrderList(pages,phone,state));
    }


    @Override
    public Integer selectHelpSendOrderCountByTime(String time){
        return baseMapper.selectHelpSendOrderCountByTime(time);
    }

}