package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpTaskDetails;

import java.util.List;


public interface HelpTaskDetailsService {

    Result saveAll(List<HelpTaskDetails> HelpTaskDetails);

    List<HelpTaskDetails> selectByHelpTaskId(Long helpTaskId);
}
