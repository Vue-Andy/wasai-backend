package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.Config;
import com.sqx.modules.helpTask.dao.CashOutDao;
import com.sqx.modules.helpTask.entity.CashOut;
import com.sqx.modules.helpTask.service.CashOutService;
import com.sqx.modules.message.dao.MessageInfoDao;
import com.sqx.modules.message.entity.MessageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weixin.popular.api.MessageAPI;
import weixin.popular.bean.message.templatemessage.TemplateMessage;
import weixin.popular.bean.message.templatemessage.TemplateMessageItem;
import weixin.popular.bean.message.templatemessage.TemplateMessageResult;
import weixin.popular.support.TokenManager;

import javax.websocket.SendResult;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 提现申请记录
 */
@Service
public class CashOutServiceImpl extends ServiceImpl<CashOutDao, CashOut> implements CashOutService {

    /** 提现申请记录 */
    @Autowired
    private CashOutDao cashOutDao;
    /** 通用配置 */
    @Autowired
    private CommonInfoService commonInfoService;
    /** app用户 */
    @Autowired
    private UserService userService;
    @Autowired
    private MessageInfoDao messageInfoDao;


    @Override
    public PageUtils selectCashOutList(Map<String,Object> params){
        String zhifubaoName = (String)params.get("zhifubaoName");
        String zhifubao = (String)params.get("zhifubao");
        String userId = (String)params.get("userId");
        IPage<CashOut> page = this.page(
                new Query<CashOut>().getPage(params),
                new QueryWrapper<CashOut>()
                        .eq(StringUtils.isNotBlank(zhifubaoName),"zhifubao_name", zhifubaoName)
                        .eq(StringUtils.isNotBlank(zhifubao),"zhifubao", zhifubao)
                        .eq(StringUtils.isNotBlank(userId),"user_id", userId)
                        .orderByDesc("id")
        );
        return new PageUtils(page);
    }


    @Override
    public CashOut selectById(Long id){
        return cashOutDao.selectById(id);
    }

    @Override
    public int saveBody(CashOut cashOut){
        return cashOutDao.insert(cashOut);
    }


    @Override
    public int update(CashOut cashOut){
        return cashOutDao.updateById(cashOut);
    }



    @Override
    public void cashOutSuccess(String openId, String date, String money, String payWay, String url) {
        UserEntity userByWxId = userService.selectUserByOpenId(openId);
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setState(String.valueOf(5));
        messageInfo.setContent("您好，您的提现转账成功，请注意查收！提现金额【" + money + "元】！支付宝收款账号 " + payWay + "感谢您的使用！如有疑问请在公众号中发送您的问题联系客服");
        messageInfo.setTitle("提现成功通知");
        messageInfo.setUserName(userByWxId.getNickName());
        messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        messageInfo.setCreateAt(sdf.format(now));
        messageInfoDao.insert(messageInfo);
        if (userByWxId.getClientid() != null) {
            userService.pushToSingle("提现成功通知", "您好，您的提现转账成功，请注意查收！提现金额【" + money + "元】！支付宝收款账号 " + payWay + "感谢您的使用！如有疑问请在公众号中发送您的问题联系客服", userByWxId.getClientid());
        }
        CommonInfo three = commonInfoService.findOne(39);
        String apkey ="";
        if (three != null) {
            apkey = three.getValue();
        }
        LinkedHashMap<String, TemplateMessageItem> data = new LinkedHashMap<>();
        data.put("first", new TemplateMessageItem("您好，您的提现转账成功，请注意查收", "#d71345"));
        data.put("keyword1", new TemplateMessageItem(money + " 元", "#d71345"));
        data.put("keyword2", new TemplateMessageItem(date, "#d71345"));
        data.put("remark", new TemplateMessageItem("支付宝收款账号 " + payWay + "感谢您的使用！如有疑问请在公众号中发送您的问题联系客服", null));
        sendWxMessage(apkey, data, openId, url);
    }

    /**
     * 退款成功通知
     *
     * @param
     * @param date
     * @param money
     * @param url
     */
    @Override
    public void refundSuccess(UserEntity userByWxId, String date, String money, String url,String content) {
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setState(String.valueOf(5));
        messageInfo.setContent(content);
        messageInfo.setTitle("提现失败提醒");
        messageInfo.setUserName(userByWxId.getNickName());
        messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        messageInfo.setCreateAt(sdf.format(now));
        messageInfoDao.insert(messageInfo);
        if (userByWxId.getClientid() != null) {
            userService.pushToSingle("提现失败提醒", content, userByWxId.getClientid());
        }
        CommonInfo three = commonInfoService.findOne(77);
        String apkey ="";
        if (three != null) {
            apkey = three.getValue();
        }
        LinkedHashMap<String, TemplateMessageItem> data = new LinkedHashMap<>();
        data.put("first", new TemplateMessageItem("您好，您发起的提现失败了", "#d71345"));
        data.put("keyword1", new TemplateMessageItem(money + " 元", "#d71345"));
        data.put("keyword2", new TemplateMessageItem(date, "#d71345"));
        data.put("keyword3", new TemplateMessageItem(content, "#d71345"));
        data.put("remark", new TemplateMessageItem("请您按照失败原因修改相关信息后，重新提现！", null));
        sendWxMessage(apkey, data, userByWxId.getOpenId(), url);
    }

    @Override
    public Double selectCashOutSum(Long userId, Date startTime, Date endTime) {
        return cashOutDao.selectCashOutSum(userId,startTime,endTime);
    }

    @Override
    public Double sumMoney(String time, Integer flag) {
        return cashOutDao.sumMoney(time,flag);
    }

    @Override
    public Integer countMoney(String time, Integer flag) {
        return cashOutDao.countMoney(time,flag);
    }

    @Override
    public Integer stayMoney(String time, Integer flag) {
        return cashOutDao.stayMoney(time,flag);
    }


    @Override
    public List<CashOut> selectCashOutLimit3() {
        return cashOutDao.selectCashOutLimit3();
    }

    private void sendWxMessage(String templateId, LinkedHashMap<String, TemplateMessageItem> data, String openid, String url) {
        TemplateMessage templateMessage = new TemplateMessage();
        templateMessage.setTouser(openid);
        templateMessage.setTemplate_id(templateId);
        templateMessage.setData(data);
        templateMessage.setUrl(url);
        TemplateMessageResult templateMessageResult = MessageAPI.messageTemplateSend(getWxToken(), templateMessage);
        if (templateMessageResult.isSuccess()) {
            new SendResult();
        } else {
            new SendResult();
        }
    }

    private String getWxToken() {
        try {
            //微信appid
            CommonInfo one = commonInfoService.findOne(5);
            return TokenManager.getToken( one.getValue() );
        } catch (Exception e) {
            throw new RuntimeException("GET_ACCESS_TOKEN_FAIL");
        }
    }





}
