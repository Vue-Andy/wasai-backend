package com.sqx.modules.helpTask.service;


import com.sqx.modules.helpTask.entity.HelpMaintainDetails;

import java.util.List;

public interface HelpMaintainDetailsService {

    List<HelpMaintainDetails> selectByHelpMaintainId(Long helpMaintainId);


}
