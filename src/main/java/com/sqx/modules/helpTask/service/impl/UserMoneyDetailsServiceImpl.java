package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.modules.helpTask.dao.UserMoneyDetailsDao;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 用户信誉分数明细
 */
@Service
public class UserMoneyDetailsServiceImpl extends ServiceImpl<UserMoneyDetailsDao, UserMoneyDetails> implements UserMoneyDetailsService {

    /** 用户金额明细 */
    private final UserMoneyDetailsDao userMoneyDetailsDao;

    @Autowired
    public UserMoneyDetailsServiceImpl(UserMoneyDetailsDao userMoneyDetailsDao) {
        this.userMoneyDetailsDao = userMoneyDetailsDao;
    }


    @Override
    public PageUtils selectUserMoneyDetailsList(Map<String, Object> params) {
        String paramKey = (String)params.get("userId");
        IPage<UserMoneyDetails> page = this.page(
                new Query<UserMoneyDetails>().getPage(params),
                new QueryWrapper<UserMoneyDetails>()
                        .eq(StringUtils.isNotBlank(paramKey),"user_id", paramKey).orderByDesc("id")
        );
        return new PageUtils(page);
    }

    @Override
    public Integer insert(UserMoneyDetails userMoneyDetails) {
        userMoneyDetailsDao.insert(userMoneyDetails);
        return 1;
    }

}