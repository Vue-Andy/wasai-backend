package com.sqx.modules.helpTask.service;


import com.sqx.modules.helpTask.entity.CashClassify;

import java.util.List;

public interface CashClassifyService {

    int saveBody(CashClassify cashClassify);

    int updateClassify(CashClassify cashClassify);

    int deleteClassifyById(Long id);

    int deleteClassifyByIds(String ids);

    List<CashClassify> selectCashClassifyList();

}
