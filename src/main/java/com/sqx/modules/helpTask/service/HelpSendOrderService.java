package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpSendOrder;
import org.apache.ibatis.annotations.Param;

import java.util.Date;


public interface HelpSendOrderService {

    HelpSendOrder selectByHelpTaskId(Long helpTaskId, Long userId);

    int selectHelpTaskCountByUserIdAndTime(Date time,Long userId);

    Integer selectCountByHelpTaskIdAndUserId(Long helpTaskId, Long userId);

    Integer savebody(HelpSendOrder helpSendOrder);


    HelpSendOrder selectById(Long helpSendOrderId);

    Integer updateHelpSendOrder(HelpSendOrder helpSendOrder, HelpSendOrder oldHelpSendOrder);

    Result updateHelpSendOrders(Long userId,Integer status,Integer category);

    Integer update(Long id, Integer state);

    Integer selectResidueNum(Long helpTaskId, Integer state);

    Integer endHelpSendOrder(Long helpSendOrderId);

    Integer selectEndNum(Long helpTaskId);

    Integer selectHelpSendOrderCount(Long userId, Date startTime,Date endTime);

    /**
     * 今日接单数量
     * @param time
     * @return
     */
    Result sendOrder();

    Double sumMoneyBySend(String time,Integer flag);

    Integer countBySend(String time,Integer flag);

    PageUtils selectSendOrderByTaskId(Long helpTaskId, int page,int limit);

    PageUtils selectSendOrderList(int page,int limit,String phone,Integer state);

    Integer selectHelpSendOrderCountByTime(String time);

}
