package com.sqx.modules.helpTask.service.impl;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConstants;
import com.github.wxpay.sdk.WXPayUtil;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.Config;
import com.sqx.modules.helpTask.config.WXConfig;
import com.sqx.modules.helpTask.dao.PayDetailsDao;
import com.sqx.modules.helpTask.entity.PayDetails;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.service.WxService;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.helpTask.utils.MD5Util;
import com.sqx.modules.helpTask.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fang
 * @date 2020/2/26
 */
@Service
public class WxServiceImpl implements WxService {
    private static final String SPBILL_CREATE_IP = "127.0.0.1";
    private static final String TRADE_TYPE_APP = "APP";
    private static final String TRADE_TYPE_NATIVE = "NATIVE";
    private static final String TRADE_TYPE_JSAPI = "JSAPI";

    private final Logger log = LoggerFactory.getLogger(WxServiceImpl.class);

    private final PayDetailsDao payDetailsDao;
    private final UserMoneyService userMoneyService;
    private final UserMoneyDetailsService userMoneyDetailsService;
    private final CommonInfoService commonInfoService;
    @Autowired
    private UserService userService;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    public WxServiceImpl(PayDetailsDao payDetailsDao, UserMoneyService userMoneyService, UserMoneyDetailsService userMoneyDetailsService, CommonInfoService commonInfoService) {
        this.payDetailsDao = payDetailsDao;
        this.userMoneyService = userMoneyService;
        this.userMoneyDetailsService = userMoneyDetailsService;
        this.commonInfoService = commonInfoService;
    }

    @Override
    public String payBack(String resXml,Integer type) {
        WXConfig config = null;
        try {
            config = new WXConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(type==1){
            config.setAppId(commonInfoService.findOne(74).getValue());
        }else if(type==2){
            config.setAppId(commonInfoService.findOne(5).getValue());
        }else{
            config.setAppId(commonInfoService.findOne(45).getValue());
        }
        config.setKey(commonInfoService.findOne(75).getValue());
        config.setMchId(commonInfoService.findOne(76).getValue());
        WXPay wxpay = new WXPay(config);
        String xmlBack = "";
        Map<String, String> notifyMap = null;
        try {
            notifyMap = WXPayUtil.xmlToMap(resXml);         // 调用官方SDK转换成map类型数据
            if (wxpay.isPayResultNotifySignatureValid(notifyMap)) {//验证签名是否有效，有效则进一步处理

                String return_code = notifyMap.get("return_code");//状态
                String out_trade_no = notifyMap.get("out_trade_no");//商户订单号
                if (return_code.equals("SUCCESS")) {
                    if (out_trade_no != null) {
                        // 注意特殊情况：订单已经退款，但收到了支付结果成功的通知，不应把商户的订单状态从退款改成支付成功
                        // 注意特殊情况：微信服务端同样的通知可能会多次发送给商户系统，所以数据持久化之前需要检查是否已经处理过了，处理了直接返回成功标志
                        //业务数据持久化
                        PayDetails payDetails=payDetailsDao.selectByOrderId(out_trade_no);
                        if(payDetails.getState()==0){
                            payDetailsDao.updateState(payDetails.getId(),1,new Date());
                            if(payDetails.getType()==1 || payDetails.getType()==3){
                                userMoneyService.updateCannotMoney(1,payDetails.getUserId(),payDetails.getMoney());
                                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                                userMoneyDetails.setUserId(payDetails.getUserId());
                                userMoneyDetails.setTitle("充值："+payDetails.getMoney());
                                userMoneyDetails.setContent("微信充值金额："+payDetails.getMoney());
                                userMoneyDetails.setType(0);
                                userMoneyDetails.setMoney(payDetails.getMoney());
                                userMoneyDetails.setCreateTime(sdf.format(new Date()));
                                userMoneyDetailsService.insert(userMoneyDetails);
                            }else{
                                UserEntity user=new UserEntity();
                                user.setUserId(payDetails.getUserId());
                                user.setMember(Integer.parseInt(payDetails.getRemark()));
                                userService.updateById(user);
                            }
                        }
                        System.err.println("微信手机支付回调成功订单号:" + out_trade_no + "");
                        xmlBack = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>" + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
                    } else {
                        System.err.println("微信手机支付回调成功订单号:" + out_trade_no + "");
                        xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                    }
                }else{
                }
                return xmlBack;
            } else {
                // 签名错误，如果数据里没有sign字段，也认为是签名错误
                System.err.println("手机支付回调通知签名错误");
                xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                return xmlBack;
            }
        } catch (Exception e) {
            System.err.println("手机支付回调通知失败" + e);
            xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
        return xmlBack;
    }


    @Override
    public String getGeneralOrder(){
        Date date=new Date();
        String newString = String.format("%0"+4+"d", (int)((Math.random()*9+1)*1000));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String format = sdf.format(date);
        return format+newString;
    }


    /**
     * 微信支付
     * @param money  金额 整数
     * @param userId  用户id
     * @param type 类型 1App支付 2NATIVE支付 3JSAPI支付
     * @param classify 分类 1 充值  2 开通会员
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public Map doUnifiedOrder(String money,Long userId,Integer type,Integer classify,String remark) throws Exception {
        CommonInfo oneu = commonInfoService.findOne(19);
        String url;
        if(type==1){
            url=oneu.getValue()+"/sqx_fast/api/order/notify";
        }else if(type==2){
            url=oneu.getValue()+"/sqx_fast/api/order/notifyJsApi";
        }else{
            url=oneu.getValue()+"/sqx_fast/api/order/notifyMp";
        }
        CommonInfo one = commonInfoService.findOne(12);
        log.info("回调地址："+url);
        try {
            String generateNonceStr = WXPayUtil.generateNonceStr();
            String outTradeNo=getGeneralOrder();
            WXConfig config = new WXConfig();
            if(type==1){
                config.setAppId(commonInfoService.findOne(74).getValue());
            }else if(type==2){
                config.setAppId(commonInfoService.findOne(5).getValue());
            }else{
                config.setAppId(commonInfoService.findOne(45).getValue());
            }
            config.setKey(commonInfoService.findOne(75).getValue());
            config.setMchId(commonInfoService.findOne(76).getValue());
            WXPay wxpay = new WXPay(config);
            Map<String, String> data = new HashMap<>();
            data.put("appid", config.getAppID());
            data.put("mch_id", config.getMchID());
            data.put("nonce_str", generateNonceStr);
            String body = one==null?"任务助力":one.getValue();
            data.put("body", body);
            //生成商户订单号，不可重复
            data.put("out_trade_no", outTradeNo);
            data.put("total_fee", money);
            //自己的服务器IP地址
            data.put("spbill_create_ip", SPBILL_CREATE_IP);
            //异步通知地址（请注意必须是外网）
            data.put("notify_url", url);
            //交易类型
            data.put("trade_type", type==1?TRADE_TYPE_APP:type==2?TRADE_TYPE_NATIVE:TRADE_TYPE_JSAPI);
            //附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
            data.put("attach", "");
            data.put("sign", WXPayUtil.generateSignature(data, config.getKey(),
                    WXPayConstants.SignType.MD5));
            UserEntity userById = userService.selectById(userId);
            if (type ==2){
                data.put("openid",userById.getOpenId());
            }else if(type ==3){
                data.put("openid",userById.getWxId());
            }

            //使用官方API请求预付订单
            Map<String, String> response = wxpay.unifiedOrder(data);
            if ("SUCCESS".equals(response.get("return_code"))) {//主要返回以下5个参数
                //创建订单信息
                PayDetails payDetails=new PayDetails();
                payDetails.setState(0);
                payDetails.setCreateTime(sdf.format(new Date()));
                payDetails.setOrderId(outTradeNo);
                payDetails.setUserId(userId);
                payDetails.setMoney(AmountCalUtils.divide(Double.parseDouble(money),100));
                payDetails.setClassify(1);
                if(classify==2){
                    payDetails.setType(2);
                    payDetails.setRemark(remark);
                }else{
                    payDetails.setType(type);
                }
                payDetailsDao.insert(payDetails);
                if(type==1){
                    Map<String, String> param = new HashMap<>();
                    param.put("appid", config.getAppID());
                    param.put("partnerid", response.get("mch_id"));
                    param.put("prepayid", response.get("prepay_id"));
                    param.put("package", "Sign=WXPay");
                    param.put("noncestr", generateNonceStr);
                    param.put("timestamp", System.currentTimeMillis() / 1000 + "");
                    param.put("sign", WXPayUtil.generateSignature(param, config.getKey(),
                            WXPayConstants.SignType.MD5));
                    param.put("outtradeno", outTradeNo);
                    return param;
                }else if(type==2){
                    Map<String, String> param = new HashMap<>();
                    param.put("code_url", response.get("code_url"));
                    return param;
                }else{
                    Map<String, String> param = new HashMap<>();
                    param.put("appid", config.getAppID());
                    param.put("partnerid", response.get("mch_id"));
                    param.put("prepayid", response.get("prepay_id"));
                    param.put("noncestr", generateNonceStr);
                    String timestamp = System.currentTimeMillis() / 1000 + "";
                    param.put("timestamp",timestamp);
                    /*param.put("sign", WXPayUtil.generateSignature(param, config.getKey(),
                            WXPayConstants.SignType.MD5));*/
                    String stringSignTemp = "appId=" + config.getAppID() + "&nonceStr=" + generateNonceStr + "&package=prepay_id=" + response.get("prepay_id") + "&signType=MD5&timeStamp=" + timestamp+ ""+"&key="+config.getKey();
                    String sign = MD5Util.md5Encrypt32Upper(stringSignTemp).toUpperCase();
                    param.put("sign",sign);
                    param.put("outtradeno", outTradeNo);
                    param.put("package", "prepay_id="+response.get("prepay_id"));//给前端返回的值
                    param.put("mweb_url", response.get("mweb_url"));
                    param.put("trade_type", response.get("trade_type"));
                    param.put("return_msg", response.get("return_msg"));
                    param.put("result_code", response.get("result_code"));
                    param.put("signType", "MD5");
                    return param;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("下单失败");
        }
        throw new Exception("下单失败");
    }
}