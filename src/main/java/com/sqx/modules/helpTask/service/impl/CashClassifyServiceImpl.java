package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.CashClassifyDao;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.service.CashClassifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 提现分类
 */
@Service
public class CashClassifyServiceImpl extends ServiceImpl<CashClassifyDao, CashClassify> implements CashClassifyService {

    @Autowired
    private CashClassifyDao cashClassifyDao;

    @Override
    public  int saveBody(CashClassify cashClassify){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cashClassify.setCreateTime(sdf.format(new Date()));
        return cashClassifyDao.insert(cashClassify);
    }

    @Override
    public  int updateClassify(CashClassify cashClassify){
        return cashClassifyDao.updateById(cashClassify);
    }

    @Override
    public int deleteClassifyById(Long id){
        return cashClassifyDao.deleteById(id);
    }

    @Override
    public int deleteClassifyByIds(String ids){
        for(String id:ids.split(",")){
            cashClassifyDao.deleteById(Long.parseLong(id));
        }
        return 1;
    }

    @Override
    public List<CashClassify> selectCashClassifyList(){
        return cashClassifyDao.selectCashClassifyList();
    }




}
