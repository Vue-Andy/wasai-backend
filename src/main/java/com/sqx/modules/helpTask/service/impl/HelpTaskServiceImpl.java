package com.sqx.modules.helpTask.service.impl;


import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.dao.HelpTakeOrderDao;
import com.sqx.modules.helpTask.dao.HelpTaskDao;
import com.sqx.modules.helpTask.dao.TimedTaskDao;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.message.dao.MessageInfoDao;
import com.sqx.modules.message.entity.MessageInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 助力信息
 */
@Service
public class HelpTaskServiceImpl extends ServiceImpl<HelpTaskDao, HelpTask> implements HelpTaskService {

    /** 助力信息方法 */
    @Autowired
    private HelpTaskDao helpTaskDao;
    /** 平台抽成 */
    @Autowired
    private HelpRateService helpRateService;
    /** 助力步骤详细信息 */
    @Autowired
    private HelpTaskDetailsService helpTaskDetailsService;
    /** 派单信息 */
    @Autowired
    private HelpTakeOrderService helpTakeOrderService;
    /** 派单信息 */
    @Autowired
    private HelpTakeOrderDao helpTakeOrderDao;
    /** 定时任务 */
    @Autowired
    private  TimedTaskDao timedTaskDao;
    /** 用户金额 */
    @Autowired
    private  UserMoneyService userMoneyService;
    /** 用户金额明细 */
    @Autowired
    private  UserMoneyDetailsService userMoneyDetailsService;
    /** 接单信息 */
    @Autowired
    private  HelpSendOrderService helpSendOrderService;
    /** 维权信息 */
    @Autowired
    private  HelpMaintainService helpMaintainService;
    /** 信誉分 */
    @Autowired
    private  HelpUserScoreService helpUserScoreService;
    /** 信誉明细分 */
    @Autowired
    private  HelpUserScoreDetailsService helpUserScoreDetailsService;
    @Autowired
    private  UserService userService;
    @Autowired
    private  MessageInfoDao messageInfoDao;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private HelpClassifyDetailsService helpClassifyDetailsService;
    /** 浏览记录 */
    @Autowired
    private  HelpBrowsingHistoryService helpBrowsingHistoryService;
    /** 接单详细步骤信息 */
    @Autowired
    private  HelpSendOrderDetailsService helpSendOrderDetailsService;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public HelpTask selectById(Long Id) {
        return helpTaskDao.selectById(Id);
    }

    @Override
    public Result saveBody(HelpTask helpTask) {
        helpTaskDao.insert(helpTask);
        return Result.success();
    }

    @Override
    public Result selectHelpTaskDetails(Long id,Long userId){
        Map<String, Object> map = getRate(userId);
        HelpTask helpTask=selectById(id);
        List<HelpTaskDetails> list=helpTaskDetailsService.selectByHelpTaskId(id);
        HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(id);
        if(helpTakeOrder!=null){
            UserEntity user = userService.selectById(helpTakeOrder.getUserId());
            if(user!=null){
                helpTask.setUserId(user.getUserId());
                helpTask.setNickName(user.getNickName());
                helpTask.setImageUrl(user.getImageUrl());
            }
        }
        map.put("helpTask",helpTask);
        map.put("helpTaskDetailsList",list);
        map.put("helpTakeOrder",helpTakeOrder);
        if(userId==null){
            map.put("flag",1);
        }else{
            Long helpBrowsingId=helpBrowsingHistoryService.selectHelpBrowsingHistoryByUserId(userId,id);
            //判断是否浏览过这个活动  没有则新建 有则更新浏览时间
            if(helpBrowsingId==null){
                HelpBrowsingHistory helpBrowsingHistory=new HelpBrowsingHistory();
                helpBrowsingHistory.setCreateTime(sdf.format(new Date()));
                helpBrowsingHistory.setHelpTaskId(id);
                helpBrowsingHistory.setUserId(userId);
                helpBrowsingHistoryService.insert(helpBrowsingHistory);
            }else{
                helpBrowsingHistoryService.updateCreateTimeByUserId(id,new Date());
            }
            HelpSendOrder helpSendOrder=helpSendOrderService.selectByHelpTaskId(id,userId);
            //判断活动自己是否参与
            if(helpSendOrder==null){
                map.put("flag",1);
            }else{
                List<HelpSendOrderDetails> list1=helpSendOrderDetailsService.selectByHelpSendOrderId(helpSendOrder.getId());
                map.put("flag",helpSendOrder.getState()+2);
                map.put("helpSendOrder",helpSendOrder);
                map.put("HelpSendOrderDetailsList",list1);
            }
        }
        return Result.success().put("data",map);
    }


    @Override
    public List<HelpTask> selectRecommendHelpTask(){
        return helpTaskDao.selectRecommendHelpTask();
    }

    @Override
    @Transactional
    public Result saveHelpTask(HelpTask helpTask, String helpTaskDetailss,Long userId ,Double money) {
        UserMoney userMoney = userMoneyService.selectByUserId(userId);
        String date=sdf.format(new Date());
        helpTask.setState(0);
        helpTask.setCreateTime(date);
        helpTask.setOrderTime(date);
        helpTask.setEndNum(0);
        HelpClassifyDetails helpClassifyDetails = helpClassifyDetailsService.selectById(helpTask.getClassifyDetailsId());
        if(helpClassifyDetails==null){
            return Result.error("分类不存在，请选择其他分类！");
        }
        helpTask.setLabel(helpClassifyDetails.getClassifyIcon());
        if(userMoney.getCannotMoney()<money){
            Double money1=AmountCalUtils.add(userMoney.getCannotMoney(),userMoney.getMayMoney());
            if(money1>money){
                helpTaskDao.insertById(helpTask);
                money1=AmountCalUtils.sub(money,userMoney.getCannotMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userId);
                userMoneyDetails.setTitle("[派单扣款]派单ID："+helpTask.getId());
                userMoneyDetails.setContent("派单扣款："+userMoney.getCannotMoney());
                userMoneyDetails.setType(2);
                userMoneyDetails.setMoney(userMoney.getCannotMoney());
                userMoneyDetails.setCreateTime(date);
                if(userMoney.getCannotMoney()>0.00){
                    userMoneyService.updateCannotMoney(2,userId,userMoney.getCannotMoney());
                    userMoneyDetailsService.insert(userMoneyDetails);
                }
                userMoneyDetails.setContent("派单费用扣费，不可提现金额不足，从可提现金额中扣除："+money1);
                userMoneyDetails.setMoney(money1);
                userMoneyDetailsService.insert(userMoneyDetails);
                userMoneyService.updateMayMoney(2,userId,money1);
            }else{
                return Result.error("账户金额不足，请充值！");
            }
        }else{
            helpTaskDao.insertById(helpTask);
            userMoneyService.updateCannotMoney(2,userId,money);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("[派单扣款]派单ID："+helpTask.getId());
            userMoneyDetails.setContent("派单扣款："+money);
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(money);
            userMoneyDetails.setCreateTime(date);
            userMoneyDetailsService.insert(userMoneyDetails);
        }
        List<HelpTaskDetails> helpTaskDetailsList= JSONArray.parseArray(helpTaskDetailss,HelpTaskDetails.class);
        for(HelpTaskDetails helpTaskDetails:helpTaskDetailsList){
            helpTaskDetails.setHelpTaskId(helpTask.getId());
        }
        helpTaskDetailsService.saveAll(helpTaskDetailsList);
        HelpTakeOrder helpTakeOrder=new HelpTakeOrder();
        helpTakeOrder.setUserId(userId);
        helpTakeOrder.setHelpTaskId(helpTask.getId());
        helpTakeOrder.setCreateTime(date);
        helpTakeOrder.setState(0);
        helpTakeOrderService.saveBody(helpTakeOrder);
        return Result.success();
    }

    @Override
    public PageUtils selectHelpTaskByClassify(Map<String, Object> params){
        String paramKey = (String)params.get("classifyDetailsId");
        IPage<HelpTask> page = this.page(
                new Query<HelpTask>().getPage(params),
                new QueryWrapper<HelpTask>()
                        .eq("state",1)
                        .eq(StringUtils.isNotBlank(paramKey),"classify_details_id", paramKey)
        );
        List<HelpTask> list = page.getRecords();
        for(HelpTask helpTask:list){
            HelpClassifyDetails helpClassifyDetails = helpClassifyDetailsService.selectById(helpTask.getClassifyDetailsId());
            if(helpClassifyDetails!=null){
                helpTask.setClassifyDetailsName(helpClassifyDetails.getClassifyDeatilsName());
            }else{
                helpTask.setClassifyDetailsName("");
            }
        }
        return new PageUtils(page);
    }

    @Override
    public PageUtils selectByTitle(Map<String, Object> params){
        String paramKey = (String)params.get("title");
        IPage<HelpTask> page = this.page(
                new Query<HelpTask>().getPage(params),
                new QueryWrapper<HelpTask>()
                        .eq("state",1)
                        .like(StringUtils.isNotBlank(paramKey),"title", paramKey)
        );
        return new PageUtils(page);
    }




    @Override
    @Transactional
    public int updateHelpTask(String helpTakeIds,Integer state,String content){
        for(String id:helpTakeIds.split(",")){
            Long helpTakeId=Long.parseLong(id);
            HelpTask helpTask=helpTaskDao.selectById(helpTakeId);
            if(helpTask.getState()==0){//只有待审核的可以审核
                HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpTask.getId());
                UserEntity userByWxId = userService.selectById(helpTakeOrder.getUserId());
                MessageInfo messageInfo = new MessageInfo();
                if(state==1){ //同意
                    //创建定时任务记录
                    TimedTask timedTask=new TimedTask();
                    timedTask.setType(1);
                    timedTask.setObjectId(helpTask.getId());
                    timedTask.setTitle("助力活动任务定时");
                    timedTask.setCreateTime(new Date());
                    try {
                        timedTask.setEndTime(sdf.parse(helpTask.getEndTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    timedTaskDao.insert(timedTask);
                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的派单申请已经通过！");
                    if (userByWxId.getClientid() != null) {
                        userService.pushToSingle("派代申请", userByWxId.getNickName() + " 您好！您的派单申请已经通过！", userByWxId.getClientid());
                    }
                }else if(state==2){//拒绝则退还费用
                    Double money=AmountCalUtils.mul(helpTask.getTaskOriginalPrice(),helpTask.getTaskNum());
                    userMoneyService.updateCannotMoney(1,helpTakeOrder.getUserId(),money);
                    UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                    userMoneyDetails.setUserId(helpTakeOrder.getUserId());
                    userMoneyDetails.setTitle("[派单拒绝]派单ID："+helpTask.getId());
                    userMoneyDetails.setContent("派单退款："+money);
                    userMoneyDetails.setType(1);
                    userMoneyDetails.setMoney(money);
                    userMoneyDetails.setCreateTime(sdf.format(new Date()));
                    userMoneyDetailsService.insert(userMoneyDetails);
                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的派单申请被拒绝，金额自动退回，原因如下："+content+"！");
                    if (userByWxId.getClientid() != null) {
                        userService.pushToSingle("派代申请", userByWxId.getNickName() + " 您好！您的派单申请被拒绝，金额自动退回，原因如下："+content+"！", userByWxId.getClientid());
                    }
                }

                messageInfo.setState(String.valueOf(6));
                messageInfo.setTitle("派代申请");
                messageInfo.setUserName(userByWxId.getNickName());
                messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                messageInfo.setCreateAt(sdf.format(now));
                messageInfoDao.insert(messageInfo);
                //修改助力任务的状态
                helpTaskDao.updateHelpTaskState(state,helpTakeOrder.getHelpTaskId());
                //修改派单的状态信息
                helpTakeOrderDao.updateHelpTakeOrder(state,helpTakeOrder.getId(),content,helpTakeOrder.getCategory());

            }
        }
        return 1;
    }

    @Override
    public Result cancellation(Long helpTaskId){
        HelpTask helpTask=helpTaskDao.selectById(helpTaskId);
        if(helpTask.getState()==0){
            HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpTask.getId());
            Double money=AmountCalUtils.mul(helpTask.getTaskOriginalPrice(),helpTask.getTaskNum());
            userMoneyService.updateCannotMoney(1,helpTakeOrder.getUserId(),money);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(helpTakeOrder.getUserId());
            userMoneyDetails.setTitle("[派单作废]派单ID："+helpTask.getId());
            userMoneyDetails.setContent("派单作废退款："+money);
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(money);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
            //修改助力任务的状态
            helpTaskDao.updateHelpTaskState(4,helpTakeOrder.getHelpTaskId());
            //修改派单的状态信息
            helpTakeOrderDao.updateHelpTakeOrder(3,helpTakeOrder.getId(),"作废",helpTakeOrder.getCategory());
            return Result.success();
        }else{
            return Result.error("只有待审核的任务才可以作废！");
        }

    }


    @Override
    public Result cancellations(Long helpTaskId,String content){
        HelpTask helpTask=helpTaskDao.selectById(helpTaskId);
        if(!helpTask.getState().equals(2)){
            HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpTask.getId());
            Double money=AmountCalUtils.mul(helpTask.getTaskOriginalPrice(),helpTask.getTaskNum());
            userMoneyService.updateCannotMoney(1,helpTakeOrder.getUserId(),money);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(helpTakeOrder.getUserId());
            userMoneyDetails.setTitle("[派单下架]派单ID："+helpTask.getId());
            userMoneyDetails.setContent("您好，您的任务已经被下架，原因："+content+"，请遵守平台规定！");
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(money);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
            //修改助力任务的状态
            helpTaskDao.updateHelpTaskState(2,helpTakeOrder.getHelpTaskId());
            //修改派单的状态信息
            helpTakeOrderDao.updateHelpTakeOrder(3,helpTakeOrder.getId(),userMoneyDetails.getContent(),helpTakeOrder.getCategory());
        }
        return Result.success();
    }




    @Override
    public Result quicknessHelpTask(Long helpTakeId){
        HelpTask helpTask=helpTaskDao.selectById(helpTakeId);
        if(helpTask==null){
            return Result.error("任务不存在，请刷新后重试！");
        }
        if(helpTask.getState()!=0){
            return Result.error("任务已经通过审核了！");
        }
        HelpTakeOrder helpTakeOrder = helpTakeOrderDao.selectHelpTakeOrderByHelpTaskId(helpTakeId);
        CommonInfo one = commonInfoService.findOne(106);
        UserMoney userMoney = userMoneyService.selectByUserId(helpTakeOrder.getUserId());
        double v = Double.parseDouble(one.getValue());
        if(userMoney.getCannotMoney()>=v){
            userMoneyService.updateCannotMoney(2,helpTakeOrder.getUserId(),v);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(helpTakeOrder.getUserId());
            userMoneyDetails.setTitle("[秒审核]审核任务");
            userMoneyDetails.setContent("秒审核任务，扣除不可提现金额:"+v);
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(v);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
        }else{
            double add = AmountCalUtils.add(userMoney.getCannotMoney(), userMoney.getMayMoney());
            if(add>=v){
                Double sub = AmountCalUtils.sub(v, userMoney.getCannotMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(helpTakeOrder.getUserId());
                userMoneyDetails.setTitle("[秒审核]审核任务");
                userMoneyDetails.setType(2);
                userMoneyDetails.setCreateTime(sdf.format(new Date()));
                if(userMoney.getCannotMoney()>0){
                    userMoneyService.updateCannotMoney(2,helpTakeOrder.getUserId(),userMoney.getCannotMoney());
                    userMoneyDetails.setContent("秒审核任务，扣除不可提现金额:"+userMoney.getCannotMoney());
                    userMoneyDetails.setMoney(userMoney.getCannotMoney());
                    userMoneyDetailsService.insert(userMoneyDetails);
                }
                userMoneyService.updateMayMoney(2,helpTakeOrder.getUserId(),sub);
                userMoneyDetails.setContent("秒审核任务，不可提现金额不足，可提现金额扣除:"+sub);
                userMoneyDetails.setMoney(sub);
                userMoneyDetailsService.insert(userMoneyDetails);
            }else{
                Result.error("钱包金额不足！");
            }
        }
        UserEntity userByWxId = userService.selectById(helpTakeOrder.getUserId());
        //创建定时任务记录
        TimedTask timedTask=new TimedTask();
        timedTask.setType(1);
        timedTask.setObjectId(helpTask.getId());
        timedTask.setTitle("助力活动任务定时");
        timedTask.setCreateTime(new Date());
        try {
            timedTask.setEndTime(sdf.parse(helpTask.getEndTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        timedTaskDao.insert(timedTask);
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setContent(userByWxId.getNickName() + " 您好！您的派单申请已经通过！");
        if (userByWxId.getClientid() != null) {
            userService.pushToSingle("派代申请", userByWxId.getNickName() + " 您好！您的派单申请已经通过！", userByWxId.getClientid());
        }
        messageInfo.setState(String.valueOf(6));
        messageInfo.setTitle("派代申请");
        messageInfo.setUserName(userByWxId.getNickName());
        messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        messageInfo.setCreateAt(sdf.format(now));
        messageInfoDao.insert(messageInfo);
        //修改助力任务的状态
        helpTaskDao.updateHelpTaskState(1,helpTakeOrder.getHelpTaskId());
        //修改派单的状态信息
        helpTakeOrderDao.updateHelpTakeOrder(1,helpTakeOrder.getId(),"秒审核通过",helpTakeOrder.getCategory());
        return Result.success();
    }

    @Override
    public Result helpTaskAddNum(Long helpTakeId,Integer num){
        if(num<0){
            return Result.error("数量不能低于0");
        }
        HelpTask helpTask=helpTaskDao.selectById(helpTakeId);
        if(helpTask==null){
            return Result.error("任务不存在，请刷新后重试！");
        }
        if(helpTask.getState()!=1){
            return Result.error("当前任务状态无法进行增加数量");
        }
        HelpTakeOrder helpTakeOrder = helpTakeOrderDao.selectHelpTakeOrderByHelpTaskId(helpTakeId);
        UserMoney userMoney = userMoneyService.selectByUserId(helpTakeOrder.getUserId());
        double v = AmountCalUtils.mul(num,helpTask.getTaskOriginalPrice());
        if(userMoney.getCannotMoney()>=v){
            userMoneyService.updateCannotMoney(2,helpTakeOrder.getUserId(),v);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(helpTakeOrder.getUserId());
            userMoneyDetails.setTitle("[加量]任务Id："+helpTask.getId());
            userMoneyDetails.setContent("增加数量："+num+"，扣除不可提现金额:"+v);
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(v);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
        }else{
            double add = AmountCalUtils.add(userMoney.getCannotMoney(), userMoney.getMayMoney());
            if(add>=v){
                Double sub = AmountCalUtils.sub(v, userMoney.getCannotMoney());
                userMoneyService.updateCannotMoney(2,helpTakeOrder.getUserId(),userMoney.getCannotMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(helpTakeOrder.getUserId());
                userMoneyDetails.setTitle("[加量]任务Id："+helpTask.getId());
                userMoneyDetails.setContent("增加数量："+num+"，扣除不可提现金额:"+userMoney.getCannotMoney());
                userMoneyDetails.setType(2);
                userMoneyDetails.setMoney(userMoney.getCannotMoney());
                userMoneyDetails.setCreateTime(sdf.format(new Date()));
                userMoneyDetailsService.insert(userMoneyDetails);
                userMoneyService.updateMayMoney(2,helpTakeOrder.getUserId(),sub);
                userMoneyDetails.setContent("增加数量："+num+"，不可提现金额不足，可提现金额扣除:"+sub);
                userMoneyDetails.setMoney(sub);
                userMoneyDetailsService.insert(userMoneyDetails);
            }else{
                return Result.error("钱包金额不足！");
            }
        }
        int i = helpTask.getTaskNum() + num;
        helpTask.setTaskNum(i);
        helpTaskDao.updateById(helpTask);
        return Result.success();
    }

    @Override
    public Result helpTaskAddPrice(Long helpTakeId, Double price){
        if(price<0){
            return Result.error("价格不能低于0.01");
        }
        HelpTask helpTask=helpTaskDao.selectById(helpTakeId);
        if(helpTask==null){
            return Result.error("任务不存在，请刷新后重试！");
        }
        if(helpTask.getState()!=1){
            return Result.error("当前任务状态无法进行增加数量");
        }
        HelpTakeOrder helpTakeOrder = helpTakeOrderDao.selectHelpTakeOrderByHelpTaskId(helpTakeId);
        UserMoney userMoney = userMoneyService.selectByUserId(helpTakeOrder.getUserId());
        Double sub1 = price;
        Integer num=helpTask.getTaskNum()-helpTask.getEndNum();
        if(num==0){
            return Result.error("任务数量不足，无法加价！");
        }
        double v = AmountCalUtils.mul(sub1,num);
        if(userMoney.getCannotMoney()>=v){
            userMoneyService.updateCannotMoney(2,helpTakeOrder.getUserId(),v);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(helpTakeOrder.getUserId());
            userMoneyDetails.setTitle("[加价]任务Id："+helpTask.getId());
            userMoneyDetails.setContent("增加任务价格，扣除不可提现金额:"+v);
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(v);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
        }else{
            double add = AmountCalUtils.add(userMoney.getCannotMoney(), userMoney.getMayMoney());
            if(add>=v){
                Double sub = AmountCalUtils.sub(v, userMoney.getCannotMoney());
                userMoneyService.updateCannotMoney(2,helpTakeOrder.getUserId(),userMoney.getCannotMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(helpTakeOrder.getUserId());
                userMoneyDetails.setTitle("[加价]任务Id："+helpTask.getId());
                userMoneyDetails.setContent("增加任务价格，扣除不可提现金额:"+userMoney.getCannotMoney());
                userMoneyDetails.setType(2);
                userMoneyDetails.setMoney(userMoney.getCannotMoney());
                userMoneyDetails.setCreateTime(sdf.format(new Date()));
                userMoneyDetailsService.insert(userMoneyDetails);
                userMoneyService.updateMayMoney(2,helpTakeOrder.getUserId(),sub);
                userMoneyDetails.setContent("增加任务价格，不可提现金额不足，可提现金额扣除:"+sub);
                userMoneyDetails.setMoney(sub);
                userMoneyDetailsService.insert(userMoneyDetails);
            }else{
                return Result.error("钱包金额不足！");
            }
        }
        double add = AmountCalUtils.add(helpTask.getTaskOriginalPrice(), price);
        helpTask.setTaskOriginalPrice(add);
        helpTaskDao.updateById(helpTask);
        return Result.success();
    }


    @Override
    public PageUtils selectMyHelpTask(int page,int limit,Integer state,Long userId){
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        if(state==null||state==-1){
            return new PageUtils(helpTaskDao.selectMyHelpTask(pages,null,userId));
        }
        return new PageUtils(helpTaskDao.selectMyHelpTask(pages,state,userId));
    }

    @Override
    public PageUtils selectParticipationHelpTask(int page,int limit,Integer state, Long userId){
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        if(state==null||state==-1){
            return new PageUtils(helpTaskDao.selectParticipationHelpTask(pages,null,userId));
        }
        return new PageUtils(helpTaskDao.selectParticipationHelpTask(pages,state,userId));
    }

    @Override
    public PageUtils selectHelpTaskListByPhone(int page,int limit,Integer state,String sort,String phone) {
        Page<HelpTask> pages = new Page<>(page, limit);
        IPage<HelpTask> helpTaskIPage = null;
        if (state == null || state == -1) {
            helpTaskIPage = helpTaskDao.selectHelpTaskListByPhone(pages, null, sort,phone);
        } else {
            helpTaskIPage = helpTaskDao.selectHelpTaskListByPhone(pages, state, sort,phone);
        }
        return new PageUtils(helpTaskIPage);
    }

    @Override
    public Result selectHelpTaskList(int page,int limit,Integer state,Integer sort,Long userId){
        Page<HelpTask> pages=new Page<>(page,limit);
        IPage<HelpTask> helpTaskIPage=null;
        if(state==null||state==-1){
            helpTaskIPage = helpTaskDao.selectHelpTaskList(pages, null, sort);
        }else{
            helpTaskIPage = helpTaskDao.selectHelpTaskList(pages, state, sort);
        }
        PageUtils pageUtils = new PageUtils(helpTaskIPage);
        Map<String, Object> rate = getRate(userId);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }

    @Override
    public Result selectHelpTaskListV1(int page,int limit,Integer state,Integer sort,Long userId,Integer desc,Integer classifyId,Integer classifyDetailsId){
        Page<HelpTask> pages=new Page<>(page,limit);
        IPage<HelpTask> helpTaskIPage=null;
        if(state==null||state==-1){
            helpTaskIPage = helpTaskDao.selectHelpTaskListV1(pages, null, sort,desc,classifyId,classifyDetailsId);
        }else{
            helpTaskIPage = helpTaskDao.selectHelpTaskListV1(pages, state, sort,desc,classifyId,classifyDetailsId);
        }
        PageUtils pageUtils = new PageUtils(helpTaskIPage);
        Map<String, Object> rate = getRate(userId);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }

    @Override
    public Map<String,Object> getRate(Long userId){
        Map<String,Object> map=new HashMap<>();
        //直属
        List<HelpRate> zhihelpRates = helpRateService.selectRateByClassifyList(5,1);
        //非直属
        List<HelpRate> feihelpRates = helpRateService.selectRateByClassifyList(6,1);
        List<HelpRate> pinghelpRates = helpRateService.selectRateByClassifyList(1,1);
        if(userId==null || userId==-1){
            map.put("ping",pinghelpRates.get(3));
            map.put("zhishu",zhihelpRates.get(3));
            map.put("feizhishu",feihelpRates.get(3));
        }else{
            UserEntity userEntity = userService.selectById(userId);
            if(userEntity!=null){
                map.put("ping",pinghelpRates.get(userEntity.getMember()-1));
                UserEntity userEntity1 = userService.selectUserByInvitationCode(userEntity.getInviterCode());
                if(userEntity1!=null){
                    map.put("zhishu",zhihelpRates.get(userEntity1.getMember()-1));
                    UserEntity userEntity2 = userService.selectUserByInvitationCode(userEntity1.getInviterCode());
                    if(userEntity2!=null){
                        map.put("feizhishu",feihelpRates.get(userEntity2.getMember()-1));
                    }else{
                        map.put("feizhishu",feihelpRates.get(3));
                    }
                }else{
                    map.put("zhishu",zhihelpRates.get(3));
                    map.put("feizhishu",feihelpRates.get(3));
                }
            }else{
                map.put("ping",pinghelpRates.get(3));
                map.put("zhishu",zhihelpRates.get(3));
                map.put("feizhishu",feihelpRates.get(3));
            }
        }
        return map;
    }



    @Override
    public PageUtils selectHelpTaskListByClassifyId(int page, int limit, Long classifyId) {
        Page<HelpTask> pages=new Page<>(page,limit);
        IPage<HelpTask> helpTaskIPage = helpTaskDao.selectHelpTaskListByClassifyId(pages, classifyId);
        List<HelpTask> list = helpTaskIPage.getRecords();
        for(HelpTask helpTask:list){
            HelpClassifyDetails helpClassifyDetails = helpClassifyDetailsService.selectById(helpTask.getClassifyDetailsId());
            if(helpClassifyDetails!=null){
                helpTask.setClassifyDetailsName(helpClassifyDetails.getClassifyDeatilsName());
            }else{
                helpTask.setClassifyDetailsName("");
            }
        }
        return new PageUtils(helpTaskIPage);
    }


    @Override
    public PageUtils selectAuditHelpTask(int page,int limit,Integer state, Long userId) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        if(state==null||state==-1){
            return new PageUtils(helpTaskDao.selectAuditHelpTask(pages,null,userId));
        }
        return new PageUtils(helpTaskDao.selectAuditHelpTask(pages,state,userId));
    }


    @Transactional
    @Override
    public int finishHelpTask(HelpTask helpTask){
        if(helpTask.getState()!=4){
            String date=sdf.format(new Date());
            //计算拒绝和放弃的数量
            Integer count=helpSendOrderService.selectEndNum(helpTask.getId());
            HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpTask.getId());
            if(count==null){
                count=0;
            }
            //拒绝的数量加上剩余的数量
            count+=(helpTask.getTaskNum()-helpTask.getEndNum());
            if(count>0){
                Double money=AmountCalUtils.mul(count,helpTask.getTaskOriginalPrice());
                DecimalFormat formater = new DecimalFormat();
                formater.setMaximumFractionDigits(2);
                formater.setGroupingSize(0);
                formater.setRoundingMode(RoundingMode.FLOOR);
                money=Double.parseDouble(formater.format(money));
                userMoneyService.updateCannotMoney(1,helpTakeOrder.getUserId(),money);
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(helpTakeOrder.getUserId());
                userMoneyDetails.setTitle("[派单结算]，派单ID："+helpTask.getId());
                userMoneyDetails.setContent("派单结算："+money);
                userMoneyDetails.setType(1);
                userMoneyDetails.setMoney(money);
                userMoneyDetails.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetails);
            }
            //判断下是否有维权失败的记录 若有则不加信誉分
            Integer exits=helpMaintainService.selectCount(helpTask.getId(),1);
            if(exits==null || exits==0){
                helpUserScoreService.updateUserScore(helpTakeOrder.getUserId(),1,2);
                HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                helpUserScoreDetails.setUserId(helpTakeOrder.getUserId());
                helpUserScoreDetails.setTitle("[派单完成]任务Id："+helpTakeOrder.getHelpTaskId());
                helpUserScoreDetails.setContent("增加积分：2");
                helpUserScoreDetails.setCreateTime(date);
                helpUserScoreDetails.setScore(2);
                helpUserScoreDetails.setType(1);
                helpUserScoreDetailsService.insert(helpUserScoreDetails);
            }
            helpTaskDao.updateHelpTaskState(4,helpTask.getId());
            helpTakeOrderDao.updateHelpTakeOrder(3,helpTakeOrder.getId(),"结算完成",0);
            timedTaskDao.deleteByObjectId(helpTask.getId(),4);
        }

        return 1;
    }

    @Override
    public Double sumPrice(String time, Integer flag) {
        return helpTaskDao.sumPrice(time,flag);
    }

    @Override
    public Integer countHelpTaskByCreateTime(String time, Integer flag) {
        return helpTaskDao.countHelpTaskByCreateTime(time,flag);
    }

    @Override
    public Result selectTopHelpTask(Integer page,Integer limit){
        Page<HelpTask> pages=new Page<>(page,limit);
        return Result.success().put("data",new PageUtils(helpTaskDao.selectTopHelpTask(pages)));
    }


    @Override
    public Integer selectHelpTaskCountByTime(String time){
        return helpTaskDao.selectHelpTaskCountByTime(time);
    }
    
    @Override
    public Double selectHelpTaskSumPriceByTime(String time){
        return helpTaskDao.selectHelpTaskSumPriceByTime(time);
    }


}
