package com.sqx.modules.helpTask.service;

import com.sqx.modules.helpTask.entity.HelpSendOrder;
import com.sqx.modules.helpTask.entity.HelpSendOrderDetails;
import com.sqx.modules.helpTask.entity.HelpTask;

import java.util.List;

public interface HelpSendOrderDetailsService {


    List<HelpSendOrderDetails> selectByHelpSendOrderId(Long helpTakeOrderId);

    public int saveAll(List<HelpSendOrderDetails> helpTakeOrderList, HelpTask helpTask, HelpSendOrder helpSendOrder);
}
