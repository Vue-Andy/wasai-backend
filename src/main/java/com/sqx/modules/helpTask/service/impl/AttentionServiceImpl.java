package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.dao.AttentionDao;
import com.sqx.modules.helpTask.dao.CashClassifyDao;
import com.sqx.modules.helpTask.entity.Attention;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpTask;
import com.sqx.modules.helpTask.service.AttentionService;
import com.sqx.modules.helpTask.service.CashClassifyService;
import com.sqx.modules.helpTask.service.HelpTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 关注
 * @author fang
 * @date 2021/1/18
 */
@Service
public class AttentionServiceImpl extends ServiceImpl<AttentionDao, Attention> implements AttentionService {

    @Autowired
    private AttentionDao attentionDao;
    @Autowired
    private HelpTaskService helpTaskService;
    @Autowired
    private UserService userService;

    /**
     *  关注或取消关注
     * @param userId 用户id
     * @param byUserId 被关注用户id
     * @return
     */
    @Override
    public Result insertAttention(Long userId,Long byUserId){
        Attention attention = attentionDao.selectAttentionByUserIdAndByUserId(userId, byUserId);
        if(attention!=null){
            attentionDao.deleteById(attention.getAttentionId());
            return Result.success("取消关注").put("data",2);
        }
        attention=new Attention();
        attention.setUserId(userId);
        attention.setByUserId(byUserId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        attention.setCreateTime(sdf.format(new Date()));
        attentionDao.insert(attention);
        return Result.success("关注成功").put("data",1);
    }

    /**
     *  查询我关注的用户列表
     * @param page 页码
     * @param limit 大小
     * @param userId 用户id
     * @return
     */
    @Override
    public Result selectAttentionList(Integer page,Integer limit,Long userId){
        Page<Map<String,Object>> pages=new Page<Map<String,Object>>(page,limit);
        return Result.success().put("data",new PageUtils(attentionDao.selectAttentionList(pages,userId)));
    }

    /**
     *  查询我关注的用户发布的任务
     * @param page 页码
     * @param limit 大小
     * @param userId 用户id
     * @return
     */
    @Override
    public Result selectHelpTaskListByAttention(Integer page,Integer limit,Long userId){
        Page<HelpTask> pages=new Page<HelpTask>(page,limit);
        PageUtils pageUtils = new PageUtils(attentionDao.selectHelpTaskListByAttention(pages, userId));
        Map<String, Object> rate = helpTaskService.getRate(userId);
        rate.put("pageUtils",pageUtils);
        return Result.success().put("data",rate);
    }


    /**
     *  根据用户id 查询发布的任务
     * @param page
     * @param limit
     * @param userId
     * @return
     */
    @Override
    public Result selectHelpTaskListByUserId(Integer page,Integer limit,Long userId,Long byUserId){
        Page<HelpTask> pages=new Page<HelpTask>(page,limit);
        PageUtils pageUtils = new PageUtils(attentionDao.selectHelpTaskListByUserId(pages, byUserId));
        Map<String, Object> rate = helpTaskService.getRate(userId);
        rate.put("pageUtils",pageUtils);
        UserEntity userEntity = userService.selectById(byUserId);
        rate.put("imageUrl",userEntity.getImageUrl());
        String nickName = userEntity.getNickName();
        nickName = nickName.replaceAll("(\\d{3})\\d*([0-9a-zA-Z]{4})","$1****$2");
        rate.put("nickName",nickName);
        Attention attention = attentionDao.selectAttentionByUserIdAndByUserId(userId, byUserId);
        if(attention!=null){
            rate.put("attention",1);
        }else{
            rate.put("attention",2);
        }
        return Result.success().put("data",rate);
    }





}
