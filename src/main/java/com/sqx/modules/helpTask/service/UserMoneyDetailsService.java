package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;

import java.util.Map;


public interface UserMoneyDetailsService {

    PageUtils selectUserMoneyDetailsList(Map<String, Object> params);

    Integer insert(UserMoneyDetails userMoneyDetails);


}
