package com.sqx.modules.helpTask.service;


import com.sqx.modules.helpTask.entity.HelpClassifyDetails;

import java.util.List;

public interface HelpClassifyDetailsService {

    List<HelpClassifyDetails> selectList(Long classifyId);

    List<HelpClassifyDetails> selectList(String classifyName);

    List<HelpClassifyDetails> selectList();

    HelpClassifyDetails selectById(Long id);

    int insert(HelpClassifyDetails helpClassifyDetails);

    void saveAll(List<HelpClassifyDetails> list);

    int updateHelpClassifyDetails(HelpClassifyDetails helpClassifyDetails);

    void deleteById(Long id);

    void deleteByIdList(String ids);

}
