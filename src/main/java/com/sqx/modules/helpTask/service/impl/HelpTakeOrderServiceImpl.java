package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.dao.HelpTakeOrderDao;
import com.sqx.modules.helpTask.entity.HelpTakeOrder;
import com.sqx.modules.helpTask.service.HelpTakeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

/**
 * 派单信息
 */
@Service
public class HelpTakeOrderServiceImpl extends ServiceImpl<HelpTakeOrderDao, HelpTakeOrder> implements HelpTakeOrderService {

    /** 派单信息 */
    private final HelpTakeOrderDao helpTakeOrderDao;

    @Autowired
    public HelpTakeOrderServiceImpl(HelpTakeOrderDao helpTakeOrderDao) {
        this.helpTakeOrderDao = helpTakeOrderDao;
    }

    @Override
    public Result saveBody(HelpTakeOrder helpTakeOrder) {
        helpTakeOrderDao.insert(helpTakeOrder);
        return Result.success();
    }

    @Override
    public HelpTakeOrder selectHelpTakeOrderByHelpTaskId(Long helpTaskId) {
        return helpTakeOrderDao.selectHelpTakeOrderByHelpTaskId(helpTaskId);
    }

    @Override
    public Integer selectCountByHelpTaskIdAndUserId(Long helpTaskId, Long userId) {
        return helpTakeOrderDao.selectCountByHelpTaskIdAndUserId(helpTaskId,userId);
    }

    @Override
    public HelpTakeOrder selectById(Long helpTakeOrderId) {
        return helpTakeOrderDao.selectById(helpTakeOrderId);
    }

    @Override
    public Integer selectHelpTakeOrderCount(Long userId, Date startTime, Date endTime) {
        return helpTakeOrderDao.selectHelpTakeOrderCount(userId,startTime,endTime);
    }

    @Override
    public Result takeOrder() {
        Calendar cal = Calendar.getInstance();
        return Result.success().put("data", helpTakeOrderDao.takeOrder(cal.getTime()));
    }

    @Override
    public int updateHelpTakeOrder(HelpTakeOrder helpTakeOrder){
        return helpTakeOrderDao.updateById(helpTakeOrder);
    }


}