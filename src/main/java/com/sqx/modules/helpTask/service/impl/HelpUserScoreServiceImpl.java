package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.HelpUserScoreDao;
import com.sqx.modules.helpTask.entity.HelpUserScore;
import com.sqx.modules.helpTask.service.HelpUserScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户信誉分数明细
 */
@Service
public class HelpUserScoreServiceImpl extends ServiceImpl<HelpUserScoreDao, HelpUserScore> implements HelpUserScoreService {

    /** 用户信誉分数明细 */
    private final HelpUserScoreDao helpUserScoreDao;

    @Autowired
    public HelpUserScoreServiceImpl(HelpUserScoreDao helpUserScoreDao) {
        this.helpUserScoreDao = helpUserScoreDao;
    }


    @Override
    public HelpUserScore selectByUserId(Long userId) {
        HelpUserScore helpUserScore= helpUserScoreDao.selectByUserId(userId);
        if(helpUserScore==null){
            helpUserScore=new HelpUserScore();
            helpUserScore.setUserId(userId);
            helpUserScore.setScore(50);
            helpUserScore.setAddScore(0);
            helpUserScore.setReduceScore(0);
            insert(helpUserScore);
        }
        return helpUserScore;
    }

    @Override
    public int insert(HelpUserScore helpUserScore) {
        helpUserScoreDao.insert(helpUserScore);
        return 1;
    }

    @Override
    public Integer updateUserScore(Long userId, Integer type, Integer score) {
        HelpUserScore helpUserScore = selectByUserId(userId);
        if(type==1){
            Integer s=helpUserScore.getScore()+score;
            if(s>=100){
                return helpUserScoreDao.updateUserScoreAdd2(userId,100);
            }
            return helpUserScoreDao.updateUserScoreAdd(userId,score);
        }else{
            Integer s=helpUserScore.getScore()-score;
            if(s<=0){
                return helpUserScoreDao.updateUserScoreAdd2(userId,0);
            }
            return helpUserScoreDao.updateUserScoreSub(userId,score);
        }

    }

}