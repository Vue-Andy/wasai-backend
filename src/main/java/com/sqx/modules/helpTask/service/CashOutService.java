package com.sqx.modules.helpTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.helpTask.entity.CashOut;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface CashOutService {

    PageUtils selectCashOutList(Map<String,Object> params);

    int saveBody(CashOut cashOut);

    int update(CashOut cashOut);

    CashOut selectById(Long id);

    void cashOutSuccess(String openId, String date, String money, String payWay, String url);

    List<CashOut> selectCashOutLimit3();

    void refundSuccess(UserEntity userByWxId, String date, String money, String url, String content);

    Double selectCashOutSum(Long userId, Date startTime, Date endTime);

    Double sumMoney(String time,Integer flag);

    Integer countMoney(String time,Integer flag);

    Integer stayMoney(String time,Integer flag);

}
