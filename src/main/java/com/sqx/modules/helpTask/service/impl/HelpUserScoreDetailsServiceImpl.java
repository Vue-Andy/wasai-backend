package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.HelpUserScoreDetailsDao;
import com.sqx.modules.helpTask.entity.HelpUserScoreDetails;
import com.sqx.modules.helpTask.service.HelpUserScoreDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户信誉分数信息
 */
@Service
public class HelpUserScoreDetailsServiceImpl extends ServiceImpl<HelpUserScoreDetailsDao, HelpUserScoreDetails> implements HelpUserScoreDetailsService {

    /** 用户信誉分数信息 */
    private final HelpUserScoreDetailsDao helpUserScoreDetailsDao;

    @Autowired
    public HelpUserScoreDetailsServiceImpl(HelpUserScoreDetailsDao helpUserScoreDetailsDao) {
        this.helpUserScoreDetailsDao = helpUserScoreDetailsDao;
    }


    @Override
    public List<HelpUserScoreDetails> selectHelpUserScoreDetailsList(int page,int limit,Long userId){
        Page<HelpUserScoreDetails> pages=new Page<>(page,limit);
        return helpUserScoreDetailsDao.selectHelpUserScoreDetailsList(pages,userId);
    }

    @Override
    public int insert(HelpUserScoreDetails helpUserScoreDetails) {
        return helpUserScoreDetailsDao.insert(helpUserScoreDetails);
    }

}
