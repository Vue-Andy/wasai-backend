package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.dao.HelpSendOrderDao;
import com.sqx.modules.helpTask.dao.HelpSendOrderDetailsDao;
import com.sqx.modules.helpTask.dao.TimedTaskDao;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.HelpSendOrderDetailsService;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 接单详细信息
 */
@Service
public class HelpSendOrderDetailsServiceImpl extends ServiceImpl<HelpSendOrderDetailsDao, HelpSendOrderDetails> implements HelpSendOrderDetailsService {

    /** 接单详细信息 */
    @Autowired
    private HelpSendOrderDetailsDao helpSendOrderDetailsDao;
    /** 接单详细信息自定义方法 */
    @Autowired
    private HelpSendOrderDao helpSendOrderDao;
    /** 定时任务 */
    @Autowired
    private TimedTaskDao timedTaskDao;
    @Autowired
    private HelpRateService helpRateService;
    @Autowired
    private UserService userService;


    @Override
    public List<HelpSendOrderDetails> selectByHelpSendOrderId(Long helpSendOrderId) {
        return helpSendOrderDetailsDao.selectByHelpSendOrderId(helpSendOrderId);
    }

    @Override
    @Transactional
    public int saveAll(List<HelpSendOrderDetails> helpTakeOrderList, HelpTask helpTask, HelpSendOrder helpSendOrder) {
        helpSendOrderDetailsDao.delete(new QueryWrapper<HelpSendOrderDetails>().eq("help_send_order_id",helpSendOrder.getId()));
        for(HelpSendOrderDetails helpSendOrderDetails:helpTakeOrderList){
            helpSendOrderDetails.setHelpSendOrderId(helpSendOrder.getId());
            helpSendOrderDetailsDao.insert(helpSendOrderDetails);
        }
        //添加定时任务
        TimedTask timedTask=new TimedTask();
        timedTask.setType(3);
        timedTask.setObjectId(helpSendOrder.getId());
        timedTask.setTitle("助力待审核任务定时");
        timedTask.setCreateTime(new Date());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE,helpTask.getAuditTime());
        timedTask.setEndTime(cal.getTime());
        timedTaskDao.insert(timedTask);
        //删除定时任务
        timedTaskDao.deleteByObjectId(helpSendOrder.getId(),2);
        List<HelpRate> helpRates = helpRateService.selectRateByClassifyList(1,1);
        List<HelpRate> zhihelpRates = helpRateService.selectRateByClassifyList(5,1);
        List<HelpRate> feihelpRates = helpRateService.selectRateByClassifyList(6,1);
        //计算接单价格

        //平台扣除利率
        Double percentage=0.00;
        //直属扣除利率
        Double zhipercentage=0.00;
        Long zhiUserId=0L;
        //非直属扣除利率
        Double feipercentage=0.00;
        Long feiUserId=0L;
        //当前用户
        UserEntity userEntity = userService.selectById(helpSendOrder.getUserId());
        percentage=helpRates.get(userEntity.getMember()-1).getRate();
        //直属用户
        UserEntity userEntity1 = userService.selectUserByInvitationCode(userEntity.getInviterCode());
        if(userEntity1!=null){
            zhipercentage=zhihelpRates.get(userEntity1.getMember()-1).getRate();
            zhiUserId=userEntity1.getUserId();
            //非直属用户
            UserEntity userEntity2 = userService.selectUserByInvitationCode(userEntity1.getInviterCode());
            if(userEntity2!=null){
                feipercentage=feihelpRates.get(userEntity2.getMember()-1).getRate();
                feiUserId=userEntity2.getUserId();
            }else{
                //没有非直属用户时 将这笔费用给平台 按照最高扣费
                percentage=AmountCalUtils.add(percentage,feihelpRates.get(3).getRate());
            }
        }else{
            //没有直属用户时 将直属 非直属费用都给平台 按照最高扣费
            percentage=AmountCalUtils.add(percentage,zhihelpRates.get(3).getRate());
            percentage=AmountCalUtils.add(percentage,feihelpRates.get(3).getRate());
        }
        //用户可收入金额
        Double money=0.00;
        //直属用户收入
        Double zhimoney=0.00;
        //非直属用户收入
        Double feimoney=0.00;
        Double taskOriginalPrice = helpTask.getTaskOriginalPrice();

        percentage=AmountCalUtils.mul(taskOriginalPrice,percentage);
        money=AmountCalUtils.sub(taskOriginalPrice,percentage);
        zhimoney=AmountCalUtils.mul(taskOriginalPrice,zhipercentage);
        money=AmountCalUtils.sub(money,zhimoney);

        feimoney=AmountCalUtils.mul(taskOriginalPrice,feipercentage);
        money=AmountCalUtils.sub(money,feimoney);

        //对金额进行处理 保留两位 不四舍五入
        DecimalFormat formater = new DecimalFormat();
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(0);
        formater.setRoundingMode(RoundingMode.FLOOR);
        money=Double.parseDouble(formater.format(money));
        zhimoney=Double.parseDouble(formater.format(zhimoney));
        feimoney=Double.parseDouble(formater.format(feimoney));
        Double sumMoney=0.00;
        sumMoney=AmountCalUtils.add(money,zhimoney);
        sumMoney=AmountCalUtils.add(sumMoney,feimoney);

        //平台收入
        Double pingMoney=taskOriginalPrice-sumMoney;
        pingMoney=Double.parseDouble(formater.format(pingMoney));
        helpSendOrderDao.updateHelpSendOrderByContents(helpSendOrder.getId(),1,helpSendOrder.getContent(),money,zhimoney,feimoney,pingMoney,zhiUserId,feiUserId);
        return 1;
    }


}
