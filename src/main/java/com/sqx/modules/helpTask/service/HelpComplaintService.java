package com.sqx.modules.helpTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpComplaint;

import java.util.List;

public interface HelpComplaintService {

    int insertHelpComplaint(HelpComplaint helpComplaint);

    int updateHelpComplaint(HelpComplaint helpComplaint);

    int deleteHelpComplaintByIds(String ids);

    PageUtils selectHelpComplaint(int page, int limit, String content, Long userId);

    HelpComplaint selectHelpComplaintDetails(Long id);

    Result solveHelpComplaint(String ids, int state);

}
