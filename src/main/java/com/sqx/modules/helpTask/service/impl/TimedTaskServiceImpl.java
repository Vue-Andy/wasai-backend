package com.sqx.modules.helpTask.service.impl;

import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.dao.*;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 定时任务类
 */
@Service
public class TimedTaskServiceImpl  {

    @Autowired
    private TimedTaskDao timedTaskDao;
    @Autowired
    private HelpTaskDao helpTaskDao;
    @Autowired
    private HelpTakeOrderDao helpTakeOrderDao;
    /** 接单信息自定义方法类 */
    @Autowired
    private HelpSendOrderDao helpSendOrderDao;
    /** 用户钱包 */
    @Autowired
    private UserMoneyService userMoneyService;
    /** 用户钱包明细 */
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    /** 信誉分 */
    @Autowired
    private HelpUserScoreService helpUserScoreService;
    /** 信誉明细分 */
    @Autowired
    private HelpUserScoreDetailsService helpUserScoreDetailsService;
    /** 维权信息 */
    @Autowired
    private HelpMaintainService helpMaintainService;
    /** 助力信息 */
    @Autowired
    private HelpTaskService helpTaskService;
    @Autowired
    private HelpProfitDao helpProfitDao;
    @Autowired
    private UserService userService;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    /**
     * 获取已经结束的助力任务
     */
    @Scheduled(cron="0 */1 * * * ?")
    public void getEndTask() {
        List<TimedTask> list=timedTaskDao.selectEndTime();
        if(list.size()>0){
            //先将所有的定时任务进行删除 防止业务处理时间过长导致重复处理
            StringBuffer sb=new StringBuffer(list.get(0).getId().toString());
            for(TimedTask timedTask:list){
                sb.append(","+timedTask.getId());
            }
            timedTaskDao.deleteByIds(sb.toString());
            for(TimedTask timedTask:list){
                if(timedTask.getType()==1){//修改助力活动的状态
                    updateHelpTask(timedTask);
                }else if(timedTask.getType()==2){//修改参加活动的状态
                    updateSendOrderState5(timedTask);
                }else if(timedTask.getType()==3){//修改接单助力活动为通过状态
                    updateSendOrderState2(timedTask);
                }else if(timedTask.getType()==4){//修改派单信息为结算
                    updateHelpTakeOrderState3(timedTask);
                }else if(timedTask.getType()==5){//修改派单信息为结算
                    updateHelpTakeIsTop(timedTask);
                }
            }
        }
    }

    //修改助力活动方法
    @Transactional
    public void updateHelpTask(TimedTask timedTask){
        //获取我的派单信息
        HelpTakeOrder helpTakeOrder=helpTakeOrderDao.selectHelpTakeOrderByHelpTaskId(timedTask.getObjectId());
        if(helpTakeOrder!=null){
            //修改助力活动状态为结束
            helpTaskDao.updateHelpTaskState(2,timedTask.getObjectId());
            //修改我的派单信息为结束
            helpTakeOrderDao.updateHelpTakeOrder(helpTakeOrder.getState(),helpTakeOrder.getId(),"已结束",0);
            //助力活动结束之后 定时三天后自动结算
            TimedTask newTimedTask=new TimedTask();
            newTimedTask.setType(4);
            newTimedTask.setObjectId(timedTask.getObjectId());
            newTimedTask.setTitle("助力活动自动结算");
            newTimedTask.setCreateTime(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE,3);
            newTimedTask.setEndTime(cal.getTime());
            timedTaskDao.insert(newTimedTask);
        }

    }

    //修改接单助力活动为延期状态
    @Transactional
    public void updateSendOrderState5(TimedTask timedTask){
        HelpSendOrder helpSendOrder=helpSendOrderDao.selectById(timedTask.getObjectId());
        if(helpSendOrder!=null){
            //修改接单状态
            helpSendOrderDao.updateHelpSendOrder(timedTask.getObjectId(),7,"已延期",0);
            //超时未提交扣除信用分
            helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),2,1);
            HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
            helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
            helpUserScoreDetails.setTitle("任务超时，Id："+helpSendOrder.getHelpTaskId());
            helpUserScoreDetails.setContent("扣除积分：1");
            helpUserScoreDetails.setCreateTime(sdf.format(new Date()));
            helpUserScoreDetails.setScore(1);
            helpUserScoreDetails.setType(2);
            helpUserScoreDetailsService.insert(helpUserScoreDetails);
        }
    }

    //修改接单助力活动为通过状态
    @Transactional
    public void updateSendOrderState2(TimedTask timedTask){
        HelpSendOrder helpSendOrder=helpSendOrderDao.selectById(timedTask.getObjectId());
        if(helpSendOrder!=null){
            String date=sdf.format(new Date());
            UserEntity userByWxId = userService.selectById(helpSendOrder.getUserId());
            //修改接单状态
            helpSendOrderDao.updateHelpSendOrder(timedTask.getObjectId(),2,"已同意",0);
            //给接单人增加金额
            HelpTask helpTask= helpTaskDao.selectById(helpSendOrder.getHelpTaskId());
            userMoneyService.updateMayMoney(1,helpSendOrder.getUserId(),helpSendOrder.getMoney());
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(helpSendOrder.getUserId());
            userMoneyDetails.setTitle("[完成任务]任务ID："+helpTask.getId());
            userMoneyDetails.setContent("增加金额:"+helpSendOrder.getMoney());
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(helpSendOrder.getMoney());
            userMoneyDetails.setCreateTime(date);
            userMoneyDetailsService.insert(userMoneyDetails);
            //完成接单增加信誉分
            helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),1,1);
            HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
            helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
            helpUserScoreDetails.setTitle("[完成任务]任务Id："+helpSendOrder.getHelpTaskId());
            helpUserScoreDetails.setContent("增加积分：1");
            helpUserScoreDetails.setCreateTime(date);
            helpUserScoreDetails.setScore(1);
            helpUserScoreDetails.setType(1);
            helpUserScoreDetailsService.insert(helpUserScoreDetails);
            if(helpSendOrder.getZhiUserId()!=null && helpSendOrder.getZhiUserId()!=0L){
                userMoneyService.updateMayMoney(1,helpSendOrder.getZhiUserId(),helpSendOrder.getZhiMoney());
                UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                userMoneyDetail.setUserId(helpSendOrder.getZhiUserId());
                userMoneyDetail.setTitle("[直属赏金]任务ID："+helpTask.getId());
                userMoneyDetail.setContent("直属用户："+userByWxId.getNickName()+"完成任务，增加金额:"+helpSendOrder.getZhiMoney());
                userMoneyDetail.setType(1);
                userMoneyDetail.setMoney(helpSendOrder.getZhiMoney());
                userMoneyDetail.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetail);
            }
            if(helpSendOrder.getFeiUserId()!=null && helpSendOrder.getFeiUserId()!=0L){
                userMoneyService.updateMayMoney(1,helpSendOrder.getFeiUserId(),helpSendOrder.getFeiMoney());
                UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                userMoneyDetail.setUserId(helpSendOrder.getFeiUserId());
                userMoneyDetail.setTitle("[非直属赏金]任务ID："+helpTask.getId());
                userMoneyDetail.setContent("非直属用户："+userByWxId.getNickName()+"完成任务，增加金额:"+helpSendOrder.getFeiMoney());
                userMoneyDetail.setType(1);
                userMoneyDetail.setMoney(helpSendOrder.getFeiMoney());
                userMoneyDetail.setCreateTime(date);
                userMoneyDetailsService.insert(userMoneyDetail);
            }
            //完成一单之后给平台计算利润
            HelpProfit helpProfit=new HelpProfit();
            helpProfit.setCreateTime(date);
            helpProfit.setHelpTaskId(helpTask.getId());
            helpProfit.setHelpSendOrderId(helpSendOrder.getId());
            helpProfit.setProfit(helpSendOrder.getPingMoney());
            helpProfitDao.insert(helpProfit);
        }
    }

    //修改派单信息为结算
    @Transactional
    public void updateHelpTakeOrderState3(TimedTask timedTask){
        HelpTask helpTask=helpTaskDao.selectById(timedTask.getObjectId());
        Integer count=helpMaintainService.selectCount(helpTask.getId(),0);
        if(count==null || count==0){
            helpTaskService.finishHelpTask(helpTask);
        }

    }

    //将置顶任务下架
    public void updateHelpTakeIsTop(TimedTask timedTask){
        HelpTask helpTask = helpTaskDao.selectById(timedTask.getObjectId());
        if(helpTask!=null){
         helpTask.setIsTop(0);
         helpTaskDao.updateById(helpTask);
        }
    }


}
