package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.dao.*;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.*;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.message.dao.MessageInfoDao;
import com.sqx.modules.message.entity.MessageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 维权信息
 */
@Service
public class HelpMaintainServiceImpl extends ServiceImpl<HelpMaintainDao, HelpMaintain> implements HelpMaintainService {

    /** 维权信息 */
    @Autowired
    private HelpMaintainDao helpMaintainDao;
    /** 接单信息 */
    @Autowired
    private HelpSendOrderService helpSendOrderService;
    /** 维权详细信息 */
    @Autowired
    private HelpMaintainDetailsDao helpMaintainDetailsDao;
    /** 接单信息自定义方法类 */
    @Autowired
    private HelpSendOrderDao helpSendOrderDao;
    /** 助力信息自定义方法类 */
    @Autowired
    private HelpTaskDao helpTaskDao;
    /** 用户钱包 */
    @Autowired
    private UserMoneyService userMoneyService;
    /** 用户钱包明细 */
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    /** 信誉分 */
    @Autowired
    private HelpUserScoreService helpUserScoreService;
    /** 信誉明细分 */
    @Autowired
    private HelpUserScoreDetailsService helpUserScoreDetailsService;
    /** 信誉明细分 */
    @Autowired
    private HelpTakeOrderService helpTakeOrderService;
    @Autowired
    private HelpProfitDao helpProfitDao;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageInfoDao messageInfoDao;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Override
    @Transactional
    public int insert(HelpMaintain helpMaintain, HelpSendOrder helpSendOrder) {
        helpMaintain.setCreateTime(sdf.format(new Date()));
        helpMaintain.setState(0);
        helpMaintainDao.insert(helpMaintain);
        HelpMaintainDetails oldHelpMaintainDetails=new HelpMaintainDetails();
        oldHelpMaintainDetails.setHelpMaintainId(helpMaintain.getId());
        oldHelpMaintainDetails.setClassify(1);
        oldHelpMaintainDetails.setContent(helpSendOrder.getAuditContent());
//        oldHelpMaintainDetails.setPicture(helpSendOrder.getPicture());
        oldHelpMaintainDetails.setCreateTime(helpSendOrder.getAuditTime());
        oldHelpMaintainDetails.setUserId(helpMaintain.getTakeOrderUserId());
        helpMaintainDetailsDao.insert(oldHelpMaintainDetails);
        HelpMaintainDetails helpMaintainDetails=new HelpMaintainDetails();
        helpMaintainDetails.setHelpMaintainId(helpMaintain.getId());
        helpMaintainDetails.setClassify(2);
        helpMaintainDetails.setContent(helpSendOrder.getContent());
//        helpMaintainDetails.setPicture(helpSendOrder);
        helpMaintainDetails.setCreateTime(sdf.format(new Date()));
        helpMaintainDetails.setUserId(helpMaintain.getSendOrderUserId());
        helpMaintainDetailsDao.insert(helpMaintainDetails);
        helpSendOrderService.update(helpSendOrder.getId(),4);//接单状态修改为维权
        return 1;
    }

    @Override
    public Integer auditHelpMaintain(String helpMaintainIds,Integer state,String content){
        for(String id:helpMaintainIds.split(",")){
            Long helpMaintainId=Long.parseLong(id);
            state=state-1;
            String date=sdf.format(new Date());
            HelpMaintain oldHelpMaintain = helpMaintainDao.selectById(helpMaintainId);
            if(oldHelpMaintain.getState()==0){
                HelpSendOrder helpSendOrder=helpSendOrderDao.selectById(oldHelpMaintain.getHelpSendOrderId());
                HelpTakeOrder helpTakeOrder=helpTakeOrderService.selectById(oldHelpMaintain.getHelpTakeOrderId());
                UserEntity userSendOrder = userService.selectById(helpSendOrder.getUserId());
                UserEntity userTakeOrder = userService.selectById(helpTakeOrder.getUserId());
                MessageInfo messageInfo = new MessageInfo();
                MessageInfo messageInfo1 = new MessageInfo();
                if(state==1){//接单人获胜
                    //先获取之前的接单拒绝原因，并将扣除的费用加回来
                    HelpTask helpTask= helpTaskDao.selectById(helpSendOrder.getHelpTaskId());
                    if(helpSendOrder.getCategory()==1){
                        helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),1,2);
                        HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                        helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
                        helpUserScoreDetails.setTitle("[维权获胜]任务Id："+helpSendOrder.getHelpTaskId());
                        helpUserScoreDetails.setContent("增加积分：2");
                        helpUserScoreDetails.setCreateTime(date);
                        helpUserScoreDetails.setScore(2);
                        helpUserScoreDetails.setType(1);
                        helpUserScoreDetailsService.insert(helpUserScoreDetails);
                    }else if(helpSendOrder.getCategory()==2){
                        helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),1,3);
                        HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                        helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
                        helpUserScoreDetails.setTitle("[维权获胜]任务Id："+helpSendOrder.getHelpTaskId());
                        helpUserScoreDetails.setContent("增加积分：3");
                        helpUserScoreDetails.setCreateTime(date);
                        helpUserScoreDetails.setScore(3);
                        helpUserScoreDetails.setType(1);
                        helpUserScoreDetailsService.insert(helpUserScoreDetails);
                    }
                    //修改接单状态
                    helpSendOrderDao.updateHelpSendOrder(oldHelpMaintain.getHelpSendOrderId(),2,"已同意",0);
                    //给接单人增加金额
                    userMoneyService.updateMayMoney(1,helpSendOrder.getUserId(),helpSendOrder.getMoney());
                    UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                    userMoneyDetails.setUserId(helpSendOrder.getUserId());
                    userMoneyDetails.setTitle("[完成任务]任务ID："+helpTask.getId());
                    userMoneyDetails.setContent("增加金额:"+helpSendOrder.getMoney());
                    userMoneyDetails.setType(1);
                    userMoneyDetails.setMoney(helpSendOrder.getMoney());
                    userMoneyDetails.setCreateTime(date);
                    userMoneyDetailsService.insert(userMoneyDetails);
                    //完成接单增加信誉分
                    helpUserScoreService.updateUserScore(helpSendOrder.getUserId(),1,1);
                    HelpUserScoreDetails helpUserScoreDetails=new HelpUserScoreDetails();
                    helpUserScoreDetails.setUserId(helpSendOrder.getUserId());
                    helpUserScoreDetails.setTitle("[完成任务]任务Id："+helpSendOrder.getHelpTaskId());
                    helpUserScoreDetails.setContent("增加积分：1");
                    helpUserScoreDetails.setCreateTime(date);
                    helpUserScoreDetails.setScore(1);
                    helpUserScoreDetails.setType(1);
                    helpUserScoreDetailsService.insert(helpUserScoreDetails);

                    //判定派单人失败 扣除信用分
                    helpUserScoreService.updateUserScore(helpTakeOrder.getUserId(),2,1);
                    HelpUserScoreDetails helpUserScoreDetails1=new HelpUserScoreDetails();
                    helpUserScoreDetails1.setUserId(helpTakeOrder.getUserId());
                    helpUserScoreDetails1.setTitle("[维权失败]任务Id："+helpSendOrder.getHelpTaskId());
                    helpUserScoreDetails1.setContent("扣除积分：1");
                    helpUserScoreDetails1.setCreateTime(date);
                    helpUserScoreDetails1.setScore(1);
                    helpUserScoreDetails1.setType(2);
                    helpUserScoreDetailsService.insert(helpUserScoreDetails);
                    if(helpSendOrder.getZhiUserId()!=null && helpSendOrder.getZhiUserId()!=0L){
                        userMoneyService.updateMayMoney(1,helpSendOrder.getZhiUserId(),helpSendOrder.getZhiMoney());
                        UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                        userMoneyDetail.setUserId(helpSendOrder.getZhiUserId());
                        userMoneyDetail.setTitle("[直属赏金]任务ID："+helpTask.getId());
                        userMoneyDetail.setContent("直属用户："+userSendOrder.getNickName()+"完成任务，增加金额:"+helpSendOrder.getZhiMoney());
                        userMoneyDetail.setType(1);
                        userMoneyDetail.setMoney(helpSendOrder.getZhiMoney());
                        userMoneyDetail.setCreateTime(date);
                        userMoneyDetailsService.insert(userMoneyDetail);
                    }
                    if(helpSendOrder.getFeiUserId()!=null && helpSendOrder.getFeiUserId()!=0L){
                        userMoneyService.updateMayMoney(1,helpSendOrder.getFeiUserId(),helpSendOrder.getFeiMoney());
                        UserMoneyDetails userMoneyDetail=new UserMoneyDetails();
                        userMoneyDetail.setUserId(helpSendOrder.getFeiUserId());
                        userMoneyDetail.setTitle("[非直属赏金]任务ID："+helpTask.getId());
                        userMoneyDetail.setContent("非直属用户："+userSendOrder.getNickName()+"完成任务，增加金额:"+helpSendOrder.getFeiMoney());
                        userMoneyDetail.setType(1);
                        userMoneyDetail.setMoney(helpSendOrder.getFeiMoney());
                        userMoneyDetail.setCreateTime(date);
                        userMoneyDetailsService.insert(userMoneyDetail);
                    }
                    //完成一单之后给平台计算利润
                    HelpProfit helpProfit=new HelpProfit();
                    helpProfit.setCreateTime(date);
                    helpProfit.setHelpTaskId(helpTask.getId());
                    helpProfit.setHelpSendOrderId(helpSendOrder.getId());
                    helpProfit.setProfit(helpSendOrder.getPingMoney());
                    helpProfitDao.insert(helpProfit);
                    messageInfo.setContent(userSendOrder.getNickName() + " 您好！您的维权判定成功！");
                    if (userSendOrder.getClientid() != null) {
                        userService.pushToSingle("任务维权", userSendOrder.getNickName() + " 您好！您的维权判定成功！", userSendOrder.getClientid());
                    }
                    messageInfo1.setContent(userTakeOrder.getNickName() + " 您好！您的维权判定失败！");
                    if (userTakeOrder.getClientid() != null) {
                        userService.pushToSingle("任务维权", userTakeOrder.getNickName() + " 您好！您的维权判定失败！", userTakeOrder.getClientid());
                    }
                }else{
                    messageInfo.setContent(userSendOrder.getNickName() + " 您好！您的维权判定失败！");
                    if (userSendOrder.getClientid() != null) {
                        userService.pushToSingle("任务维权", userSendOrder.getNickName() + " 您好！您的维权判定失败！", userSendOrder.getClientid());
                    }
                    messageInfo1.setContent(userTakeOrder.getNickName() + " 您好！您的维权判定成功！");
                    if (userTakeOrder.getClientid() != null) {
                        userService.pushToSingle("任务维权", userTakeOrder.getNickName() + " 您好！您的维权判定成功！", userTakeOrder.getClientid());
                    }
                    //修改接单状态
                    helpSendOrderDao.updateHelpSendOrder(oldHelpMaintain.getHelpSendOrderId(),6,"维权失败",0);
                }
                messageInfo.setState(String.valueOf(6));
                messageInfo.setTitle("任务维权");
                messageInfo.setUserName(userSendOrder.getNickName());
                messageInfo.setUserId(String.valueOf(userSendOrder.getUserId()));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                messageInfo.setCreateAt(sdf.format(now));
                messageInfoDao.insert(messageInfo);
                messageInfo1.setState(String.valueOf(6));
                messageInfo1.setTitle("任务维权");
                messageInfo1.setUserName(userSendOrder.getNickName());
                messageInfo1.setUserId(String.valueOf(userSendOrder.getUserId()));
                messageInfo1.setCreateAt(sdf.format(now));
                messageInfoDao.insert(messageInfo1);
                return helpMaintainDao.updateState(helpMaintainId,state,content);
            }
        }

        return 1;
    }

    @Override
    public IPage<HelpMaintainModel> selectHelpMaintainList(int page, int limit) {
        Page<HelpMaintainModel> pages=new Page<>(page,limit);
        return helpMaintainDao.selectHelpMaintainList(pages);
    }


    @Override
    public PageUtils selectHelpMaintainListBySendOrder(int page, int limit, Long sendOrderUserId, Integer state) {
        Page<Map<String, Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpMaintainDao.selectHelpMaintainListBySendOrder(pages,sendOrderUserId,state));
    }

    @Override
    public PageUtils selectHelpMaintainListByTakeOrder(int page, int limit,Long takeOrderUserId, Integer state) {
        Page<Map<String, Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpMaintainDao.selectHelpMaintainListByTakeOrder(pages,takeOrderUserId,state));
    }

    @Override
    public HelpMaintain selectById(Long helpMaintainId) {
        return helpMaintainDao.selectById(helpMaintainId);
    }

    @Override
    public Integer selectCount(Long helpTaskId,Integer state) {
        return helpMaintainDao.selectCount(helpTaskId,state);
    }
}