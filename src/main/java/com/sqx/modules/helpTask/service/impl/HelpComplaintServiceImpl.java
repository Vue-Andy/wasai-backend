package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.dao.CashClassifyDao;
import com.sqx.modules.helpTask.dao.HelpComplaintDao;
import com.sqx.modules.helpTask.dao.HelpTakeOrderDao;
import com.sqx.modules.helpTask.dao.HelpTaskDao;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpComplaint;
import com.sqx.modules.helpTask.entity.HelpTakeOrder;
import com.sqx.modules.helpTask.entity.HelpTask;
import com.sqx.modules.helpTask.service.CashClassifyService;
import com.sqx.modules.helpTask.service.HelpComplaintService;
import com.sqx.modules.helpTask.service.HelpTakeOrderService;
import com.sqx.modules.helpTask.service.HelpTaskService;
import com.sqx.modules.message.entity.MessageInfo;
import com.sqx.modules.message.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 提现分类
 */
@Service
public class HelpComplaintServiceImpl extends ServiceImpl<HelpComplaintDao, HelpComplaint> implements HelpComplaintService {

    @Autowired
    private HelpComplaintDao helpComplaintDao;
    @Autowired
    private HelpTaskDao helpTaskDao;
    @Autowired
    private HelpTakeOrderService helpTakeOrderService;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;

    @Override
    public int insertHelpComplaint(HelpComplaint helpComplaint){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        helpComplaint.setState(1);
        helpComplaint.setCreateTime(sdf.format(new Date()));
        return helpComplaintDao.insert(helpComplaint);
    }

    @Override
    public int updateHelpComplaint(HelpComplaint helpComplaint){
        return helpComplaintDao.updateById(helpComplaint);
    }

    @Override
    public Result solveHelpComplaint(String ids,int state){
        for(String id:ids.split(",")){
            HelpComplaint helpComplaint = helpComplaintDao.selectById(Long.parseLong(id));
            if(helpComplaint.getState()==1){
                helpComplaint.setState(2);
                helpComplaintDao.updateById(helpComplaint);
                HelpTask helpTask = helpTaskDao.selectById(helpComplaint.getHelpTaskId());
                if(helpTask!=null){
                    if(helpTask.getState()!=2){
                        if(state==2){
                            helpTask.setState(2);
                            HelpTakeOrder helpTakeOrder = helpTakeOrderService.selectHelpTakeOrderByHelpTaskId(helpTask.getId());
                            helpTakeOrder.setState(3);
                            helpTaskDao.updateById(helpTask);
                            helpTakeOrderService.updateHelpTakeOrder(helpTakeOrder);
                            UserEntity userEntity = userService.selectById(helpTakeOrder.getUserId());
                            MessageInfo messageInfo=new MessageInfo();
                            messageInfo.setContent("您好，您的任务已经被下架，请遵守平台规定！");
                            messageInfo.setState(String.valueOf(6));
                            messageInfo.setTitle("任务下架通知！");
                            messageInfo.setUserName(userEntity.getNickName());
                            messageInfo.setUserId(String.valueOf(userEntity.getUserId()));
                            messageService.saveBody(messageInfo);

                            messageInfo.setContent("您好，您的投诉平台已经审核，该任务已经下架，感谢你为平台环境做出的贡献！");
                            messageInfo.setTitle("投诉结果通知！");
                            messageInfo.setUserName(userEntity.getNickName());
                            messageInfo.setUserId(String.valueOf(userEntity.getUserId()));
                            messageService.saveBody(messageInfo);

                        }else{
                            UserEntity userEntity = userService.selectById(helpComplaint.getUserId());
                            MessageInfo messageInfo=new MessageInfo();
                            messageInfo.setContent("您好，您的投诉平台已经审核，暂不做下架处理，感谢你为平台环境做出的贡献！");
                            messageInfo.setTitle("投诉结果通知！");
                            messageInfo.setState(String.valueOf(6));
                            messageInfo.setUserName(userEntity.getNickName());
                            messageInfo.setUserId(String.valueOf(userEntity.getUserId()));
                            messageService.saveBody(messageInfo);
                        }
                    }
                }
            }


        }
        return Result.success();
    }


    @Override
    public int deleteHelpComplaintByIds(String ids){
        int i=0;
        for(String id:ids.split(",")){
             i+=helpComplaintDao.deleteById(Long.parseLong(id));
        }
        return i;
    }

    @Override
    public PageUtils selectHelpComplaint(int page,int limit,String content,Long userId){
        Page<HelpComplaint> pages=new Page<HelpComplaint>(page,limit);
        return new PageUtils(helpComplaintDao.selectHelpComplaintList(pages,content,userId));
    }

    @Override
    public HelpComplaint selectHelpComplaintDetails(Long id) {
        return helpComplaintDao.selectHelpComplaintDetails(id);
    }


}
