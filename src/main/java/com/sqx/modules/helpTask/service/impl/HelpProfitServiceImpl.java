package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.dao.HelpProfitDao;
import com.sqx.modules.helpTask.entity.HelpProfit;
import com.sqx.modules.helpTask.service.HelpProfitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * 收入明细
 */
@Service
public class HelpProfitServiceImpl extends ServiceImpl<HelpProfitDao, HelpProfit> implements HelpProfitService {

    private final HelpProfitDao helpProfitDao;

    @Autowired
    public HelpProfitServiceImpl(HelpProfitDao helpProfitDao) {
        this.helpProfitDao = helpProfitDao;
    }

    @Override
    public PageUtils selectHelpProfitList(int page,int limit,String createTime,String endTime){
        Page<HelpProfit> pages=new Page<>(page,limit);
        if(StringUtils.isEmpty(endTime)){
            endTime=createTime;
        }
        return new PageUtils(helpProfitDao.selectHelpProfitList(pages,createTime,endTime));
    }

    @Override
    public Double selectSumProfit(String createTime, String endTime) {
        return helpProfitDao.selectSumProfit(createTime,endTime);
    }

    @Override
    public Result statistical() {
        Calendar cal = Calendar.getInstance();
        Double month = helpProfitDao.statisticalMonth(cal.getTime());
        Double day = helpProfitDao.statisticalDay(cal.getTime());
        Map map = new HashMap<>();
        map.put("month", month);
        map.put("day", day);
        return Result.success().put("data", map);
    }

    @Override
    public Double sumMoneyByProfit(String time, Integer flag) {
        return helpProfitDao.sumMoneyByProfit(time,flag);
    }

    @Override
    public Double sumMemberMoneyByProfit(String time, Integer flag) {
        return helpProfitDao.sumMemberMoneyByProfit(time,flag);
    }

    @Override
    public PageUtils incomeAnalysis(int page, int limit, String time, Integer flag) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpProfitDao.incomeAnalysis(pages,time,flag));
    }
}
