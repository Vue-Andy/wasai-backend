package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.modules.helpTask.entity.HelpBrowsingHistory;

import java.util.Date;


public interface HelpBrowsingHistoryService {

    int insert(HelpBrowsingHistory helpBrowsingHistory);

    PageUtils selectHelpBrowsingHistoryList(int page, int limit, Long userId);

    Long selectHelpBrowsingHistoryByUserId(Long userId, Long helpTaskId);

    int updateCreateTimeByUserId(Long helpBrowsingId, Date createTime);

}
