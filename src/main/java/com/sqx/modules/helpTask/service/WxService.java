package com.sqx.modules.helpTask.service;

import java.util.Map;

/**
 * @author fang
 * @date 2020/2/26
 */
public interface WxService {

    String payBack(String resXml,Integer type);

    Map doUnifiedOrder(String money, Long userId, Integer type,Integer classify,String remark) throws Exception;

    public String getGeneralOrder();

}