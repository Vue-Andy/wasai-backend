package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.dao.HelpTaskDetailsDao;
import com.sqx.modules.helpTask.entity.HelpTaskDetails;
import com.sqx.modules.helpTask.service.HelpTaskDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 助力详细信息
 */
@Service
public class HelpTaskDetailsServiceImpl extends ServiceImpl<HelpTaskDetailsDao, HelpTaskDetails> implements HelpTaskDetailsService {

    /** 助力详细信息 */
    private final HelpTaskDetailsDao helpTaskDetailsDao;

    @Autowired
    public HelpTaskDetailsServiceImpl(HelpTaskDetailsDao helpTaskDetailsDao) {
        this.helpTaskDetailsDao = helpTaskDetailsDao;
    }

    @Override
    public Result saveAll(List<HelpTaskDetails> HelpTaskDetailsList) {
        for (HelpTaskDetails helpTaskDetails:HelpTaskDetailsList){
            helpTaskDetailsDao.insert(helpTaskDetails);
        }
        return Result.success();
    }

    @Override
    public List<HelpTaskDetails> selectByHelpTaskId(Long helpTaskId) {
        return helpTaskDetailsDao.selectByHelpTaskId(helpTaskId);
    }


}
