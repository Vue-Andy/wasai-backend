package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.HelpRateDao;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.service.HelpRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 平台抽成
 */
@Service
public class HelpRateServiceImpl extends ServiceImpl<HelpRateDao, HelpRate> implements HelpRateService {

    /** 平台抽成 */
    private final HelpRateDao helpRateDao;

    @Autowired
    public HelpRateServiceImpl(HelpRateDao helpRateDao) {
        this.helpRateDao = helpRateDao;
    }

    @Override
    public List<HelpRate> findAll() {
        return helpRateDao.findAll();
    }

    @Override
    public HelpRate selectRateByTypeId(Integer type,Integer classify ) {
        return helpRateDao.selectRateByTypeId(type,classify);
    }

    @Override
    public List<HelpRate> selectRateByClassifyList(Integer classify,Integer isShopTask) {
        return helpRateDao.selectRateByRateList(classify,isShopTask);
    }

    @Override
    public int updateByMoney(Long id,Double money){
        HelpRate helpRate=new HelpRate();
        helpRate.setId(id);
        helpRate.setRate(money);
        return helpRateDao.updateById(helpRate);
    }

    @Override
    public List<HelpRate> selectRateList(){
        return helpRateDao.selectRateList();
    }



}
