package com.sqx.modules.helpTask.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.dao.HelpProfitDao;
import com.sqx.modules.helpTask.dao.PayDetailsDao;
import com.sqx.modules.helpTask.dao.UserMoneyDao;
import com.sqx.modules.helpTask.dao.UserMoneyDetailsDao;
import com.sqx.modules.helpTask.entity.*;
import com.sqx.modules.helpTask.service.CashOutService;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.AmountCalUtils;
import com.sqx.modules.integral.service.UserIntegralService;
import com.sqx.modules.invite.service.InviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用户信誉分数明细
 */
@Service
public class UserMoneyServiceImpl extends ServiceImpl<UserMoneyDao, UserMoney> implements UserMoneyService {

    /** 用户信誉分数明细 */
    @Autowired
    private UserMoneyDao userMoneyDao;
    @Autowired
    private UserMoneyDetailsDao userMoneyDetailsDao;
    @Autowired
    private UserService userService;
    @Autowired
    private CashOutService cashOutService;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private HelpRateService helpRateService;
    @Autowired
    private InviteService inviteService;
    @Autowired
    private PayDetailsDao payDetailsDao;
    @Autowired
    private HelpProfitDao helpProfitDao;
    /** 用户金额信息 */
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;
    @Autowired
    private UserIntegralService userIntegralService;


    @Override
    public Result sendMoney(int integralNum,double money){
        if(integralNum<=0){
            return Result.error("积分输入有误，请重新输入！");
        }
        if(money<=0){
            return Result.error("金额输入有误，请重新输入！");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Long> longs = userIntegralService.selectUserIntegral(integralNum);
        for(Long userId:longs){
            updateMayMoney(1,userId,money);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("[积分奖励]平台积分奖励");
            userMoneyDetails.setContent("恭喜您，积分达到平台要求，赠送您账户金额:"+money);
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(money);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
        }
        Double d=AmountCalUtils.mul(longs.size(),money);
        //对金额进行处理 保留两位 不四舍五入
        DecimalFormat formater = new DecimalFormat();
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(0);
        formater.setRoundingMode(RoundingMode.FLOOR);
        return Result.success("发送成功，共发送："+longs.size()+"个人，总计金额："+formater.format(d)+"元");
    }





    @Override
    public UserMoney selectByUserId(Long userId) {
        UserMoney userMoney= userMoneyDao.selectByUserId(userId);
        if(userMoney==null){
            userMoney=new UserMoney();
            userMoney.setUserId(userId);
            userMoney.setCannotMoney(0.00);
            userMoney.setMayMoney(0.00);
            insert(userMoney);
            userMoney= userMoneyDao.selectByUserId(userId);
        }
        return userMoney;
    }

    @Override
    public Integer insert(UserMoney userMoney) {
        userMoneyDao.insert(userMoney);
        return 1;
    }

    /**
     *  不可提现金额
     * @param type 操作方式 1是加 2是减
     * @param money 金额
     * @return
     */
    @Override
    public Integer updateCannotMoney(int type,Long userId,Double money){
        //操作金额前先判断下是否有钱包
        UserMoney userMoney=selectByUserId(userId);
        if(type==1){
            return userMoneyDao.updateCannotMoneyAdd(userId,money);
        }else{
            return userMoneyDao.updateCannotMoneySub(userId,money);
        }
    }

    /**
     *  可提现金额
     * @param type 操作方式 1是加 2是减
     * @param money 金额
     * @return
     */
    @Override
    public Integer updateMayMoney(int type,Long userId,Double money){
        //操作金额前先判断下是否有钱包
        UserMoney userMoney=selectByUserId(userId);
        if(type==1){
            return userMoneyDao.updateMayMoneyMoneyAdd(userId,money);
        }else{
            return userMoneyDao.updateMayMoneyMoneySub(userId,money);
        }
    }


    @Override
    @Transactional
    public Result cashMoney(Long userId,Double money){
        if(money==null || money<=0.00){
            return Result.error(-100,"请不要输入小于0的数字,请输入正确的提现金额！");
        }
        UserMoney userMoney=selectByUserId(userId);
        CommonInfo one = commonInfoService.findOne(87);
        if(one!=null && money<Double.parseDouble(one.getValue())){
            return Result.error(-100,"输入金额不满足最低提现金额，请重新输入！");
        }
        CommonInfo one1 = commonInfoService.findOne(114);
        Double mul = AmountCalUtils.mul(money, Double.parseDouble(one1.getValue()));
        if(mul<0.01){
            mul=0.01;
        }
        Double moneys=AmountCalUtils.add(money,mul);
        //提现判断金额是否足够
        if(userMoney.getMayMoney()>=moneys){
            //扣除可提现金额
            userMoneyDao.updateMayMoneyMoneySub(userId,moneys);
            //增加金额操作记录
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("提现："+money);
            userMoneyDetails.setContent("支付宝提现金额："+money+"元，扣除手续费："+mul+"元");
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(money);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsDao.insert(userMoneyDetails);
            UserEntity userEntity = userService.selectById(userId);
            CashOut cashOut = new CashOut();
            cashOut.setState(0);
            cashOut.setZhifubao(userEntity.getZhifubao());
            cashOut.setZhifubaoName(userEntity.getZhifubaoName());
            cashOut.setMoney(money.toString());
            cashOut.setRelationId(mul.toString());
            cashOut.setCreateAt(sdf.format(new Date()));
            cashOut.setUserId(userId);
            cashOutService.saveBody(cashOut);
            return Result.success("提现成功，将在三个工作日内到账，请耐心等待！");
        }else if(userMoney.getMayMoney()>=money){
            Double sub = AmountCalUtils.sub(money, mul);
            //扣除可提现金额
            userMoneyDao.updateMayMoneyMoneySub(userId,money);
            //增加金额操作记录
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("提现："+sub);
            userMoneyDetails.setContent("支付宝提现金额："+sub+"元，扣除手续费："+mul+"元");
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(sub);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsDao.insert(userMoneyDetails);
            UserEntity userEntity = userService.selectById(userId);
            CashOut cashOut = new CashOut();
            cashOut.setState(0);
            cashOut.setZhifubao(userEntity.getZhifubao());
            cashOut.setZhifubaoName(userEntity.getZhifubaoName());
            cashOut.setMoney(sub.toString());
            cashOut.setRelationId(mul.toString());
            cashOut.setCreateAt(sdf.format(new Date()));
            cashOut.setUserId(userId);
            cashOutService.saveBody(cashOut);
            return Result.success("提现成功，将在三个工作日内到账，请耐心等待！");
        }else{
            return Result.error(-100,"金额不足，请输入正确的提现金额！");
        }
    }


    @Transactional
    @Override
    public Result openMember(Long userId,Integer member){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        member=member+1;
        if(member>4){
            return Result.error("请求异常，请刷新后重试！");
        }
        UserEntity user = userService.selectById(userId);
        if(user.getMember()>=member){
            return Result.error("您当前的等级已经达到或者超过你要开通的会员了！");
        }
        UserMoney userMoney = selectByUserId(userId);
        HelpRate helpRate = helpRateService.selectRateByTypeId(member, 2);
        String content=member==2?"初级会员":member==3?"中级会员":"高级会员";
        if(helpRate.getRate()>userMoney.getCannotMoney()){
            double add = AmountCalUtils.add(userMoney.getCannotMoney(), userMoney.getMayMoney());
            if(helpRate.getRate()>add){
                return Result.error("您当前的额账户余额不足，请充值后在进行开通！");
            }else{
                Double sub = AmountCalUtils.sub(helpRate.getRate(), userMoney.getCannotMoney());
                if(userMoney.getCannotMoney()>0.00){
                    updateCannotMoney(2,userId,userMoney.getCannotMoney());
                }
                updateMayMoney(2,userId,sub);
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userId);
                userMoneyDetails.setTitle("[开通会员]会员扣费");
                userMoneyDetails.setContent("开通"+content+"会员扣费，从不可提现中扣除:"+userMoney.getCannotMoney());
                userMoneyDetails.setType(2);
                userMoneyDetails.setMoney(userMoney.getCannotMoney());
                userMoneyDetails.setCreateTime(format);
                if(userMoney.getCannotMoney()>0.00){
                    userMoneyDetailsService.insert(userMoneyDetails);
                }
                userMoneyDetails.setContent("开通"+content+"会员扣费，不可提现金额不足，从可提现金额中扣除:"+sub);
                userMoneyDetails.setMoney(sub);
                userMoneyDetailsService.insert(userMoneyDetails);
            }
        }else{
            updateCannotMoney(2,userId,helpRate.getRate());
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("[开通会员]会员扣费");
            userMoneyDetails.setContent("开通"+content+"会员扣费，从不可提现中扣除:"+helpRate.getRate());
            userMoneyDetails.setType(2);
            userMoneyDetails.setMoney(helpRate.getRate());
            userMoneyDetails.setCreateTime(format);
            userMoneyDetailsService.insert(userMoneyDetails);
        }
        HelpProfit helpProfit=new HelpProfit();
        helpProfit.setCreateTime(format);
        helpProfit.setHelpTaskId(-1L);
        helpProfit.setHelpSendOrderId(-1L);
        helpProfit.setProfit(helpRate.getRate());
        helpProfitDao.insert(helpProfit);
        user.setUserId(user.getUserId());
        user.setMember(member);
        userService.updateById(user);
        UserEntity userEntity = userService.selectById(user.getUserId());
        //开通会员同样给赏金
        inviteService.updateInvite(userEntity,sdf.format(new Date()));
        PayDetails payDetails=new PayDetails();
        payDetails.setState(-1);
        payDetails.setCreateTime(sdf.format(new Date()));
        payDetails.setOrderId("钱包支付开通会员");
        payDetails.setUserId(userId);
        payDetails.setMoney(helpRate.getRate());
        payDetails.setType(2);
        payDetails.setRemark(member.toString());
        payDetailsDao.insert(payDetails);
        return Result.success();
    }


}