package com.sqx.modules.helpTask.service;


import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.CashClassify;
import com.sqx.modules.helpTask.entity.HelpTaskRefresh;

import java.util.List;

public interface HelpTaskRefreshService {

    int insertHelpTaskRefresh(HelpTaskRefresh helpTaskRefresh);

    int selectHelpTaskRefreshCount(Long userId,String time);

    Result refreshTask(Long userId, Long helpTaskId);

    Result topHelpTask(Long userId,Long helpTaskId,Integer time);

    Result refreshTaskMoney(Long userId, Long helpTaskId);

}
