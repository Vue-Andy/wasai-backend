package com.sqx.modules.helpTask.service;

import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpRate;

import java.util.List;


public interface HelpRateService {

    List<HelpRate> findAll();

    HelpRate selectRateByTypeId(Integer type,Integer classify );

    List<HelpRate> selectRateByClassifyList(Integer classify,Integer isShopTask);

    List<HelpRate> selectRateList();

    int updateByMoney(Long id,Double money);


}
