package com.sqx.modules.helpTask.service;

import com.sqx.modules.helpTask.entity.HelpUserScore;


public interface HelpUserScoreService {

    HelpUserScore selectByUserId(Long userId);

    int insert(HelpUserScore helpUserScore);

    Integer updateUserScore(Long userId, Integer type, Integer score);
}
