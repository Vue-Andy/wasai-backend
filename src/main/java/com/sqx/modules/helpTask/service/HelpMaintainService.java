package com.sqx.modules.helpTask.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.helpTask.entity.HelpMaintain;
import com.sqx.modules.helpTask.entity.HelpMaintainModel;
import com.sqx.modules.helpTask.entity.HelpSendOrder;


public interface HelpMaintainService {


//    int insert(HelpMaintain helpMaintain, HelpMaintainDetails helpMaintainDetails, HelpSendOrder helpSendOrder);

    int insert(HelpMaintain helpMaintain, HelpSendOrder helpSendOrder);

    PageUtils selectHelpMaintainListBySendOrder(int page, int limit, Long sendOrderUserId, Integer state);

    PageUtils selectHelpMaintainListByTakeOrder(int page, int limit,Long takeOrderUserId, Integer state);

    HelpMaintain selectById(Long helpMaintainId);

    Integer selectCount(Long helpTaskId, Integer state);

    Integer auditHelpMaintain(String helpMaintainId, Integer state, String content);

    IPage<HelpMaintainModel> selectHelpMaintainList(int page, int limit);


}
