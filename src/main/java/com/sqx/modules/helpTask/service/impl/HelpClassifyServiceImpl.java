package com.sqx.modules.helpTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.helpTask.dao.HelpClassifyDao;
import com.sqx.modules.helpTask.dao.HelpTaskDao;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.helpTask.entity.HelpClassifyDetails;
import com.sqx.modules.helpTask.entity.HelpClassifyModel;
import com.sqx.modules.helpTask.service.HelpClassifyDetailsService;
import com.sqx.modules.helpTask.service.HelpClassifyService;
import com.sqx.modules.helpTask.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 一级分类
 */
@Service
public class HelpClassifyServiceImpl extends ServiceImpl<HelpClassifyDao, HelpClassify> implements HelpClassifyService {

    @Autowired
    private HelpClassifyDao helpClassifyDao;
    @Autowired
    private HelpClassifyDetailsService helpClassifyDetailsService;
    @Autowired
    private HelpTaskDao helpTaskDao;

    @Override
    public List<HelpClassifyModel> getHelpClassifyModel(){
        List<HelpClassify> list1=helpClassifyDao.selectList();
        List<HelpClassifyModel> list3= BeanHelper.copyWithCollection(list1,HelpClassifyModel.class);
        List<HelpClassifyDetails> list2=helpClassifyDetailsService.selectList();
        for(HelpClassifyModel helpClassify:list3){
            List<HelpClassifyDetails> list4=new ArrayList<>();
            for(HelpClassifyDetails helpClassifyDetails:list2){
                if(helpClassify.getId().equals(helpClassifyDetails.getClassifyId())){
                    list4.add(helpClassifyDetails);
                }
            }
            helpClassify.setList(list4);
        }
        return list3;
    }




    @Override
    public HelpClassify selectById(Long id) {
        return helpClassifyDao.selectById(id);
    }

    @Override
    public List<HelpClassify> selectList() {
        return helpClassifyDao.selectList();
    }

    @Override
    public int insert(HelpClassify helpClassify) {
        return helpClassifyDao.insert(helpClassify);
    }

    @Override
    public void saveAll(List<HelpClassify> list) {
        for(HelpClassify helpClassify:list){
            helpClassifyDao.insert(helpClassify);
        }
    }

    @Override
    public int update(HelpClassify helpClassify) {
        return helpClassifyDao.updateById(helpClassify);
    }

    @Override
    public void deleteById(Long id) {
        helpClassifyDao.deleteById(id);
    }


    @Override
    @Transactional
    public void deleteByIdList(String ids) {
        String []id=ids.split(",");
        for(String i:id){
            helpClassifyDao.deleteById(Long.parseLong(i));
        }

    }

}
