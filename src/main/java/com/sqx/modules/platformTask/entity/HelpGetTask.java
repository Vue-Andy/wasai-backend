package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author fang
 * @date 2020/7/15
 * 领取的平台任务
 */

@Data
@TableName("help_get_task")
public class HelpGetTask {
    private Long id;
    private Long helpTaskPlatformId;
    private String picture;
    private String content;
    private Long userId;
    private Integer state;
    private String auditContent;
    private String createTime;

}