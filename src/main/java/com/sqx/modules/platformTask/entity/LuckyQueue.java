package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serializable;

/**
 * 队列规则
 * @author fang
 * @date 2020/7/20
 */
@Data
@TableName("lucky_queue")
public class LuckyQueue implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 队列名称
     */
    private String name;

    /**
     * 队列等级（1、2、3）
     */
    private Integer sort;

    /**
     * 额度
     */
    private Double luckyValue;

    /**
     * 推荐奖
     */
    private Double recommendMoney;

    /**
     * 派送红包
     */
    private Double handOut;

    private Integer numberOfPeople;

    private Integer startPerson;

    private Integer intervalPerson;

    private Integer state;

    /**
     * 时间
     */
    private String createTime;

}
