package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author fang
 * @date 2020/7/15
 */

@Data
@TableName("help_lucky_value")
public class HelpLuckyValue {
    private Long id;
    private Double luckyValue;
    private Double accruingAmounts;
    private Long userId;



}