package com.sqx.modules.platformTask.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serializable;

/**
 * 幸运值队列
 * @author fang
 * @date 2020/7/20
 */

@Data
@TableName("lucky_value_queue")
public class LuckyValueQueue implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 所在队列id
     */
    private Long luckyQueueId;

    /**
     * 状态 1正常 2晋级申请中
     */
    private Integer state;

    /**
     * 时间
     */
    private String createTime;


}
