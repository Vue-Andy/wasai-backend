package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  platform_classify
 * @author fang 2020-06-11
 */

@Data
@TableName("platform_classify")
public class PlatformClassify implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 一级分类id
     */
    private Long id;

    /**
     * 名称
     */
    private String classifyName;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 排序
     */
    private String sort;



}
