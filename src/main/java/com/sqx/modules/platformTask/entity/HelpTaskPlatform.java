package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author fang
 * @date 2020/7/15
 * 平台任务
 */

@Data
@TableName("help_task_platform")
public class HelpTaskPlatform {
    private Long id;
    private Long classifyId;

    @TableField(exist = false)
    private String classifyName;

    private Double luckyValue;
    private String title;
    private String titlePicture;
    private String content;
    private Integer state;
    private String createTime;

}