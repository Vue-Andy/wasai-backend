package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serializable;

/**
 * 用户晋升队列记录审核表
 * @author fang
 * @date 2020/7/20
 */
@Data
@TableName("lucky_queue_record")
public class LuckyQueueRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户晋升队列记录审核表id
     */
    private Long id;

    /**
     * 队列id
     */
    private Long luckyQueueId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 状态（0待审核 1同意 2拒绝）
     */
    private Integer state;

    /**
     * 审核意见
     */
    private String auditContent;

    /**
     * 时间
     */
    private String createTime;


}
