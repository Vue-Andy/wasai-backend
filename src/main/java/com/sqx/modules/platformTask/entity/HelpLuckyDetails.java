package com.sqx.modules.platformTask.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author fang
 * @date 2020/7/20
 */

@Data
@TableName("help_lucky_details")
public class HelpLuckyDetails {
    private Long id;
    private Long userId;
    private Long helpTaskId;
    private String title;
    private String content;
    private Double luckyValue;
    private String createTime;


}