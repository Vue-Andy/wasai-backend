package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.platformTask.entity.LuckyQueueRecord;
import com.sqx.modules.platformTask.entity.LuckyValueQueue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface LuckyValueQueueDao extends BaseMapper<LuckyValueQueue> {

    int updateLuckyValueQueue(@Param("userId") Long userId, @Param("luckyQueueId") Long luckyQueueId, @Param("state") Integer state, @Param("createTime") String createTime);

    LuckyValueQueue selectLuckyValueQueueByUserId(@Param("userId") Long userId);

    Integer selectLuckQueueLocationByUserId(@Param("userId") Long userId, @Param("luckyQueueId") Long luckyQueueId);

    Integer selectLuckQueueCount(@Param("luckyQueueId") Long luckyQueueId);

    Integer selectUserLuckyQueueByInvitation(@Param("userId") Long superior, @Param("sort") Integer sort);

    List<Map<String,Object>> selectLuckQueueLocation(@Param("luckyQueueId") Long luckyQueueId);

    Map<String,Object> selectLuckQueueLocationByUserIds( @Param("userId") Long userId, @Param("luckyQueueId") Long luckyQueueId);

    IPage<Map<String,Object>> selectLuckyQueueByUser(Page<Map<String, Object>> page, @Param("superior") Long superior);

    List<Long> selectLuckyQueueUserIdByLuckyQueueId(@Param("luckyQueueId") Long luckyQueueId);



}
