package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.platformTask.entity.HelpGetTask;
import com.sqx.modules.platformTask.entity.LuckyQueue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface LuckyQueueDao extends BaseMapper<LuckyQueue> {

    List<LuckyQueue> selectLuckyQueueList();

    List<LuckyQueue> selectLuckyQueueListDesc();

    List<LuckyQueue> selectLuckyQueueListASC();

    LuckyQueue selectLuckyQueueBySort(@Param("sort") Integer sort);

    LuckyQueue selectLuckyQueueById(@Param("id") Long id);

    int updateLuckyQueueStateById(@Param("id") Long id);


}
