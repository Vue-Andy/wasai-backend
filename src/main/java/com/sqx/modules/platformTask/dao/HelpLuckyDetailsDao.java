package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.platformTask.entity.HelpGetTask;
import com.sqx.modules.platformTask.entity.HelpLuckyDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface HelpLuckyDetailsDao extends BaseMapper<HelpLuckyDetails> {

    IPage<HelpLuckyDetails> selectHelpLuckyDetailsListByUserId(Page<Map<String, Object>> page, @Param("userId") Long userId);


}
