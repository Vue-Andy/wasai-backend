package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.platformTask.entity.LuckyQueue;
import com.sqx.modules.platformTask.entity.LuckyQueueRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface LuckyQueueRecordDao extends BaseMapper<LuckyQueueRecord> {

    int updateLuckyQueueRecord(@Param("id") Long id, @Param("state") Integer state, @Param("auditContent") String auditContent);

    LuckyQueueRecord selectByUserId(@Param("userId") Long userId);

    IPage<Map<String,Object>> selectLuckyQueueRecordList(Page<Map<String, Object>> page);

    IPage<Map<String,Object>> selectLuckyQueueRecordByUserId(Page<Map<String, Object>> page, @Param("userId") Long userId);


}
