package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.platformTask.entity.HelpGetTask;
import com.sqx.modules.platformTask.entity.HelpTaskPlatform;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface HelpTaskPlatformDao extends BaseMapper<HelpTaskPlatform> {

    IPage<HelpTaskPlatform> selectHelpTaskPlatformListByState(Page<HelpTaskPlatform> page, @Param("state") Integer state);

    IPage<HelpTaskPlatform> selectHelpTaskPlatformList(Page<HelpTaskPlatform> page, @Param("classifyId") Long classifyId);

    int updateHelpTaskPlatformStateById(@Param("id") Long id, @Param("state") Integer state);

    Map<String,Object> selectByIds(@Param("id") Long id);



}
