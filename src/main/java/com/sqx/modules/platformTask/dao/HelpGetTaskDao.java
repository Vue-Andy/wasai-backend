package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.platformTask.entity.HelpGetTask;
import com.sqx.modules.platformTask.entity.PlatformClassify;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface HelpGetTaskDao extends BaseMapper<HelpGetTask> {

    IPage<Map<String,Object>> selectHelpGetTaskList(Page<Map<String, Object>> page, @Param("userId") Long userId, @Param("state") Integer state);

    Map<String,Object> selectHelpGetTaskById(@Param("id") Long id, @Param("userId") Long userId);

    Integer selectCount(@Param("id") Long id, @Param("userId") Long userId);

    int updateHelpGetTaskState(@Param("id") Long id, @Param("state") Integer state, @Param("auditContent") String auditContent);

    IPage<Map<String,Object>> selectHelpGetTaskLists(Page<Map<String, Object>> page);

    IPage<Map<String,Object>> selectUnderwayHelpGetTaskList(Page<Map<String, Object>> page);


}
