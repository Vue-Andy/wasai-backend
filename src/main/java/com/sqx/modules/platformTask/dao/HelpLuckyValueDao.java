package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.platformTask.entity.HelpLuckyDetails;
import com.sqx.modules.platformTask.entity.HelpLuckyValue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface HelpLuckyValueDao extends BaseMapper<HelpLuckyValue> {

    HelpLuckyValue selectByUserId(@Param("userId") Long userId);

    int updateHelpLuckyValue(@Param("userId") Long userId, @Param("luckyValue") Double luckyValue);

    int updateHelpLuckyValueAccruingAmounts(@Param("userId") Long userId, @Param("accruingAmounts") Double accruingAmounts);

    IPage<Map<String,Object>> selectLuckyQueueRanking(Page<Map<String, Object>> page,@Param("luckyQueueId") Long luckyQueueId,@Param("phone") String phone);



}
