package com.sqx.modules.platformTask.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.invite.entity.Invite;
import com.sqx.modules.platformTask.entity.PlatformClassify;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface PlatformClassifyDao extends BaseMapper<PlatformClassify> {

    List<PlatformClassify> selectList();


}
