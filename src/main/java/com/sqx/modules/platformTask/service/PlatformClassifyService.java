package com.sqx.modules.platformTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.platformTask.entity.PlatformClassify;

import java.util.Date;
import java.util.List;

public interface PlatformClassifyService {

    PlatformClassify selectById(Long id);

    List<PlatformClassify> selectList();

    int insert(PlatformClassify helpClassify);

    void saveAll(List<PlatformClassify> list);

    int update(PlatformClassify helpClassify);

    void deleteById(Long id);

    void deleteByIdList(String ids);

}
