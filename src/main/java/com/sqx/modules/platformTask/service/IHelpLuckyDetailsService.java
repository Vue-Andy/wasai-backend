package com.sqx.modules.platformTask.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.entity.HelpLuckyDetails;
import org.springframework.data.domain.Pageable;

public interface IHelpLuckyDetailsService {

    int saveBody(HelpLuckyDetails helpLuckyDetails);

    PageUtils selectHelpLuckyDetailsListByUserId(int page, int limit, Long userId);

}
