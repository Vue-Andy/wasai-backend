package com.sqx.modules.platformTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.entity.LuckyValueQueue;

import java.util.List;
import java.util.Map;

public interface ILuckyValueQueueService {

    int saveBody(LuckyValueQueue luckyValueQueue);

    int updateLuckyValueQueue(Long userId, Long luckyQueueId, Integer state, String createTime);

    LuckyValueQueue selectLuckyValueQueueByUserId(Long userId);

    Integer selectUserLuckyQueueByInvitation(Long userId, Integer sort);

    Integer selectLuckQueueLocationByUserId(Long userId, Long luckyQueueId);

    Integer selectLuckQueueCount(Long luckyQueueId);

    List<Map<String,Object>> selectLuckQueueLocation(Long luckyQueueId);

    Map<String,Object> selectLuckQueueLocationByUserIds( Long userId, Long luckyQueueId);

    PageUtils selectLuckyQueueByUser(int page, int limit, Long superior);

    int updateLuckyValueQueue(Integer state, Long id);

    List<Long> selectLuckyQueueUserIdByLuckyQueueId(Long luckyQueueId);

}
