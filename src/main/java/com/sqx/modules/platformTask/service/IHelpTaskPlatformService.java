package com.sqx.modules.platformTask.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.entity.HelpTaskPlatform;

public interface IHelpTaskPlatformService {

    int saveBody(HelpTaskPlatform helpTaskPlatform);

    int updateHelpTaskPlatform(HelpTaskPlatform helpTaskPlatform);

    void deleteHelpTaskPlatformById(Long id);

    void deleteHelpTaskPlatformByIds(String ids);

    PageUtils selectHelpTaskPlatformListByState(int page, int limit, Integer state);

    PageUtils selectHelpTaskPlatformList(int page, int limit, Long classifyId);

    int updateHelpTaskPlatformStateById(Long id, Integer state);

    HelpTaskPlatform selectById(Long id);

}
