package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.dao.HelpLuckyDetailsDao;
import com.sqx.modules.platformTask.dao.PlatformClassifyDao;
import com.sqx.modules.platformTask.entity.HelpLuckyDetails;
import com.sqx.modules.platformTask.entity.PlatformClassify;
import com.sqx.modules.platformTask.service.IHelpLuckyDetailsService;
import com.sqx.modules.platformTask.service.PlatformClassifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 幸运值详情
 */
@Service
public class HelpLuckyDetailsServiceImpl extends ServiceImpl<HelpLuckyDetailsDao, HelpLuckyDetails> implements IHelpLuckyDetailsService {


    @Autowired
    private HelpLuckyDetailsDao helpLuckyDetailsDao;

    @Override
    public int saveBody(HelpLuckyDetails helpLuckyDetails) {
        return helpLuckyDetailsDao.insert(helpLuckyDetails);
    }

    @Override
    public PageUtils selectHelpLuckyDetailsListByUserId(int page, int limit, Long userId) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpLuckyDetailsDao.selectHelpLuckyDetailsListByUserId(pages,userId));
    }
}
