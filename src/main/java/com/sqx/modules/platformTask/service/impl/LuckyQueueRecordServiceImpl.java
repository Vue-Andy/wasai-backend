package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.message.entity.MessageInfo;
import com.sqx.modules.message.service.MessageService;
import com.sqx.modules.platformTask.dao.HelpLuckyValueDao;
import com.sqx.modules.platformTask.dao.LuckyQueueRecordDao;
import com.sqx.modules.platformTask.dao.LuckyValueQueueDao;
import com.sqx.modules.platformTask.entity.HelpLuckyValue;
import com.sqx.modules.platformTask.entity.LuckyQueue;
import com.sqx.modules.platformTask.entity.LuckyQueueRecord;
import com.sqx.modules.platformTask.entity.LuckyValueQueue;
import com.sqx.modules.platformTask.service.IHelpLuckyValueService;
import com.sqx.modules.platformTask.service.ILuckyQueueRecordService;
import com.sqx.modules.platformTask.service.ILuckyQueueService;
import com.sqx.modules.platformTask.service.ILuckyValueQueueService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户晋升队列记录
 */
@Service
public class LuckyQueueRecordServiceImpl extends ServiceImpl<LuckyQueueRecordDao, LuckyQueueRecord> implements ILuckyQueueRecordService {

    @Autowired
    private LuckyQueueRecordDao luckyQueueRecordDao;
    @Autowired
    private ILuckyValueQueueService iLuckyValueQueueService;
    @Autowired
    private ILuckyQueueService iLuckyQueueService;
    @Autowired
    private UserService userService;
    @Autowired
    private IHelpLuckyValueService iHelpLuckyValueService;
    @Autowired
    private HelpLuckyValueDao helpLuckyValueDao;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;

    @Override
    public int saveBody(LuckyQueueRecord luckyQueueRecord) {
        return luckyQueueRecordDao.insert(luckyQueueRecord);
    }

    @Override
    public int auditLuckyQueueRecord(String ids, Integer state, String auditContent) {
        for(String i:ids.split(",")){
            Long id=Long.parseLong(i);
            LuckyQueueRecord luckyQueueRecord = luckyQueueRecordDao.selectById(id);
            if(luckyQueueRecord!=null && luckyQueueRecord.getState()==0){
                LuckyQueue oldLuckyQueue=null;
                LuckyValueQueue oldLuckyValueQueue = iLuckyValueQueueService.selectLuckyValueQueueByUserId(luckyQueueRecord.getUserId());
                if(state==1){
                    luckyQueueRecordDao.updateLuckyQueueRecord(id,state,"同意");
                    if(oldLuckyValueQueue!=null){
                        oldLuckyQueue = iLuckyQueueService.selectLuckyQueueById(oldLuckyValueQueue.getLuckyQueueId());
                    }
                    LuckyQueue newLuckyQueue = iLuckyQueueService.selectLuckyQueueById(luckyQueueRecord.getLuckyQueueId());
                    closeAnAccount(luckyQueueRecord.getUserId(),oldLuckyQueue,newLuckyQueue,luckyQueueRecord);
                }else{
                    luckyQueueRecordDao.updateLuckyQueueRecord(id,state,auditContent);
                    if(oldLuckyValueQueue!=null){
                        iLuckyValueQueueService.updateLuckyValueQueue(1,luckyQueueRecord.getUserId());
                    }
                    LuckyValueQueue luckyValueQueue = iLuckyValueQueueService.selectLuckyValueQueueByUserId(luckyQueueRecord.getUserId());
                    //获取当前人的队列  拒绝后重新判断
                    List<Long> longs = iLuckyValueQueueService.selectLuckyQueueUserIdByLuckyQueueId(luckyValueQueue.getLuckyQueueId());
                    for(Long userId1:longs){
                        UserEntity userInfo1 = userService.selectById(userId1);
                        if(userInfo1!=null){
                            updateLucky(userInfo1);
                        }
                    }
                }
            }
        }
        return 0;
    }

    /**
     * 审核通过后进行结算
     */
    private void closeAnAccount(Long userId,LuckyQueue oldLuckyQueue,LuckyQueue newLuckyQueue,LuckyQueueRecord luckyQueueRecord){
        //晋级成功之后先看是否有邀请人 如果有邀请人则给邀请人对应的邀请赏金
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        UserEntity userInfo = userService.selectById(userId);
        if(userInfo!=null){
            if(userInfo.getSuperior()!=null){
                UserEntity userByRelationId = userService.selectById(userInfo.getSuperior());
                if(userByRelationId!=null){
                    //给推荐人奖励晋级等级推荐金额
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setContent(userByRelationId.getNickName() + " 您好！您邀请的好友："+userInfo.getNickName()+",已经晋级成功，奖励您推荐奖："+newLuckyQueue.getRecommendMoney());
                    if (userByRelationId.getClientid() != null) {
                        userService.pushToSingle("推荐奖通知", userByRelationId.getNickName() + " 您好！您邀请的好友："+userInfo.getNickName()+",已经晋级成功，奖励您推荐奖："+newLuckyQueue.getRecommendMoney(), userByRelationId.getClientid());
                    }
                    messageInfo.setState(String.valueOf(6));
                    messageInfo.setTitle("推荐奖通知！");
                    messageInfo.setUserName(userByRelationId.getNickName());
                    messageInfo.setUserId(String.valueOf(userByRelationId.getUserId()));
                    messageService.saveBody(messageInfo);
                    userMoneyService.updateMayMoney(1,userByRelationId.getUserId(),newLuckyQueue.getRecommendMoney());
                    UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                    userMoneyDetails.setUserId(userByRelationId.getUserId());
                    userMoneyDetails.setTitle("[推荐奖]好友晋级："+userInfo.getNickName());
                    userMoneyDetails.setContent("增加金额:"+newLuckyQueue.getRecommendMoney());
                    userMoneyDetails.setType(1);
                    userMoneyDetails.setMoney(newLuckyQueue.getRecommendMoney());
                    userMoneyDetails.setCreateTime(sdf.format(new Date()));
                    userMoneyDetailsService.insert(userMoneyDetails);
                    /*userRepository.updateJiFenAdd(newLuckyQueue.getRecommendMoney().toString(),userByRelationId.getId());
                    iMoneyDetailsService.saveBody(userByRelationId.getId(),1,3,"红包榜收入","红包榜推荐奖励，晋级成员："+userInfo.getNickName()+",推荐奖金额:"+newLuckyQueue.getRecommendMoney().toString(),newLuckyQueue.getRecommendMoney());*/
                }
            }

            //先判断自己有没有上一个等级
            if(oldLuckyQueue!=null){
                //晋级成功之后给自己增加金额 给自己奖励上一个等级的晋级红包（派送红包
                helpLuckyValueDao.updateHelpLuckyValueAccruingAmounts(userInfo.getUserId(),oldLuckyQueue.getHandOut());
                MessageInfo messageInfo = new MessageInfo();
                messageInfo.setContent(userInfo.getNickName() + " 您好！您已晋级，奖励派送红包："+oldLuckyQueue.getHandOut());
                if (userInfo.getClientid() != null) {
                    userService.pushToSingle("晋级奖通知", userInfo.getNickName() + " 您好！您已晋级，奖励派送红包："+oldLuckyQueue.getHandOut(), userInfo.getClientid());
                }
                messageInfo.setState(String.valueOf(6));
                messageInfo.setTitle("晋级奖通知！");
                messageInfo.setUserName(userInfo.getNickName());
                messageInfo.setUserId(String.valueOf(userInfo.getUserId()));
                messageService.saveBody(messageInfo);
                userMoneyService.updateMayMoney(1,userInfo.getUserId(),oldLuckyQueue.getHandOut());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userInfo.getUserId());
                userMoneyDetails.setTitle("[晋级奖]成功晋级");
                userMoneyDetails.setContent("增加金额:"+oldLuckyQueue.getHandOut());
                userMoneyDetails.setType(1);
                userMoneyDetails.setMoney(oldLuckyQueue.getHandOut());
                userMoneyDetails.setCreateTime(sdf.format(new Date()));
                userMoneyDetailsService.insert(userMoneyDetails);
                /*userRepository.updateJiFenAdd(oldLuckyQueue.getHandOut().toString(),userInfo.getId());
                iMoneyDetailsService.saveBody(userInfo.getId(),1,3,"红包榜收入","红包榜晋级奖励金额:"+oldLuckyQueue.getHandOut().toString(),oldLuckyQueue.getHandOut());*/
                iLuckyValueQueueService.updateLuckyValueQueue(luckyQueueRecord.getUserId(),luckyQueueRecord.getLuckyQueueId(),1,sdf.format(new Date()));
            }else{
                //没有上级则默认创建一个
                LuckyValueQueue luckyValueQueue=new LuckyValueQueue();
                luckyValueQueue.setUserId(userId);
                luckyValueQueue.setCreateTime(sdf.format(new Date()));
                luckyValueQueue.setLuckyQueueId(newLuckyQueue.getId());
                luckyValueQueue.setState(1);
                iLuckyValueQueueService.saveBody(luckyValueQueue);
            }
            //获取当前人晋级的队列的所有人 进行判断。。。
            List<Long> longs = iLuckyValueQueueService.selectLuckyQueueUserIdByLuckyQueueId(luckyQueueRecord.getLuckyQueueId());
            for(Long userId1:longs){
                UserEntity userInfo1 = userService.selectById(userId1);
                if(userInfo1!=null){
                    updateLucky(userInfo1);
                }
            }
        }
    }

    /**
     * 计算幸运值队列是否晋级  判断条件必须同时满足
     * 1 积分达到
     * 2 邀请的人必须成为同等或以上会员
     * 3 队列在1-13之间 必须满足13个人 之后9的倍数 必须满足
     */
    private int updateLucky(UserEntity userByWxId){
        MessageInfo messageInfo = new MessageInfo();
        //先判断是否已经发起过申请
        LuckyQueueRecord oldLuckyQueueRecord = selectByUserId(userByWxId.getUserId());
        if(oldLuckyQueueRecord==null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            LuckyValueQueue luckyValueQueue = iLuckyValueQueueService.selectLuckyValueQueueByUserId(userByWxId.getUserId());
            HelpLuckyValue helpLuckyValue = iHelpLuckyValueService.selectByUserId(userByWxId.getUserId());
            //获取平台积分晋级积分条件 判断当前积分是否满足 积分晋级条件
            List<LuckyQueue> luckyQueues = iLuckyQueueService.selectLuckyQueueListAsc();
            //判断是否已经达到最高等级
            if((luckyValueQueue==null)||(!luckyValueQueue.getLuckyQueueId().equals(luckyQueues.get(luckyQueues.size()-1).getId()))) {
                for(int j=0;j<luckyQueues.size();j++){
                    if (helpLuckyValue.getLuckyValue() >= luckyQueues.get(j).getLuckyValue()) {
                        //如果用户所在队列为空  则必须是第一队列
                        if (luckyValueQueue == null) {
                            //第一队列条件满足  提交晋级申请
                            LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                            luckyQueueRecord.setLuckyQueueId(luckyQueues.get(0).getId());
                            luckyQueueRecord.setUserId(userByWxId.getUserId());
                            luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                            luckyQueueRecord.setState(0);
                            saveBody(luckyQueueRecord);
                            messageInfo.setContent(userByWxId.getNickName() + " 您好！您的红包榜晋级条件已满足！自动帮您申请晋级新的队列:"+luckyQueues.get(0).getName());
                            if (userByWxId.getClientid() != null) {
                                userService.pushToSingle("队列晋级通知", userByWxId.getNickName() + " 您好！您的红包榜晋级条件已满足！自动帮您申请晋级新的队列:"+luckyQueues.get(0).getName(), userByWxId.getClientid());
                            }
                            messageInfo.setState(String.valueOf(6));
                            messageInfo.setTitle("队列晋级通知！");
                            messageInfo.setUserName(userByWxId.getNickName());
                            messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
                            messageService.saveBody(messageInfo);
                            return 1;
                        } else {
                            //除晋级第一队列外 其他队列晋级需要满足条件方可
                            if(luckyQueues.get(j).getSort()!=1){
                                LuckyQueue luckyQueue1 = iLuckyQueueService.selectLuckyQueueById(luckyValueQueue.getLuckyQueueId());
                                //用户所在队列必须等于前一个队列 才可晋级
                                if (luckyQueue1.getSort() .equals( luckyQueues.get(j-1).getSort())) {
                                    //判断邀请的好友是否满足条件  邀请的好友是否达到当前级别或者级别以上
                                    Integer integer = iLuckyValueQueueService.selectUserLuckyQueueByInvitation(userByWxId.getUserId(), luckyQueues.get(j-1).getSort());
                                    //邀请的好友必须大于等于自己当前等级晋级所需人数
                                    if (integer != null && integer >= luckyQueue1.getNumberOfPeople()) {
                                        //判断自己所在当前队列的位置
                                        Integer integer1 = iLuckyValueQueueService.selectLuckQueueLocationByUserId(userByWxId.getUserId(), luckyValueQueue.getLuckyQueueId());
                                        //当前队列总人数
                                        Integer integer2 = iLuckyValueQueueService.selectLuckQueueCount(luckyValueQueue.getLuckyQueueId());
                                        //队列晋级条件 前13名必须凑够13人方可晋级  后面按照9人一队为顺序 方可晋级
                                        if (integer1 != null && integer2 != null) {
                                            if (integer1 <= luckyQueue1.getStartPerson()) {
                                                //队列人数大于13人 提交晋级申请
                                                if (integer2 >= luckyQueue1.getStartPerson()) {
                                                    //条件满足 提交晋级申请
                                                    LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                                                    luckyQueueRecord.setLuckyQueueId(luckyQueues.get(j).getId());
                                                    luckyQueueRecord.setUserId(userByWxId.getUserId());
                                                    luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                                                    luckyQueueRecord.setState(0);
                                                    saveBody(luckyQueueRecord);
                                                    iLuckyValueQueueService.updateLuckyValueQueue(2,luckyValueQueue.getUserId());
                                                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的红包榜晋级条件已满足！自动帮您申请晋级新的队列:"+luckyQueues.get(j).getName());
                                                    if (userByWxId.getClientid() != null) {
                                                        userService.pushToSingle("队列晋级通知", userByWxId.getNickName() + " 您好！您的红包榜晋级条件已满足！自动帮您申请晋级新的队列:"+luckyQueues.get(j).getName(), userByWxId.getClientid());
                                                    }
                                                    messageInfo.setState(String.valueOf(6));
                                                    messageInfo.setTitle("队列晋级通知！");
                                                    messageInfo.setUserName(userByWxId.getNickName());
                                                    messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
                                                    messageService.saveBody(messageInfo);
                                                    return 1;
                                                }
                                            } else {
                                                //根据当前所在位置 进行计算 必须满足9人方可晋级
                                                integer1 = integer1 - luckyQueue1.getStartPerson();
                                                integer2 = integer2 - luckyQueue1.getStartPerson();
                                                //根据当前位置和9的倍数 获取所在队列
                                                int i = integer1 / luckyQueue1.getIntervalPerson();
                                                //判断是否能够除尽
                                                int count;
                                                if (integer1 % luckyQueue1.getIntervalPerson() == 0) {
                                                    count = i * luckyQueue1.getIntervalPerson();
                                                } else {
                                                    count = (i * luckyQueue1.getIntervalPerson()) + luckyQueue1.getIntervalPerson();
                                                }
                                                //判断自己所在队列人数是否满足条件
                                                if (integer2 >= count) {
                                                    //条件满足 提交晋级申请
                                                    LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                                                    luckyQueueRecord.setLuckyQueueId(luckyQueues.get(j).getId());
                                                    luckyQueueRecord.setUserId(userByWxId.getUserId());
                                                    luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                                                    luckyQueueRecord.setState(0);
                                                    saveBody(luckyQueueRecord);
                                                    iLuckyValueQueueService.updateLuckyValueQueue(2,luckyValueQueue.getUserId());
                                                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的红包榜晋级条件已满足！自动帮您申请晋级新的队列:"+luckyQueues.get(j).getName());
                                                    if (userByWxId.getClientid() != null) {
                                                        userService.pushToSingle("队列晋级通知", userByWxId.getNickName() + " 您好！您的红包榜晋级条件已满足！自动帮您申请晋级新的队列:"+luckyQueues.get(j).getName(), userByWxId.getClientid());
                                                    }
                                                    messageInfo.setState(String.valueOf(6));
                                                    messageInfo.setTitle("队列晋级通知！");
                                                    messageInfo.setUserName(userByWxId.getNickName());
                                                    messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
                                                    messageService.saveBody(messageInfo);
                                                    return 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }




    @Override
    public PageUtils selectLuckyQueueRecordList(int page, int limit) {
        Page<Map<String,Object>> pages = new Page<>(page, limit);
        return new PageUtils(luckyQueueRecordDao.selectLuckyQueueRecordList(pages));
    }

    @Override
    public PageUtils selectLuckyQueueRecordByUserId(int page, int limit, Long userId) {
        Page<Map<String,Object>> pages = new Page<>(page, limit);
        return new PageUtils(luckyQueueRecordDao.selectLuckyQueueRecordByUserId(pages,userId));
    }

    @Override
    public LuckyQueueRecord selectByUserId(Long userId) {
        return luckyQueueRecordDao.selectByUserId(userId);
    }
}
