package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.message.entity.MessageInfo;
import com.sqx.modules.message.service.MessageService;
import com.sqx.modules.platformTask.dao.HelpGetTaskDao;
import com.sqx.modules.platformTask.dao.HelpTaskPlatformDao;
import com.sqx.modules.platformTask.entity.*;
import com.sqx.modules.platformTask.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接单
 */
@Service
public class HelpGetTaskServiceImpl extends ServiceImpl<HelpGetTaskDao, HelpGetTask> implements IHelpGetTaskService {

    @Autowired
    private HelpGetTaskDao helpGetTaskDao;
    @Autowired
    private HelpTaskPlatformDao helpTaskPlatformDao;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private IHelpLuckyValueService luckyValueService;
    @Autowired
    private IHelpLuckyDetailsService iHelpLuckyDetailsService;
    @Autowired
    private ILuckyQueueRecordService iLuckyQueueRecordService;
    @Autowired
    private ILuckyQueueService luckyQueueService;
    @Autowired
    private ILuckyValueQueueService luckyValueQueueService;



    @Override
    public int saveBody(Long helpTaskPlatformId,Long userId){
        HelpGetTask helpGetTask=new HelpGetTask();
        helpGetTask.setHelpTaskPlatformId(helpTaskPlatformId);
        helpGetTask.setUserId(userId);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        helpGetTask.setState(0);
        helpGetTask.setCreateTime(sdf.format(new Date()));
        return helpGetTaskDao.insert(helpGetTask);
    }

    @Override
    public int updateHelpGetTaskById(HelpGetTask helpGetTask){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        helpGetTask.setCreateTime(sdf.format(new Date()));
        return helpGetTaskDao.updateById(helpGetTask);
    }

    @Override
    public PageUtils selectUnderwayHelpGetTaskList(int page,int limit) {
        Page<Map<String,Object>> pages = new Page<>(page, limit);
        return new PageUtils(helpGetTaskDao.selectUnderwayHelpGetTaskList(pages));
    }

    @Override
    public void deleteHelpGetTaskById(Long id){
        helpGetTaskDao.deleteById(id);
    }

    @Override
    public void deleteHelpGetTaskByIds(String ids){
        for(String id:ids.split(",")){
            helpGetTaskDao.deleteById(Long.parseLong(id));
        }
    }

    @Override
    public PageUtils selectHelpGetTaskList(int page,int limit, Long userId,Integer state){
        Page<Map<String,Object>> pages = new Page<>(page, limit);
        return new PageUtils(helpGetTaskDao.selectHelpGetTaskList(pages,userId,state));
    }

    @Override
    public Map<String,Object> selectHelpTaskPlatform(Long id){
        return helpTaskPlatformDao.selectByIds(id);
    }

    @Override
    public Map<String,Object> selectHelpGetTaskById(Long id,Long userId){
        Integer count = helpGetTaskDao.selectCount(id, userId);
        Map<String, Object> map=new HashMap<>();
        if(count==null || count==0){
            Map<String, Object> map1 = helpTaskPlatformDao.selectByIds(id);
            map.put("flag",1);
            map.put("data",map1);
            return map;
        }
        Map<String, Object> map1 = helpGetTaskDao.selectHelpGetTaskById(id, userId);
        map.put("flag",2);
        map.put("data",map1);
        return map;
    }

    @Override
    public PageUtils selectHelpGetTaskList(int page,int limit){
        Page<Map<String,Object>> pages = new Page<>(page, limit);
        return new PageUtils(helpGetTaskDao.selectHelpGetTaskLists(pages));
    }

    @Override
    public int  helpGetTaskRemind(Long id){
        HelpGetTask helpGetTask = selectById(id);
        Map<String, Object> map = helpTaskPlatformDao.selectByIds(helpGetTask.getHelpTaskPlatformId());
        UserEntity userByWxId = userService.selectById(helpGetTask.getUserId());
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单已经很长时间没有去完成了，平台温馨提示您请尽快完成任务，超时会自动取消哦！任务标题:"+map.get("title"));
        if (userByWxId.getClientid() != null) {
            userService.pushToSingle("任务提醒", userByWxId.getNickName() + " 您好！您的接单已经很长时间没有去完成了，平台温馨提示您请尽快完成任务，超时会自动取消哦！任务标题:"+map.get("title"), userByWxId.getClientid());
        }
        messageInfo.setState(String.valueOf(6));
        messageInfo.setTitle("任务提醒");
        messageInfo.setUserName(userByWxId.getNickName());
        messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
        messageService.saveBody(messageInfo);
        return 1;
    }

    @Override
    public int exitHelpGetTask(Long id){
        HelpGetTask helpGetTask = selectById(id);
        helpGetTask.setState(4);
        updateHelpGetTaskById(helpGetTask);
        Map<String, Object> map = helpTaskPlatformDao.selectByIds(helpGetTask.getHelpTaskPlatformId());
        UserEntity userByWxId = userService.selectById(helpGetTask.getUserId());
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单已经很长时间没有去完成了，平台已经自动帮您取消了！任务标题:"+map.get("title"));
        if (userByWxId.getClientid() != null) {
            userService.pushToSingle("任务超时", userByWxId.getNickName() + " 您好！您的接单已经很长时间没有去完成了，平台已经自动帮您取消了！任务标题:"+map.get("title"), userByWxId.getClientid());
        }
        messageInfo.setState(String.valueOf(6));
        messageInfo.setTitle("任务超时");
        messageInfo.setUserName(userByWxId.getNickName());
        messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
        messageService.saveBody(messageInfo);
        return 1;
    }



    @Override
    @Transactional
    public int auditHelpGetTask(String ids,Integer state,String auditContent){
        for(String id:ids.split(",")){
            Long helpGetTaskId=Long.parseLong(id);
            HelpGetTask helpGetTask = helpGetTaskDao.selectById(helpGetTaskId);
            MessageInfo messageInfo = new MessageInfo();
            if(helpGetTask.getState()==1){
                UserEntity userByWxId = userService.selectById(helpGetTask.getUserId());
                if(state==1){//同意
                    helpGetTaskDao.updateHelpGetTaskState(helpGetTaskId,2,"审核成功");
                    Map<String, Object> stringObjectMap = helpTaskPlatformDao.selectByIds(helpGetTask.getHelpTaskPlatformId());
                    luckyValueService.updateHelpLuckyValue(helpGetTask.getUserId(),Double.parseDouble(stringObjectMap.get("luckyValue").toString()));
                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务已通过！幸运值增加："+stringObjectMap.get("luckyValue").toString());
                    if (userByWxId.getClientid() != null) {
                        userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务已通过！", userByWxId.getClientid());
                    }
                    //幸运值详细信息
                    HelpLuckyDetails helpLuckyDetails=new HelpLuckyDetails();
                    helpLuckyDetails.setUserId(userByWxId.getUserId());
                    helpLuckyDetails.setHelpTaskId(helpGetTask.getHelpTaskPlatformId());
                    helpLuckyDetails.setTitle("接单任务");
                    helpLuckyDetails.setContent(userByWxId.getNickName() + " 您好！您的接单任务已通过！幸运值增加："+stringObjectMap.get("luckyValue").toString());
                    helpLuckyDetails.setLuckyValue(Double.parseDouble(stringObjectMap.get("luckyValue").toString()));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    helpLuckyDetails.setCreateTime(sdf.format(new Date()));
                    iHelpLuckyDetailsService.saveBody(helpLuckyDetails);
                    updateLucky(userByWxId);
                }else{//拒绝
                    helpGetTaskDao.updateHelpGetTaskState(helpGetTaskId,3,auditContent);
                    messageInfo.setContent(userByWxId.getNickName() + " 您好！您的接单任务被拒绝！拒绝原因："+auditContent);
                    if (userByWxId.getClientid() != null) {
                        userService.pushToSingle("接单任务", userByWxId.getNickName() + " 您好！您的接单任务被拒绝！拒绝原因："+auditContent, userByWxId.getClientid());
                    }
                }
                messageInfo.setState(String.valueOf(6));
                messageInfo.setTitle("接单任务通知！");
                messageInfo.setUserName(userByWxId.getNickName());
                messageInfo.setUserId(String.valueOf(userByWxId.getUserId()));
                messageService.saveBody(messageInfo);
            }
        }
        return 1;
    }

    /**
     * 计算幸运值队列是否晋级  判断条件必须同时满足
     * 1 积分达到
     * 2 邀请的人必须成为同等或以上会员
     * 3 队列在1-13之间 必须满足13个人 之后9的倍数 必须满足
     */
    @Override
    public int updateLucky(UserEntity userByWxId){
        //先判断是否已经发起过申请
        LuckyQueueRecord oldLuckyQueueRecord = iLuckyQueueRecordService.selectByUserId(userByWxId.getUserId());
        if(oldLuckyQueueRecord==null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            LuckyValueQueue luckyValueQueue = luckyValueQueueService.selectLuckyValueQueueByUserId(userByWxId.getUserId());
            HelpLuckyValue helpLuckyValue = luckyValueService.selectByUserId(userByWxId.getUserId());
            //获取平台积分晋级积分条件 判断当前积分是否满足 积分晋级条件
            List<LuckyQueue> luckyQueues = luckyQueueService.selectLuckyQueueListAsc();
            //判断是否已经达到最高等级
            if((luckyValueQueue==null)||(!luckyValueQueue.getLuckyQueueId().equals(luckyQueues.get(luckyQueues.size()-1).getId()))) {
                for(int j=0;j<luckyQueues.size();j++){
                    if (helpLuckyValue.getLuckyValue() >= luckyQueues.get(j).getLuckyValue()) {
                        //如果用户所在队列为空  则必须是第一队列
                        if (luckyValueQueue == null) {
                            //第一队列条件满足  提交晋级申请
                            LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                            luckyQueueRecord.setLuckyQueueId(luckyQueues.get(0).getId());
                            luckyQueueRecord.setUserId(userByWxId.getUserId());
                            luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                            luckyQueueRecord.setState(0);
                            iLuckyQueueRecordService.saveBody(luckyQueueRecord);
                            return 1;
                        } else {
                            //除晋级第一队列外 其他队列晋级需要满足条件方可
                            if(luckyQueues.get(j).getSort()!=1){
                                LuckyQueue luckyQueue1 = luckyQueueService.selectLuckyQueueById(luckyValueQueue.getLuckyQueueId());
                                //用户所在队列必须等于前一个队列 才可晋级
                                if (luckyQueue1.getSort() .equals( luckyQueues.get(j-1).getSort())) {
                                    //判断邀请的好友是否满足条件  邀请的好友是否达到当前级别或者级别以上
                                    Integer integer = luckyValueQueueService.selectUserLuckyQueueByInvitation(userByWxId.getUserId(), luckyQueues.get(j-1).getSort());
                                    //邀请的好友必须大于等于自己当前等级晋级所需人数
                                    if (integer != null && integer >= luckyQueue1.getNumberOfPeople()) {
                                        //判断自己所在当前队列的位置
                                        Integer integer1 = luckyValueQueueService.selectLuckQueueLocationByUserId(userByWxId.getUserId(), luckyValueQueue.getLuckyQueueId());
                                        //当前队列总人数
                                        Integer integer2 = luckyValueQueueService.selectLuckQueueCount(luckyValueQueue.getLuckyQueueId());
                                        //队列晋级条件 前13名必须凑够13人方可晋级  后面按照9人一队为顺序 方可晋级
                                        if (integer1 != null && integer2 != null) {
                                            if (integer1 <= luckyQueue1.getStartPerson()) {
                                                //队列人数大于13人 提交晋级申请
                                                if (integer2 >= luckyQueue1.getStartPerson()) {
                                                    //条件满足 提交晋级申请
                                                    LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                                                    luckyQueueRecord.setLuckyQueueId(luckyQueues.get(j).getId());
                                                    luckyQueueRecord.setUserId(userByWxId.getUserId());
                                                    luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                                                    luckyQueueRecord.setState(0);
                                                    iLuckyQueueRecordService.saveBody(luckyQueueRecord);
                                                    luckyValueQueueService.updateLuckyValueQueue(2,luckyValueQueue.getUserId());
                                                    //有人晋级后 需要将队列重新刷一遍
                                                    List<Long> longs = luckyValueQueueService.selectLuckyQueueUserIdByLuckyQueueId(luckyQueues.get(j-1).getId());
                                                    for(Long userId1:longs){
                                                        UserEntity userInfo1 = userService.selectById(userId1);
                                                        if(userInfo1!=null){
                                                            updateLucky1(userInfo1);
                                                        }
                                                    }
                                                    return 1;
                                                }
                                            } else {
                                                //根据当前所在位置 进行计算 必须满足9人方可晋级
                                                integer1 = integer1 - luckyQueue1.getStartPerson();
                                                integer2 = integer2 - luckyQueue1.getStartPerson();
                                                //根据当前位置和9的倍数 获取所在队列
                                                int i = integer1 / luckyQueue1.getIntervalPerson();
                                                //判断是否能够除尽
                                                int count;
                                                if (integer1 % luckyQueue1.getIntervalPerson() == 0) {
                                                    count = i * luckyQueue1.getIntervalPerson();
                                                } else {
                                                    count = (i * luckyQueue1.getIntervalPerson()) + luckyQueue1.getIntervalPerson();
                                                }
                                                //判断自己所在队列人数是否满足条件
                                                if (integer2 >= count) {
                                                    //条件满足 提交晋级申请
                                                    LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                                                    luckyQueueRecord.setLuckyQueueId(luckyQueues.get(j).getId());
                                                    luckyQueueRecord.setUserId(userByWxId.getUserId());
                                                    luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                                                    luckyQueueRecord.setState(0);
                                                    iLuckyQueueRecordService.saveBody(luckyQueueRecord);
                                                    luckyValueQueueService.updateLuckyValueQueue(2,luckyValueQueue.getUserId());
                                                    //有人晋级后 需要将队列重新刷一遍
                                                    List<Long> longs = luckyValueQueueService.selectLuckyQueueUserIdByLuckyQueueId(luckyQueues.get(j-1).getId());
                                                    for(Long userId1:longs){
                                                        UserEntity userInfo1 = userService.selectById(userId1);
                                                        if(userInfo1!=null){
                                                            updateLucky1(userInfo1);
                                                        }
                                                    }
                                                    return 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }


    private int updateLucky1(UserEntity userByWxId){
        //先判断是否已经发起过申请
        LuckyQueueRecord oldLuckyQueueRecord = iLuckyQueueRecordService.selectByUserId(userByWxId.getUserId());
        if(oldLuckyQueueRecord==null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            LuckyValueQueue luckyValueQueue = luckyValueQueueService.selectLuckyValueQueueByUserId(userByWxId.getUserId());
            HelpLuckyValue helpLuckyValue = luckyValueService.selectByUserId(userByWxId.getUserId());
            //获取平台积分晋级积分条件 判断当前积分是否满足 积分晋级条件
            List<LuckyQueue> luckyQueues = luckyQueueService.selectLuckyQueueListAsc();
            //判断是否已经达到最高等级
            if((luckyValueQueue==null)||(!luckyValueQueue.getLuckyQueueId().equals(luckyQueues.get(luckyQueues.size()-1).getId()))) {
                for(int j=0;j<luckyQueues.size();j++){
                    if (helpLuckyValue.getLuckyValue() >= luckyQueues.get(j).getLuckyValue()) {
                        //如果用户所在队列为空  则必须是第一队列
                        if (luckyValueQueue == null) {
                            //第一队列条件满足  提交晋级申请
                            LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                            luckyQueueRecord.setLuckyQueueId(luckyQueues.get(0).getId());
                            luckyQueueRecord.setUserId(userByWxId.getUserId());
                            luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                            luckyQueueRecord.setState(0);
                            iLuckyQueueRecordService.saveBody(luckyQueueRecord);
                            return 1;
                        } else {
                            //除晋级第一队列外 其他队列晋级需要满足条件方可
                            if(luckyQueues.get(j).getSort()!=1){
                                LuckyQueue luckyQueue1 = luckyQueueService.selectLuckyQueueById(luckyValueQueue.getLuckyQueueId());
                                //用户所在队列必须等于前一个队列 才可晋级
                                if (luckyQueue1.getSort() .equals( luckyQueues.get(j-1).getSort())) {
                                    //判断邀请的好友是否满足条件  邀请的好友是否达到当前级别或者级别以上
                                    Integer integer = luckyValueQueueService.selectUserLuckyQueueByInvitation(userByWxId.getUserId(), luckyQueues.get(j-1).getSort());
                                    //邀请的好友必须大于等于自己当前等级晋级所需人数
                                    if (integer != null && integer >= luckyQueue1.getNumberOfPeople()) {
                                        //判断自己所在当前队列的位置
                                        Integer integer1 = luckyValueQueueService.selectLuckQueueLocationByUserId(userByWxId.getUserId(), luckyValueQueue.getLuckyQueueId());
                                        //当前队列总人数
                                        Integer integer2 = luckyValueQueueService.selectLuckQueueCount(luckyValueQueue.getLuckyQueueId());
                                        //队列晋级条件 前13名必须凑够13人方可晋级  后面按照9人一队为顺序 方可晋级
                                        if (integer1 != null && integer2 != null) {
                                            if (integer1 <= luckyQueue1.getStartPerson()) {
                                                //队列人数大于13人 提交晋级申请
                                                if (integer2 >= luckyQueue1.getStartPerson()) {
                                                    //条件满足 提交晋级申请
                                                    LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                                                    luckyQueueRecord.setLuckyQueueId(luckyQueues.get(j).getId());
                                                    luckyQueueRecord.setUserId(userByWxId.getUserId());
                                                    luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                                                    luckyQueueRecord.setState(0);
                                                    iLuckyQueueRecordService.saveBody(luckyQueueRecord);
                                                    luckyValueQueueService.updateLuckyValueQueue(2,luckyValueQueue.getUserId());
                                                    return 1;
                                                }
                                            } else {
                                                //根据当前所在位置 进行计算 必须满足9人方可晋级
                                                integer1 = integer1 - luckyQueue1.getStartPerson();
                                                integer2 = integer2 - luckyQueue1.getStartPerson();
                                                //根据当前位置和9的倍数 获取所在队列
                                                int i = integer1 / luckyQueue1.getIntervalPerson();
                                                //判断是否能够除尽
                                                int count;
                                                if (integer1 % luckyQueue1.getIntervalPerson() == 0) {
                                                    count = i * luckyQueue1.getIntervalPerson();
                                                } else {
                                                    count = (i * luckyQueue1.getIntervalPerson()) + luckyQueue1.getIntervalPerson();
                                                }
                                                //判断自己所在队列人数是否满足条件
                                                if (integer2 >= count) {
                                                    //条件满足 提交晋级申请
                                                    LuckyQueueRecord luckyQueueRecord = new LuckyQueueRecord();
                                                    luckyQueueRecord.setLuckyQueueId(luckyQueues.get(j).getId());
                                                    luckyQueueRecord.setUserId(userByWxId.getUserId());
                                                    luckyQueueRecord.setCreateTime(sdf.format(new Date()));
                                                    luckyQueueRecord.setState(0);
                                                    iLuckyQueueRecordService.saveBody(luckyQueueRecord);
                                                    luckyValueQueueService.updateLuckyValueQueue(2,luckyValueQueue.getUserId());
                                                    return 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }




    @Override
    public int updateLuckys(UserEntity userByWxId){
        //先判断是否已经发起过申请
        LuckyQueueRecord luckyQueueRecord = iLuckyQueueRecordService.selectByUserId(userByWxId.getUserId());
        if(luckyQueueRecord!=null){
            return 0;
        }
        LuckyValueQueue luckyValueQueue = luckyValueQueueService.selectLuckyValueQueueByUserId(userByWxId.getUserId());
        HelpLuckyValue helpLuckyValue = luckyValueService.selectByUserId(userByWxId.getUserId());
        //获取平台积分晋级积分条件 判断当前积分是否满足 积分晋级条件
        List<LuckyQueue> luckyQueues = luckyQueueService.selectLuckyQueueListDesc();
        //判断是否已经达到最高等级
        if((luckyValueQueue==null)||(!luckyValueQueue.getLuckyQueueId().equals(luckyQueues.get(luckyQueues.size()-1).getId()))) {
            for (LuckyQueue luckyQueue : luckyQueues) {
                if (helpLuckyValue.getLuckyValue() > luckyQueue.getLuckyValue()) {
                    //如果用户所在队列为空  则必须是第一队列
                    if (luckyValueQueue == null && luckyQueue.getSort() == 1) {
                        //第一队列条件满足  提交晋级申请
                        return 1;
                    } else {
                        //除晋级第一队列外 其他队列晋级需要满足条件方可
                        if (luckyValueQueue != null) {
                            LuckyQueue luckyQueue1 = luckyQueueService.selectLuckyQueueById(luckyValueQueue.getLuckyQueueId());
                            //用户所在队列必须小于当前队列 才可晋级
                            if (luckyQueue1.getSort() < luckyQueue.getSort()) {
                                //判断邀请的好友是否满足条件  邀请的好友是否达到当前级别或者级别以上
                                Integer integer = luckyValueQueueService.selectUserLuckyQueueByInvitation(userByWxId.getUserId(), luckyQueue.getSort());
                                //邀请的好友必须大于等于自己当前等级晋级所需人数
                                if (integer != null && integer >= luckyQueue1.getNumberOfPeople()) {
                                    //判断自己所在当前队列的位置
                                    Integer integer1 = luckyValueQueueService.selectLuckQueueLocationByUserId(userByWxId.getUserId(), luckyValueQueue.getLuckyQueueId());
                                    //当前队列总人数
                                    Integer integer2 = luckyValueQueueService.selectLuckQueueCount(luckyValueQueue.getLuckyQueueId());
                                    //队列晋级条件 前13名必须凑够13人方可晋级  后面按照9人一队为顺序 方可晋级
                                    if (integer1 != null && integer2 != null) {
                                        if (integer1 <= luckyQueue1.getStartPerson()) {
                                            //队列人数大于13人 提交晋级申请
                                            if (integer2 >= luckyQueue1.getStartPerson()) {
                                                //条件满足 提交晋级申请
                                                return 1;
                                            }
                                        } else {
                                            //根据当前所在位置 进行计算 必须满足9人方可晋级
                                            integer1 = integer - luckyQueue1.getStartPerson();
                                            integer2 = integer2 - luckyQueue1.getStartPerson();
                                            //根据当前位置和9的倍数 获取所在队列
                                            int i = integer1 / luckyQueue1.getIntervalPerson();
                                            //判断是否能够除尽
                                            int count;
                                            if (integer1 % luckyQueue1.getIntervalPerson() == 0) {
                                                count = i * luckyQueue1.getIntervalPerson();
                                            } else {
                                                count = (i * luckyQueue1.getIntervalPerson()) + luckyQueue1.getIntervalPerson();
                                            }
                                            //判断自己所在队列人数是否满足条件
                                            if (integer2 >= count) {
                                                //条件满足 提交晋级申请
                                                return 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    break;
                }
            }
        }
        return 0;
    }




    @Override
    public HelpGetTask selectById(Long id) {
        return helpGetTaskDao.selectById(id);
    }

    @Override
    public Integer selectCount(Long id, Long userId) {
        return helpGetTaskDao.selectCount(id,userId);
    }



}
