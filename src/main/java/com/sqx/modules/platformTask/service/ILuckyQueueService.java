package com.sqx.modules.platformTask.service;

import com.sqx.modules.platformTask.entity.LuckyQueue;

import java.util.List;

public interface ILuckyQueueService {

    int saveBody(LuckyQueue luckyQueue);

    int update(LuckyQueue luckyQueue);

    List<LuckyQueue> selectLuckyQueueList();

    LuckyQueue selectLuckyQueueBySort(Integer sort);

    LuckyQueue selectLuckyQueueById(Long id);

    List<LuckyQueue> selectLuckyQueueListDesc();

    List<LuckyQueue> selectLuckyQueueListAsc();

    int deleteById(Long id);



}
