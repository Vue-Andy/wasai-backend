package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.dao.HelpLuckyValueDao;
import com.sqx.modules.platformTask.entity.HelpLuckyValue;
import com.sqx.modules.platformTask.service.IHelpLuckyValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 幸运值
 */
@Service
public class HelpLuckyValueServiceImpl extends ServiceImpl<HelpLuckyValueDao, HelpLuckyValue> implements IHelpLuckyValueService {

    @Autowired
    private HelpLuckyValueDao helpLuckyValueDao;

    @Override
    public int saveBody(HelpLuckyValue helpLuckyValue) {
        return helpLuckyValueDao.insert(helpLuckyValue);
    }

    @Override
    public HelpLuckyValue selectByUserId(Long userId) {
        HelpLuckyValue helpLuckyValue = helpLuckyValueDao.selectByUserId(userId);
        if(helpLuckyValue==null){
            helpLuckyValue=new HelpLuckyValue();
            helpLuckyValue.setLuckyValue(0.00);
            helpLuckyValue.setUserId(userId);
            helpLuckyValue.setAccruingAmounts(0.00);
            saveBody(helpLuckyValue);
        }
        return helpLuckyValue;
    }

    @Override
    public int updateHelpLuckyValue(Long userId, Double luckyValue) {
        return helpLuckyValueDao.updateHelpLuckyValue(userId,luckyValue);
    }

    @Override
    public PageUtils selectLuckyQueueRanking(int page, int limit, Long luckyQueueId, String phone) {
        Page<Map<String,Object>> pages=new Page<>(page,limit);
        return new PageUtils(helpLuckyValueDao.selectLuckyQueueRanking(pages,luckyQueueId,phone));
    }
}
