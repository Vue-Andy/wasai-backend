package com.sqx.modules.platformTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.entity.HelpLuckyValue;

public interface IHelpLuckyValueService {

    int saveBody(HelpLuckyValue helpLuckyValue);

    HelpLuckyValue selectByUserId(Long userId);

    int updateHelpLuckyValue(Long userId, Double luckyValue);

    PageUtils selectLuckyQueueRanking(int page,int limit,Long luckyQueueId, String phone);


}
