package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.HelpRate;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.HelpRateService;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.invite.dao.InviteDao;
import com.sqx.modules.invite.entity.Invite;
import com.sqx.modules.invite.service.InviteMoneyService;
import com.sqx.modules.invite.service.InviteService;
import com.sqx.modules.platformTask.dao.PlatformClassifyDao;
import com.sqx.modules.platformTask.entity.PlatformClassify;
import com.sqx.modules.platformTask.service.PlatformClassifyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 平台任务分类
 */
@Service
public class PlatformClassifyServiceImpl extends ServiceImpl<PlatformClassifyDao, PlatformClassify> implements PlatformClassifyService {


    @Autowired
    private PlatformClassifyDao platformClassifyDao;

    @Override
    public PlatformClassify selectById(Long id) {
        return platformClassifyDao.selectById(id);
    }

    @Override
    public List<PlatformClassify> selectList() {
        return platformClassifyDao.selectList();
    }

    @Override
    public int insert(PlatformClassify helpClassify) {
        return platformClassifyDao.insert(helpClassify);
    }

    @Override
    public void saveAll(List<PlatformClassify> list) {
        for(PlatformClassify platformClassify:list){
            platformClassifyDao.insert(platformClassify);
        }
    }

    @Override
    public int update(PlatformClassify platformClassify) {
        return platformClassifyDao.updateById(platformClassify);
    }

    @Override
    public void deleteById(Long id) {
        platformClassifyDao.deleteById(id);
    }

    @Override
    public void deleteByIdList(String ids) {
        String []id=ids.split(",");
        for(String i:id){
            platformClassifyDao.deleteById(Long.parseLong(i));
        }
    }
}
