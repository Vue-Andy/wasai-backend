package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.dao.HelpLuckyValueDao;
import com.sqx.modules.platformTask.dao.LuckyValueQueueDao;
import com.sqx.modules.platformTask.entity.HelpLuckyValue;
import com.sqx.modules.platformTask.entity.LuckyValueQueue;
import com.sqx.modules.platformTask.service.IHelpLuckyValueService;
import com.sqx.modules.platformTask.service.ILuckyValueQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 幸运值队列
 */
@Service
public class LuckyValueQueueServiceImpl extends ServiceImpl<LuckyValueQueueDao, LuckyValueQueue> implements ILuckyValueQueueService {

    @Autowired
    private LuckyValueQueueDao luckyValueQueueDao;

    @Override
    public int saveBody(LuckyValueQueue luckyValueQueue) {
        return luckyValueQueueDao.insert(luckyValueQueue);
    }

    @Override
    public int updateLuckyValueQueue(Long userId, Long luckyQueueId, Integer state, String createTime) {
        return luckyValueQueueDao.updateLuckyValueQueue(userId,luckyQueueId,state,createTime);
    }

    @Override
    public LuckyValueQueue selectLuckyValueQueueByUserId(Long userId) {
        return luckyValueQueueDao.selectLuckyValueQueueByUserId(userId);
    }

    @Override
    public Integer selectUserLuckyQueueByInvitation(Long userId, Integer sort) {
        return luckyValueQueueDao.selectUserLuckyQueueByInvitation(userId,sort);
    }

    @Override
    public Integer selectLuckQueueLocationByUserId(Long userId, Long luckyQueueId) {
        return luckyValueQueueDao.selectLuckQueueLocationByUserId(userId,luckyQueueId);
    }

    @Override
    public Integer selectLuckQueueCount(Long luckyQueueId) {
        return luckyValueQueueDao.selectLuckQueueCount(luckyQueueId);
    }

    @Override
    public List<Map<String, Object>> selectLuckQueueLocation(Long luckyQueueId) {
        return luckyValueQueueDao.selectLuckQueueLocation(luckyQueueId);
    }

    @Override
    public Map<String, Object> selectLuckQueueLocationByUserIds(Long userId, Long luckyQueueId) {
        return luckyValueQueueDao.selectLuckQueueLocationByUserIds(userId,luckyQueueId);
    }

    @Override
    public PageUtils selectLuckyQueueByUser(int page, int limit, Long superior) {
        Page<Map<String,Object>> pages = new Page<>(page, limit);
        return new PageUtils(luckyValueQueueDao.selectLuckyQueueByUser(pages,superior));
    }

    @Override
    public int updateLuckyValueQueue(Integer state, Long id) {
        LuckyValueQueue luckyValueQueue=new LuckyValueQueue();
        luckyValueQueue.setId(id);
        luckyValueQueue.setState(state);
        return luckyValueQueueDao.updateById(luckyValueQueue);
    }

    @Override
    public List<Long> selectLuckyQueueUserIdByLuckyQueueId(Long luckyQueueId) {
        return luckyValueQueueDao.selectLuckyQueueUserIdByLuckyQueueId(luckyQueueId);
    }
}
