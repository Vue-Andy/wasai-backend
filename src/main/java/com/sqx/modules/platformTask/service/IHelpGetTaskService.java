package com.sqx.modules.platformTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.platformTask.entity.HelpGetTask;
import com.sqx.modules.platformTask.entity.PlatformClassify;

import java.util.List;
import java.util.Map;

public interface IHelpGetTaskService {

    int saveBody(Long helpTaskPlatformId, Long userId);

    int updateHelpGetTaskById(HelpGetTask helpGetTask);

    PageUtils selectUnderwayHelpGetTaskList(int page, int limit);

    void deleteHelpGetTaskById(Long id);

    void deleteHelpGetTaskByIds(String ids);

    Map<String,Object> selectHelpTaskPlatform(Long id);

    PageUtils selectHelpGetTaskList(int page, int limit, Long userId, Integer state);

    Map<String,Object> selectHelpGetTaskById(Long id, Long userId);

    PageUtils selectHelpGetTaskList(int page, int limit);

    int auditHelpGetTask(String ids, Integer state, String auditContent);

    HelpGetTask selectById(Long id);

    Integer selectCount(Long id, Long userId);

    int updateLuckys(UserEntity userByWxId);

    int updateLucky(UserEntity userByWxId);

    int  helpGetTaskRemind(Long id);

    int exitHelpGetTask(Long id);

}
