package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.dao.HelpTaskPlatformDao;
import com.sqx.modules.platformTask.entity.HelpTaskPlatform;
import com.sqx.modules.platformTask.service.IHelpTaskPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 平台任务
 */
@Service
public class HelpTaskPlatformServiceImpl extends ServiceImpl<HelpTaskPlatformDao, HelpTaskPlatform> implements IHelpTaskPlatformService {

    @Autowired
    private HelpTaskPlatformDao helpTaskPlatformDao;

    @Override
    public int saveBody(HelpTaskPlatform helpTaskPlatform) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        helpTaskPlatform.setCreateTime(sdf.format(new Date()));
        helpTaskPlatform.setState(1);
        return helpTaskPlatformDao.insert(helpTaskPlatform);
    }

    @Override
    public int updateHelpTaskPlatform(HelpTaskPlatform helpTaskPlatform) {
        return helpTaskPlatformDao.updateById(helpTaskPlatform);
    }


    @Override
    public void deleteHelpTaskPlatformById(Long id) {
        helpTaskPlatformDao.deleteById(id);
    }

    @Override
    public void deleteHelpTaskPlatformByIds(String ids) {
        for(String id:ids.split(",")){
            helpTaskPlatformDao.deleteById(Long.parseLong(id));
        }
    }

    @Override
    public PageUtils selectHelpTaskPlatformListByState(int page, int limit, Integer state) {
        Page<HelpTaskPlatform> pages = new Page<>(page, limit);
        return new PageUtils(helpTaskPlatformDao.selectHelpTaskPlatformListByState(pages,state));
    }

    @Override
    public PageUtils selectHelpTaskPlatformList(int page, int limit, Long classifyId) {
        Page<HelpTaskPlatform> pages = new Page<>(page, limit);
        return new PageUtils(helpTaskPlatformDao.selectHelpTaskPlatformList(pages,classifyId));
    }

    @Override
    public int updateHelpTaskPlatformStateById(Long id, Integer state) {
        return helpTaskPlatformDao.updateHelpTaskPlatformStateById(id,state);
    }

    @Override
    public HelpTaskPlatform selectById(Long id) {
        return helpTaskPlatformDao.selectById(id);
    }
}
