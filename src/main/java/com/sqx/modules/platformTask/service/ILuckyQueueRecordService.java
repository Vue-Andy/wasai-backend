package com.sqx.modules.platformTask.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.platformTask.entity.LuckyQueueRecord;

import java.util.Map;

public interface ILuckyQueueRecordService {


    int saveBody(LuckyQueueRecord luckyQueueRecord);

    int auditLuckyQueueRecord(String ids, Integer state, String auditContent);

    PageUtils selectLuckyQueueRecordList(int page, int limit);

    PageUtils selectLuckyQueueRecordByUserId(int page, int limit, Long userId);

    LuckyQueueRecord selectByUserId(Long userId);

  
}
