package com.sqx.modules.platformTask.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.platformTask.dao.LuckyQueueDao;
import com.sqx.modules.platformTask.entity.LuckyQueue;
import com.sqx.modules.platformTask.service.ILuckyQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 队列规则
 */
@Service
public class LuckyQueueServiceImpl extends ServiceImpl<LuckyQueueDao, LuckyQueue> implements ILuckyQueueService {

    @Autowired
    private LuckyQueueDao luckyQueueDao;

    @Override
    public int saveBody(LuckyQueue luckyQueue) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        luckyQueue.setCreateTime(sdf.format(new Date()));
        luckyQueue.setState(1);
        return luckyQueueDao.insert(luckyQueue);
    }

    @Override
    public int update(LuckyQueue luckyQueue) {
        return luckyQueueDao.updateById(luckyQueue);
    }

    @Override
    public List<LuckyQueue> selectLuckyQueueList() {
        return luckyQueueDao.selectLuckyQueueList();
    }

    @Override
    public LuckyQueue selectLuckyQueueBySort(Integer sort) {
        return luckyQueueDao.selectLuckyQueueBySort(sort);
    }

    @Override
    public LuckyQueue selectLuckyQueueById(Long id) {
        return luckyQueueDao.selectLuckyQueueById(id);
    }

    @Override
    public List<LuckyQueue> selectLuckyQueueListDesc() {
        return luckyQueueDao.selectLuckyQueueListDesc();
    }

    @Override
    public List<LuckyQueue> selectLuckyQueueListAsc() {
        return luckyQueueDao.selectLuckyQueueListASC();
    }

    @Override
    public int deleteById(Long id) {
        return luckyQueueDao.deleteById(id);
    }
}
