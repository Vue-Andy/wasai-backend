package com.sqx.modules.platformTask.controller;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.entity.UserEntity;
import com.sqx.modules.app.service.UserService;
import com.sqx.modules.platformTask.entity.*;
import com.sqx.modules.platformTask.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/7/15
 */
@RestController
@Api(value = "平台任务", tags = {"平台任务"})
@RequestMapping(value = "/helpTaskPlatform")
public class HelpTaskPlatformController {

    private final IHelpTaskPlatformService iHelpTaskPlatformService;
    private final IHelpGetTaskService iHelpGetTaskeSrvice;
    private final IHelpLuckyValueService iHelpLuckyValueService;
    private final IHelpLuckyDetailsService luckyDetailsService;
    private final ILuckyValueQueueService iLuckyValueQueueService;
    private final UserService userService;
    private final ILuckyQueueService luckyQueueService;

    @Autowired
    public HelpTaskPlatformController(IHelpTaskPlatformService iHelpTaskPlatformService, IHelpGetTaskService iHelpGetTaskeSrvice, IHelpLuckyValueService iHelpLuckyValueService, IHelpLuckyDetailsService luckyDetailsService, ILuckyValueQueueService iLuckyValueQueueService, UserService userService, ILuckyQueueService luckyQueueService) {
        this.iHelpTaskPlatformService = iHelpTaskPlatformService;
        this.iHelpGetTaskeSrvice = iHelpGetTaskeSrvice;
        this.iHelpLuckyValueService = iHelpLuckyValueService;
        this.luckyDetailsService = luckyDetailsService;
        this.iLuckyValueQueueService = iLuckyValueQueueService;
        this.userService = userService;
        this.luckyQueueService = luckyQueueService;
    }


    @RequestMapping(value = "/saveHelpTaskPlatform", method = RequestMethod.POST)
    @ApiOperation("新建任务")
    @ResponseBody
    public Result saveHelpTaskPlatform(@RequestBody HelpTaskPlatform helpTaskPlatform) {
        iHelpTaskPlatformService.saveBody(helpTaskPlatform);
        return Result.success();
    }

    @RequestMapping(value = "/updateHelpTaskPlatform", method = RequestMethod.POST)
    @ApiOperation("修改任务")
    @ResponseBody
    public Result updateHelpTaskPlatform(@RequestBody HelpTaskPlatform helpTaskPlatform) {
        iHelpTaskPlatformService.updateHelpTaskPlatform(helpTaskPlatform);
        return Result.success();
    }

    @RequestMapping(value = "/deleteHelpTaskPlatformById", method = RequestMethod.POST)
    @ApiOperation("删除任务")
    @ResponseBody
    public Result deleteHelpTaskPlatformById(Long id) {
        iHelpTaskPlatformService.deleteHelpTaskPlatformById(id);
        return Result.success();
    }

    @RequestMapping(value = "/deleteHelpTaskPlatformByIds", method = RequestMethod.POST)
    @ApiOperation("删除多个任务")
    @ResponseBody
    public Result deleteHelpTaskPlatformByIds(String ids) {
        iHelpTaskPlatformService.deleteHelpTaskPlatformByIds(ids);
        return Result.success();
    }

    @RequestMapping(value = "/updateHelpTaskPlatformStateById", method = RequestMethod.POST)
    @ApiOperation("修改任务状态  1正常 2下架")
    @ResponseBody
    public Result updateHelpTaskPlatformStateById(Long id,Integer state) {
        iHelpTaskPlatformService.updateHelpTaskPlatformStateById(id,state);
        return Result.success();
    }

    @RequestMapping(value = "/selectHelpTaskPlatformList", method = RequestMethod.GET)
    @ApiOperation("查看所有任务")
    @ResponseBody
    public Result selectHelpTaskPlatformList(int page,int limit) {
        return Result.success().put("data",iHelpTaskPlatformService.selectHelpTaskPlatformListByState(page,limit,null));
    }

    @RequestMapping(value = "/selectHelpTaskPlatformListByClassifyId", method = RequestMethod.GET)
    @ApiOperation("根据分类查看任务")
    @ResponseBody
    public Result selectHelpTaskPlatformListByClassifyId(int page,int limit,Long classifyId) {
        return Result.success().put("data",iHelpTaskPlatformService.selectHelpTaskPlatformList(page,limit,classifyId));
    }


    @RequestMapping(value = "/selectHelpGetTaskById", method = RequestMethod.GET)
    @ApiOperation("查看任务详细信息用户端")
    @ResponseBody
    public Result selectHelpGetTaskById(Long id,Long userId) {
        return Result.success().put("data",iHelpGetTaskeSrvice.selectHelpGetTaskById(id,userId));
    }

    @RequestMapping(value = "/selectHelpTaskPlatformById", method = RequestMethod.GET)
    @ApiOperation("查看任务详细信息管理平台")
    @ResponseBody
    public Result selectHelpGetTaskById(Long id) {
        return Result.success().put("data",iHelpGetTaskeSrvice.selectHelpTaskPlatform(id));
    }

    @RequestMapping(value = "/selectHelpGetTaskListByUserId", method = RequestMethod.GET)
    @ApiOperation("查看我的任务列表")
    @ResponseBody
    public Result selectHelpGetTaskListByUserId(int page,int limit,Long userId,Integer state) {
        return Result.success().put("data",iHelpGetTaskeSrvice.selectHelpGetTaskList(page,limit,userId,state));
    }

    @RequestMapping(value = "/selectHelpGetTaskList", method = RequestMethod.GET)
    @ApiOperation("查看所有接单任务列表")
    @ResponseBody
    public Result selectHelpGetTaskList(int page,int limit) {
        return Result.success().put("data",iHelpGetTaskeSrvice.selectHelpGetTaskList(page,limit));
    }

    @RequestMapping(value = "/selectUnderwayHelpGetTaskList", method = RequestMethod.GET)
    @ApiOperation("查看进行中的接单任务")
    @ResponseBody
    public Result selectUnderwayHelpGetTaskList(int page,int limit) {
        return Result.success().put("data",iHelpGetTaskeSrvice.selectHelpGetTaskList(page,limit));
    }

    @RequestMapping(value = "/helpGetTaskRemind/{id}", method = RequestMethod.POST)
    @ApiOperation("对进行中的任务进行提醒")
    @ResponseBody
    public Result helpGetTaskRemind(@PathVariable Long id) {
        iHelpGetTaskeSrvice.helpGetTaskRemind(id);
        return Result.success();
    }

    @RequestMapping(value = "/selectUnderwayHelpGetTaskList/{id}", method = RequestMethod.POST)
    @ApiOperation("对进行中的任务进行放弃")
    @ResponseBody
    public Result exitHelpGetTask(@PathVariable Long id) {
        iHelpGetTaskeSrvice.exitHelpGetTask(id);
        return Result.success();
    }

    @RequestMapping(value = "/insertHelpGetTask", method = RequestMethod.POST)
    @ApiOperation("领取任务")
    @ResponseBody
    public Result insertHelpGetTask(Long helpTaskPlatformId,Long userId) {
        HelpTaskPlatform helpTaskPlatform = iHelpTaskPlatformService.selectById(helpTaskPlatformId);
        if(helpTaskPlatform!=null && helpTaskPlatform.getState()==1){
            Integer count = iHelpGetTaskeSrvice.selectCount(helpTaskPlatformId, userId);
            if(count!=null && count>0){
                return Result.error(-100,"不能重复领取任务！");
            }
            iHelpGetTaskeSrvice.saveBody(helpTaskPlatformId,userId);
            return Result.success();
        }else{
            return Result.error(-100,"任务已经下架！");

        }
    }


    @RequestMapping(value = "/auditHelpGetTask/{ids}/{state}/{auditContent}", method = RequestMethod.POST)
    @ApiOperation("审核任务")
    @ResponseBody
    public Result auditHelpGetTask(@PathVariable("ids") String ids, @PathVariable("state") Integer state, @PathVariable("auditContent") String auditContent) {
        iHelpGetTaskeSrvice.auditHelpGetTask(ids,state,auditContent);
        return Result.success();
    }


    @RequestMapping(value = "/updateHelpGetTask", method = RequestMethod.POST)
    @ApiOperation("提交任务")
    @ResponseBody
    public Result updateHelpGetTask(Long id,Long userId,String picture,String content) {
        HelpGetTask helpGetTask = iHelpGetTaskeSrvice.selectById(id);
        if(helpGetTask==null){
            return Result.error(-100,"请先领取任务后再进行提交！");
        }
        if(helpGetTask.getState()!=0){
            return Result.error(-100,"任务已经做过了，不能重复提交！");
        }
        if(!helpGetTask.getUserId().equals(userId)){
            return Result.error(-100,"提交失败，请刷新后重试！");
        }
        helpGetTask.setPicture(picture);
        helpGetTask.setContent(content);
        helpGetTask.setState(1);
        iHelpGetTaskeSrvice.updateHelpGetTaskById(helpGetTask);
        return Result.success();
    }


    @RequestMapping(value = "/giveUpHelpGetTask", method = RequestMethod.POST)
    @ApiOperation("放弃任务")
    @ResponseBody
    public Result giveUpHelpGetTask(Long id,Long userId) {
        HelpGetTask helpGetTask = iHelpGetTaskeSrvice.selectById(id);
        if(helpGetTask==null){
            return Result.error(-100,"请先领取任务后再进行提交！");
        }
        if(helpGetTask.getState()!=0){
            return Result.error(-100,"任务已经做过了，不能放弃！");
        }
        if(!helpGetTask.getUserId().equals(userId)){
            return Result.error(-100,"提交失败，请刷新后重试！");
        }
        helpGetTask.setState(4);
        iHelpGetTaskeSrvice.updateHelpGetTaskById(helpGetTask);
        return Result.success();
    }


    @RequestMapping(value = "/selectLuckyValue", method = RequestMethod.GET)
    @ApiOperation("查看我的幸运值")
    @ResponseBody
    public Result selectLuckyValue(Long userId) {
        HelpLuckyValue helpLuckyValue = iHelpLuckyValueService.selectByUserId(userId);
        LuckyValueQueue luckyValueQueue = iLuckyValueQueueService.selectLuckyValueQueueByUserId(userId);
        Map<String,Object> map=new HashMap<>();
        if(luckyValueQueue==null){
            List<LuckyQueue> luckyQueues = luckyQueueService.selectLuckyQueueListAsc();
            map.put("asRequired",luckyQueues.get(0).getLuckyValue());
        }else{
            LuckyQueue luckyQueue = luckyQueueService.selectLuckyQueueById(luckyValueQueue.getLuckyQueueId());
            map.put("asRequired",luckyQueue.getLuckyValue());
        }
        map.put("helpLuckyValue",helpLuckyValue);

        return Result.success().put("data",map);
    }

    @RequestMapping(value = "/selectLuckyDetails", method = RequestMethod.GET)
    @ApiOperation("查看我的幸运值详细信息")
    @ResponseBody
    public Result selectLuckyDetails(int page,int limit,Long userId) {
        return Result.success().put("data",luckyDetailsService.selectHelpLuckyDetailsListByUserId(page,limit,userId));
    }

    @RequestMapping(value = "/selectLuckyQueueList", method = RequestMethod.GET)
    @ApiOperation("查看所有的队列列表")
    @ResponseBody
    public Result selectLuckyQueueList() {
        return Result.success().put("data",luckyQueueService.selectLuckyQueueListAsc());
    }

    @RequestMapping(value = "/selectLuckyQueueDetails", method = RequestMethod.GET)
    @ApiOperation("查看队列榜信息")
    @ResponseBody
    public Result selectLuckyQueueDetails(Long userId) {
        Map<String,Object> map=new HashMap<>();
        LuckyValueQueue luckyValueQueue = iLuckyValueQueueService.selectLuckyValueQueueByUserId(userId);
        UserEntity userByWxId = userService.selectById(userId);
        if(luckyValueQueue==null){
            map.put("flag",1);
            HelpLuckyValue helpLuckyValue = iHelpLuckyValueService.selectByUserId(userId);
            map.put("user",userByWxId);
            map.put("luckyValue",helpLuckyValue.getLuckyValue());
            map.put("upgrade",iHelpGetTaskeSrvice.updateLuckys(userByWxId));
            return Result.success(map);
        }
        List<Map<String, Object>> maps = iLuckyValueQueueService.selectLuckQueueLocation(luckyValueQueue.getLuckyQueueId());
        Map<String, Object> stringObjectMap = iLuckyValueQueueService.selectLuckQueueLocationByUserIds(userId, luckyValueQueue.getLuckyQueueId());
        map.put("flag",2);
        map.put("userList",maps);
        map.put("user",stringObjectMap);
        map.put("upgrade",iHelpGetTaskeSrvice.updateLuckys(userByWxId));
        return Result.success(map);
    }


    @RequestMapping(value = "/selectLuckyQueueRanking", method = RequestMethod.GET)
    @ApiOperation("管理平台查看红包榜排名")
    @ResponseBody
    public Result selectLuckyQueueRanking(int page,int limit,String phone,Long id) {
        return Result.success().put("data",iHelpLuckyValueService.selectLuckyQueueRanking(page,limit,id,phone));
    }



    @RequestMapping(value = "/selectLuckyQueueByUser", method = RequestMethod.GET)
    @ApiOperation("查看我邀请的好友详细信息")
    @ResponseBody
    public Result selectLuckyQueueByUser(int page,int limit,Long userId) {
        UserEntity userByWxId = userService.selectById(userId);
        PageUtils pageUtils = iLuckyValueQueueService.selectLuckyQueueByUser(page, limit, userByWxId.getUserId());
        Map<String,Object> map=new HashMap<>();
        map.put("user",userByWxId);
        map.put("userList",pageUtils);
        return Result.success().put("data",map);
    }



}