package com.sqx.modules.platformTask.controller;

import com.sqx.common.utils.Result;
import com.sqx.modules.helpTask.entity.HelpClassify;
import com.sqx.modules.platformTask.entity.PlatformClassify;
import com.sqx.modules.platformTask.service.PlatformClassifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author fang
 * @date 2020/6/11
 */
@RestController
@Api(value = "平台助力分类", tags = {"平台助力分类"})
@RequestMapping(value = "/platformClassify")
public class PlatformClassifyController {

    @Autowired
    private PlatformClassifyService platformClassifyService;


    @RequestMapping(value = "/saveHelpClassify", method = RequestMethod.POST)
    @ApiOperation("新建分类")
    @ResponseBody
    public Result saveHelpClassify(@RequestBody PlatformClassify platformClassify) {
        platformClassifyService.insert(platformClassify);
        return Result.success();
    }

    @RequestMapping(value = "/updateHelpClassify", method = RequestMethod.POST)
    @ApiOperation("修改分类")
    @ResponseBody
    public Result updateHelpClassify(@RequestBody PlatformClassify platformClassify) {
        platformClassifyService.update(platformClassify);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyById", method = RequestMethod.POST)
    @ApiOperation("删除分类")
    @ResponseBody
    public Result deleteClassifyById(Long id) {
        platformClassifyService.deleteById(id);
        return Result.success();
    }

    @RequestMapping(value = "/deleteClassifyByIds", method = RequestMethod.POST)
    @ApiOperation("删除多个分类")
    @ResponseBody
    public Result deleteClassifyByIds(String ids) {
        platformClassifyService.deleteByIdList(ids);
        return Result.success();
    }

    @RequestMapping(value = "/selectClassifyById", method = RequestMethod.GET)
    @ApiOperation("根据id查看分类详细信息")
    @ResponseBody
    public Result selectClassifyById(Long id) {
        return Result.success().put("data",platformClassifyService.selectById(id));
    }


    @RequestMapping(value = "/selectClassifyList", method = RequestMethod.GET)
    @ApiOperation("查看分类")
    @ResponseBody
    public Result selectClassifyList() {
        return Result.success().put("data",platformClassifyService.selectList());
    }



}