package com.sqx.modules.platformTask.controller;

import com.sqx.common.utils.Result;
import com.sqx.modules.platformTask.entity.LuckyQueue;
import com.sqx.modules.platformTask.service.ILuckyQueueRecordService;
import com.sqx.modules.platformTask.service.ILuckyQueueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

/**
 * @author fang
 * @date 2020/7/20
 */
@RestController
@Api(value = "幸运值管理", tags = {"幸运值管理"})
@RequestMapping(value = "/lucky")
public class LuckyController {

    private final ILuckyQueueService iLuckyQueueService;
    private final ILuckyQueueRecordService iLuckyQueueRecordService;


    public LuckyController(ILuckyQueueService iLuckyQueueService, ILuckyQueueRecordService iLuckyQueueRecordService) {
        this.iLuckyQueueService = iLuckyQueueService;
        this.iLuckyQueueRecordService = iLuckyQueueRecordService;
    }

    @RequestMapping(value = "/selectLuckyQueueList", method = RequestMethod.GET)
    @ApiOperation("查询队列类型列表")
    @ResponseBody
    public Result selectLuckyQueueList() {
        return Result.success().put("data",iLuckyQueueService.selectLuckyQueueList());
    }


    @RequestMapping(value = "/updateLuckyQueue", method = RequestMethod.POST)
    @ApiOperation("修改队列类型")
    @ResponseBody
    public Result updateLuckyQueue(@RequestBody LuckyQueue luckyQueue) {
        iLuckyQueueService.update(luckyQueue);
        return Result.success();
    }

    @RequestMapping(value = "/insertLuckyQueue", method = RequestMethod.POST)
    @ApiOperation("新增队列类型")
    @ResponseBody
    public Result insertLuckyQueue(@RequestBody LuckyQueue luckyQueue) {
        LuckyQueue luckyQueue1 = iLuckyQueueService.selectLuckyQueueBySort(luckyQueue.getSort());
        if(luckyQueue1!=null){
            return Result.error(-100,"已经添加过相同等级的队列了！");
        }
        iLuckyQueueService.saveBody(luckyQueue);
        return Result.success();
    }

    @RequestMapping(value = "/deleteLuckyQueue", method = RequestMethod.POST)
    @ApiOperation("删除幸运值类型")
    @ResponseBody
    public Result deleteLuckyQueue(Long id) {
        iLuckyQueueService.deleteById(id);
        return Result.success();
    }


    @RequestMapping(value = "/selectLuckyQueueRecordList", method = RequestMethod.GET)
    @ApiOperation("查询用户幸运值队列审核")
    @ResponseBody
    public Result selectLuckyQueueRecordList(int page,int limit) {
        return Result.success().put("data",iLuckyQueueRecordService.selectLuckyQueueRecordList(page,limit));
    }


    @RequestMapping(value = "/auditLuckyQueueRecord", method = RequestMethod.POST)
    @ApiOperation("审核幸运值队列")
    @ResponseBody
    public Result auditLuckyQueueRecord(String ids,Integer state,String auditContent) {
        iLuckyQueueRecordService.auditLuckyQueueRecord(ids,state,auditContent);
        return Result.success();
    }

    @RequestMapping(value = "/selectLuckyQueueRecordByUserId", method = RequestMethod.GET)
    @ApiOperation("查看用户晋级记录")
    @ResponseBody
    public Result selectLuckyQueueRecordByUserId(int page,int limit,Long userId) {
        return Result.success().put("data",iLuckyQueueRecordService.selectLuckyQueueRecordByUserId(page,limit,userId));
    }










}