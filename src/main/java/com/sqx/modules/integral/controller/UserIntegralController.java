package com.sqx.modules.integral.controller;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.integral.entity.SendMoneyIntegral;
import com.sqx.modules.integral.service.SendMoneyIntegralDetailsService;
import com.sqx.modules.integral.service.SendMoneyIntegralService;
import com.sqx.modules.integral.service.UserIntegralDetailsService;
import com.sqx.modules.integral.service.UserIntegralService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(value = "用户积分", tags = {"用户积分"})
@RequestMapping(value = "/integral")
public class UserIntegralController {

    @Autowired
    private UserIntegralService userIntegralService;
    @Autowired
    private UserIntegralDetailsService userIntegralDetailsService;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private SendMoneyIntegralService sendMoneyIntegralService;
    @Autowired
    private SendMoneyIntegralDetailsService sendMoneyIntegralDetailsService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("查看用户积分")
    @ResponseBody
    public Result selectByUserId(@PathVariable Long id) {
        return Result.success().put("data",userIntegralService.selectById(id));
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    @ApiOperation("查看用户积分详细列表信息")
    @ResponseBody
    public Result selectUserIntegralDetailsByUserId(int page,int limit,Long userId) {
        Map<String,Object> map=new HashMap<>(2);
        map.put("page",page);
        map.put("limit",limit);
        return Result.success().put("data",userIntegralDetailsService.selectUserIntegralDetailsByUserId(map,userId));
    }


    @RequestMapping(value = "/signIn", method = RequestMethod.GET)
    @ApiOperation("签到")
    @ResponseBody
    public Result signIn(Long userId) {
        CommonInfo one = commonInfoService.findOne(111);
        if("是".equals(one.getValue())){
            return userIntegralService.signIn(userId);
        }
        return Result.error("签到积分暂停使用！");
    }

    @RequestMapping(value = "/creditsExchange", method = RequestMethod.GET)
    @ApiOperation("积分兑换")
    @ResponseBody
    public Result creditsExchange(Long userId,Integer money) {
        CommonInfo one = commonInfoService.findOne(111);
        if("是".equals(one.getValue())){
            return userIntegralService.creditsExchange(userId,money);
        }
        return Result.error("签到积分暂停使用！");
    }

    @RequestMapping(value = "/insertSendMoney", method = RequestMethod.POST)
    @ApiOperation("添加积分推送活动")
    @ResponseBody
    public Result insertSendMoney(@RequestBody SendMoneyIntegral sendMoneyIntegral) {
        int i=sendMoneyIntegralService.insertSendMoney(sendMoneyIntegral);
        return i>0?Result.success():Result.error("添加失败，请刷新后重试！");
    }

    @RequestMapping(value = "/updateSendMoney", method = RequestMethod.POST)
    @ApiOperation("修改积分推送活动")
    @ResponseBody
    public Result updateSendMoney(@RequestBody SendMoneyIntegral sendMoneyIntegral) {
        int i=sendMoneyIntegralService.updateSendMoney(sendMoneyIntegral);
        return i>0?Result.success():Result.error("修改失败，请刷新后重试！");
    }

    @RequestMapping(value = "/deleteSendMoney", method = RequestMethod.POST)
    @ApiOperation("删除积分推送活动")
    @ResponseBody
    public Result deleteSendMoney(String sendMoneyIds) {
        int i=sendMoneyIntegralService.deleteSendMoneyByIds(sendMoneyIds);
        return i>0?Result.success():Result.error("删除失败，请刷新后重试！");
    }

    @RequestMapping(value = "/selectSendMoneyList", method = RequestMethod.GET)
    @ApiOperation("查看积分推送活动")
    @ResponseBody
    public Result selectSendMoneyList(int page,int limit,@RequestParam(required = false) String integralName) {
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        PageUtils pageUtils = sendMoneyIntegralService.selectSendMoneyList(map, integralName);
        return Result.success().put("data",pageUtils);
    }

    @RequestMapping(value = "/selectSendMoneyDetailsList", method = RequestMethod.GET)
    @ApiOperation("查看积分推送活动详细信息")
    @ResponseBody
    public Result selectSendMoneyDetailsList(int page,int limit,Long sendMoneyId) {
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        PageUtils pageUtils = sendMoneyIntegralDetailsService.selectSendMoneyDetailsList(page, limit, sendMoneyId);
        return Result.success().put("data",pageUtils);
    }


}
