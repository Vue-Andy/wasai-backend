package com.sqx.modules.integral.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.integral.entity.UserIntegral;
import com.sqx.modules.integral.entity.UserIntegralDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author fang
 * @date 2020/9/18
 */
@Mapper
public interface UserIntegralDetailsDao extends BaseMapper<UserIntegralDetails> {

    UserIntegralDetails selectUserIntegralDetailsByUserId(@Param("userId") Long userId,@Param("time") Date time);

}