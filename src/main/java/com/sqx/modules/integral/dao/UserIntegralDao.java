package com.sqx.modules.integral.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.integral.entity.UserIntegral;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/9/18
 */
@Mapper
public interface UserIntegralDao extends BaseMapper<UserIntegral> {

    int updateIntegral(@Param("type") int type,@Param("userId") Long userId,@Param("num") Integer num);

    List<Long> selectUserIntegral(Integer integralNum);

    List<Long> selectUserIdListByIntegralNumAndSendMoneyId(@Param("integralNum") Integer integralNum,@Param("sendMoneyId") Long sendMoneyId);

}