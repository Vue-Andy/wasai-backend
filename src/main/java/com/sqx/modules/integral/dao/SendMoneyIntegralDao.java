package com.sqx.modules.integral.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.integral.entity.SendMoneyIntegral;
import com.sqx.modules.integral.entity.UserIntegral;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/9/18
 */
@Mapper
public interface SendMoneyIntegralDao extends BaseMapper<SendMoneyIntegral> {

    List<SendMoneyIntegral> selectSendMoneyIntegralList();

}