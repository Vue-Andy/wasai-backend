package com.sqx.modules.integral.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sqx.modules.integral.entity.SendMoneyIntegralDetails;
import com.sqx.modules.integral.entity.UserIntegral;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
@Mapper
public interface SendMoneyIntegralDetailsDao extends BaseMapper<SendMoneyIntegralDetails> {

    IPage<Map<String,Object>> selectSendMoneyIntegralDetailsList(Page<Map<String,Object>> page,Long sendMoneyId);

}