package com.sqx.modules.integral.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fang
 * @date 2020/10/27
 */
@Data
@TableName("send_money_Integral_details")
public class SendMoneyIntegralDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 积分推送详细id
     */
    @TableId
    private Long sendMoneyDetailsId;

    /**
     * 积分推送id
     */
    private Long sendMoneyId;

    /**
     * 用户id
     */
    private Long userId;



    private String createTime;
}