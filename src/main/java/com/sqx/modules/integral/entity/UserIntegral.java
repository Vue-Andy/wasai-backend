package com.sqx.modules.integral.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import static com.baomidou.mybatisplus.annotation.IdType.INPUT;

/**
 * @author fang
 * @date 2020/9/18
 */
@Data
@TableName("user_integral")
public class UserIntegral {

    /**
     * 用户id
     */
    @TableId(type = INPUT)
    private Long userId;

    /**
     * 积分数量
     */
    private Integer integralNum;




}