package com.sqx.modules.integral.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fang
 * @date 2020/10/27
 */
@Data
@TableName("send_money_Integral")
public class SendMoneyIntegral implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 积分奖励金额规则id
     */
    @TableId
    private Long sendMoneyId;

    private String integralName;

    private String describes;

    /**
     * 积分
     */
    private Integer integralNum;

    /**
     * 金额
     */
    private Double money;

    /**
     * 状态（1启用 2停止）
     */
    private Integer state;

    /**
     * 创建时间
     */
    private String createTime;
}