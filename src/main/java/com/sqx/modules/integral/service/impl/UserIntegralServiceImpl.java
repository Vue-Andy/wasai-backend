package com.sqx.modules.integral.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.Result;
import com.sqx.modules.common.dao.CommonInfoDao;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.integral.dao.UserIntegralDao;
import com.sqx.modules.integral.dao.UserIntegralDetailsDao;
import com.sqx.modules.integral.entity.UserIntegral;
import com.sqx.modules.integral.entity.UserIntegralDetails;
import com.sqx.modules.integral.service.UserIntegralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author fang
 * @date 2020/9/18
 */
@Service
public class UserIntegralServiceImpl extends ServiceImpl<UserIntegralDao, UserIntegral> implements UserIntegralService {

    @Autowired
    private UserIntegralDao userIntegralDao;
    @Autowired
    private UserIntegralDetailsDao userIntegralDetailsDao;
    @Autowired
    private CommonInfoService commonInfoService;
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;


    @Override
    public List<Long> selectUserIdListByIntegralNumAndSendMoneyId(Integer integralNum, Long sendMoneyId) {
        return userIntegralDao.selectUserIdListByIntegralNumAndSendMoneyId(integralNum, sendMoneyId);
    }

    @Override
    public UserIntegral selectById(Long id){
        UserIntegral userIntegral = userIntegralDao.selectById(id);
        if(userIntegral==null){
            userIntegral=new UserIntegral();
            userIntegral.setUserId(id);
            userIntegral.setIntegralNum(0);
            userIntegralDao.insert(userIntegral);
        }
        return userIntegral;
    }

    @Override
    public Result signIn(Long userId){
        //先判断今天是否签过到
        UserIntegralDetails userIntegralDetails1 = userIntegralDetailsDao.selectUserIntegralDetailsByUserId(userId, new Date());
        if(userIntegralDetails1!=null){
            return Result.error("今天已经签到过了，请明天再来吧！");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        selectById(userId);
        //每月初始积分
        CommonInfo one = commonInfoService.findOne(100);
        //累计签到叠加积分
        CommonInfo two = commonInfoService.findOne(101);
        Calendar cal=Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        //获取当前日期时第几天  第一天则重新开始计时
        int num=0;
        if(day==1){
            num=Integer.parseInt(one.getValue());
        }else{
            int day1 = cal.get(Calendar.DATE);
            cal.set(Calendar.DATE, day1 - 1);
            //获取昨天的签到
            UserIntegralDetails userIntegralDetails2 = userIntegralDetailsDao.selectUserIntegralDetailsByUserId(userId, cal.getTime());
            //判断昨天是否签到
            if(userIntegralDetails2!=null){
                num=userIntegralDetails2.getNum()+Integer.parseInt(two.getValue());
            }else{
                num=Integer.parseInt(one.getValue());
            }
        }
        userIntegralDao.updateIntegral(1,userId,num);
        UserIntegralDetails userIntegralDetails=new UserIntegralDetails();
        userIntegralDetails.setClassify(1);
        userIntegralDetails.setContent("签到获得:"+num+"积分");
        userIntegralDetails.setCreateTime(sdf.format(new Date()));
        userIntegralDetails.setNum(num);
        userIntegralDetails.setType(1);
        userIntegralDetails.setUserId(userId);
        userIntegralDetailsDao.insert(userIntegralDetails);
        return Result.success("签到成功，获得"+num+"积分");
    }

    @Override
    public int updateIntegral(int type, Long userId, Integer num) {
        return userIntegralDao.updateIntegral(type,userId,num);
    }

    @Override
    public Result creditsExchange(Long userId,Integer money){
        UserIntegral userIntegral = selectById(userId);
        CommonInfo one = commonInfoService.findOne(102);
        Integer num=Integer.parseInt(one.getValue())*money;
        if(userIntegral.getIntegralNum()>=num){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            updateIntegral(2,userId,num);
            UserIntegralDetails userIntegralDetails=new UserIntegralDetails();
            userIntegralDetails.setClassify(0);
            userIntegralDetails.setContent("积分兑换金额，消耗积分："+num+",兑换金额："+money);
            userIntegralDetails.setCreateTime(sdf.format(new Date()));
            userIntegralDetails.setNum(num);
            userIntegralDetails.setType(2);
            userIntegralDetails.setUserId(userId);
            userIntegralDetailsDao.insert(userIntegralDetails);
            double v = Double.parseDouble(String.valueOf(money));
            userMoneyService.updateMayMoney(1,userId,v);
            UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
            userMoneyDetails.setUserId(userId);
            userMoneyDetails.setTitle("[积分]积分兑换");
            userMoneyDetails.setContent("增加金额:"+money);
            userMoneyDetails.setType(1);
            userMoneyDetails.setMoney(v);
            userMoneyDetails.setCreateTime(sdf.format(new Date()));
            userMoneyDetailsService.insert(userMoneyDetails);
            return Result.success("积分兑换成功！");
        }else{
            return Result.error("积分数量不足！");
        }
    }

    @Override
    public List<Long> selectUserIntegral(Integer integralNum) {
        return userIntegralDao.selectUserIntegral(integralNum);
    }



}