package com.sqx.modules.integral.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.modules.integral.entity.SendMoneyIntegralDetails;

import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
public interface SendMoneyIntegralDetailsService {

    int insertSendMoneyDetails(SendMoneyIntegralDetails sendMoneyIntegralDetails);

    int deleteSendMoneyDetailsById(Long sendMoneyDetailsId);

    int deleteSendMoneyDetailsBySendMoneyId(Long sendMoneyId);

    PageUtils selectSendMoneyDetailsList(int page,int limit, Long sendMoneyId);

}