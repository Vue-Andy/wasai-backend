package com.sqx.modules.integral.service;

import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Result;
import com.sqx.modules.integral.entity.SendMoneyIntegral;
import com.sqx.modules.integral.entity.UserIntegral;

import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
public interface SendMoneyIntegralService {

    int insertSendMoney(SendMoneyIntegral sendMoneyIntegral);

    int updateSendMoney(SendMoneyIntegral sendMoneyIntegral);

    int deleteSendMoneyByIds(String sendMoneyIds);

    PageUtils selectSendMoneyList(Map<String,Object> params, String integralName);

    List<SendMoneyIntegral> selectSendMoneyList();


}