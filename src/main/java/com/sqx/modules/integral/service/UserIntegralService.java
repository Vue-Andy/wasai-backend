package com.sqx.modules.integral.service;

import com.sqx.common.utils.Result;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.integral.entity.UserIntegral;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/9/18
 */
public interface UserIntegralService {

    List<Long> selectUserIdListByIntegralNumAndSendMoneyId( Integer integralNum, Long sendMoneyId);

    UserIntegral selectById(Long id);

    Result signIn(Long userId);

    int updateIntegral(int type,Long userId, Integer num);

    Result creditsExchange(Long userId,Integer money);

    List<Long> selectUserIntegral(Integer integralNum);


}