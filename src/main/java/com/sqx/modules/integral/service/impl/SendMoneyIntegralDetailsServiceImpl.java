package com.sqx.modules.integral.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.common.utils.Result;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.StringUtils;
import com.sqx.modules.integral.dao.SendMoneyIntegralDetailsDao;
import com.sqx.modules.integral.dao.UserIntegralDao;
import com.sqx.modules.integral.dao.UserIntegralDetailsDao;
import com.sqx.modules.integral.entity.SendMoneyIntegral;
import com.sqx.modules.integral.entity.SendMoneyIntegralDetails;
import com.sqx.modules.integral.entity.UserIntegral;
import com.sqx.modules.integral.entity.UserIntegralDetails;
import com.sqx.modules.integral.service.SendMoneyIntegralDetailsService;
import com.sqx.modules.integral.service.SendMoneyIntegralService;
import com.sqx.modules.integral.service.UserIntegralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
@Service
public class SendMoneyIntegralDetailsServiceImpl extends ServiceImpl<SendMoneyIntegralDetailsDao, SendMoneyIntegralDetails> implements SendMoneyIntegralDetailsService {

    @Autowired
    private SendMoneyIntegralDetailsDao sendMoneyIntegralDetailsDao;
    @Autowired
    private SendMoneyIntegralService sendMoneyIntegralService;
    @Autowired
    private UserIntegralService userIntegralService;
    @Autowired
    private UserMoneyService userMoneyService;
    @Autowired
    private UserMoneyDetailsService userMoneyDetailsService;

    @Override
    public int insertSendMoneyDetails(SendMoneyIntegralDetails sendMoneyIntegralDetails){
        return sendMoneyIntegralDetailsDao.insert(sendMoneyIntegralDetails);
    }

    @Override
    public int deleteSendMoneyDetailsById(Long sendMoneyDetailsId){
        return sendMoneyIntegralDetailsDao.deleteById(sendMoneyDetailsId);
    }

    @Override
    public int deleteSendMoneyDetailsBySendMoneyId(Long sendMoneyId){
        return sendMoneyIntegralDetailsDao.delete(new QueryWrapper<SendMoneyIntegralDetails>().eq("send_money_id",sendMoneyId));
    }

    @Override
    public PageUtils selectSendMoneyDetailsList(int page,int limit, Long sendMoneyId){
        Page<Map<String,Object>> pages=new Page<>(page, limit);
        IPage<Map<String, Object>> mapIPage = sendMoneyIntegralDetailsDao.selectSendMoneyIntegralDetailsList(pages, sendMoneyId);
        return new PageUtils(mapIPage);
    }


    @Scheduled(cron="0 */1 * * * ?")
    public synchronized void getReach(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<SendMoneyIntegral> sendMoneyIntegrals = sendMoneyIntegralService.selectSendMoneyList();
        for(SendMoneyIntegral sendMoneyIntegral:sendMoneyIntegrals){
            List<Long> longs = userIntegralService.selectUserIdListByIntegralNumAndSendMoneyId(sendMoneyIntegral.getIntegralNum(), sendMoneyIntegral.getSendMoneyId());
            for(Long userId:longs){
                userMoneyService.updateMayMoney(1,userId,sendMoneyIntegral.getMoney());
                UserMoneyDetails userMoneyDetails=new UserMoneyDetails();
                userMoneyDetails.setUserId(userId);
                userMoneyDetails.setTitle("[积分奖励]平台积分奖励");
                userMoneyDetails.setContent("恭喜您，积分达到平台要求，赠送您账户金额:"+sendMoneyIntegral.getMoney());
                userMoneyDetails.setType(1);
                userMoneyDetails.setMoney(sendMoneyIntegral.getMoney());
                userMoneyDetails.setCreateTime(sdf.format(new Date()));
                userMoneyDetailsService.insert(userMoneyDetails);
                SendMoneyIntegralDetails sendMoneyIntegralDetails=new SendMoneyIntegralDetails();
                sendMoneyIntegralDetails.setCreateTime(sdf.format(new Date()));
                sendMoneyIntegralDetails.setSendMoneyId(sendMoneyIntegral.getSendMoneyId());
                sendMoneyIntegralDetails.setUserId(userId);
                insertSendMoneyDetails(sendMoneyIntegralDetails);
            }
        }
    }








}