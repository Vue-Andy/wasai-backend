package com.sqx.modules.integral.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.common.utils.Result;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.entity.UserMoneyDetails;
import com.sqx.modules.helpTask.service.UserMoneyDetailsService;
import com.sqx.modules.helpTask.service.UserMoneyService;
import com.sqx.modules.helpTask.utils.StringUtils;
import com.sqx.modules.integral.dao.SendMoneyIntegralDao;
import com.sqx.modules.integral.dao.UserIntegralDao;
import com.sqx.modules.integral.dao.UserIntegralDetailsDao;
import com.sqx.modules.integral.entity.SendMoneyIntegral;
import com.sqx.modules.integral.entity.UserIntegral;
import com.sqx.modules.integral.entity.UserIntegralDetails;
import com.sqx.modules.integral.service.SendMoneyIntegralService;
import com.sqx.modules.integral.service.UserIntegralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
@Service
public class SendMoneyIntegralServiceImpl extends ServiceImpl<SendMoneyIntegralDao, SendMoneyIntegral> implements SendMoneyIntegralService {

   @Autowired
    private SendMoneyIntegralDao sendMoneyIntegralDao;

    @Override
   public int insertSendMoney(SendMoneyIntegral sendMoneyIntegral){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sendMoneyIntegral.setCreateTime(sdf.format(new Date()));
       return sendMoneyIntegralDao.insert(sendMoneyIntegral);
   }

    @Override
   public int updateSendMoney(SendMoneyIntegral sendMoneyIntegral){
       return sendMoneyIntegralDao.updateById(sendMoneyIntegral);
   }

    @Override
   public int deleteSendMoneyByIds(String sendMoneyIds){
       int i=0;
       for(String id:sendMoneyIds.split(",")){
           i+= sendMoneyIntegralDao.deleteById(Long.parseLong(id));
       }
       return i;
   }

    @Override
   public PageUtils selectSendMoneyList(Map<String,Object> params,String integralName){
       IPage<SendMoneyIntegral> page = this.page(
               new Query<SendMoneyIntegral>().getPage(params),
               new QueryWrapper<SendMoneyIntegral>().eq(StringUtils.isNotBlank(integralName),"integral_name",integralName)
       );
       return new PageUtils(page);
   }

    @Override
   public List<SendMoneyIntegral> selectSendMoneyList(){
       return sendMoneyIntegralDao.selectList(new QueryWrapper<>());
   }






}