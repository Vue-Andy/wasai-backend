package com.sqx.modules.integral.service;

import com.sqx.common.utils.PageUtils;

import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
public interface UserIntegralDetailsService {

    PageUtils selectUserIntegralDetailsByUserId(Map<String,Object> params, Long userId);

}