package com.sqx.modules.integral.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.modules.commodity.entity.Commodity;
import com.sqx.modules.integral.dao.UserIntegralDao;
import com.sqx.modules.integral.dao.UserIntegralDetailsDao;
import com.sqx.modules.integral.entity.UserIntegral;
import com.sqx.modules.integral.entity.UserIntegralDetails;
import com.sqx.modules.integral.service.UserIntegralDetailsService;
import com.sqx.modules.integral.service.UserIntegralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author fang
 * @date 2020/9/18
 */
@Service
public class UserIntegralDetailsServiceImpl extends ServiceImpl<UserIntegralDetailsDao, UserIntegralDetails> implements UserIntegralDetailsService {

    @Autowired
    private UserIntegralDetailsDao userIntegralDetailsDao;

    @Override
    public PageUtils selectUserIntegralDetailsByUserId(Map<String,Object> params, Long userId){
        IPage<UserIntegralDetails> page = this.page(
                new Query<UserIntegralDetails>().getPage(params),
                new QueryWrapper<UserIntegralDetails>().eq(userId!=null,"user_id",userId).orderByDesc("create_time")
        );
        return new PageUtils(page);
    }


}