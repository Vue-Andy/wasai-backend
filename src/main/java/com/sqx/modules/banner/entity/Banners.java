package com.sqx.modules.banner.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 广告位
 */
@Data
@TableName("banners")
public class Banners implements Serializable {
    
    @TableId(type = IdType.INPUT)
    private Long id;
    
    private String createAt;
    
    private String imageUrl;
    
    private String url;
    
    private String state;


}
