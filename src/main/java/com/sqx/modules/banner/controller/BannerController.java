package com.sqx.modules.banner.controller;


import com.sqx.common.utils.Result;
import com.sqx.modules.banner.entity.Banner;
import com.sqx.modules.banner.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @author fang
 * @date 2020/7/9
 */
@Slf4j
@RestController
@Api(value = "banner图", tags = {"banner图"})
@RequestMapping(value = "/banner")
public class BannerController {


    @Autowired
    private BannerService bannerService;


    @RequestMapping(value = "/selectBannerList", method = RequestMethod.GET)
    @ApiOperation("查询所有banner图")
    @ResponseBody
    public Result selectBannerList(Integer classify,Integer state){
        return Result.success().put("data",bannerService.selectBannerList(classify,state));
    }

    @RequestMapping(value = "/selectBannerPage", method = RequestMethod.GET)
    @ApiOperation("查询所有banner图")
    @ResponseBody
    public Result selectBannerPage(Integer page,Integer limit,Integer classify,Integer state){
        return Result.success().put("data",bannerService.selectPage(page,limit,classify,state));
    }


    @RequestMapping(value = "/selectInvitationBackground", method = RequestMethod.GET)
    @ApiOperation("查看邀请背景图")
    @ResponseBody
    public Result selectInvitationBackground(){
        List<Banner> banners = bannerService.selectBannerList(5, null);
        if(banners.size()>0){
            return Result.success().put("data",banners.get(0));
        }else{
            return Result.success().put("data",null);
        }
        /*if(banners.size()>0){
            //随机返回一张图片
            Random random=new Random();
            int i=random.nextInt(banners.size());
            Result.success().put("data",banners.get(i));
        }*/

    }

    @RequestMapping(value = "/selectBannerById", method = RequestMethod.GET)
    @ApiOperation("根据id查看详细信息")
    @ResponseBody
    public Result selectBannerById(Long id){
        return Result.success().put("data",bannerService.selectBannerById(id));
    }

    @RequestMapping(value = "/updateBannerStateById", method = RequestMethod.POST)
    @ApiOperation("隐藏banner图")
    @ResponseBody
    public Result updateBannerStateById(Long id){
        bannerService.updateBannerStateById(id);
        return Result.success();
    }

    @RequestMapping(value = "/updateBannerById", method = RequestMethod.POST)
    @ApiOperation("修改banner图")
    @ResponseBody
    public Result updateBannerById(Banner banner){
        bannerService.updateBannerById(banner);
        return Result.success();
    }

    @RequestMapping(value = "/deleteBannerById", method = RequestMethod.POST)
    @ApiOperation("删除banner图")
    @ResponseBody
    public Result deleteBannerById(Long id){
        bannerService.deleteBannerById(id);
        return Result.success();
    }

    @RequestMapping(value = "/insertBanner", method = RequestMethod.POST)
    @ApiOperation("添加banner图")
    @ResponseBody
    public Result insertBanner(@RequestBody Banner banner){
        if(banner.getClassify()==5){
            List<Banner> banners = bannerService.selectBannerList(5, null);
            if(banners.size()>0){
                return Result.error("邀请背景图只能存在一个！");
            }
        }
        bannerService.insertBanner(banner);
        return Result.success();
    }



}