package com.sqx.modules.banner.controller;


import com.sqx.common.utils.Result;
import com.sqx.modules.banner.entity.Banner;
import com.sqx.modules.banner.service.BannerService;
import com.sqx.modules.banner.service.BannersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/9
 */
@Slf4j
@RestController
@Api(value = "广告位管理", tags = {"广告位管理"})
@RequestMapping(value = "/banner")
public class BannersController {


    @Autowired
    private BannersService iBannerService;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("管理平台广告位详情")
    @ResponseBody
    public Result getBanner(@PathVariable Integer id) {
        return Result.success().put("data",iBannerService.selectBannersById(Long.valueOf(id)));
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ApiOperation("管理平台添加广告位")
    @ResponseBody
    public Result addBanner(@PathVariable String id, @RequestParam String url, @RequestParam String image) {
        iBannerService.insertBanners(id, url, image);
        return Result.success();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ApiOperation("管理平台删除广告位")
    public Result deleteBanner(@PathVariable int id) {
        iBannerService.deleteBannersById(Long.parseLong(id+""));
        return Result.success();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation("管理平台获取全部广告位")
    @ResponseBody
    public Result getBannerList() {
        return Result.success().put("data",iBannerService.selectBannerList());
    }

    @RequestMapping(value = "/updateState/{id}/{state}", method = RequestMethod.POST)
    @ApiOperation("管理平台修改广告位状态")
    @ResponseBody
    public Result updateState(@PathVariable("state") String state, @PathVariable("id") String id) {
        iBannerService.updateBannersStateById(state, id);
        return Result.success();
    }


    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    @ApiOperation("用户端获取全部广告位")
    @ResponseBody
    public Result getBannerUserList() {
        return Result.success().put("data",iBannerService.selectBannerList());
    }



}