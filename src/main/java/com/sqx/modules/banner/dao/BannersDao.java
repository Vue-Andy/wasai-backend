package com.sqx.modules.banner.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.banner.entity.Banner;
import com.sqx.modules.banner.entity.Banners;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fang
 * @date 2020/7/9
 */
@Mapper
public interface BannersDao extends BaseMapper<Banners> {


}
