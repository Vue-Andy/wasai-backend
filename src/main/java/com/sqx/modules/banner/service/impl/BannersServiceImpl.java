package com.sqx.modules.banner.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.banner.dao.BannerDao;
import com.sqx.modules.banner.dao.BannersDao;
import com.sqx.modules.banner.entity.Banner;
import com.sqx.modules.banner.entity.Banners;
import com.sqx.modules.banner.service.BannerService;
import com.sqx.modules.banner.service.BannersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * banner图
 */
@Service
public class BannersServiceImpl extends ServiceImpl<BannersDao, Banners> implements BannersService {


    @Autowired
    private BannersDao bannersDao;


    @Override
    public List<Banners> selectBannerList() {
        return bannersDao.selectList(null);
    }

    @Override
    public int insertBanners(String id, String url, String image) {
        Banners banner = new Banners();
        banner.setId(Long.valueOf(id));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        banner.setImageUrl(image);
        banner.setCreateAt(sdf.format(now));
        banner.setState("true");
        banner.setUrl(url);
        if(banner.getId()!=null){
            bannersDao.updateById(banner);
        }else{
            bannersDao.insert(banner);
        }

        return 0;
    }

    @Override
    public Banners selectBannersById(Long id) {
        return bannersDao.selectById(id);
    }

    @Override
    public int deleteBannersById(Long id) {
        return bannersDao.deleteById(id);
    }

    @Override
    public int updateBanners(Banners banners) {
        return bannersDao.updateById(banners);
    }

    @Override
    public int updateBannersStateById(String state, String id) {
        Banners banner = new Banners();
        banner.setId(Long.parseLong(id));
        banner.setState(state);
        return bannersDao.updateById(banner);
    }
}
