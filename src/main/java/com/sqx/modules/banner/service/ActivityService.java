package com.sqx.modules.banner.service;


import com.sqx.modules.banner.entity.Activity;
import com.sqx.modules.banner.entity.Banners;

import java.util.List;

public interface ActivityService {

    List<Activity> selectByState(String state);

    Activity selectActivityById(Long id);

    int insertActivity(Long id,Activity info);

    int deleteActivity(Long id);

    List<Activity> selectActivity();

}
