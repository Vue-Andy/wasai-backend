package com.sqx.modules.banner.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.modules.banner.dao.ActivityDao;
import com.sqx.modules.banner.dao.BannerDao;
import com.sqx.modules.banner.entity.Activity;
import com.sqx.modules.banner.entity.Banner;
import com.sqx.modules.banner.service.ActivityService;
import com.sqx.modules.banner.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 活动推广
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityDao, Activity> implements ActivityService {

    @Autowired
    private ActivityDao activityDao;

    @Override
    public List<Activity> selectByState(String state){
        return activityDao.selectByState(state);
    }

    @Override
    public Activity selectActivityById(Long id){
        return activityDao.selectById(id);
    }

    @Override
    public int insertActivity(Long id,Activity info){
        Activity activity = new Activity();
        activity.setId(id);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        activity.setImageUrl(info.getImageUrl());
        activity.setTitle(info.getTitle());
        activity.setCreateAt(sdf.format(now));
        activity.setState(info.getState());
        activity.setUrl(info.getUrl());
        if(activity.getId()!=null){
            activityDao.updateById(activity);
        }else{
            activityDao.insert(activity);
        }
        return 1;
    }

    @Override
    public int deleteActivity(Long id){
        return activityDao.deleteById(id);
    }

    @Override
    public List<Activity> selectActivity(){
        return activityDao.selectList(null);
    }


}
