package com.sqx.modules.banner.service;


import com.sqx.modules.banner.entity.Banner;
import com.sqx.modules.banner.entity.Banners;

import java.util.List;

public interface BannersService {

    List<Banners> selectBannerList();

    int insertBanners(String id, String url, String image);

    Banners selectBannersById(Long id);

    int deleteBannersById(Long id);

    int updateBanners(Banners banners);

    int updateBannersStateById(String state,String id);

}
