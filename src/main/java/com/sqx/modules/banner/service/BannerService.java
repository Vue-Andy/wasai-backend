package com.sqx.modules.banner.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.banner.entity.Banner;

import java.util.List;

public interface BannerService {

    List<Banner> selectBannerList(Integer classify,Integer state);

    PageUtils selectPage(Integer page, Integer limit, Integer classify, Integer state);

    int saveBody(String image,String url,Integer sort);

    Banner selectBannerById(Long id);

    int deleteBannerById(Long id);

    int updateBannerStateById(Long id);

    int updateBannerById(Banner banner);

    int insertBanner(Banner banner);

}
