package com.sqx.modules.chat.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 聊天会话内容
 */
@Data
@Entity
public class ChatContent {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long chatContentId; //会话内容id
    @Column(columnDefinition = "text")
    private String content; //内容
    @Column
    private Integer type; //类型（1文字 2图片 3订单 4商品）
    @Column(columnDefinition = "int default 1")
    private Integer status; //是否已读(1未读 2已读)
    @Column
    private String createTime; //创建时间
    @Column
    private Integer sendType; //消息来源（1用户消息 2商户消息）
    @Column
    private String userId; //用户id
    @Column
    private Long storeId; //店铺id（单商户传0）
    /**会话信息*/
    @Column
    private Long chatId; //会话Id
    @Transient
    private Chat chat; //聊天会话
}
