package com.sqx.modules.chat.service;


import com.sqx.modules.chat.entity.Chat;
import com.sqx.modules.shopping.utils.Result;

public interface ChatService {

    /**
     * 商家端未读消息
     * @param storeId
     * @return
     */
    Result count(Long storeId);

    /**
     * 用户端未读消息
     * @param userId
     * @return
     */
    Result userCount(String userId);

    /**
     * 商家端会话列表
     * @param storeId
     * @return
     */
    Result findAll(Long storeId, String userName);

    /**
     * 用户端会话列表
     * @param userId
     * @return
     */
    Result userList(String userId, String storeName);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(Long id);

    //添加
    Result saveBody(Chat entity);

    //修改
    Result updateBody(Chat entity);

}
