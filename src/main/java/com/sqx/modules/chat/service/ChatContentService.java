package com.sqx.modules.chat.service;


import com.sqx.modules.chat.entity.ChatContent;
import com.sqx.modules.shopping.utils.Result;

public interface ChatContentService {

    /**
     * 用户聊天内容列表
     * @param chatId
     * @return
     */
    Result findAll(Long chatId);

    /**
     * 店铺聊天内容列表
     * @param chatId
     * @return
     */
    Result storeList(Long chatId);

    //查询
    Result findOne(Long id);

    //删除
    Result delete(String ids);

    /**
     * 发送消息
     * @param entity
     * @return
     */
    Result saveBody(ChatContent entity);

    //修改
    Result updateBody(ChatContent entity);

}
