package com.sqx.modules.chat.controller;

import com.sqx.modules.app.service.UserService;
import com.sqx.modules.chat.entity.Chat;
import com.sqx.modules.chat.service.ChatService;
import com.sqx.modules.shopping.utils.Result;
import com.sqx.modules.shopping.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value="聊天会话",tags={"聊天会话"})
@RequestMapping(value = "/chat")
public class ChatController {
    @Autowired
    private ChatService service;
    @Autowired
    private UserService userService;

    @GetMapping("/count")
    @ApiOperation("商家端未读消息总数")
    public Result count(@ApiParam("店铺id(总后台商户传0)") @RequestParam Long storeId) {
        return service.count(storeId);
    }

    @GetMapping("/userCount")
    @ApiOperation("用户端未读消息总数")
    public Result userCount(@RequestParam String userId) {
        return service.userCount(userId);
    }

    @GetMapping("/list")
    @ApiOperation("商家端会话列表")
    public Result findAll(@ApiParam("店铺id(总后台商户传0)") @RequestParam Long storeId,
                          @ApiParam("用户昵称") @RequestParam(required = false) String userName) {
        return service.findAll(storeId, userName);
    }

    @GetMapping("/userList")
    @ApiOperation("用户端会话列表")
    public Result userList(String userId, @ApiParam("店铺名称") @RequestParam(required = false) String storeName) {
        return service.userList(userId, storeName);
    }

    @GetMapping("/find")
    @ApiOperation("查询")
    public Result findOne(Long id) {
        return service.findOne(id);
    }

    @PostMapping("/save")
    @ApiOperation("用户端发起聊天")
    public Result saveBody(@RequestBody Chat entity) {
        return service.saveBody(entity);
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public Result updateBody(@RequestBody Chat entity) {
        return service.updateBody(entity);
    }

    @GetMapping("/delete")
    @ApiOperation("删除聊天会话")
    public Result delete(Long id) {
        return service.delete(id);
    }


    @GetMapping(value = "/getUserByInvitationCode")
    @ApiOperation("用户端和管理平台获取用户详情")
    public Result getUserByInvitationCode(String invitationCode) {
        return ResultUtil.success(userService.selectUserByInvitationCode(invitationCode));
    }

}
