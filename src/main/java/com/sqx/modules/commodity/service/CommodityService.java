package com.sqx.modules.commodity.service;


import com.sqx.common.utils.PageUtils;
import com.sqx.modules.commodity.entity.Commodity;
import com.sqx.modules.helpTask.entity.CashClassify;

import java.util.List;
import java.util.Map;

public interface CommodityService {

    PageUtils selectCommodityList(Map<String,Object> params);

    Commodity selectById(Long id);

    int saveBody(Commodity commodity);

    void saveBodyAll(List<Commodity> list);

    int update(Commodity commodity);

    void delete(Long id);

    void deleteByIdList(String ids);

}
