package com.sqx.modules.commodity.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sqx.common.utils.PageUtils;
import com.sqx.common.utils.Query;
import com.sqx.modules.commodity.dao.CommodityDao;
import com.sqx.modules.commodity.entity.Commodity;
import com.sqx.modules.commodity.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * 精选好物
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityDao, Commodity> implements CommodityService {

    @Autowired
    private CommodityDao commodityDao;


    @Override
    public PageUtils selectCommodityList(Map<String,Object> params) {
        IPage<Commodity> page = this.page(
                new Query<Commodity>().getPage(params)
        );
        return new PageUtils(page);
    }

    @Override
    public Commodity selectById(Long id) {
        return commodityDao.selectById(id);
    }

    @Override
    public int saveBody(Commodity commodity) {
        return commodityDao.insert(commodity);
    }

    @Override
    public void saveBodyAll(List<Commodity> list) {
        for(Commodity commodity:list){
            commodityDao.insert(commodity);
        }
    }


    @Override
    public int update(Commodity commodity) {
        return commodityDao.updateById(commodity);
    }

    @Override
    public void delete(Long id) {
        commodityDao.deleteById(id);
    }



    @Override
    public void deleteByIdList(String ids) {
        for(String id:ids.split(",")){
            commodityDao.deleteById(Long.parseLong(id));
        }
    }
}
