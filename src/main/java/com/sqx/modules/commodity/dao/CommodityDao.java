package com.sqx.modules.commodity.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sqx.modules.commodity.entity.Commodity;
import com.sqx.modules.helpTask.entity.CashOut;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author fang
 * @date 2020/7/8
 */
@Mapper
public interface CommodityDao extends BaseMapper<Commodity> {

}
