package com.sqx.modules.commodity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *  commodity
 * @author 房 2020-05-20
 */
@Data
@TableName("commodity")
public class Commodity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private Long id;


    private String activityid;


    private String activity_type;


    private Double coupon_condition;


    private Integer couponendtime;


    private String couponexplain;


    private String couponmoney;


    private Integer couponnum;


    private Integer couponreceive;


    private Integer couponreceive2;


    private Integer couponstarttime;


    private Integer couponsurplus;


    private String couponurl;


    private String cuntao;


    private Double deposit;


    private Double deposit_deduct;


    private Double discount;


    private Integer down_type;


    private Integer end_time;


    private String fqcat;


    private String general_index;


    private String guide_article;


    private Integer is_brand;


    private Integer is_explosion;


    private Integer is_live;


    private Integer is_shipping;


    private String isquality;


    private String itemdesc;


    private Double itemendprice;


    private String itemid;


    private String itempic;


    private String itempic_copy;


    private Double itemprice;


    private String itemsale;


    private String itemsale2;


    private String itemshorttitle;


    private String itemtitle;


    private String me;


    private String online_users;


    private String original_article;


    private String original_img;


    private String planlink;


    private String presale_discount_fee_text;


    private Integer presale_end_time;


    private Integer presale_start_time;


    private Integer presale_tail_end_time;


    private Integer presale_tail_start_time;


    private Integer product_id;


    private String report_status;


    private String seller_id;


    private String seller_name;


    private String sellernick;


    private String shopid;


    private String shopname;


    private String shoptype;


    private String son_category;


    private Integer start_time;


    private Integer starttime;


    private String taobao_image;


    private Double tkmoney;


    private Double tkrates;


    private String tktype;


    private String todaycouponreceive;


    private String todaysale;


    private Integer userid;


    private String videoid;


}
