package com.sqx.modules.commodity.entity;

public class Req {
    private String appKey;
    private String appSecret;
    /**
     * 口令弹框logoURL
     */
    private String logo;

    /**
     * 口令弹框内容
     */
    private String text;

    /**
     * 口令跳转目标页
     */
    private String url;

    public Req() {
    }

    public Req(String appKey, String appSecret, String logo, String text, String url) {
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.logo = logo;
        this.text = text;
        this.url = url;
    }



    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
