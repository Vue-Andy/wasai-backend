package com.sqx.modules.commodity.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.sqx.common.utils.Result;
import com.sqx.modules.app.utils.HttpClient;
import com.sqx.modules.commodity.entity.Commodity;
import com.sqx.modules.commodity.entity.Req;
import com.sqx.modules.commodity.service.CommodityService;
import com.sqx.modules.common.entity.CommonInfo;
import com.sqx.modules.common.service.CommonInfoService;
import com.sqx.modules.helpTask.config.Config;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkTpwdCreateRequest;
import com.taobao.api.response.TbkTpwdCreateResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 房
 * \* Date: 2020/5/20
 * \* Time: 13:49
 * \*
 * \
 */
@RestController
@Api(value = "商品", tags = {"商品"})
@RequestMapping(value = "/commodity")
public class CommodityController {

    @Autowired
   private CommodityService commodityService;
    @Autowired
    private CommonInfoService commonInfoService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation("添加商品")
    @ResponseBody
    public Result save(@RequestBody Commodity commodity) {
        commodityService.saveBody(commodity);
        return Result.success();
    }

    @RequestMapping(value = "/saveList", method = RequestMethod.POST)
    @ApiOperation("添加商品多个商品")
    @ResponseBody
    public Result saveList(String commoditys) {
        List<Commodity> list= JSONArray.parseArray(commoditys,Commodity.class);
        commodityService.saveBodyAll(list);
        return Result.success();
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation("修改商品")
    @ResponseBody
    public Result update(@RequestBody Commodity commodity) {
        commodityService.update(commodity);
        return Result.success();
    }

    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    @ApiOperation("删除商品")
    @ResponseBody
    public Result deleteById(Long id) {
        commodityService.delete(id);
        return Result.success();
    }

    @RequestMapping(value = "/deleteByIdList", method = RequestMethod.POST)
    @ApiOperation("删除多个商品")
    @ResponseBody
    public Result deleteByIdList(String ids) {
        commodityService.deleteByIdList(ids);
        return Result.success();
    }

    @RequestMapping(value = "/selectById", method = RequestMethod.GET)
    @ApiOperation("根据id查看详细信息")
    @ResponseBody
    public Result selectById(Long id) {
        return Result.success().put("data",commodityService.selectById(id));
    }

    @RequestMapping(value = "/selectCommodityList", method = RequestMethod.GET)
    @ApiOperation("查看所有信息")
    @ResponseBody
    public Result selectCommodityList(int page,int limit) {
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        return Result.success().put("data",commodityService.selectCommodityList(map));
    }

    @RequestMapping(value = "/tbwd", method = RequestMethod.POST)
    @ApiOperation("用户端获取淘口令的api")
    @ResponseBody
    public String getTbwd(@RequestBody Req req1) {
        try {
            TaobaoClient client = new DefaultTaobaoClient("http://gw.api.taobao.com/router/rest", req1.getAppKey(), req1.getAppSecret());
            TbkTpwdCreateRequest req = new TbkTpwdCreateRequest();
            req.setText(req1.getText());
            req.setUrl(req1.getUrl());
            req.setLogo(req1.getLogo());
            TbkTpwdCreateResponse rsp = client.execute(req);
            return rsp.getBody();
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return "";
    }

    @RequestMapping(value = "/doTpwdCoverts", method = RequestMethod.GET)
    @ApiOperation("淘口令解析")
    @ResponseBody
    public String doTpwdCoverts(@RequestParam String tpwd) {
        try {
            CommonInfo three = commonInfoService.findOne(30);
            String pattern = "([\\p{Sc}])\\w{8,12}([\\p{Sc}])";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(tpwd);
            String group ="";
            if (m.find()) {
                group=m.group();
                System.out.println("match: " + group);
            }
            System.out.println("End");
            String apkey = three.getValue();
            String word = URLEncoder.encode(group, "utf-8");
            String url = Config.MIAOYOUQUN_URL+"/taoke/doTpwdCovert?apkey="+apkey+"&pid=mm_479070090_650650095_109264550298&content="+word+"&tbname=kloran";
            String s = HttpClient.doGet(url);
            if (JSON.parseObject(s).getString("code").equals("200")) {
                String data = JSON.parseObject(s).getString("data");
                return JSON.parseObject(data).getString("item_id");
            }else{
                return null;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

    }

}