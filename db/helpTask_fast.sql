/*
 Navicat Premium Data Transfer

 Source Server         : 省钱兄
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : cdb-k1cwi22q.cd.tencentcdb.com:10001
 Source Schema         : helpTask_shop

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 26/08/2021 13:28:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_at` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES (4, '2020-09-03 22:13:42', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358312292.png', '1', '/pages/discovery/list', '好物圈');
INSERT INTO `activity` VALUES (5, '2020-06-17 09:42:36', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358296803.png', '1', '/pages/index/list?title=超值大牌&type=8', '超值大牌');
INSERT INTO `activity` VALUES (6, '2020-06-17 09:42:22', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358282896.png', '1', '/pages/index/list?title=9.9包邮&type=2', '9.9包邮');
INSERT INTO `activity` VALUES (7, '2020-06-17 09:42:08', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358269302.png', '1', '/pages/index/tuiguang?cid=9', '母婴精选');
INSERT INTO `activity` VALUES (8, '2020-07-08 16:03:23', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358254867.png', '1', '/pages/index/list?title=30元精选&type=3', '30元精选');
INSERT INTO `activity` VALUES (9, '2020-07-08 16:02:32', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358240332.png', '1', '/pages/index/list?title=巨划算&type=4', '巨划算');
INSERT INTO `activity` VALUES (10, '2020-07-08 16:03:13', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358223123.png', '1', '/pages/hot/index', '热销榜');
INSERT INTO `activity` VALUES (11, '2020-09-03 22:11:20', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358205962.png', '1', '/pages/index/list?title=好物优选&type=6', '好物优选');
INSERT INTO `activity` VALUES (12, '2020-09-03 22:11:06', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592358145687.png', '1', '/pages/index/list?title=好物优选&type=7', '好物优选');
INSERT INTO `activity` VALUES (13, '2021-04-09 18:39:12', 'https://shegnqx.oss-cn-beijing.aliyuncs.com/20200806/3442204e30c4445f9a025f0ff547d920.png', '1', '/pages/index/list?title=好物优选&type=9', '好物优选');
INSERT INTO `activity` VALUES (14, '2020-06-17 10:30:31', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592361171417.png', '2', '/pages/index/tuiguang?cid=1', '女装精选-体验白菜价网购');
INSERT INTO `activity` VALUES (15, '2020-06-17 10:30:15', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592361155328.png', '2', '/pages/index/tuiguang?cid=10', '居家必备-超低价高质量');
INSERT INTO `activity` VALUES (16, '2020-09-03 22:12:11', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592361107789.png', '2', '/pages/index/tuiguang?cid=2', '男装专区-每日优选商品');
INSERT INTO `activity` VALUES (17, '2020-06-17 10:28:32', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592361052423.png', '2', '/pages/index/tuiguang?cid=4', '精选美妆-OhMyGod买它');
INSERT INTO `activity` VALUES (18, '2020-06-17 10:17:33', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592360392629.png', '2', '/pages/index/list?title=每日上新&type=1', '每日上新-售完为止-热卖好货数量有限');
INSERT INTO `activity` VALUES (19, '2020-06-17 10:17:15', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1592360375195.png', '2', '/pages/index/food?title=爆款美食&type=9', '爆款美食-网红美食-刷新超低折扣价');

-- ----------------------------
-- Table structure for activity_message_info
-- ----------------------------
DROP TABLE IF EXISTS `activity_message_info`;
CREATE TABLE `activity_message_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `create_at` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `image` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `is_see` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `send_state` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `send_time` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `state` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `title` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `url` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `type` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `platform` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `to_user_name` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `from_user_name` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `open_id` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `wx_biz_msg_crypt` text CHARACTER SET big5 COLLATE big5_chinese_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37319 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `consignee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '收货人',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '详细地址',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电话',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `is_default` int(11) NULL DEFAULT NULL COMMENT '是否默认地址',
  `provinces` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '省市区',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 127 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for adverti
-- ----------------------------
DROP TABLE IF EXISTS `adverti`;
CREATE TABLE `adverti`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片路径',
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '页面路由',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of adverti
-- ----------------------------
INSERT INTO `adverti` VALUES (1, '2021-08-14 17:46:02', 'https://sac.xianmxkj.com/img/20210814/15c7f30c6fb040c4a2832abf2decf045.png', '/pages/index/mian');

-- ----------------------------
-- Table structure for app
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `android_wgt_url` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `create_at` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `des` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `ios_wgt_url` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `method` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `version` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `wgt_url` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app
-- ----------------------------
INSERT INTO `app` VALUES (5, 'https://taskshop.xianmxkj.com/taskshop.apk', '2020-07-20 18:35:00', '升级', '', 'false', '1.0', 'https://taskshop.xianmxkj.com/taskshop.apk');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `classify_id` bigint(20) NULL DEFAULT NULL COMMENT '分类id',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '内容',
  `article_url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '地址',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '时间',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常 2隐藏',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for article_browsing_history
-- ----------------------------
DROP TABLE IF EXISTS `article_browsing_history`;
CREATE TABLE `article_browsing_history`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '浏览记录id',
  `article_id` bigint(20) NULL DEFAULT NULL COMMENT '文章id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_browsing_history
-- ----------------------------
INSERT INTO `article_browsing_history` VALUES (2, 8, '2020-08-14 17:50:13', 143);

-- ----------------------------
-- Table structure for article_classify
-- ----------------------------
DROP TABLE IF EXISTS `article_classify`;
CREATE TABLE `article_classify`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `classify_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分类名称',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_classify
-- ----------------------------
INSERT INTO `article_classify` VALUES (1, '精选单品', '2020-06-10 03:29:22', '精选单品', 0, 1);
INSERT INTO `article_classify` VALUES (2, '好货专场', '2020-06-10 03:29:34', '好货专场', 0, 1);
INSERT INTO `article_classify` VALUES (28, '免单购', '2020-08-13 14:22:03', '', 0, 1);
INSERT INTO `article_classify` VALUES (29, '最新热点', '2020-08-13 14:22:03', '', 0, 1);

-- ----------------------------
-- Table structure for article_collect
-- ----------------------------
DROP TABLE IF EXISTS `article_collect`;
CREATE TABLE `article_collect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '收藏id',
  `article_id` bigint(20) NULL DEFAULT NULL COMMENT '文章id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '时间',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for attention
-- ----------------------------
DROP TABLE IF EXISTS `attention`;
CREATE TABLE `attention`  (
  `attention_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '关注id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `by_user_id` int(11) NULL DEFAULT NULL COMMENT '被关注用户id',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`attention_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '创建时间',
  `image_url` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '图片地址',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常 2隐藏',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '跳转页面地址',
  `sort` int(11) NULL DEFAULT 1 COMMENT '顺序',
  `classify` int(11) NULL DEFAULT NULL COMMENT '分类 1banner图 2首页分类 3活动',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `describes` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES (2, '2021-08-17 18:10:40', 'https://taskshop.xianmxkj.com/img/20210816/41f27e6ca4f04689a9e695482aadc580.png', 1, '', 2, 1, 'banner2', 'banner2');
INSERT INTO `banner` VALUES (18, '2021-01-28 10:24:39', 'https://shegnqx.oss-cn-beijing.aliyuncs.com/20200903/ff3931c6710144dabf814360ff34b4f3.png', 1, '/pages/invitation/invitationUser', 1, 3, '活动', '');
INSERT INTO `banner` VALUES (22, '2020-09-03 18:37:15', 'https://shegnqx.oss-cn-beijing.aliyuncs.com/20200903/19da43bbb1dd4772b0d4b319bfc732cd.png', 1, '/pages/my/ranking', 1, 4, '精品1', '精品1');
INSERT INTO `banner` VALUES (26, '2020-09-03 18:37:15', 'https://shegnqx.oss-cn-beijing.aliyuncs.com/20200804/e718b0b65ac74e38a9f2d0b326ecc836.png', 1, '/pages/my/wallet', 1, 4, '精品2', '精品2');
INSERT INTO `banner` VALUES (40, '2021-06-30 17:44:59', 'https://renwu.xiansqx.com/img/20210630/67020c61b13d4b59ae268712b3f2b4a4.png', 1, '', 1, 5, '背景图', '给你说个做任务赚钱的工具叫省钱兄！我用好久了都赚了不少！点链接来跟我一起做任务赚钱吧');
INSERT INTO `banner` VALUES (54, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/dd6af39e22f84c8c9c0e1d4b66019833.png', 1, '/pages/index/classIfyItemTask?id=6', 1, 2, '投票', '投票');
INSERT INTO `banner` VALUES (55, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/235429311461440983b9301dd226eb16.png', 1, '/pages/index/classIfyItemTask?id=28', 1, 2, '问卷调查', '问卷调查');
INSERT INTO `banner` VALUES (56, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/c0b6b1b037ea4405a4071733ae9fa0df.png', 1, '/pages/index/classIfyItemTask?id=7', 1, 2, '注册', '注册');
INSERT INTO `banner` VALUES (57, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/e676d81d035e4b8681ee433aafaa2301.png', 1, '/pages/index/classIfyItemTask?id=11', 1, 2, '辅助相关', '辅助相关');
INSERT INTO `banner` VALUES (58, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/42cc42e9420744ff844f589eb03d4f98.png', 1, '/pages/index/classIfyItemTask?id=12', 1, 2, '评论专区', '评论专区');
INSERT INTO `banner` VALUES (64, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/ec68fe421c1848b4aa8603a7dc2e7922.png', 1, '/pages/index/classIfyItemTask?id=13', 1, 2, '转发专区', '转发专区');
INSERT INTO `banner` VALUES (65, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/38097b2edb9a4f9e87080475136b2de6.png', 1, '/pages/index/classIfyItemTask?id=14', 1, 2, '点赞专区', '点赞专区');
INSERT INTO `banner` VALUES (66, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/62ef47b349d84d43b87a8a3b743fcd30.png', 1, '/pages/index/classIfyItemTask?id=15', 1, 2, '下载专区', '下载专区');
INSERT INTO `banner` VALUES (67, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/a4678b525a6e4c95b36b0fd6e08343e3.png', 1, '/pages/index/classIfyItemTask?id=22', 1, 2, '关注专区', '关注专区');
INSERT INTO `banner` VALUES (68, '2021-08-02 13:48:27', 'https://renwu.xiansqx.com/img/20210802/efe6755b015a4e50b9a40fd9a6edc208.png', 1, '/pages/index/classIfyItemTask?id=23', 1, 2, '砍价专区', '砍价专区');

-- ----------------------------
-- Table structure for banners
-- ----------------------------
DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_at` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of banners
-- ----------------------------
INSERT INTO `banners` VALUES (1, '2021-04-22 01:29:37', 'http://shegnqx.oss-cn-beijing.aliyuncs.com/1594190920471.jpg', 'true', '/pages/index/tuiguang?cid=7');

-- ----------------------------
-- Table structure for cash_classify
-- ----------------------------
DROP TABLE IF EXISTS `cash_classify`;
CREATE TABLE `cash_classify`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '提现分类id',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '分类额度',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cash_classify
-- ----------------------------
INSERT INTO `cash_classify` VALUES (1, 10.00, NULL);
INSERT INTO `cash_classify` VALUES (2, 20.00, NULL);
INSERT INTO `cash_classify` VALUES (3, 50.00, NULL);
INSERT INTO `cash_classify` VALUES (4, 100.00, NULL);
INSERT INTO `cash_classify` VALUES (5, 150.00, NULL);
INSERT INTO `cash_classify` VALUES (6, 200.00, NULL);

-- ----------------------------
-- Table structure for cash_out
-- ----------------------------
DROP TABLE IF EXISTS `cash_out`;
CREATE TABLE `cash_out`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '申请提现id',
  `create_at` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '申请时间',
  `is_out` bit(1) NULL DEFAULT NULL COMMENT '是否转账',
  `money` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '提现金额',
  `out_at` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '转账时间',
  `relation_id` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '会员编号',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `zhifubao` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '支付宝账号',
  `zhifubao_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '支付宝姓名',
  `order_number` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '订单编号',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态 0待转账 1成功 -1退款',
  `refund` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原因',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_name`(`relation_id`, `user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 94 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `chat_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `store_head` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `store_id` bigint(20) NULL DEFAULT NULL,
  `store_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_head` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `store_count` int(11) NULL DEFAULT 0,
  `user_count` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`chat_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 348 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for chat_content
-- ----------------------------
DROP TABLE IF EXISTS `chat_content`;
CREATE TABLE `chat_content`  (
  `chat_content_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chat_id` bigint(20) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT 1,
  `store_id` bigint(20) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `send_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`chat_content_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1230 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for commodity
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `activityid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `coupon_condition` double NULL DEFAULT NULL,
  `couponendtime` int(11) NULL DEFAULT NULL,
  `couponexplain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `couponmoney` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `couponnum` int(11) NULL DEFAULT NULL,
  `couponreceive` int(11) NULL DEFAULT NULL,
  `couponreceive2` int(11) NULL DEFAULT NULL,
  `couponstarttime` int(11) NULL DEFAULT NULL,
  `couponsurplus` int(11) NULL DEFAULT NULL,
  `couponurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `cuntao` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `deposit` double NULL DEFAULT NULL,
  `deposit_deduct` double NULL DEFAULT NULL,
  `discount` double NULL DEFAULT NULL,
  `down_type` int(11) NULL DEFAULT NULL,
  `end_time` int(11) NULL DEFAULT NULL,
  `fqcat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `general_index` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `guide_article` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `is_brand` int(11) NULL DEFAULT NULL,
  `is_explosion` int(11) NULL DEFAULT NULL,
  `is_live` int(11) NULL DEFAULT NULL,
  `is_shipping` int(11) NULL DEFAULT NULL,
  `isquality` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itemdesc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itemendprice` double NULL DEFAULT NULL,
  `itemid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itempic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itempic_copy` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itemprice` double NULL DEFAULT NULL,
  `itemsale` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itemsale2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itemshorttitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `itemtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `me` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `online_users` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `original_article` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `original_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `planlink` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `presale_discount_fee_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `presale_end_time` int(11) NULL DEFAULT NULL,
  `presale_start_time` int(11) NULL DEFAULT NULL,
  `presale_tail_end_time` int(11) NULL DEFAULT NULL,
  `presale_tail_start_time` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `report_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `seller_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `sellernick` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `shopid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `shopname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `shoptype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `son_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `start_time` int(11) NULL DEFAULT NULL,
  `starttime` int(11) NULL DEFAULT NULL,
  `taobao_image` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `tkmoney` double NULL DEFAULT NULL,
  `tkrates` double NULL DEFAULT NULL,
  `tktype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `todaycouponreceive` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `todaysale` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `userid` int(11) NULL DEFAULT NULL,
  `videoid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 156 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity
-- ----------------------------

-- ----------------------------
-- Table structure for common_info
-- ----------------------------
DROP TABLE IF EXISTS `common_info`;
CREATE TABLE `common_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '配置id',
  `create_at` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '时间',
  `max` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `min` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '对应值',
  `condition_from` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '分类',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 203 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_info
-- ----------------------------
INSERT INTO `common_info` VALUES (1, '2021-08-18 10:19:32', NULL, '客服二维码', 1, 'https://taobao.xianmxkj.com/custom.jpg', 'xitong');
INSERT INTO `common_info` VALUES (2, '2021-05-06 16:21:19', NULL, '公众号二维码', 2, 'https://renwu.xiansqx.com', 'xitong');
INSERT INTO `common_info` VALUES (6, '2020-07-09 17:59:45', NULL, '微信公众号APPID', 5, '', 'weixin');
INSERT INTO `common_info` VALUES (7, '2020-02-25 19:31:57', NULL, '淘宝APPID', 6, '', 'taobao');
INSERT INTO `common_info` VALUES (8, '2020-02-25 19:32:06', NULL, '淘宝秘钥', 7, '', 'taobao');
INSERT INTO `common_info` VALUES (9, '2021-08-17 15:30:02', NULL, '短信服务商（1 开启腾讯云  2开启阿里云 3开启短信包）', 163, '3', 'duanxin');
INSERT INTO `common_info` VALUES (10, '2020-02-25 19:32:36', NULL, '淘宝PID', 9, '', 'taobao');
INSERT INTO `common_info` VALUES (11, '2020-02-25 19:32:45', NULL, '好单库key	', 10, '', 'taobao');
INSERT INTO `common_info` VALUES (12, '2020-08-10 15:06:55', NULL, '淘宝名', 11, '', 'taobao');
INSERT INTO `common_info` VALUES (16, '2020-02-25 20:43:38', NULL, '微信公众号秘钥', 21, '', 'weixin');
INSERT INTO `common_info` VALUES (20, '2021-02-25 22:46:18', NULL, '后台管理平台域名配置', 20, 'https://taskshopadmin.xianmxkj.com', 'xitong');
INSERT INTO `common_info` VALUES (21, '2021-05-06 16:21:12', NULL, 'h5域名配置', 19, 'https://taskshop.xianmxkj.com', 'xitong');
INSERT INTO `common_info` VALUES (23, '2021-08-18 10:22:06', NULL, '后台服务名称	', 12, '优享帮', 'xitong');
INSERT INTO `common_info` VALUES (27, '2020-03-09 20:46:10', NULL, '通用APP下载地址	', 25, 'https://www.pgyer.com/sqxx', 'xitong');
INSERT INTO `common_info` VALUES (32, '2020-03-28 00:46:19', NULL, '瞄有券秘钥	', 30, '', 'taobao');
INSERT INTO `common_info` VALUES (33, '2021-08-17 15:29:15', NULL, '腾讯云短信clientId	', 31, '', 'duanxin');
INSERT INTO `common_info` VALUES (34, '2021-08-17 15:29:18', NULL, '腾讯云短信clientSecret	', 32, '', 'duanxin');
INSERT INTO `common_info` VALUES (46, '2021-08-18 10:20:26', NULL, '客服微信号', 44, '13689362990', 'xitong');
INSERT INTO `common_info` VALUES (47, '2020-03-29 00:21', NULL, '微信小程序APPID', 45, '', 'weixin');
INSERT INTO `common_info` VALUES (48, '2020-03-29 00:21', NULL, '微信小程序秘钥', 46, '', 'weixin');
INSERT INTO `common_info` VALUES (51, '2020-11-03 15:33:38', NULL, '分享安卓下载地址', 49, 'https://www.pgyer.com/hb2v', 'xitong');
INSERT INTO `common_info` VALUES (52, '2020-03-29 00:21:40', NULL, '分享苹果下载地址', 50, 'https://www.pgyer.com/c11o', 'xitong');
INSERT INTO `common_info` VALUES (58, '2020-03-29 00:21:40', NULL, '开启微信登录', 53, '是', 'xitong');
INSERT INTO `common_info` VALUES (69, '2020-09-23 15:53:12', NULL, 'APP消息推送PushAppKey', 60, 'uW0zDpss4H9YBco3dgOUM8', 'push');
INSERT INTO `common_info` VALUES (70, '2020-09-23 15:53:24', NULL, 'APP消息推送PushAppId', 61, 'ciYgxyP9Xb95ig2yowsIF6', 'push');
INSERT INTO `common_info` VALUES (71, '2020-09-23 15:53:30', NULL, 'APP消息推送PushMasterSecret', 62, '7mjw0QAlTD8s7TJWfAuCTA', 'push');
INSERT INTO `common_info` VALUES (72, '2020-06-04 16:34', NULL, '企业支付宝APPID', 63, '', 'zhifubao');
INSERT INTO `common_info` VALUES (73, '2020-06-04 16:34', NULL, '企业支付宝公钥', 64, '', 'zhifubao');
INSERT INTO `common_info` VALUES (74, '2020-06-04 16:34', NULL, '企业支付宝商户秘钥', 65, '', 'zhifubao');
INSERT INTO `common_info` VALUES (75, '2020-07-13 15:17', NULL, '文件上传阿里云Endpoint', 68, 'http://oss-cn-beijing.aliyuncs.com', 'oss');
INSERT INTO `common_info` VALUES (76, '2021-08-17 18:33:23', NULL, '文件上传阿里云账号accessKeyId', 69, '', 'oss');
INSERT INTO `common_info` VALUES (77, '2021-08-17 18:33:17', NULL, '文件上传阿里云账号accessKeySecret', 70, '', 'oss');
INSERT INTO `common_info` VALUES (80, '2021-08-17 18:33:11', NULL, '文件上传阿里云Bucket名称', 71, '', 'oss');
INSERT INTO `common_info` VALUES (81, '2021-08-17 18:33:01', NULL, '文件上传阿里云Bucket域名', 72, '', 'oss');
INSERT INTO `common_info` VALUES (83, '2020-07-27 15:1', NULL, '微信AppappId', 74, '', 'weixin');
INSERT INTO `common_info` VALUES (84, '2020-07-27 15:1', NULL, '微信商户key', 75, '', 'weixind');
INSERT INTO `common_info` VALUES (85, '2020-07-27 15:1', NULL, '微信商户号mchId', 76, '', 'weixind');
INSERT INTO `common_info` VALUES (89, '2020-10-28 16:57:07', NULL, '邀请好友赏金', 80, '10', 'renwu');
INSERT INTO `common_info` VALUES (90, '2020-08-10 15:05:01', NULL, '首页轮播图', 81, '是', 'shouye');
INSERT INTO `common_info` VALUES (91, '2020-08-03 14:43:00', NULL, '首页推荐分类', 82, '是', 'shouye');
INSERT INTO `common_info` VALUES (92, '2020-08-03 14:43:08', NULL, '首页活动', 83, '是', 'shouye');
INSERT INTO `common_info` VALUES (93, '2020-08-03 14:43:13', NULL, '首页精品服务', 84, '是', 'shouye');
INSERT INTO `common_info` VALUES (94, '2020-08-03 14:43:17', NULL, '首页轮播消息', 85, '是', 'shouye');
INSERT INTO `common_info` VALUES (95, '2020-08-03 14:43:21', NULL, '首页推荐任务', 86, '是', 'shouye');
INSERT INTO `common_info` VALUES (96, '2021-04-10 14:26:22', NULL, '提现最低额度', 87, '10', 'xitong');
INSERT INTO `common_info` VALUES (100, '2021-02-24 18:46:57', NULL, '官方邀请码', 88, '666666', 'xitong');
INSERT INTO `common_info` VALUES (102, '2021-08-17 15:29:22', NULL, '阿里云登陆或注册模板code（开启阿里云短信必须配置）', 90, '', 'duanxin');
INSERT INTO `common_info` VALUES (103, '2021-08-17 15:29:24', NULL, '阿里云找回密码模板code（开启阿里云短信必须配置）', 91, '', 'duanxin');
INSERT INTO `common_info` VALUES (104, '2021-08-17 15:30:05', NULL, '阿里云绑定手机号模板code（开启阿里云短信必须配置）', 92, '', 'duanxin');
INSERT INTO `common_info` VALUES (105, '2021-08-17 15:30:07', NULL, '阿里云短信accessKeyId', 93, '', 'duanxin');
INSERT INTO `common_info` VALUES (106, '2021-08-17 15:30:10', NULL, '阿里云短信accessSecret', 94, '', 'duanxin');
INSERT INTO `common_info` VALUES (109, '2020-07-27 15:17', NULL, '支付方式 1企业 2省钱兄支付平台', 97, '1', 'xitongs');
INSERT INTO `common_info` VALUES (110, '2020-10-28 12:22:40', NULL, '转账方式 1企业转账 2省钱兄支付平台 3手动转账', 98, '1', 'xitong');
INSERT INTO `common_info` VALUES (111, '2021-02-11 23:33:53', NULL, '省钱兄支付平台秘钥（转账方式开启省钱兄平台转账后启用）', 99, '', 'xitong');
INSERT INTO `common_info` VALUES (112, '2020-11-17 16:03:32', NULL, '首次签到积分', 100, '2', 'xitong');
INSERT INTO `common_info` VALUES (113, '2020-07-27 15:17', NULL, '累计签到每日叠加', 101, '3', 'xitong');
INSERT INTO `common_info` VALUES (114, '2020-09-23 18:04:18', NULL, '积分兑换比例(填写1元需要多少积分兑换数量)', 102, '1', 'xitong');
INSERT INTO `common_info` VALUES (115, '2020-10-28 15:50:25', NULL, '每日任务免费刷新次数', 103, '2', 'renwu');
INSERT INTO `common_info` VALUES (116, '2020-07-27 15:17', NULL, '任务置顶每小时价格', 104, '2', 'renwu');
INSERT INTO `common_info` VALUES (117, '2020-07-27 15:17', NULL, '任务刷新价格', 105, '3', 'renwu');
INSERT INTO `common_info` VALUES (118, '2020-07-27 15:17', NULL, '任务秒审核单价', 106, '5', 'renwu');
INSERT INTO `common_info` VALUES (119, '2021-04-12 16:53:21', NULL, '非会员每日可领取任务', 107, '5', 'renwu');
INSERT INTO `common_info` VALUES (120, '2021-03-18 13:05:02', NULL, '初级会员每日可领取任务', 108, '20', 'renwu');
INSERT INTO `common_info` VALUES (121, '2021-03-18 13:05:05', NULL, '中级会员每日可领取任务', 109, '30', 'renwu');
INSERT INTO `common_info` VALUES (122, '2021-03-18 13:05:07', NULL, '高级会员每日可领取任务', 110, '100', 'renwu');
INSERT INTO `common_info` VALUES (123, '2020-07-27 15:17', NULL, '是否开启签到积分', 111, '是', 'xitong');
INSERT INTO `common_info` VALUES (124, '2020-10-31 01:48:35', NULL, '任务最低单价', 112, '0.32', 'renwu');
INSERT INTO `common_info` VALUES (125, '2020-11-04 10:31:40', NULL, '任务最低数量', 113, '20', 'renwu');
INSERT INTO `common_info` VALUES (126, '2020-11-04 10:31:40', NULL, '提现手续费', 114, '0.01', 'xitong');
INSERT INTO `common_info` VALUES (127, '2020-11-04 10:31:40', NULL, 'Android APP 下载是否浏览器打开', 115, '否', 'xitong');
INSERT INTO `common_info` VALUES (128, '2020-11-04 10:31:40', NULL, '苹果 APP 下载是否浏览器打开', 116, '否', 'xitong');
INSERT INTO `common_info` VALUES (129, '2020-11-13 13:06:39', NULL, '下载邀请描述', 117, '悬赏、互助、兼职平台，帮人忙赚赏金！提现秒到账', 'renwu');
INSERT INTO `common_info` VALUES (130, '2021-01-11 13:13:39', NULL, '任务分享文案（文案中{code}为分享口令，{title}为任务标题，{money}为价格，可随意放置）', 118, '6.0付致文本 ￥{code}￥da開网业\r【\r@拜托帮我一下！\r@拜托帮我一下！\r@拜托帮我一下！\r【重要事情说3遍】\r@复制这条消息到【省钱兄任务平台】------#我正在做：{title}，最高可拿{money}----------- \r#你也来看看吧！\r-----------------】', 'renwu');
INSERT INTO `common_info` VALUES (131, '2020-11-13 13:06:39', NULL, '任务口令正则', 119, '((?=.*[a-zA-Z])[0-9a-zA-Z]{10})', 'renwu');
INSERT INTO `common_info` VALUES (182, '2021-08-17 16:03:21', NULL, '短信宝用户名', 164, 'youxb', 'duanxin');
INSERT INTO `common_info` VALUES (183, '2021-08-17 16:03:26', NULL, '短信宝密码', 165, 'youxb@2021', 'duanxin');
INSERT INTO `common_info` VALUES (185, '2020-07-27 15:17', NULL, '支付宝支付方式 1官方 2云购os', 167, '1', 'zhifubao');
INSERT INTO `common_info` VALUES (186, '2021-08-17 15:30:19', NULL, '云购支付宝商户号', 168, '', 'zhifubao');
INSERT INTO `common_info` VALUES (187, '2021-08-17 15:30:24', NULL, '云购支付宝秘钥', 169, '', 'zhifubao');
INSERT INTO `common_info` VALUES (188, '2020-07-27 15:17', NULL, '云购商户Id', 170, '', 'zhifubao');
INSERT INTO `common_info` VALUES (189, '2020-07-27 15:17', NULL, '云购商户秘钥', 171, '', 'zhifubao');
INSERT INTO `common_info` VALUES (190, '2021-07-31 14:55:50', NULL, '充值方式 1官方 2个人', 172, '1', 'zhifubao');
INSERT INTO `common_info` VALUES (191, '2021-08-17 15:30:29', NULL, '个人微信收款码', 173, '', 'zhifubao');
INSERT INTO `common_info` VALUES (192, '2021-08-17 15:30:33', NULL, '个人支付宝收款码', 174, '', 'zhifubao');
INSERT INTO `common_info` VALUES (200, '2021-07-19 11:32:12', NULL, '发布协议', 175, '发布协议。。发布发布', 'xieyi');
INSERT INTO `common_info` VALUES (201, '2021-08-14 19:10:04', NULL, '隐私政策', 176, '<p>本应用尊重并保护所有使用服务用户的个人隐私权。为了给您提供更准确、更有个性化的服务，本应用会按照本隐私权政策的规定使用和披露您的个人信息。但本应用将以高度的勤勉、审慎义务对待这些信息。除本隐私权政策另有规定外，在未征得您事先许可的情况下，本应用不会将这些信息对外披露或向第三方提供。本应用会不时更新本隐私权政策。 您在同意本应用服务使用协议之时，即视为您已经同意本隐私权政策全部内容。本隐私权政策属于本应用服务使用协议不可分割的一部分。</p><p>1. 适用范围</p><p>(a) 在您注册本应用帐号时，您根据本应用要求提供的个人注册信息；</p><p>(b) 在您使用本应用网络服务，或访问本应用平台网页时，本应用自动接收并记录的您的浏览器和计算机上的信息，包括但不限于您的IP地址、浏览器的类型、使用的语言、访问日期和时间、软硬件特征信息及您需求的网页记录等数据；</p><p>(c) 本应用通过合法途径从商业伙伴处取得的用户个人数据。</p><p>您了解并同意，以下信息不适用本隐私权政策：</p><p>(a) 您在使用本应用平台提供的搜索服务时输入的关键字信息；</p><p>(b) 本应用收集到的您在本应用发布的有关信息数据，包括但不限于参与活动、成交信息及评价详情；</p><p>(c) 违反法律规定或违反本应用规则行为及本应用已对您采取的措施。</p><p>2. 信息使用</p><p>(a)本应用不会向任何无关第三方提供、出售、出租、分享或交易您的个人信息，除非事先得到您的许可，或该第三方和本应用（含本应用关联公司）单独或共同为您提供服务，且在该服务结束后，其将被禁止访问包括其以前能够访问的所有这些资料。</p><p>(b) 本应用亦不允许任何第三方以任何手段收集、编辑、出售或者无偿传播您的个人信息。任何本应用平台用户如从事上述活动，一经发现，本应用有权立即终止与该用户的服务协议。</p><p>(c) 为服务用户的目的，本应用可能通过使用您的个人信息，向您提供您感兴趣的信息，包括但不限于向您发出产品和服务信息，或者与本应用合作伙伴共享信息以便他们向您发送有关其产品和服务的信息（后者需要您的事先同意）。</p><p>3. 信息披露</p><p>在如下情况下，本应用将依据您的个人意愿或法律的规定全部或部分的披露您的个人信息：</p><p>(a) 经您事先同意，向第三方披露；</p><p>(b)为提供您所要求的产品和服务，而必须和第三方分享您的个人信息；</p><p>(c) 根据法律的有关规定，或者行政或司法机构的要求，向第三方或者行政、司法机构披露；</p><p>(d) 如您出现违反中国有关法律、法规或者本应用服务协议或相关规则的情况，需要向第三方披露；</p><p>(e) 如您是适格的知识产权投诉人并已提起投诉，应被投诉人要求，向被投诉人披露，以便双方处理可能的权利纠纷；</p><p>(f) 在本应用平台上创建的某一交易中，如交易任何一方履行或部分履行了交易义务并提出信息披露请求的，本应用有权决定向该用户提供其交易对方的联络方式等必要信息，以促成交易的完成或纠纷的解决。</p><p>(g) 其它本应用根据法律、法规或者网站政策认为合适的披露。</p><p>4. 信息存储和交换</p><p>本应用收集的有关您的信息和资料将保存在本应用及（或）其关联公司的服务器上，这些信息和资料可能传送至您所在国家、地区或本应用收集信息和资料所在地的境外并在境外被访问、存储和展示。</p><p>5. Cookie的使用</p><p>(a) 在您未拒绝接受cookies的情况下，本应用会在您的计算机上设定或取用cookies ，以便您能登录或使用依赖于cookies的本应用平台服务或功能。本应用使用cookies可为您提供更加周到的个性化服务，包括推广服务。</p><p>(b) 您有权选择接受或拒绝接受cookies。您可以通过修改浏览器设置的方式拒绝接受cookies。但如果您选择拒绝接受cookies，则您可能无法登录或使用依赖于cookies的本应用网络服务或功能。</p><p>(c) 通过本应用所设cookies所取得的有关信息，将适用本政策。</p><p>6. 信息安全</p><p>(a) 本应用帐号均有安全保护功能，请妥善保管您的用户名及密码信息。本应用将通过对用户密码进行加密等安全措施确保您的信息不丢失，不被滥用和变造。尽管有前述安全措施，但同时也请您注意在信息网络上不存在“完善的安全措施”。</p><p>(b) 在使用本应用网络服务进行网上交易时，您不可避免的要向交易对方或潜在的交易对方披露自己的个人信息，如联络方式或者邮政地址。请您妥善保护自己的个人信息，仅在必要的情形下向他人提供。如您发现自己的个人信息泄密，尤其是本应用用户名及密码发生泄露，请您立即联络本应用客服，以便本应用采取相应措施。</p><p>7.本隐私政策的更改</p><p>(a)如果决定更改隐私政策，我们会在本政策中、本公司网站中以及我们认为适当的位置发布这些更改，以便您了解我们如何收集、使用您的个人信息，哪些人可以访问这些信息，以及在什么情况下我们会透露这些信息。</p><p>(b)本公司保留随时修改本政策的权利，因此请经常查看。如对本政策作出重大更改，本公司会通过网站通知的形式告知。</p>', 'xieyi');
INSERT INTO `common_info` VALUES (202, '2021-08-14 19:09:54', NULL, '注册协议', 177, '<p>尊敬的用户您好：在您使用本服务之前，请您认真阅读本用户协议，更好的了解我们所提供的服务以及您享有的权利义务。您开始使用时，即表示您已经了解并确认接受了本文件中的全部条款，包括我们对本服务条款随时做的任何修改。</p><p>一、协议的效力</p><p>本协议内容包括协议正文及所有已经发布或将来可能发布的各类规则。所有规则为本协议不可分割的组成部分，与协议正文具有同等法律效力。您承诺接受并遵守本协议的约定。如果您不同意本协议的约定，您应立即停止使用本平台服务。</p><p>二、用户行为规范</p><p>用户同意将不会利用本服务进行任何违法或不正当的活动，包括但不限于下列行为∶</p><p>发布或以其它方式传送含有下列内容之一的信息：</p><p>反对宪法所确定的基本原则的；</p><p>危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；</p><p>损害国家荣誉和利益的；</p><p>煽动民族仇恨、民族歧视、破坏民族团结的；</p><p>破坏国家宗教政策，宣扬邪教和封建迷信的；</p><p>散布谣言，扰乱社会秩序，破坏社会稳定的；</p><p>散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；</p><p>侮辱或者诽谤他人，侵害他人合法权利的；</p><p>含有虚假、诈骗、有害、胁迫、侵害他人隐私、骚扰、侵害、中伤、粗俗、猥亵、或其它道德上令人反感的内容；</p><p>含有当地法律、法规、规章、条例以及任何具有法律效力之规范所限制或禁止的其它内容的；</p><p>含有不适合在本平台展示的内容；</p><p>以任何方式危害他人的合法权益；</p><p>冒充其他任何人或机构，或以虚伪不实的方式陈述或谎称与任何人或机构有关；</p><p>将依据任何法律或合约或法定关系（例如由于雇佣关系和依据保密合约所得知或揭露之内部资料、专属及机密资料）知悉但无权传送之任何内容加以发布、发送电子邮件或以其它方式传送；</p><p>将侵害他人著作权、专利权、商标权、商业秘密、或其它专属权利（以下简称“专属权利”）之内容加以发布或以其它方式传送；</p><p>将任何广告信函、促销资料、“垃圾邮件”、““滥发信件”、“连锁信件”、“直销”或其它任何形式的劝诱资料加以发布、发送或以其它方式传送；</p><p>将设计目的在于干扰、破坏或限制任何计算机软件、硬件或通讯设备功能之计算机病毒（包括但不限于木马程序（trojan horses）、蠕虫（worms）、定时炸弹、删除蝇（cancelbots）（以下简称“病毒”）或其它计算机代码、档案和程序之任何资料，加以发布、发送或以其它方式传送；</p><p>干扰或破坏本服务或与本服务相连线之服务器和网络，或违反任何关于本服务连线网络之规定、程序、政策或规范；</p><p>跟踪、人肉搜索或以其它方式骚扰他人；</p><p>故意或非故意地违反任何适用的当地、国家法律，以及任何具有法律效力的规则；</p><p>未经合法授权而截获、篡改、收集、储存或删除他人个人信息、站内邮件或其它数据资料，或将获知的此类资料用于任何非法或不正当目的。</p><p>三、知识产权</p><p>本平台所有设计图样以及其他图样、产品及服务名称。任何人不得使用、复制或用作其他用途。未经我们许可，任何单位和个人不得私自复制、传播、展示、镜像、上载、下载、使用，或者从事任何其他侵犯我们知识产权的行为。否则，我们将追究相关法律责任。</p><p>我们鼓励用户充分利用平台自由地张贴和共享自己的信息，但这些内容必须位于公共领域内，或者用户拥有这些内容的使用权。同时，用户对于其创作并在本平台上发布的合法内容依法享有著作权及其相关权利。</p><p>四、免责声明</p><p>互联网是一个开放平台，用户将照片等个人资料上传到互联网上，有可能会被其他组织或个人复制、转载、擅改或做其它非法用途，用户必须充分意识此类风险的存在。用户明确同意其使用本服务所存在的风险将完全由其自己承担；因其使用本服务而产生的一切后果也由其自己承担，我们对用户不承担任何责任。</p><p>对于用户上传的照片、资料、证件等，已采用相关措施并已尽合理努力进行审核，但不保证其内容的正确性、合法性或可靠性，相关责任由上传上述内容的会员负责。</p><p>尽管已采取相应的技术保障措施 ，但用户仍有可能收到各类的广告信或其他不以招聘/应聘为目的邮件或其它方式传送的任何内容，本平台不承担责任。</p><p>对于各种广告信息、链接、资讯等，不保证其内容的正确性、合法性或可靠性，相关责任由广告商承担；用户通过本服务与广告商进行任何形式的通讯或商业往来，或参与促销活动，包含相关商品或服务之付款及交付，以及达成的其它任何相关条款、条件、保证或声明，完全为用户与广告商之间之行为，与本平台无关。用户因前述任何交易或前述广告商而遭受的任何性质的损失或损害，本平台不承担任何责任。</p><p>本平台不保证其提供的服务一定能满足用户的要求和期望，也不保证服务不会中断，对服务的及时性、安全性、准确性也都不作保证。对于因不可抗力或无法控制的原因造成的网络服务中断或其他缺陷，不承担任何责任。我们不对用户所发布信息的删除或储存失败承担责任。我们有权判断用户的行为是否符合本网站使用协议条款之规定，如果我们认为用户违背了协议条款的规定，我们有终止向其提供服务的权利。</p><p>本平台保留变更、中断或终止部分网络服务的权利。保留根据实际情况随时调整平台提供的服务种类、形式的权利。本平台不承担因业务调整给用户造成的损失。本平台仅提供相关服务，除此之外与本服务有关的设备（如电脑、调制解调器及其他与接入互联网有关的装置）及所需的费用（如为接入互联网而支付的电话费及上网费）均应由用户自行负担。</p>', 'xieyi');
INSERT INTO `common_info` VALUES (234, '2020-11-04 10:31:40', NULL, '上传方式 1阿里云oss  2本地', 234, '2', 'oss');

-- ----------------------------
-- Table structure for coupons
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '优惠券id',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `end_day` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '结束时间',
  `get_number` int(11) NULL DEFAULT NULL COMMENT '限领数量',
  `instructions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '使用说明',
  `put_number` int(11) NULL DEFAULT NULL COMMENT '发放数量',
  `remaining_number` int(11) NULL DEFAULT NULL COMMENT '剩余数量',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态(0正常、1已过期)',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `coupons_type` int(11) NULL DEFAULT NULL COMMENT '优惠券类型（1立减）',
  `start_day` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupons
-- ----------------------------

-- ----------------------------
-- Table structure for coupons_user
-- ----------------------------
DROP TABLE IF EXISTS `coupons_user`;
CREATE TABLE `coupons_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '优惠券用户id',
  `coupons_id` bigint(20) NULL DEFAULT NULL COMMENT '优惠券id',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `end_day` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '结束时间',
  `instructions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '使用说明',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态0正常、1已过期',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupons_user
-- ----------------------------

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `goods_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `goods_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品标题',
  `longitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '纬度',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `title_img` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `img` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详情图',
  `sum_num` int(11) NULL DEFAULT NULL COMMENT '总数量',
  `end_num` int(11) NULL DEFAULT NULL COMMENT '剩余数量',
  `goods_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品价格',
  `member_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '会员满',
  `member_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '会员返',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '普通用户满',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '普通用户返',
  `start_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束时间',
  `classify` int(11) NULL DEFAULT NULL COMMENT '分类 1 饿了么  2美团',
  `type_id` int(11) NULL DEFAULT NULL COMMENT '类型',
  `num_star` int(11) NULL DEFAULT NULL COMMENT '几颗星',
  `num_img` int(11) NULL DEFAULT NULL COMMENT '几张图',
  `num_word` int(11) NULL DEFAULT NULL COMMENT '多少字',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注  富文本',
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '小程序跳转参数',
  `status` int(11) NULL DEFAULT NULL COMMENT '1 上架 2下架',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区',
  `activity_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '金刚区分类',
  `is_goods` int(11) NULL DEFAULT NULL COMMENT '是否是精选商品  0否 1是',
  `scope` int(11) NULL DEFAULT NULL COMMENT '范围',
  `member_privilege` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员权益',
  `privilege` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '普通用户权益',
  `describes` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '门店介绍',
  `shop_describe` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商家介绍',
  `phone` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 180 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for goods_type
-- ----------------------------
DROP TABLE IF EXISTS `goods_type`;
CREATE TABLE `goods_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品类型id',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '类型名称',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '上级类型id',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_type
-- ----------------------------
INSERT INTO `goods_type` VALUES (10, '2021-06-25 10:21:46', '男装', 'https://www.gomyorder.cn/img/20200910/a04b841a1e474c16b589b8e2d3b7843e.png', 0, 1);
INSERT INTO `goods_type` VALUES (12, '2020-09-14 10:51:10', '女装', 'https://www.gomyorder.cn/img/20200910/5a75277d953b4da7a0f89939cc111e64.jpg', 0, 2);
INSERT INTO `goods_type` VALUES (13, '2020-09-14 10:50:59', '衬衫', 'https://www.gomyorder.cn/img/20200910/996b1adeac924abc99404a01e45cb091.jpg', 10, 1);
INSERT INTO `goods_type` VALUES (14, '2020-09-10 14:44:00', '袜子', 'https://www.gomyorder.cn/img/20200910/4c8d907c6d6946c38efbfe786e29ef06.png', 10, 2);
INSERT INTO `goods_type` VALUES (20, '2020-09-14 10:51:17', '童装', 'https://www.gomyorder.cn/img/20200910/135b92402aaa45de882fcb24ccf6aade.png', 0, 3);
INSERT INTO `goods_type` VALUES (21, '2020-09-10 14:45:24', '鞋子', 'https://www.gomyorder.cn/img/20200910/dc1fd03916e743f0a3adce976df05fd2.png', 20, 1);
INSERT INTO `goods_type` VALUES (23, '2020-09-14 10:51:24', '大家电', 'https://www.gomyorder.cn/img/20200910/ff31a115b22f4101a43fee026ce3335e.png', 0, 4);
INSERT INTO `goods_type` VALUES (29, '2020-09-14 14:12:29', '裤子', 'https://www.gomyorder.cn/img/20200914/b4b2551e0dc044be8d4ec5b0141eade7.png', 12, 1);
INSERT INTO `goods_type` VALUES (30, '2020-09-14 14:13:31', '上衣', 'https://www.gomyorder.cn/img/20200914/e14f254f3d04429995744407c03c4d2e.png', 12, 2);
INSERT INTO `goods_type` VALUES (31, '2020-11-30 11:05:10', '瓜果蔬菜', 'https://www.gomyorder.cn/img/20201130/2204b0202e1d4cde8dfdb1839a98c7de.png', 0, 5);
INSERT INTO `goods_type` VALUES (32, '2020-11-30 11:07:05', '苹果', 'https://www.gomyorder.cn/img/20201130/e1286331a4164c8ab7407e60b4fb5f43.png', 31, 5);
INSERT INTO `goods_type` VALUES (33, '2020-11-30 11:08:25', '电视', 'https://www.gomyorder.cn/img/20201130/5a0ff54a5b5745a3b31b1a22f53cc965.png', 23, 5);
INSERT INTO `goods_type` VALUES (34, '2020-11-30 11:10:45', '电子产品', 'https://www.gomyorder.cn/img/20201130/5db7ed61748d4383ae38b7469fce01ac.png', 23, 5);
INSERT INTO `goods_type` VALUES (42, '2021-04-07 14:11:55', '坚果', 'https://tk.gomyorder.cn/img/20210407/d418f717a76b4683bd911583bdab89f8.jpg', 0, 6);
INSERT INTO `goods_type` VALUES (43, '2021-04-07 14:12:51', '麻花', 'https://tk.gomyorder.cn/img/20210407/056a1d7ba629457d88a875788ad008de.jpg', 42, 12);
INSERT INTO `goods_type` VALUES (44, '2021-04-19 21:21:19', '测试', '', 23, 1);
INSERT INTO `goods_type` VALUES (46, '2021-08-10 18:01:59', '衣服', 'https://tk.gomyorder.cn/img/20210628/9697c4f2ba3d4a878c16cfbdf664e470.png', 0, 0);
INSERT INTO `goods_type` VALUES (48, '2021-06-28 15:27:42', '衬衫', 'https://tk.gomyorder.cn/img/20210628/3149b3330050452caa2259b8aa98e434.png', 46, 1);
INSERT INTO `goods_type` VALUES (49, '2021-06-28 18:31:17', '补贴发放付', 'https://tk.gomyorder.cn/img/20210628/18db58417212446687b21f0a6e30803b.png', 0, 7);
INSERT INTO `goods_type` VALUES (51, '2021-08-10 17:53:28', '书籍类', 'https://audi.xianmxkj.com/img/20210810/b7e259e4fa7d4fd793f34526a58f02a7.jpg', 0, 1);
INSERT INTO `goods_type` VALUES (52, '2021-08-10 17:53:11', '小孩书籍', 'https://sac.xianmxkj.com/img/20210810/e4ce41a616da4f1daca94fdbed6f42ad.jpg', 51, 1);
INSERT INTO `goods_type` VALUES (53, '2021-08-10 18:03:54', '大孩子书籍', 'https://sac.xianmxkj.com/img/20210810/b7f9db9314d44a07a5cdb37ac7533876.jpg', 51, 1);

-- ----------------------------
-- Table structure for help_browsing_history
-- ----------------------------
DROP TABLE IF EXISTS `help_browsing_history`;
CREATE TABLE `help_browsing_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '浏览记录id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '助力id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1643 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '浏览记录' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_classify
-- ----------------------------
DROP TABLE IF EXISTS `help_classify`;
CREATE TABLE `help_classify`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '助力任务分类id',
  `classify_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分类名称',
  `classify_icon` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分类图标 ',
  `classify_url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '地址',
  `describes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
  `sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '排序',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态 1正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of help_classify
-- ----------------------------
INSERT INTO `help_classify` VALUES (8, '投票', 'https://renwu.xiansqx.com/img/20210731/05ebd7e1e38647768666e410674305f7.png', 'http://www.tianmao.com', '投票', '10', -1);
INSERT INTO `help_classify` VALUES (9, '注册', 'https://renwu.xiansqx.com/img/20210731/a7381def3afe49ee9c5d2de7164b30a3.png', 'http://www.jd.com', '注册', '10', -1);
INSERT INTO `help_classify` VALUES (13, '辅助相关', 'https://renwu.xiansqx.com/img/20210731/5286e475e05940baa4c187390092e064.png', 'http://www.douyin.com', '辅助相关', '10', -1);
INSERT INTO `help_classify` VALUES (14, '评论专区', 'https://renwu.xiansqx.com/img/20210731/8051d6d29c594c95b6363cf0524573cb.png', 'http://www.meituan.com', '评论专区', '10', -1);
INSERT INTO `help_classify` VALUES (15, '转发专区', 'https://renwu.xiansqx.com/img/20210731/1acbab5e9cc244e7a89fa113725ca7c9.png', 'http://www.elm.com', '转发专区', '10', -1);
INSERT INTO `help_classify` VALUES (16, '点赞专区', 'https://renwu.xiansqx.com/img/20210731/3ef31c9acbc34fee93b603a2b82db85b.png', 'http://kuaishou.com', '点赞专区', '10', -1);
INSERT INTO `help_classify` VALUES (18, '下载专区', 'https://renwu.xiansqx.com/img/20210731/9b80301f9e5142939c6254d2bc49bdff.png', '', '下载专区', '10', 1);
INSERT INTO `help_classify` VALUES (19, '关注专区', 'https://renwu.xiansqx.com/img/20210731/2f0627da748b48218198b88f6f43fadc.png', '', '关注专区', '10', -1);
INSERT INTO `help_classify` VALUES (20, '砍价专区', 'https://renwu.xiansqx.com/img/20210731/4260842b6c3d48d4b66e9b48b7e05ce3.png', '', '砍价专区', '10', -1);

-- ----------------------------
-- Table structure for help_classify_details
-- ----------------------------
DROP TABLE IF EXISTS `help_classify_details`;
CREATE TABLE `help_classify_details`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '二级分类id',
  `classify_deatils_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '名称',
  `classify_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图标',
  `classify_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '地址',
  `classify_id` bigint(20) NULL DEFAULT NULL COMMENT '一级分类id',
  `describes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态  1正常',
  `task_num` int(11) NULL DEFAULT NULL COMMENT '任务最低数量',
  `task_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '任务最低单价',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of help_classify_details
-- ----------------------------
INSERT INTO `help_classify_details` VALUES (1, '惊喜', 'https://dss0.bdstatic.com/-0U0bnSm1A5BphGlnYG/tam-ogel/5d517fdc278e4238694717add5527c7e_222_222.jpg', '', 1, NULL, 10, 0, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (2, '惊喜2', 'https://dss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=644397205,1235585350&fm=179&app=42&f=JPEG?w=121&h=140&s=32D7A9764E734C8654E7A9FB02009039', '', 1, NULL, 10, 0, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (4, '惊喜32', 'https://dss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3690653756,2755071037&fm=26&gp=0.jpg', '', 2, NULL, 10, 0, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (6, '投票', 'https://renwu.xiansqx.com/img/20210731/d417c0b93a6b4a2cb69e107fc62dafe6.png', 'http://www.tianmao.com', 8, NULL, 10, -1, 1, 1.00, NULL);
INSERT INTO `help_classify_details` VALUES (7, '注册', 'https://renwu.xiansqx.com/img/20210731/aebf168893c5436ebe9fc0d91436d82f.png', 'http://www.jd.com', 9, NULL, 10, -1, 10, 1.00, NULL);
INSERT INTO `help_classify_details` VALUES (9, '2', 'https://shegnqx.oss-cn-beijing.aliyuncs.com/20200813/08a68791739a4fdc93f018dbc6f46d11.png', '2', 11, NULL, 10, -1, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (11, '辅助相关', 'https://renwu.xiansqx.com/img/20210731/c510f046df50422481eb5a1949b7e64e.png', 'http://www.douyin.com', 13, NULL, 10, -1, 10, 1.00, '辅助相关');
INSERT INTO `help_classify_details` VALUES (12, '评论专区', 'https://renwu.xiansqx.com/img/20210731/7f9f2b2ed44e4f558e997fbae8ee195b.png', 'http://www.meituan.com', 14, NULL, 10, -1, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (13, '转发专区', 'https://renwu.xiansqx.com/img/20210731/cf8519482d1c4b8eaffc2eb36f4420d0.png', 'http://www.elm.com', 15, NULL, 10, -1, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (14, '点赞专区', 'https://renwu.xiansqx.com/img/20210731/a01231ef5af741f9a04dac63617ac5ce.png', 'http://kuaishou.com', 16, NULL, 10, -1, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (16, '下载专区', 'https://renwu.xiansqx.com/img/20210731/9f07ef02030b48bab1e1d1d8172a6ab7.png', '', 18, NULL, 10, -1, NULL, NULL, NULL);
INSERT INTO `help_classify_details` VALUES (22, '关注专区', 'https://renwu.xiansqx.com/img/20210731/e3ce1bee30eb46f6aeb94c232ab5f450.png', '', 19, NULL, 10, -1, 5, 1.00, NULL);
INSERT INTO `help_classify_details` VALUES (23, 'app下载', 'http://renwu.xiansqx.com/img/20210119/d01417b5172545e1bb78ee1493f4cc8c.png', '', 20, NULL, 10, -1, 20, 1.00, NULL);
INSERT INTO `help_classify_details` VALUES (28, '问卷调查', 'https://renwu.xiansqx.com/img/20210731/99ae7b28e51346f0ab12bef1c4c6f6a9.png', '', 8, NULL, 10, -1, 20, 1.00, '问卷调查');
INSERT INTO `help_classify_details` VALUES (29, '4yrt', 'http://renwu.xiansqx.com/img/20210412/b66ad7447ab64a9f9ad7289597b68b3c.jpg', '', 21, NULL, 10, 2, 2, 2.00, '4yrt');

-- ----------------------------
-- Table structure for help_complaint
-- ----------------------------
DROP TABLE IF EXISTS `help_complaint`;
CREATE TABLE `help_complaint`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '任务id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投诉内容',
  `picture` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '截图',
  `state` int(255) NULL DEFAULT NULL COMMENT '状态 1待审核 2已审核',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务投诉' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_get_task
-- ----------------------------
DROP TABLE IF EXISTS `help_get_task`;
CREATE TABLE `help_get_task`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `help_task_platform_id` int(11) NULL DEFAULT NULL COMMENT '任务id',
  `picture` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '图片 多图逗号隔开',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `state` int(11) NULL DEFAULT 0 COMMENT '状态 0待提交 1待审核 2通过 3拒绝 4放弃',
  `audit_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '审核原因',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 80 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_lucky_details
-- ----------------------------
DROP TABLE IF EXISTS `help_lucky_details`;
CREATE TABLE `help_lucky_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '幸运值记录id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '任务id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `lucky_value` decimal(10, 2) NULL DEFAULT NULL COMMENT '获得幸运值',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '完成时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '幸运值获取记录详细信息' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_lucky_value
-- ----------------------------
DROP TABLE IF EXISTS `help_lucky_value`;
CREATE TABLE `help_lucky_value`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `lucky_value` decimal(10, 2) NULL DEFAULT NULL COMMENT '幸运值',
  `accruing_amounts` decimal(10, 2) NULL COMMENT '累计派送红包金额',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 131 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_maintain
-- ----------------------------
DROP TABLE IF EXISTS `help_maintain`;
CREATE TABLE `help_maintain`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '维权id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '助力id',
  `help_send_order_id` int(11) NULL DEFAULT NULL COMMENT '接单id',
  `help_take_order_id` int(11) NULL DEFAULT NULL COMMENT '派单id',
  `send_order_user_id` int(11) NULL DEFAULT NULL COMMENT '接单人id',
  `take_order_user_id` int(11) NULL DEFAULT NULL COMMENT '派单人id',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态（0待审核 1派单人胜 2接单人胜）',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '平台审核意见',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '维权详细信息' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_maintain_details
-- ----------------------------
DROP TABLE IF EXISTS `help_maintain_details`;
CREATE TABLE `help_maintain_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '维权详细信息id',
  `help_maintain_id` int(11) NULL DEFAULT NULL COMMENT '维权id',
  `classify` int(11) NULL DEFAULT NULL COMMENT '分类（1派单人发言2接单人发言）',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `picture` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_member
-- ----------------------------
DROP TABLE IF EXISTS `help_member`;
CREATE TABLE `help_member`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '查看范围id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '任务id',
  `member` int(11) NULL DEFAULT NULL COMMENT '查看范围等级  0全部 1非会员 2初级 3中级 4高级',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of help_member
-- ----------------------------

-- ----------------------------
-- Table structure for help_profit
-- ----------------------------
DROP TABLE IF EXISTS `help_profit`;
CREATE TABLE `help_profit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '收入id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '助力任务id',
  `help_send_order_id` int(11) NULL DEFAULT NULL COMMENT '接单id',
  `profit` decimal(10, 2) NULL DEFAULT NULL COMMENT '利润',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 252 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '助力任务收入明细' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_rate
-- ----------------------------
DROP TABLE IF EXISTS `help_rate`;
CREATE TABLE `help_rate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '平台抽成和费用id',
  `rate` decimal(10, 2) NULL DEFAULT NULL COMMENT '抽成比率或价格',
  `type` int(11) NULL DEFAULT NULL COMMENT '类别 1非会员 2初级会员 3中级会员 4高级会员 ',
  `classify` int(11) NULL DEFAULT NULL COMMENT '1平台扣费 2会员开通 3上级分红 4邀请人数 5直属分红 6非直属分红',
  `is_shop_task` int(11) NULL DEFAULT 1 COMMENT '1任务  2商城',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '平台抽成' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of help_rate
-- ----------------------------
INSERT INTO `help_rate` VALUES (1, 0.25, 1, 1, 1);
INSERT INTO `help_rate` VALUES (2, 0.20, 2, 1, 1);
INSERT INTO `help_rate` VALUES (3, 0.15, 3, 1, 1);
INSERT INTO `help_rate` VALUES (4, 0.10, 4, 1, 1);
INSERT INTO `help_rate` VALUES (5, 0.05, 5, 3, 1);
INSERT INTO `help_rate` VALUES (6, 100.00, 2, 2, 1);
INSERT INTO `help_rate` VALUES (7, 200.00, 3, 2, 1);
INSERT INTO `help_rate` VALUES (8, 300.00, 4, 2, 1);
INSERT INTO `help_rate` VALUES (9, 5.00, 2, 4, 1);
INSERT INTO `help_rate` VALUES (10, 10.00, 3, 4, 1);
INSERT INTO `help_rate` VALUES (11, 15.00, 4, 4, 1);
INSERT INTO `help_rate` VALUES (12, 0.03, 1, 5, 1);
INSERT INTO `help_rate` VALUES (13, 0.05, 2, 5, 1);
INSERT INTO `help_rate` VALUES (14, 0.08, 3, 5, 1);
INSERT INTO `help_rate` VALUES (15, 0.10, 4, 5, 1);
INSERT INTO `help_rate` VALUES (16, 0.01, 1, 6, 1);
INSERT INTO `help_rate` VALUES (17, 0.03, 2, 6, 1);
INSERT INTO `help_rate` VALUES (18, 0.05, 3, 6, 1);
INSERT INTO `help_rate` VALUES (19, 0.08, 4, 6, 1);
INSERT INTO `help_rate` VALUES (20, 0.05, 1, 1, 2);
INSERT INTO `help_rate` VALUES (21, 0.10, 2, 1, 2);
INSERT INTO `help_rate` VALUES (22, 0.15, 3, 1, 2);
INSERT INTO `help_rate` VALUES (23, 0.20, 4, 1, 2);
INSERT INTO `help_rate` VALUES (24, 0.02, 1, 5, 2);
INSERT INTO `help_rate` VALUES (25, 0.04, 2, 5, 2);
INSERT INTO `help_rate` VALUES (26, 0.06, 3, 5, 2);
INSERT INTO `help_rate` VALUES (27, 0.08, 4, 5, 2);
INSERT INTO `help_rate` VALUES (28, 0.01, 1, 6, 2);
INSERT INTO `help_rate` VALUES (29, 0.02, 2, 6, 2);
INSERT INTO `help_rate` VALUES (30, 0.04, 3, 6, 2);
INSERT INTO `help_rate` VALUES (31, 0.06, 4, 6, 2);

-- ----------------------------
-- Table structure for help_send_order
-- ----------------------------
DROP TABLE IF EXISTS `help_send_order`;
CREATE TABLE `help_send_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '接单id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '助力id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `order_form_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '接单id(显示在页面上)',
  `state` int(11) NULL DEFAULT 0 COMMENT '状态（0接单成功 1提交待审核 2审核成功 3拒绝 4维权 5放弃  6维权拒绝 7超时 ）',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接单对应昵称',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格（接单时收取的价格）',
  `audit_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '拒绝原因',
  `category` int(11) NULL DEFAULT NULL COMMENT '拒绝类型',
  `picture` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `audit_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '审核时间',
  `zhi_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '直属收入',
  `fei_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '非直属收入',
  `zhi_user_id` int(11) NULL DEFAULT NULL COMMENT '直属用户id',
  `fei_user_id` int(11) NULL DEFAULT NULL COMMENT '非直属用户id',
  `ping_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '平台收益',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 573 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '接单' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for help_send_order_details
-- ----------------------------
DROP TABLE IF EXISTS `help_send_order_details`;
CREATE TABLE `help_send_order_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '接单详细id',
  `help_send_order_id` int(11) NULL DEFAULT NULL COMMENT '接单id',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `sort` int(11) NULL DEFAULT 0 COMMENT '顺序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 275 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '接单详细信息' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_take_order
-- ----------------------------
DROP TABLE IF EXISTS `help_take_order`;
CREATE TABLE `help_take_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '助力id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `send_orders_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '派单id(页面显示)',
  `state` int(11) NULL DEFAULT 0 COMMENT '状态(0待审核 1审核成功 2拒绝 3结算完成)',
  `audit_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '拒绝原因',
  `category` int(11) NULL DEFAULT NULL COMMENT '拒绝类型',
  `picture` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `audit_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 252 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '发单' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_task
-- ----------------------------
DROP TABLE IF EXISTS `help_task`;
CREATE TABLE `help_task`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '助力id',
  `task_id` int(11) NULL DEFAULT NULL COMMENT '任务id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `task_original_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格（原价）',
  `task_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格（平台扣除费用后价格）',
  `task_num` int(11) NULL DEFAULT NULL COMMENT '数量',
  `end_num` int(11) NULL DEFAULT NULL COMMENT '剩余数量',
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `open_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打开方式',
  `open_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打开内容',
  `open_app` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打开app分类',
  `verify_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '验证信息',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `audit_time` bigint(11) NULL DEFAULT NULL COMMENT '审核时间(按秒)',
  `restrict_time` bigint(11) NULL DEFAULT NULL COMMENT '任务限时(按秒)',
  `end_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '截止时间',
  `state` int(11) NULL DEFAULT 0 COMMENT '状态（0待审核 1正常  2结束  3拒绝）',
  `classify_details_id` int(11) NULL DEFAULT NULL COMMENT '二级分类id',
  `is_top` int(11) NULL DEFAULT 0 COMMENT '是否置顶 0 否 1是',
  `order_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '排序时间',
  `classify` int(11) NULL DEFAULT NULL COMMENT '客户端分类 1不限 1限安卓  3限苹果',
  `tag` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标签',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `leave_word` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '留言',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 273 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_task_details
-- ----------------------------
DROP TABLE IF EXISTS `help_task_details`;
CREATE TABLE `help_task_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '步骤id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '助力id',
  `picture` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `is_verify` int(11) NULL DEFAULT 0 COMMENT '是否为验证图片（0否 1是）',
  `sort` int(255) NULL DEFAULT 1 COMMENT '顺序（1、2..）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 328 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '助力步骤' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_task_platform
-- ----------------------------
DROP TABLE IF EXISTS `help_task_platform`;
CREATE TABLE `help_task_platform`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `classify_id` int(11) NULL DEFAULT NULL COMMENT '分类id',
  `lucky_value` decimal(10, 2) NULL DEFAULT NULL COMMENT '幸运值',
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `title_picture` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题图片',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '内容',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常 2下架 ',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_task_refresh
-- ----------------------------
DROP TABLE IF EXISTS `help_task_refresh`;
CREATE TABLE `help_task_refresh`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务每日刷新id',
  `help_task_id` int(11) NULL DEFAULT NULL COMMENT '任务id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_user_score
-- ----------------------------
DROP TABLE IF EXISTS `help_user_score`;
CREATE TABLE `help_user_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `score` int(11) NULL DEFAULT 50 COMMENT '信用分数',
  `add_score` int(11) NULL DEFAULT NULL COMMENT '累计增加分数',
  `reduce_score` int(11) NULL DEFAULT NULL COMMENT '累计减少分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 497 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '信用分' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for help_user_score_details
-- ----------------------------
DROP TABLE IF EXISTS `help_user_score_details`;
CREATE TABLE `help_user_score_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '信用详细id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `score` int(11) NULL DEFAULT NULL COMMENT '分数',
  `type` int(11) NULL DEFAULT 1 COMMENT '类别（1收入2支出）',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9405 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '信用分详细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for invite
-- ----------------------------
DROP TABLE IF EXISTS `invite`;
CREATE TABLE `invite`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '邀请者id',
  `invitee_user_id` int(11) NULL DEFAULT NULL COMMENT '被邀请者id',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态 0非会员 1会员',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '邀请收益',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 710 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邀请信息' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for invite_money
-- ----------------------------
DROP TABLE IF EXISTS `invite_money`;
CREATE TABLE `invite_money`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '邀请收益钱包id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `money_sum` decimal(10, 2) NULL DEFAULT NULL COMMENT '总获取收益',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '当前金额',
  `cash_out` decimal(10, 2) NULL DEFAULT NULL COMMENT '累计提现',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 143 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for lucky_queue
-- ----------------------------
DROP TABLE IF EXISTS `lucky_queue`;
CREATE TABLE `lucky_queue`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '队列名称',
  `sort` int(10) NULL DEFAULT NULL COMMENT '队列等级（1、2、3）',
  `lucky_value` decimal(10, 2) NULL DEFAULT NULL COMMENT '额度',
  `recommend_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '推荐奖',
  `hand_out` decimal(10, 2) NULL DEFAULT NULL COMMENT '派送红包',
  `number_of_people` int(11) NULL DEFAULT NULL COMMENT '所需人数',
  `start_person` int(11) NULL DEFAULT NULL COMMENT '队列开始所需人数',
  `interval_person` int(11) NULL DEFAULT NULL COMMENT '队列间隔人数',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '时间',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常 2下架',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '幸运值队列' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lucky_queue
-- ----------------------------
INSERT INTO `lucky_queue` VALUES (1, 'A榜单', 1, 200.00, 100.00, 500.00, 3, 13, 9, '2020-08-11 13:50:32', 1);
INSERT INTO `lucky_queue` VALUES (2, 'B榜单', 2, 400.00, 170.00, 1000.00, 5, 13, 9, '2020-07-21 12:06:11', 1);
INSERT INTO `lucky_queue` VALUES (3, 'C榜单', 3, 800.00, 240.00, 2000.00, 7, 13, 9, '2020-07-21 12:06:11', 1);
INSERT INTO `lucky_queue` VALUES (4, 'D榜单', 4, 1600.00, 330.00, 4000.00, 10, 13, 9, '2020-07-21 12:06:11', 1);
INSERT INTO `lucky_queue` VALUES (5, 'E榜单', 5, 3200.00, 450.00, 8000.00, 13, 13, 9, '2020-07-21 12:06:11', 1);
INSERT INTO `lucky_queue` VALUES (6, 'F榜单', 6, 6400.00, 580.00, 16000.00, 17, 13, 9, '2020-07-21 12:06:11', 1);
INSERT INTO `lucky_queue` VALUES (7, 'G榜单', 7, 12800.00, 700.00, 32000.00, 20, 13, 9, '2020-07-21 12:06:11', 1);

-- ----------------------------
-- Table structure for lucky_queue_record
-- ----------------------------
DROP TABLE IF EXISTS `lucky_queue_record`;
CREATE TABLE `lucky_queue_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户晋升队列记录审核表id',
  `lucky_queue_id` int(11) NULL DEFAULT NULL COMMENT '队列id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `state` int(11) NULL DEFAULT 0 COMMENT '状态（0待审核 1同意 2拒绝）',
  `audit_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '审核意见',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户晋升队列记录审核表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lucky_queue_record
-- ----------------------------
INSERT INTO `lucky_queue_record` VALUES (39, 1, 150, 0, NULL, '2020-08-13 02:32:10');

-- ----------------------------
-- Table structure for lucky_value_queue
-- ----------------------------
DROP TABLE IF EXISTS `lucky_value_queue`;
CREATE TABLE `lucky_value_queue`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `lucky_queue_id` int(11) NULL DEFAULT NULL COMMENT '所在队列id',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常 2晋级申请中',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '幸运值队列' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lucky_value_queue
-- ----------------------------
INSERT INTO `lucky_value_queue` VALUES (1, 143, 1, 1, '2020-08-10 10:32:11');

-- ----------------------------
-- Table structure for message_info
-- ----------------------------
DROP TABLE IF EXISTS `message_info`;
CREATE TABLE `message_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `create_at` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '创建时间',
  `image` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '图片',
  `is_see` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `send_state` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `send_time` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `state` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '分类',
  `title` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `url` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '地址',
  `type` varchar(600) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `platform` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4231 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for msg
-- ----------------------------
DROP TABLE IF EXISTS `msg`;
CREATE TABLE `msg`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '验证码id',
  `code` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '验证码',
  `phone` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_name`(`code`, `phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2619 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for pay_details
-- ----------------------------
DROP TABLE IF EXISTS `pay_details`;
CREATE TABLE `pay_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '充值记录id',
  `classify` int(255) NULL DEFAULT NULL COMMENT '分类（1微信 2支付宝）',
  `order_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单id',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '充值金额',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `state` int(11) NULL DEFAULT 0 COMMENT '0待支付 1支付成功 2失败',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '支付类型 1充值 2开通会员',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 626 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '充值记录' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for platform_classify
-- ----------------------------
DROP TABLE IF EXISTS `platform_classify`;
CREATE TABLE `platform_classify`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '平台任务一级分类id',
  `classify_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '名称',
  `sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '排序',
  `state` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_classify
-- ----------------------------
INSERT INTO `platform_classify` VALUES (2, '京东', '0', 1);
INSERT INTO `platform_classify` VALUES (35, '天猫', '0', 0);
INSERT INTO `platform_classify` VALUES (36, '淘宝', '0', 0);

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('sqxScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('sqxScheduler', 'TASK_1', 'DEFAULT', NULL, 'com.sqx.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002C636F6D2E7371782E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017272C9CF807874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000373717874000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
INSERT INTO `QRTZ_LOCKS` VALUES ('sqxScheduler', 'STATE_ACCESS');
INSERT INTO `QRTZ_LOCKS` VALUES ('sqxScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('sqxScheduler', 'VM-0-4-centos1629448804674', 1629955731441, 15000);

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_TRIGGERS` VALUES ('sqxScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', NULL, 1629955800000, 1629954000000, 5, 'WAITING', 'CRON', 1591063808000, 0, NULL, 2, 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002C636F6D2E7371782E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017272C9CF807874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000373717874000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES (1, 'testTask', 'sqx', '0 0/30 * * * ?', 0, '参数测试', '2020-06-02 10:08:48');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log`  (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `job_id`(`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18705 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务日志' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for self_activity
-- ----------------------------
DROP TABLE IF EXISTS `self_activity`;
CREATE TABLE `self_activity`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_at` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '创建时间',
  `image_url` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '图片地址',
  `state` varchar(255) CHARACTER SET big5 COLLATE big5_chinese_ci NULL DEFAULT NULL COMMENT '状态',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '链接',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = big5 COLLATE = big5_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_activity
-- ----------------------------
INSERT INTO `self_activity` VALUES (9, '2021-08-14 17:46:57', 'https://sac.xianmxkj.com/img/20210814/e789217b2cb44cd8858d55a070f28eea.png', '1', '/package/pages/zysc/my/myList', '我的订单');
INSERT INTO `self_activity` VALUES (10, '2021-08-14 17:47:01', 'https://sac.xianmxkj.com/img/20210814/7002be2606d7479788526e2c019f972e.png', '1', '/package/pages/zysc/my/rebateorder', '我的返利');
INSERT INTO `self_activity` VALUES (11, '2021-08-14 17:47:07', 'https://sac.xianmxkj.com/img/20210814/84ada371a06d4e59963f575e445dfe4f.png', '1', '/package/pages/zysc/my/teamorder', '团队返利');
INSERT INTO `self_activity` VALUES (12, '2021-08-14 17:47:11', 'https://sac.xianmxkj.com/img/20210814/cfef5253d29a4d7d8ed10d1f5fccea5d.png', '1', '/package/pages/zysc/categray/category', '商品分类');
INSERT INTO `self_activity` VALUES (13, '2021-08-14 17:47:17', 'https://sac.xianmxkj.com/img/20210814/817ee4de38324deab648694df9e005a5.png', '1', '/pages/invitation/invitationUser', '邀请好友');
INSERT INTO `self_activity` VALUES (20, '2021-08-14 17:47:26', 'https://sac.xianmxkj.com/img/20210814/2b8d149e1b8f414fa0d5664ab120f86c.png', '3', '/pages/index/tuiguang?cid=1', '精选好物');
INSERT INTO `self_activity` VALUES (21, '2021-08-14 17:47:31', 'https://sac.xianmxkj.com/img/20210814/4afc5b64da264a66aa6cb42ac2a710d9.png', '3', '/pages/index/tuiguang?cid=10', '精选好物');
INSERT INTO `self_activity` VALUES (22, '2021-08-14 17:47:49', 'https://sac.xianmxkj.com/img/20210814/d3f73c0b288d4ae585a03a2a9067ff23.png', '4', '/pages/miandan/miandan', '热卖榜单');
INSERT INTO `self_activity` VALUES (23, '2021-08-14 17:47:40', 'https://sac.xianmxkj.com/img/20210814/1bac9f6827d64beaacd69e297907f149.png', '4', '/pages/index/tuiguang?cid=4', '热卖榜单');
INSERT INTO `self_activity` VALUES (24, '2021-08-14 17:48:26', 'https://sac.xianmxkj.com/img/20210814/984deaa5e1e444918f977226f00f6504.png', '5', '/pages/index/list?title=每日上新&type=1', '每日上新');
INSERT INTO `self_activity` VALUES (25, '2021-07-13 16:07:04', 'http://api.shengqianxiong.com.cn/img/20201030/7c711e4c74ac47ddac3045e0edf87760.png', '5', '/pages/index/food?title=爆款美食&type=9', '爆款美食');
INSERT INTO `self_activity` VALUES (35, '2021-08-16 16:41:32', 'https://sac.xianmxkj.com/img/20210814/9858a5e63c9e4c7aa3591665c09e22a2.png', '1', '/package/pages/zysc/categray/search?cid=48&name=衬衫', '女装');
INSERT INTO `self_activity` VALUES (36, '2021-08-20 09:33:49', 'https://sac.xianmxkj.com/img/20210814/646bea281fb148f090315ec24d4b6a7a.png', '1', '/package/pages/zysc/my/like', '我的收藏');
INSERT INTO `self_activity` VALUES (37, '2021-08-16 16:42:27', 'https://sac.xianmxkj.com/img/20210814/f49ac5c9a0ba457cbbf3df299bbba2e7.png', '1', '/package/pages/zysc/categray/search?cid=48&name=苹果', '新鲜瓜果');
INSERT INTO `self_activity` VALUES (38, '2021-08-16 16:42:32', 'https://sac.xianmxkj.com/img/20210814/11378ea337c94425853cc8905417bcfc.png', '1', '/package/pages/zysc/categray/search?cid=48&name=电视', '电视');
INSERT INTO `self_activity` VALUES (39, '2021-08-16 16:42:41', 'https://sac.xianmxkj.com/img/20210814/e4406cbc5a7b48a3859daf664da346a4.png', '1', '/package/pages/zysc/categray/search?cid=34&name=电子产品', '电子产品');

-- ----------------------------
-- Table structure for self_banner
-- ----------------------------
DROP TABLE IF EXISTS `self_banner`;
CREATE TABLE `self_banner`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片地址',
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '跳转链接',
  `sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_banner
-- ----------------------------
INSERT INTO `self_banner` VALUES (1, '2020-08-15 10:58:44', 'https://pic3.zhimg.com/ac2929dd435e78ae8efe25252008dcdd_xs.jpg?source=1940ef5c', 'www.jd.com', '1');
INSERT INTO `self_banner` VALUES (4, '2020-08-15 11:00:26', 'https://pic4.zhimg.com/v2-095892c2f4425ff90f53b946b577847b_540x450.jpeg', 'www.taobao.com', '2');

-- ----------------------------
-- Table structure for self_goods
-- ----------------------------
DROP TABLE IF EXISTS `self_goods`;
CREATE TABLE `self_goods`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `descrition` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '商品描述',
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '商品图片',
  `merchants` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商户号',
  `price` double NULL DEFAULT NULL COMMENT '价格',
  `sales` int(11) NULL DEFAULT NULL COMMENT '商品销量',
  `status` int(11) NULL DEFAULT 0 COMMENT '商品状态(默认1正常 2下架)',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品类型id',
  `member_price` double NULL DEFAULT NULL COMMENT '会员价格',
  `original_price` double NULL DEFAULT NULL COMMENT '商品原价',
  `commission_price` double NULL DEFAULT NULL COMMENT '商品佣金',
  `home_goods` int(11) NULL DEFAULT NULL COMMENT '首页商品(0默认 1是首页商品)',
  `is_select` int(11) NULL DEFAULT NULL COMMENT '精选好物(0默认 1精选好物)',
  `buy_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '必买理由',
  `cover_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品封面图片',
  `is_express` int(11) NULL DEFAULT NULL COMMENT '是否需要发货(1普通商品需要发货 2虚拟商品无需发货)',
  `is_recommend` int(11) NULL DEFAULT NULL COMMENT '每日推荐(0默认 1推荐商品)',
  `postage_price` double NULL DEFAULT NULL COMMENT '邮费',
  `type_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品类型id',
  `is_postage` int(11) NULL DEFAULT NULL,
  `is_ji_fen_goods` int(11) NULL DEFAULT NULL,
  `brand_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods
-- ----------------------------

INSERT INTO `self_goods` VALUES (63, '2020-09-24 14:45:50', '<p><img src=\"https://tk.gomyorder.cn/img/20200924/9f7af3e5ceb249498c950e5b3dd50112.jpg\" alt=\"\" /></p>\n<p><img src=\"https://tk.gomyorder.cn/img/20200924/3dac2bca3ba248fa8d0aaeeca6ca4d95.jpg\" alt=\"\" /></p>\n<p><img src=\"https://tk.gomyorder.cn/img/20200924/fa13cd3e64354448a59fe69d3e3f220a.jpg\" alt=\"\" /></p>', 'https://tk.gomyorder.cn/img/20200924/7bae1c7b42d04943adb64b901cdca345.jpg,https://tk.gomyorder.cn/img/20200924/a401af5d892c4226bc5fdb31946bccb3.jpg,https://tk.gomyorder.cn/img/20200924/2c081e77d2c047d2b383de763b2a736b.jpg,https://tk.gomyorder.cn/img/20200924/7d729c59272e46d9bac220f19f94b4f0.jpg', '', 1, 1577, 1, '乔丹女运动鞋2020秋季网面跑步鞋气垫缓震增高旅游鞋黑粉色波鞋子', NULL, 1, 5, 0.01, 1, 1, '有种脾气叫，不放弃。', 'https://tk.gomyorder.cn/img/20200924/8bc17c26f578441981efb5c0d55a761f.jpg', 1, 1, 0, '13', NULL, NULL, NULL);
INSERT INTO `self_goods` VALUES (65, '2020-09-24 14:49:11', '<p><img src=\"https://tk.gomyorder.cn/img/20200924/c932c5b3a8fa45bf909102d42ef93ec8.jpg\" alt=\"\" width=\"790\" height=\"764\" /></p>\n<p><img src=\"https://tk.gomyorder.cn/img/20200924/5b15208c7db247a3991b757d40d75fd3.jpg\" alt=\"\" width=\"790\" height=\"711\" /></p>\n<p><img src=\"https://tk.gomyorder.cn/img/20200924/fcf368702631418aa193f76b9ecc7aab.jpg\" alt=\"\" /></p>\n<p><img src=\"https://tk.gomyorder.cn/img/20200924/9b0e4dda36194296ac3349f1218fd498.jpg\" alt=\"\" /></p>', 'https://tk.gomyorder.cn/img/20200924/1dfcd54fcc0a49a9bef6d6c93e354464.jpg,https://tk.gomyorder.cn/img/20200924/1523374d86b34c25a9af121c3238adee.jpg,https://tk.gomyorder.cn/img/20200924/13e991739f384fc3bbff71b65cab2b3b.jpg', '', 1, 2112, 1, '百草味-零食大礼包网红爆款休闲充饥夜宵小吃饼干组合一整箱送礼', NULL, 1, 2, 0.01, 1, 1, '600款零食 一站购 1元起开抢', 'https://tk.gomyorder.cn/img/20200924/6896a2717e3c419094f62a3c107455e6.jpg', 1, 1, 0, '21', NULL, NULL, NULL);
INSERT INTO `self_goods` VALUES (66, '2020-09-24 14:52:21', '<p><img src=\"https://tk.gomyorder.cn/img/20201107/7bddfab214274981b86892d1a64078c5.png\" alt=\"\" /></p>\n<p><img src=\"https://tk.gomyorder.cn/img/20201107/3fbe401efe264924a3aa34ef113bc1f1.png\" alt=\"\" width=\"775\" height=\"775\" /></p>', 'https://api.shengqianxiong.com.cn/img/20210820/a59e127b5f1d435bb813543293609640.png', '', 1, 54214, 1, '维达手帕纸超韧4层8张18包卫生纸巾 自然无香面巾纸 新旧交替发货', NULL, 1, 5, 0.1, 1, 1, '出门必备易携带，超韧细密湿水不易破', 'https://api.shengqianxiong.com.cn/img/20210820/a61d772ddf6e4eaaae843993c91170d7.png', 1, 1, 0, '30', NULL, NULL, NULL);
INSERT INTO `self_goods` VALUES (97, '2021-08-10 16:12:36', '<p>好好学习，天天向上。</p>', 'https://api.shengqianxiong.com.cn/img/20210810/d08193121f8549199294a3a0f53a9f22.jpg,https://api.shengqianxiong.com.cn/img/20210810/21001828cf2c465896995c6360f486e7.jpg,https://api.shengqianxiong.com.cn/img/20210810/852c4e1c2ef84f5087a58503bf32cdb8.jpg', '', 55, 12, 1, '计算机与科学书籍·', NULL, 55, 36, 0.2, 1, 1, '有用的东西不需要理由', 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 1, 1, 0, '51', NULL, NULL, NULL);
INSERT INTO `self_goods` VALUES (98, '2021-08-11 18:09:32', '<p>嗯</p>', 'https://api.shengqianxiong.com.cn/img/20210811/7d923d42836a4d82af306ec7163dbff6.jpg,https://api.shengqianxiong.com.cn/img/20210811/1339d822bf9042b99d85971752d4055d.jpg', '', 85, 10, 1, '哲学', NULL, 85, 95, 0.1, 1, 1, '对你们有用哦', 'https://api.shengqianxiong.com.cn/img/20210811/7e2a40c37b1745539da07793e1c1c7dc.jpg', 3, 1, -4, '53', NULL, NULL, NULL);
INSERT INTO `self_goods` VALUES (100, '2021-08-14 10:11:28', '<p>今天 你吃了吗</p>', 'https://api.shengqianxiong.com.cn/img/20210814/19222cef43a845209f081ab6a38c4e2f.jpg,https://api.shengqianxiong.com.cn/img/20210814/48949a3bbb53412391ea33290e626f32.jpg', '', 50, 100, 1, '测试商品', NULL, 50, 100, 10, 1, 1, '迎娶白富美 走上人生巅峰', 'https://api.shengqianxiong.com.cn/img/20210814/7bb076847871417d9d153c1b126f99f2.jpg', 1, 1, 0, '48', NULL, NULL, 1);

-- ----------------------------
-- Table structure for self_goods_attr
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_attr`;
CREATE TABLE `self_goods_attr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `attr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '属性名称',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `rule_id` bigint(20) NULL DEFAULT NULL COMMENT '规格模板id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 127 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_attr
-- ----------------------------
INSERT INTO `self_goods_attr` VALUES (1, '帽子', 52, NULL);
INSERT INTO `self_goods_attr` VALUES (2, '衣服', 53, NULL);
INSERT INTO `self_goods_attr` VALUES (3, '衣服', 54, NULL);
INSERT INTO `self_goods_attr` VALUES (4, '衣服', 55, NULL);
INSERT INTO `self_goods_attr` VALUES (5, '衣服', 56, NULL);
INSERT INTO `self_goods_attr` VALUES (6, '衣服', 57, NULL);
INSERT INTO `self_goods_attr` VALUES (7, '衣服', 58, NULL);
INSERT INTO `self_goods_attr` VALUES (8, '衣服', 59, NULL);
INSERT INTO `self_goods_attr` VALUES (13, '衣服', 76, 8);
INSERT INTO `self_goods_attr` VALUES (15, '帽子', 77, 9);
INSERT INTO `self_goods_attr` VALUES (20, '衣服', 78, 8);
INSERT INTO `self_goods_attr` VALUES (25, '衣服', 79, 8);
INSERT INTO `self_goods_attr` VALUES (45, '衣服', 83, 8);
INSERT INTO `self_goods_attr` VALUES (91, '衣服', 94, 8);
INSERT INTO `self_goods_attr` VALUES (117, '衣服', 99, 8);
INSERT INTO `self_goods_attr` VALUES (121, '衣服', 97, 8);
INSERT INTO `self_goods_attr` VALUES (122, '衣服1423', 96, 8);
INSERT INTO `self_goods_attr` VALUES (123, '帽子', 95, 9);
INSERT INTO `self_goods_attr` VALUES (126, '衣服', 100, 8);

-- ----------------------------
-- Table structure for self_goods_attr_value
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_attr_value`;
CREATE TABLE `self_goods_attr_value`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '属性值组合:{尺寸: \"7寸\", 颜色: \"红底\"}',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片',
  `member_price` double NULL DEFAULT NULL COMMENT '会员价格',
  `original_price` double NULL DEFAULT NULL COMMENT '原价',
  `price` double NULL DEFAULT NULL COMMENT '价格',
  `sales` int(11) NULL DEFAULT NULL,
  `stock` int(11) NULL DEFAULT NULL,
  `attr_id` bigint(20) NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 285 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_attr_value
-- ----------------------------
INSERT INTO `self_goods_attr_value` VALUES (1, '大', 52, NULL, NULL, NULL, NULL, NULL, NULL, 1, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (2, '红色', 52, NULL, NULL, NULL, NULL, NULL, NULL, 1, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (3, '紫色,红色', 53, NULL, NULL, NULL, NULL, NULL, NULL, 2, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (4, 'm', 53, NULL, NULL, NULL, NULL, NULL, NULL, 2, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (5, '紫色,红色', 54, NULL, NULL, NULL, NULL, NULL, NULL, 3, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (6, 'm', 54, NULL, NULL, NULL, NULL, NULL, NULL, 3, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (7, '紫色,红色', 55, NULL, NULL, NULL, NULL, NULL, NULL, 4, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (8, 'm', 55, NULL, NULL, NULL, NULL, NULL, NULL, 4, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (11, '紫色,红色', 57, NULL, NULL, NULL, NULL, NULL, NULL, 6, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (12, 'm', 57, NULL, NULL, NULL, NULL, NULL, NULL, 6, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (13, '紫色,红色', 58, NULL, NULL, NULL, NULL, NULL, NULL, 7, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (14, 'm', 58, NULL, NULL, NULL, NULL, NULL, NULL, 7, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (15, '紫色,红色', 59, NULL, NULL, NULL, NULL, NULL, NULL, 8, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (16, 'm', 59, NULL, NULL, NULL, NULL, NULL, NULL, 8, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (33, '紫色,红色', 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (34, 'm', 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (43, '紫色,红色', 79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (44, 'm', 79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (83, '紫色,红色', 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (84, 'm', 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (175, '紫色,红色', 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (176, 'm', 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (237, '红,黄,蓝', 97, NULL, NULL, NULL, NULL, NULL, NULL, 121, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (238, 'x,l', 97, NULL, NULL, NULL, NULL, NULL, NULL, 121, '大小');
INSERT INTO `self_goods_attr_value` VALUES (239, '紫色,红色', 96, NULL, NULL, NULL, NULL, NULL, NULL, 122, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (240, 'm', 96, NULL, NULL, NULL, NULL, NULL, NULL, 122, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (241, '大', 95, NULL, NULL, NULL, NULL, NULL, NULL, 123, '尺码');
INSERT INTO `self_goods_attr_value` VALUES (242, '红色', 95, NULL, NULL, NULL, NULL, NULL, NULL, 123, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (261, '红色', 99, NULL, NULL, NULL, NULL, NULL, NULL, 117, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (262, 'm', 99, NULL, NULL, NULL, NULL, NULL, NULL, 117, '大小');
INSERT INTO `self_goods_attr_value` VALUES (263, '工装', 99, NULL, NULL, NULL, NULL, NULL, NULL, 117, '风格');
INSERT INTO `self_goods_attr_value` VALUES (264, '粉色,五颜六色', 99, NULL, NULL, NULL, NULL, NULL, NULL, 117, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (281, '红色', 100, NULL, NULL, NULL, NULL, NULL, NULL, 126, '颜色');
INSERT INTO `self_goods_attr_value` VALUES (282, 'm', 100, NULL, NULL, NULL, NULL, NULL, NULL, 126, '大小');
INSERT INTO `self_goods_attr_value` VALUES (283, '工装', 100, NULL, NULL, NULL, NULL, NULL, NULL, 126, '风格');
INSERT INTO `self_goods_attr_value` VALUES (284, '粉色,黑色,红色', 100, NULL, NULL, NULL, NULL, NULL, NULL, 126, '颜色');

-- ----------------------------
-- Table structure for self_goods_brand
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_brand`;
CREATE TABLE `self_goods_brand`  (
  `brand_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `names` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`brand_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_brand
-- ----------------------------
INSERT INTO `self_goods_brand` VALUES (1, '马来西亚', '2020-10-28 10:05:10', 'INTI');
INSERT INTO `self_goods_brand` VALUES (2, '泰国', '2020-10-28 10:18:25', '西那瓦');
INSERT INTO `self_goods_brand` VALUES (3, '英国', '2020-10-28 10:19:21', '伯明翰');
INSERT INTO `self_goods_brand` VALUES (5, '腾讯', '2020-11-10 16:07:18', '腾讯视频');

-- ----------------------------
-- Table structure for self_goods_comment
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_comment`;
CREATE TABLE `self_goods_comment`  (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `goods_id` bigint(20) NULL DEFAULT NULL,
  `img` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sku` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_header` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_id` bigint(20) NULL DEFAULT NULL,
  `sku_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `reply` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `score` int(11) NULL DEFAULT NULL,
  `score_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_comment
-- ----------------------------
INSERT INTO `self_goods_comment` VALUES (5, 'nasdaksjdlkajsldjalsd j', '2020-11-05 11:41:41', 77, 'https://api.shengqianxiong.com.cn/img/20201105/d666ceaca8864e8ba7d2244086c391dd.png,https://api.shengqianxiong.com.cn/img/20201105/c920450d1c9f49f99b5f057e0880a511.png,https://api.shengqianxiong.com.cn/img/20201105/8ea7db33749f419597c69c3f62772923.png', '粉色', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 499, '858', NULL, 1, 3);
INSERT INTO `self_goods_comment` VALUES (10, '11111122sssss', '2020-11-05 17:10:42', 77, 'https://api.shengqianxiong.com.cn/img/20201105/049e0d41f69740d18f5760c27527ef61.jpg', '黑色,35', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 498, '840', NULL, 2, 3);
INSERT INTO `self_goods_comment` VALUES (11, '22sss22ss', '2020-11-05 17:12:21', 32, 'https://api.shengqianxiong.com.cn/img/20201105/5fc782c12a2941bcb5da0d237faabbe6.png', '米色,36', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 528, '849', NULL, 2, 3);
INSERT INTO `self_goods_comment` VALUES (12, '1324234234', '2020-11-05 17:41:56', 77, 'https://api.shengqianxiong.com.cn/img/20201105/4dbb6e65ed6842d3918f52a01ad44496.png,https://api.shengqianxiong.com.cn/img/20201105/2cccf021109b454e9f090759552f6ee7.png', '粉色', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 497, '858', NULL, 4, 1);
INSERT INTO `self_goods_comment` VALUES (13, '2232cffcffff', '2020-11-05 17:42:12', 77, 'https://api.shengqianxiong.com.cn/img/20201105/565e57c2739a44eba3c31e29c7d2431f.png,https://api.shengqianxiong.com.cn/img/20201105/9608fdf58c96416490c038d604c1de4c.png', '紫色,m', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 494, '862', NULL, 4, 1);
INSERT INTO `self_goods_comment` VALUES (14, '东西可以使用，效果不错，皮炎平痔疮膏效果就是好，美滴很', '2020-11-05 17:44:47', 77, 'https://api.shengqianxiong.com.cn/img/20201105/62dc07df43d24166aba762d9a98660ad.png,https://api.shengqianxiong.com.cn/img/20201105/98161f6c10e24f9a93ae6f027f8d96bd.png', '', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 491, '792', NULL, 4, 1);
INSERT INTO `self_goods_comment` VALUES (15, '545455656546546', '2020-11-05 17:46:57', 30, 'https://api.shengqianxiong.com.cn/img/20201105/cb4366cf9f1b4a63b0310616213fbb77.png,https://api.shengqianxiong.com.cn/img/20201105/7ab1fe6fa35d4fd9af4cf1b58590d644.png', '红色,m', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 529, '866', '感谢购买', 3, 2);
INSERT INTO `self_goods_comment` VALUES (16, '4545454545', '2020-11-05 17:48:02', 30, 'https://api.shengqianxiong.com.cn/img/20201105/0a1f79c15ca4420ca4b5a5e8aee857ea.png,https://api.shengqianxiong.com.cn/img/20201105/ee4c8351339140e7b3407aba6a194921.png', '紫色,m', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 530, '867', '很好', 5, 1);
INSERT INTO `self_goods_comment` VALUES (17, '丝袜所大所大所', '2020-11-05 17:50:20', 41, 'https://api.shengqianxiong.com.cn/img/20201105/5c2a6046dec24f308fe8b9af27047709.png,https://api.shengqianxiong.com.cn/img/20201105/ea675b1e923c4e328852dc7e3fc5702e.jpg,https://api.shengqianxiong.com.cn/img/20201105/e480bb28e4284e57a456af5e8f437484.png,https://api.shengqianxiong.com.cn/img/20201105/d2116810e4814be79c39853816fa2538.png', '粉色', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 531, '858', NULL, 5, 1);
INSERT INTO `self_goods_comment` VALUES (18, '很不错', '2020-11-05 23:08:54', 77, '', '', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJzujbuXe270Zn1lauty7WicST5qeHySibSnzL8wQV0YQNQe1wsHCzcMnxmDBfh4j6Ntp4zMSjmXg3Q/132', 7458, '晓东', 512, '400', NULL, 5, 1);
INSERT INTO `self_goods_comment` VALUES (19, '宝宝比较喜欢', '2020-11-06 13:32:51', 41, 'https://api.shengqianxiong.com.cn/img/20201106/a9f3fa074fba4299bfefa689f1a5e1ca.png', '粉色', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLERLpErfNmoicBiaoTaey3JJ4ica0uvKkOB7SPDzb51qPRB8s8FToJmf4tficfu8086mVcE88PWoRcnA/132', 7515, 'T', 541, '858', NULL, 4, 1);
INSERT INTO `self_goods_comment` VALUES (20, '你好 东西很不错 五星好评', '2020-11-06 16:44:55', 30, 'https://api.shengqianxiong.com.cn/img/20201106/abac68a0ca374fa29480275b20e9f899.jpg,https://api.shengqianxiong.com.cn/img/20201106/38d737bf882e439ba6ac0526b1783af7.jpg,https://api.shengqianxiong.com.cn/img/20201106/a7d22c1121c548738484db95ee8ceca4.jpg,https://api.shengqianxiong.com.cn/img/20201106/9b4cb688d8104558b3ecf861d5471b02.jpg', '红色,m', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLERLpErfNmoicBiaoTaey3JJ4ica0uvKkOB7SPDzb51qPRB8s8FToJmf4tficfu8086mVcE88PWoRcnA/132', 7515, 'T', 540, '866', NULL, 5, 1);
INSERT INTO `self_goods_comment` VALUES (21, '47536453654', '2020-11-10 13:29:31', 88, 'https://api.shengqianxiong.com.cn/img/20201110/5126f4d36f724eb5bd2a03bc7331ba0c.png', '', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 582, '911', NULL, 4, 1);
INSERT INTO `self_goods_comment` VALUES (22, '2353453', '2020-11-10 17:36:44', 88, 'https://api.shengqianxiong.com.cn/img/20201110/92ec9504beb741388555379709c2c3d0.png', '', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 588, '912', NULL, 4, 1);
INSERT INTO `self_goods_comment` VALUES (23, '差评差评差评差评差评差评差评差评差评差评差评差评差评差评差评差评差评', '2020-12-03 17:24:27', 99, 'https://api.shengqianxiong.com.cn/img/20201203/306e3aa945974e57a493b67fb7a78513.png', '', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 713, '1043', NULL, 1, 3);
INSERT INTO `self_goods_comment` VALUES (24, '你好好到死大声道阿萨德', '2020-12-05 10:37:58', 91, 'https://api.shengqianxiong.com.cn/img/20201205/08b5de3d03a94b96a9959b66e49bf59f.png', '', 'http://thirdwx.qlogo.cn/mmopen/67IUh86BAMLqkKedwJiamg5l2t5LW0ibWGicTtia2oc8RgtdMrGZzI4zKN2ia01blVls1qAr7k1IibQZcHm5WWKbpqwiaLwuic8xIKkr/132', 7476, 'T', 726, '985', NULL, 3, 2);
INSERT INTO `self_goods_comment` VALUES (25, '424524', '2020-12-11 11:30:56', 102, 'https://shop.shengqianxiong.com.cn/img/20201211/71acc4c67b8e41e59d51afb65b55b0fe.png', '大', 'https://api.shengqianxiong.com.cn/img/20201209/c92693f06fd44e7c9664b3db10f1f997.png', 7476, '234324', 793, '1051', NULL, 3, 2);
INSERT INTO `self_goods_comment` VALUES (26, '1121424', '2020-12-11 11:31:44', 102, 'https://shop.shengqianxiong.com.cn/img/20201211/177cca3da216419d816c03f83a623ab4.png', '大', 'https://api.shengqianxiong.com.cn/img/20201209/c92693f06fd44e7c9664b3db10f1f997.png', 7476, '234324', 787, '1051', NULL, 3, 2);
INSERT INTO `self_goods_comment` VALUES (28, '111', '2021-05-19 09:36:37', 112, '', '', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaELWPwelPCV9ExibHoLibezeyapk42yCtGSqoicwQBhuXYSBbmxEcSpLq4Xiazh4G1e1R1BuKaPU51XoQQ/132', 8150, '美匠网络', 1452, '1120', NULL, 3, 2);
INSERT INTO `self_goods_comment` VALUES (31, '东西不错', '2021-08-20 10:38:55', 60, 'https://taskshop.xianmxkj.com/img/20210820/0f01f7b1c6d1409ea4a70632c619c99d.jpg', '带钻,2-2000000', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKzWDHJPTaMNw5YuhtTKFsxUYibdibdvyZoYj8PwJOqJlsbbxcrOiaXKkRUhbUMIv7VdnSuy8ybXkWoQ/132', 1001, 'T', 571, '93', '你说啥', 5, 1);

-- ----------------------------
-- Table structure for self_goods_rule
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_rule`;
CREATE TABLE `self_goods_rule`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `rule_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '规格名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_rule
-- ----------------------------
INSERT INTO `self_goods_rule` VALUES (8, '2020-09-11 15:21:01', '衣服');
INSERT INTO `self_goods_rule` VALUES (9, '2020-09-11 15:27:02', '帽子');
INSERT INTO `self_goods_rule` VALUES (10, '2020-09-11 15:44:37', '鞋子');
INSERT INTO `self_goods_rule` VALUES (14, '2020-09-11 17:27:49', '裤子');
INSERT INTO `self_goods_rule` VALUES (15, '2020-09-11 17:38:59', '袜子');
INSERT INTO `self_goods_rule` VALUES (27, '2021-08-10 17:58:41', '首饰');
INSERT INTO `self_goods_rule` VALUES (28, '2021-08-11 17:45:25', '项链');

-- ----------------------------
-- Table structure for self_goods_rule_value
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_rule_value`;
CREATE TABLE `self_goods_rule_value`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '规格属性值',
  `rule_id` bigint(20) NULL DEFAULT NULL COMMENT '规格id',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '规格属性名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_rule_value
-- ----------------------------
INSERT INTO `self_goods_rule_value` VALUES (1, '紫色', 3, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (2, '紫色', 4, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (3, 'm', 5, '尺码');
INSERT INTO `self_goods_rule_value` VALUES (4, 'm,l', 5, NULL);
INSERT INTO `self_goods_rule_value` VALUES (5, '紫色,红色', 6, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (6, '紫色,红色,蓝色', 7, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (9, '大', 9, '尺码');
INSERT INTO `self_goods_rule_value` VALUES (10, '红色', 9, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (14, 'm', 11, '大小');
INSERT INTO `self_goods_rule_value` VALUES (15, '紫色', 12, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (16, '红色,紫色', 13, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (20, '红色', 16, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (22, '红色,白色', 17, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (23, '红色', 18, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (24, 'm', 18, '尺码');
INSERT INTO `self_goods_rule_value` VALUES (25, '红色,黑色,白色', 19, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (26, 'm,l,s', 19, '尺码');
INSERT INTO `self_goods_rule_value` VALUES (27, 'm,l', 20, '大小');
INSERT INTO `self_goods_rule_value` VALUES (28, '红色,紫色', 20, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (29, 'm,l', 21, '大小');
INSERT INTO `self_goods_rule_value` VALUES (30, '红色,紫色', 21, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (47, '黑色,白色,黑色', 22, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (52, '紫色', 10, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (53, 'm', 10, '尺码');
INSERT INTO `self_goods_rule_value` VALUES (54, '红色,紫色', 14, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (55, 'm', 14, '大小');
INSERT INTO `self_goods_rule_value` VALUES (56, '黑色,白色,紫色', 15, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (59, '黑色', 23, 'X');
INSERT INTO `self_goods_rule_value` VALUES (60, '156', 24, '大小·');
INSERT INTO `self_goods_rule_value` VALUES (61, '222', 25, '大小');
INSERT INTO `self_goods_rule_value` VALUES (68, '253', 26, '大小');
INSERT INTO `self_goods_rule_value` VALUES (78, '带钻', 27, '有无钻');
INSERT INTO `self_goods_rule_value` VALUES (79, '2-2000000', 27, '价位');
INSERT INTO `self_goods_rule_value` VALUES (93, '布灵布灵', 28, '风格');
INSERT INTO `self_goods_rule_value` VALUES (94, 'l', 28, '大小');
INSERT INTO `self_goods_rule_value` VALUES (99, '红色', 8, '颜色');
INSERT INTO `self_goods_rule_value` VALUES (100, 'm', 8, '大小');
INSERT INTO `self_goods_rule_value` VALUES (101, '工装', 8, '风格');
INSERT INTO `self_goods_rule_value` VALUES (102, '粉色,五颜六色', 8, '颜色');

-- ----------------------------
-- Table structure for self_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_sku`;
CREATE TABLE `self_goods_sku`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `sku_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'sku图片',
  `sku_original_price` double NULL DEFAULT NULL COMMENT 'sku原价',
  `sku_price` double NULL DEFAULT NULL COMMENT 'sku商品售价',
  `member_price` double NULL DEFAULT NULL COMMENT '会员价格',
  `detail_json` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'sku信息，json封装',
  `sales` int(11) NULL DEFAULT NULL COMMENT '销量',
  `stock` int(11) NULL DEFAULT NULL COMMENT '库存',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 133 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_sku
-- ----------------------------
INSERT INTO `self_goods_sku` VALUES (1, 52, NULL, 0, 0, 0, '大,红色', 0, 0);
INSERT INTO `self_goods_sku` VALUES (2, 53, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (3, 53, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (4, 54, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (5, 54, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (6, 55, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (7, 55, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (8, 57, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (9, 57, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (10, 58, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (11, 58, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (12, 59, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (13, 59, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (16, 76, NULL, 0, 0, 0, '紫色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (17, 76, NULL, 0, 0, 0, '红色,m', 0, 0);
INSERT INTO `self_goods_sku` VALUES (34, 91, 'https://tk.gomyorder.cn/img/20210512/66e26326a0a14e3abea2090bebd588f3.jfif', 100, 50, NULL, NULL, 0, 999);
INSERT INTO `self_goods_sku` VALUES (84, 97, 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 36, 55, NULL, '红,x', 0, 998);
INSERT INTO `self_goods_sku` VALUES (85, 97, 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 36, 55, NULL, '红,l', 0, 999);
INSERT INTO `self_goods_sku` VALUES (86, 97, 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 36, 55, NULL, '黄,x', 0, 999);
INSERT INTO `self_goods_sku` VALUES (87, 97, 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 36, 55, NULL, '黄,l', 0, 999);
INSERT INTO `self_goods_sku` VALUES (88, 97, 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 36, 55, NULL, '蓝,x', 0, 999);
INSERT INTO `self_goods_sku` VALUES (89, 97, 'https://api.shengqianxiong.com.cn/img/20210810/a47def2d5bfe4c07b8d6dfba2ff7fd8a.jpg', 36, 55, NULL, '蓝,l', 0, 999);
INSERT INTO `self_goods_sku` VALUES (90, 96, 'https://api.shengqianxiong.com.cn/img/20210810/99866e2ada0e4d9c8d55a56527f77deb.jpg', 50, 40, NULL, '紫色,m', 0, 999);
INSERT INTO `self_goods_sku` VALUES (91, 96, 'https://api.shengqianxiong.com.cn/img/20210810/99866e2ada0e4d9c8d55a56527f77deb.jpg', 50, 40, NULL, '红色,m', 0, 999);
INSERT INTO `self_goods_sku` VALUES (92, 95, 'https://api.shengqianxiong.com.cn/img/20210810/0f972f2dc9fe46a08dfebf508364be80.jpg', 100, 90, NULL, '大,红色', 0, 999);
INSERT INTO `self_goods_sku` VALUES (107, 99, 'https://api.shengqianxiong.com.cn/img/20210814/bcd8ed002d9446d1b2e26d12a35283f9.jpeg', 10, 0.01, NULL, '红色,m,工装,粉色', 0, 96);
INSERT INTO `self_goods_sku` VALUES (108, 99, 'https://api.shengqianxiong.com.cn/img/20210814/8c038f45f5864a6e9bbaab133ee52bea.jpg', 10, 0.01, NULL, '红色,m,工装,五颜六色', 0, 88);
INSERT INTO `self_goods_sku` VALUES (111, 65, 'https://tk.gomyorder.cn/img/20200924/6896a2717e3c419094f62a3c107455e6.jpg', 2, 1, NULL, NULL, 0, 8);
INSERT INTO `self_goods_sku` VALUES (112, 63, 'https://tk.gomyorder.cn/img/20200924/8bc17c26f578441981efb5c0d55a761f.jpg', 5, 1, NULL, NULL, 0, 999);
INSERT INTO `self_goods_sku` VALUES (119, 98, 'https://api.shengqianxiong.com.cn/img/20210811/7e2a40c37b1745539da07793e1c1c7dc.jpg', 52, 95, NULL, NULL, 0, 0);
INSERT INTO `self_goods_sku` VALUES (129, 100, 'https://api.shengqianxiong.com.cn/img/20210814/7bb076847871417d9d153c1b126f99f2.jpg', 100, 50, NULL, '红色,m,工装,粉色', 0, 0);
INSERT INTO `self_goods_sku` VALUES (130, 100, 'https://api.shengqianxiong.com.cn/img/20210814/7bb076847871417d9d153c1b126f99f2.jpg', 100, 50, NULL, '红色,m,工装,黑色', 0, 2);
INSERT INTO `self_goods_sku` VALUES (131, 100, 'https://api.shengqianxiong.com.cn/img/20210814/7bb076847871417d9d153c1b126f99f2.jpg', 100, 50, NULL, '红色,m,工装,红色', 0, 3);
INSERT INTO `self_goods_sku` VALUES (132, 66, 'https://tk.gomyorder.cn/img/20201107/e2c3514bc9824acd97841a23bc5f9998.png', 5, 1, NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for self_goods_virtual
-- ----------------------------
DROP TABLE IF EXISTS `self_goods_virtual`;
CREATE TABLE `self_goods_virtual`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '卡密内容',
  `cover_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品封面图片',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '链接',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态（1正常 2关闭 3已使用）',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_goods_virtual
-- ----------------------------
INSERT INTO `self_goods_virtual` VALUES (6, 'sadasd45455', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 15:06:06', 80, 'http://www.baidu.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (7, '12211212s1d', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 15:06:06', 80, 'http://www.baidu.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (8, 'sfdssd65656', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 15:31:22', 80, 'http://www.baidu.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (9, 'sdfsdfsd54587', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 15:31:22', 80, 'http://www.baidu.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (10, 'sdfdsf5655', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 15:31:22', 80, 'http://www.baidu.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (11, 'sfdssd65656', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 19:45:25', 80, 'http://www.baidu.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (15, 'sfdssd656586', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 19:48:41', 80, 'https://www.xiansqx.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (16, 'sfdssd659656', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 19:48:41', 80, 'https://www.xiansqx.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (17, 'sfdssd651656', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-12 19:48:41', 80, 'https://www.xiansqx.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (20, 'sfdssd65656', 'https://www.gomyorder.cn/img/20201107/10febbaeed25419f82a8d2cf7782f792.png', '2020-11-12 19:53:00', 82, 'https://www.xiansqx.com', 1, '诗凡黎衬衫女2020年新款秋装设计感小众天丝女装上衣长袖黄色衬衣');
INSERT INTO `self_goods_virtual` VALUES (22, 'dfjkhf', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-17 13:31:06', 80, 'https://www.xiansqx.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (23, 'dfjkhdgf', 'https://www.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2020-11-17 13:31:06', 80, 'https://www.xiansqx.com', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (24, 'dsadkskldsalfDSF', 'https://www.gomyorder.cn/img/20201107/10febbaeed25419f82a8d2cf7782f792.png', '2020-11-17 14:58:54', 82, 'https://www.xiansqx.com', 1, '诗凡黎衬衫女2020年新款秋装设计感小众天丝女装上衣长袖黄色衬衣');
INSERT INTO `self_goods_virtual` VALUES (25, '5546565656', 'https://www.gomyorder.cn/img/20201107/10febbaeed25419f82a8d2cf7782f792.png', '2020-11-17 14:58:54', 82, 'https://www.xiansqx.com', 1, '诗凡黎衬衫女2020年新款秋装设计感小众天丝女装上衣长袖黄色衬衣');
INSERT INTO `self_goods_virtual` VALUES (30, '5454578855', 'https://www.gomyorder.cn/img/20201107/10febbaeed25419f82a8d2cf7782f792.png', '2020-11-17 15:14:49', 82, 'https://sqx.gomyorder.cn', 1, '诗凡黎衬衫女2020年新款秋装设计感小众天丝女装上衣长袖黄色衬衣');
INSERT INTO `self_goods_virtual` VALUES (31, 'aaa', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-11-24 15:08:22', 83, 'http://tengxun.com', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (32, 'bbb', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-11-24 15:08:22', 83, 'http://tengxun.com', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (33, 'ccc', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-11-24 15:08:22', 83, 'https://m.nn.com/mcenterList.html?region_code=1&language=zh_CN&platform=2', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (34, 'dskjjfhkfh', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-12-04 13:10:27', 83, 'http://www/cj', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (35, 'sfjsf', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-12-04 13:10:27', 83, 'http://www/cj', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (36, 'sfkj', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-12-04 13:10:27', 83, 'http://www/cj', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (37, 'aa', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-12-08 10:57:49', 83, 'ss', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (38, 'wwqq', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2020-12-08 10:57:57', 83, 'ww', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (39, 'sbfsb', 'https://www.gomyorder.cn/img/20210322/77756115a5e046ee9b0463d53fb5ecfe.jpg', '2021-03-22 14:59:53', 89, 'sdvsf', 3, 'dbfg');
INSERT INTO `self_goods_virtual` VALUES (40, 'grgde', 'https://www.gomyorder.cn/img/20210322/77756115a5e046ee9b0463d53fb5ecfe.jpg', '2021-03-22 15:00:17', 89, 'dgfr', 3, 'dbfg');
INSERT INTO `self_goods_virtual` VALUES (41, 'fdhgf', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 11:04:22', 83, 'gdf', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (42, 'fdgdf', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 11:05:48', 83, 'http://www.baidu.com', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (43, 'dsgds', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 11:12:36', 83, 'https://www.baidu.com/', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (44, 'dvd', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 11:13:27', 83, 'https://sqx.gomyorder.cn', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (45, 'gregee', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 13:39:16', 83, 'reter', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (46, 'gtrg', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 14:05:15', 83, 'trg', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (47, 'fdbf', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 14:05:32', 83, 'dhb', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (48, 'bgf', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 14:05:45', 83, 'bfg', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (49, 'vd', 'https://www.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-03-24 14:06:39', 83, 'dv', 2, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (50, '111111111111111', 'https://tk.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-04-12 13:25:49', 83, '1111111', 3, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (51, '654654', 'https://tk.gomyorder.cn/img/20201118/3c8f31bfe8604dc199e52c59a15dae18.png', '2021-06-28 15:38:30', 83, '4645', 1, '腾讯视频');
INSERT INTO `self_goods_virtual` VALUES (52, '64564', 'https://tk.gomyorder.cn/img/20210628/185be6791d9d4dc99d6b90602201f2cf.png', '2021-06-29 11:28:23', 92, '436456', 1, '衬衫');
INSERT INTO `self_goods_virtual` VALUES (53, '67575', 'https://tk.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2021-06-29 16:33:42', 80, '75675', 3, '省钱兄淘宝客系统');
INSERT INTO `self_goods_virtual` VALUES (54, '儿童', 'https://tk.gomyorder.cn/img/20201107/10febbaeed25419f82a8d2cf7782f792.png', '2021-08-03 09:45:08', 82, '儿童椅44444', 1, '诗凡黎衬衫女2020年新款秋装设计感小众天丝女装上衣长袖黄色衬衣');
INSERT INTO `self_goods_virtual` VALUES (55, '没有', 'https://tk.gomyorder.cn/img/20201028/800543bfd20e4d4b818e8797146a655b.png', '2021-08-10 15:32:09', 80, 'VC', 1, '省钱兄淘宝客系统');

-- ----------------------------
-- Table structure for self_merchant_apply
-- ----------------------------
DROP TABLE IF EXISTS `self_merchant_apply`;
CREATE TABLE `self_merchant_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `audit_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生效日期',
  `audit_years` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生效年限',
  `company_address_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '公司注册地址市',
  `company_address_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '公司注册详细地址',
  `company_address_district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '公司注册地址区',
  `company_address_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '公司注册地址省',
  `company_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '营业执照编号',
  `company_license_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '营业执照照片',
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商户名称',
  `company_term` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '营业期限',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `id_card_img1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '证件照片正面',
  `id_card_img2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '证件照片国徽面',
  `id_card_img3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手持证件照片',
  `id_card_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '身份证号码',
  `id_card_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '证件类型(1中国大陆居民身份证 2中国香港居民来往内地通行证 3中国澳门居民来往内地通行证 4中国台湾居民来往内地通行证 5其他国家或地区居民护照)',
  `id_card_valid_time_end` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '身份证有效期起始时间',
  `id_card_valid_time_start` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '身份证有效期截止时间',
  `legal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '店铺负责人',
  `legal_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人电话',
  `server_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '客服电话',
  `status` int(11) NULL DEFAULT NULL COMMENT '审核状态（1待处理 2通过 3拒绝）',
  `store_address_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '店铺地址详细',
  `store_address_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '店铺地址省市',
  `store_head` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人姓名',
  `store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '店铺名称',
  `store_type` int(11) NULL DEFAULT NULL COMMENT '店铺类型（1个人 2个体工商户 3企业 4其他组织）',
  `refund_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '拒绝原因',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_merchant_apply
-- ----------------------------

-- ----------------------------
-- Table structure for self_order_relation
-- ----------------------------
DROP TABLE IF EXISTS `self_order_relation`;
CREATE TABLE `self_order_relation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `commission_price` double NULL DEFAULT NULL COMMENT '订单佣金',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单id',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分销明细',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电话',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态 1未到账 2已到账',
  `finish_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '收货时间',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `goods_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品图片',
  `goods_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品标题',
  `lower_user_id` bigint(20) NULL DEFAULT NULL COMMENT '下级用户id',
  `lower_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '下级用户名称',
  `money_from` int(11) NULL DEFAULT NULL COMMENT '佣金来源（1直属 2自己 3非直属）',
  `pay_money` double NULL DEFAULT NULL COMMENT '支付金额',
  `commission_money` double NULL DEFAULT NULL COMMENT '订单佣金',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 424 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for self_order_remind
-- ----------------------------
DROP TABLE IF EXISTS `self_order_remind`;
CREATE TABLE `self_order_remind`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `orders_id` bigint(20) NULL DEFAULT NULL COMMENT '订单id',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态1提醒 2收到',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for self_order_sale
-- ----------------------------
DROP TABLE IF EXISTS `self_order_sale`;
CREATE TABLE `self_order_sale`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '未使用',
  `refused` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `apply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `orders_id` bigint(20) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_order_sale
-- ----------------------------

-- ----------------------------
-- Table structure for self_orders
-- ----------------------------
DROP TABLE IF EXISTS `self_orders`;
CREATE TABLE `self_orders`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `consignee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '收货人',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `descrition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单备注',
  `detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '详细地址',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品图片',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电话',
  `number` int(11) NULL DEFAULT NULL COMMENT '商品个数',
  `order_num` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单号',
  `pay_money` double NULL DEFAULT NULL COMMENT '支付金额',
  `price` double NULL DEFAULT NULL COMMENT '价格',
  `status` int(11) NULL DEFAULT NULL COMMENT '订单状态（1待付款 2已付款 3已发货 4已收货 5已取消 6退款中 7已退款 8拒绝退款 9拼团中 10已评价）',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '类型',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `finish_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '完成时间',
  `pay_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '支付时间',
  `pay_way` int(11) NULL DEFAULT NULL COMMENT '支付方式（1app微信 2微信公众号 3微信小程序 4app支付宝 5H5支付宝 6零钱）',
  `user_coupons_id` bigint(20) NULL DEFAULT NULL COMMENT '用户优惠券id',
  `commission_price` double NULL DEFAULT NULL COMMENT '订单佣金',
  `relation_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '会员邀请码id',
  `provinces` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '省市区',
  `express_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '快递名称',
  `express_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '快递单号',
  `refund` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '退款理由',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `is_express` int(11) NULL DEFAULT NULL COMMENT '是否需要发货(1需要发货 2无需发货)',
  `is_refund` int(11) NULL DEFAULT NULL COMMENT '是否可以退款（2不可退款）',
  `refused_refund` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '拒绝退款理由',
  `coupon_money` double NULL DEFAULT NULL COMMENT '优惠券金额',
  `detail_json` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品sku信息',
  `express_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '发货时间',
  `group_id` bigint(20) NULL DEFAULT NULL COMMENT '拼团商品Id',
  `group_pink_id` bigint(20) NULL DEFAULT NULL COMMENT '加入拼团团体id',
  `merchant_id` bigint(20) NULL DEFAULT NULL COMMENT '商户id',
  `order_type` int(11) NULL DEFAULT NULL COMMENT '订单类型（1普通订单 2拼团 3秒杀）',
  `pay_num` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '支付单号',
  `postage_price` double NULL DEFAULT NULL COMMENT '邮费',
  `sec_kill_id` bigint(20) NULL DEFAULT NULL COMMENT '秒杀商品id',
  `sku_id` bigint(20) NULL DEFAULT NULL COMMENT '商品skuId',
  `virtual_id` bigint(20) NULL DEFAULT NULL COMMENT '虚拟商品id',
  `member_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '会员价格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 572 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for self_prize
-- ----------------------------
DROP TABLE IF EXISTS `self_prize`;
CREATE TABLE `self_prize`  (
  `prize_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '奖品id',
  `add_ji_fen` int(11) NULL DEFAULT NULL COMMENT '中奖积分',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '奖品展示名称',
  `prize_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '奖品图片',
  `prize_rule_id` bigint(20) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `weight` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`prize_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_prize
-- ----------------------------
INSERT INTO `self_prize` VALUES (1, 0, '2020-11-25 16:59:43', '谢谢惠顾', 'https://api.shengqianxiong.com.cn/img/20201125/83eca1a126ab43ddb98d50317ba98550.png', 1, 1, 100);
INSERT INTO `self_prize` VALUES (2, 10, '2020-11-25 16:59:54', '10积分', 'https://api.shengqianxiong.com.cn/img/20201125/90f5eb02d4af4083a80e8dae51bdfd78.png', 1, 1, 30);
INSERT INTO `self_prize` VALUES (3, 50, '2020-11-25 17:00:04', '50积分', 'https://api.shengqianxiong.com.cn/img/20201125/b7c494a13a06457192a2b63f9e7bcb3f.png', 1, 1, 50);
INSERT INTO `self_prize` VALUES (4, 5, '2021-03-22 14:27:33', 'tyrt', '', 5, 1, 1);
INSERT INTO `self_prize` VALUES (6, 1, '2021-06-25 16:51:49', 'tytr', 'https://h5.xuexiaedu.com/img/20210625/5ed65002c33f417eb7bfa5876723dd48.png', 7, 1, 5);

-- ----------------------------
-- Table structure for self_prize_rule
-- ----------------------------
DROP TABLE IF EXISTS `self_prize_rule`;
CREATE TABLE `self_prize_rule`  (
  `prize_rule_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '奖池id',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '活动规则',
  `ji_fen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消耗积分',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '抽奖名称',
  `number` int(11) NULL DEFAULT NULL COMMENT '每日抽取次数',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态（1开启 2关闭）',
  PRIMARY KEY (`prize_rule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_prize_rule
-- ----------------------------
INSERT INTO `self_prize_rule` VALUES (1, '2020-11-23 13:55:18', '每人每天限制抽取3次', '10', '积分抽奖', 3, 1);

-- ----------------------------
-- Table structure for self_prize_user
-- ----------------------------
DROP TABLE IF EXISTS `self_prize_user`;
CREATE TABLE `self_prize_user`  (
  `prize_user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '抽奖记录id',
  `add_ji_fen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '增加的积分',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  `less_ji_fen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '减少的积分',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '昵称',
  `prize_id` bigint(20) NULL DEFAULT NULL COMMENT '奖品id',
  `prize_rule_id` bigint(20) NULL DEFAULT NULL COMMENT '奖池id',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`prize_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 189 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for self_tenant_apply
-- ----------------------------
DROP TABLE IF EXISTS `self_tenant_apply`;
CREATE TABLE `self_tenant_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '未使用',
  `company_address_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_address_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_address_district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_address_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_license_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `company_term` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `legal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `legal_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `server_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `store_address_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `store_address_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `store_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of self_tenant_apply
-- ----------------------------

-- ----------------------------
-- Table structure for self_user_collect
-- ----------------------------
DROP TABLE IF EXISTS `self_user_collect`;
CREATE TABLE `self_user_collect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cover_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `goods_id` bigint(20) NULL DEFAULT NULL,
  `price` double NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for send_money_Integral
-- ----------------------------
DROP TABLE IF EXISTS `send_money_Integral`;
CREATE TABLE `send_money_Integral`  (
  `send_money_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '积分奖励金额规则id',
  `integral_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '积分推送活动名称',
  `describes` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `integral_num` int(11) NULL DEFAULT NULL COMMENT '积分',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态（1启用 2停止）',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`send_money_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of send_money_Integral
-- ----------------------------

-- ----------------------------
-- Table structure for send_money_Integral_details
-- ----------------------------
DROP TABLE IF EXISTS `send_money_Integral_details`;
CREATE TABLE `send_money_Integral_details`  (
  `send_money_details_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '积分推送详细id',
  `send_money_id` int(11) NULL DEFAULT NULL COMMENT '积分推送id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`send_money_details_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 623 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha`  (
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'uuid',
  `code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统验证码' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', 0, '云存储配置信息');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典码',
  `value` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典值',
  `order_num` int(11) NULL DEFAULT 0 COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type`(`type`, `code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 537 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 229 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 20);
INSERT INTO `sys_menu` VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (32, 0, '用户中心', 'userList', NULL, 1, 'yonghul', 1);
INSERT INTO `sys_menu` VALUES (34, 0, '财务中心', 'financeList', '', 1, 'caiwu', 2);
INSERT INTO `sys_menu` VALUES (35, 0, '消息中心', 'message', '', 1, 'pinglun', 3);
INSERT INTO `sys_menu` VALUES (38, 0, '首页装修', 'bannerList', '', 1, 'shangpin', 4);
INSERT INTO `sys_menu` VALUES (39, 0, '系统配置', 'allocationList', '', 1, 'zhedie', 7);
INSERT INTO `sys_menu` VALUES (40, 0, '数据中心', 'home', '', 1, 'shuju', 0);
INSERT INTO `sys_menu` VALUES (46, 32, '查看', '', 'userList:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (47, 32, '删除', '', 'userList:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (48, 34, '查看', '', 'financeList:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (49, 34, '转账', '', 'financeList:transfer', 2, '', 0);
INSERT INTO `sys_menu` VALUES (50, 34, '退款', '', 'financeList:refund', 2, '', 0);
INSERT INTO `sys_menu` VALUES (51, 34, '添加', '', 'financeList:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (52, 34, '修改', '', 'financeList:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (53, 34, '删除', '', 'financeList:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (78, 38, '查看', '', 'bannerList:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (79, 38, '添加', '', 'bannerList:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (80, 38, '修改', '', 'bannerList:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (81, 38, '删除', '', 'bannerList:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (82, 39, '查看', '', 'allocationList:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (83, 39, '修改', '', 'allocationList:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (98, 0, '更多工具', '', '', 0, 'fenleilist', 6);
INSERT INTO `sys_menu` VALUES (100, 98, '活动派送', 'missionsye', '', 1, 'pingtai', 1);
INSERT INTO `sys_menu` VALUES (105, 100, '查看', '', 'missionsye:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (106, 100, '添加', '', 'missionsye:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (107, 100, '修改', '', 'missionsye:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (108, 100, '删除', '', 'missionsye:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (109, 100, '通过', '', 'missionsye:pass', 2, '', 0);
INSERT INTO `sys_menu` VALUES (110, 100, '拒绝', '', 'missionsye:refuse', 2, '', 0);
INSERT INTO `sys_menu` VALUES (111, 100, '提醒', '', 'missionsye:warn', 2, '', 0);
INSERT INTO `sys_menu` VALUES (112, 100, '放弃', '', 'missionsye:waive', 2, '', 0);
INSERT INTO `sys_menu` VALUES (119, 0, '升级配置', 'fitmentList', '', 1, 'shouye', 10);
INSERT INTO `sys_menu` VALUES (120, 119, '查看', '', 'fitmentList:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (121, 119, '添加', '', 'fitmentList:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (122, 119, '修改', '', 'fitmentList:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (123, 119, '删除', '', 'fitmentList:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (124, 35, '查看', 'message', 'message:list', 2, 'pinglun', 3);
INSERT INTO `sys_menu` VALUES (125, 35, '消息推送', 'message', 'message:push', 2, 'pinglun', 3);
INSERT INTO `sys_menu` VALUES (133, 98, '积分推送', 'integral', '', 1, 'jifen', 2);
INSERT INTO `sys_menu` VALUES (134, 133, '查看', '', 'integral:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (135, 133, '添加', '', 'integral:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (136, 133, '修改', '', 'integral:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (137, 133, '删除', '', 'integral:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (145, 0, '任务中心', 'mission', '', 1, 'renwu1', 4);
INSERT INTO `sys_menu` VALUES (146, 145, '查看', '', 'mission:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (147, 145, '添加', '', 'mission:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (148, 145, '修改', '', 'mission:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (149, 145, '删除', '', 'mission:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (150, 145, '通过', '', 'mission:pass', 2, '', 0);
INSERT INTO `sys_menu` VALUES (151, 145, '拒绝', '', 'mission:refuse', 2, '', 0);
INSERT INTO `sys_menu` VALUES (152, 0, '任务配置', 'taskConfig', '', 1, 'config', 4);
INSERT INTO `sys_menu` VALUES (153, 152, '查看', '', 'taskConfig:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (154, 152, '修改', '', 'taskConfig:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (155, 0, '系统任务', 'system', '', 1, 'order', 4);
INSERT INTO `sys_menu` VALUES (156, 155, '查看', '', 'system:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (157, 155, '添加', '', 'system:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (158, 155, '修改', '', 'system:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (159, 155, '删除', '', 'system:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (160, 155, '发布任务', '', 'system:issue', 2, '', 0);
INSERT INTO `sys_menu` VALUES (161, 0, '投诉管理', 'missionComplain', '', 1, 'ordercenter', 4);
INSERT INTO `sys_menu` VALUES (191, 0, '自营商城', 'classifyAdmin', '', 0, 'fenleilist', 4);
INSERT INTO `sys_menu` VALUES (192, 191, '商品分类', 'classifyAdmin', '', 1, 'fenleilist', 0);
INSERT INTO `sys_menu` VALUES (193, 192, '查看', 'classifyAdmin', 'classifyAdmin:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (194, 192, '添加', 'classifyAdmin', 'classifyAdmin:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (195, 192, '修改', 'classifyAdmin', 'classifyAdmin:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (196, 192, '删除', 'classifyAdmin', 'classifyAdmin:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (197, 191, '商品管理', 'shopAdmin', '', 1, 'renwu', 0);
INSERT INTO `sys_menu` VALUES (198, 197, '查看', 'classifyAdmin', 'shopAdmin:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (199, 197, '添加', 'classifyAdmin', 'shopAdmin:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (200, 197, '修改', 'classifyAdmin', 'shopAdmin:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (201, 197, '删除', 'classifyAdmin', 'shopAdmin:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (202, 191, '虚拟商品', 'virtual', '', 1, 'fenleilist', 0);
INSERT INTO `sys_menu` VALUES (203, 202, '查看', 'classifyAdmin', 'virtual:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (204, 202, '补货', 'classifyAdmin', 'virtual:replen', 2, '', 0);
INSERT INTO `sys_menu` VALUES (205, 191, '自营订单', 'orderAdmin', '', 1, 'ordercenter', 0);
INSERT INTO `sys_menu` VALUES (206, 205, '查看', 'classifyAdmin', 'orderAdmin:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (207, 205, '去发货', 'classifyAdmin', 'orderAdmin:deliver', 2, '', 0);
INSERT INTO `sys_menu` VALUES (208, 205, '退款', 'classifyAdmin', 'orderAdmin:refund', 2, '', 0);
INSERT INTO `sys_menu` VALUES (210, 191, '待处理订单', 'disposeOrder', '', 1, 'ordercenter', 0);
INSERT INTO `sys_menu` VALUES (211, 191, '自营商城配置', 'shopConfig', '', 1, 'config', 0);
INSERT INTO `sys_menu` VALUES (212, 211, '查看', '', 'shopConfig:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (213, 211, '添加', '', 'shopConfig:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (214, 211, '修改', '', 'shopConfig:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (215, 211, '删除', '', 'shopConfig:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (216, 191, '商品规格', 'specification', '', 1, 'renwu', 0);
INSERT INTO `sys_menu` VALUES (217, 216, '查看', '', 'specification:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (218, 216, '添加', '', 'specification:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (219, 216, '修改', '', 'specification:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (220, 216, '删除', '', 'specification:delete', 2, '', 0);
INSERT INTO `sys_menu` VALUES (221, 98, '聊天室', 'vueMchat', '', 1, 'xiaoxi', 0);
INSERT INTO `sys_menu` VALUES (222, 191, '商城配置', 'taskConfig1', '', 1, 'sql', 0);
INSERT INTO `sys_menu` VALUES (223, 222, '列表', '', 'taskConfig1:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (224, 191, '商品品牌', 'brandShop', '', 1, 'editor', 0);
INSERT INTO `sys_menu` VALUES (225, 224, '查看', '', 'brandShop:list', 2, '', 0);
INSERT INTO `sys_menu` VALUES (226, 224, '添加', '', 'brandShop:add', 2, '', 0);
INSERT INTO `sys_menu` VALUES (227, 224, '修改', '', 'brandShop:update', 2, '', 0);
INSERT INTO `sys_menu` VALUES (228, 224, '删除', '', 'brandShop:delete', 2, '', 0);

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件上传' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (3, '超级管理员', '超级管理员', 35, '2020-08-10 14:28:56');
INSERT INTO `sys_role` VALUES (4, '普通管理员', '普通管理员', 1, '2020-08-10 14:35:08');
INSERT INTO `sys_role` VALUES (5, '运营人员', '运营人员', 1, '2020-08-10 14:35:29');
INSERT INTO `sys_role` VALUES (6, '财务', '财务', 1, '2020-08-10 14:35:44');
INSERT INTO `sys_role` VALUES (9, '任务测试', '任务测试', 1, '2020-12-06 14:47:43');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5123 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (123, 7, 0);
INSERT INTO `sys_role_menu` VALUES (287, 6, 34);
INSERT INTO `sys_role_menu` VALUES (288, 6, -666666);
INSERT INTO `sys_role_menu` VALUES (4018, 5, 40);
INSERT INTO `sys_role_menu` VALUES (4019, 5, 46);
INSERT INTO `sys_role_menu` VALUES (4020, 5, 48);
INSERT INTO `sys_role_menu` VALUES (4021, 5, 35);
INSERT INTO `sys_role_menu` VALUES (4022, 5, 124);
INSERT INTO `sys_role_menu` VALUES (4023, 5, 125);
INSERT INTO `sys_role_menu` VALUES (4024, 5, 78);
INSERT INTO `sys_role_menu` VALUES (4025, 5, 163);
INSERT INTO `sys_role_menu` VALUES (4026, 5, 165);
INSERT INTO `sys_role_menu` VALUES (4027, 5, 168);
INSERT INTO `sys_role_menu` VALUES (4028, 5, 164);
INSERT INTO `sys_role_menu` VALUES (4029, 5, 167);
INSERT INTO `sys_role_menu` VALUES (4030, 5, 82);
INSERT INTO `sys_role_menu` VALUES (4031, 5, 15);
INSERT INTO `sys_role_menu` VALUES (4032, 5, 19);
INSERT INTO `sys_role_menu` VALUES (4033, 5, 23);
INSERT INTO `sys_role_menu` VALUES (4034, 5, -666666);
INSERT INTO `sys_role_menu` VALUES (4035, 5, 32);
INSERT INTO `sys_role_menu` VALUES (4036, 5, 34);
INSERT INTO `sys_role_menu` VALUES (4037, 5, 38);
INSERT INTO `sys_role_menu` VALUES (4038, 5, 39);
INSERT INTO `sys_role_menu` VALUES (4039, 5, 1);
INSERT INTO `sys_role_menu` VALUES (4040, 5, 2);
INSERT INTO `sys_role_menu` VALUES (4041, 5, 3);
INSERT INTO `sys_role_menu` VALUES (4042, 5, 4);
INSERT INTO `sys_role_menu` VALUES (4163, 4, 40);
INSERT INTO `sys_role_menu` VALUES (4164, 4, 46);
INSERT INTO `sys_role_menu` VALUES (4165, 4, 48);
INSERT INTO `sys_role_menu` VALUES (4166, 4, 49);
INSERT INTO `sys_role_menu` VALUES (4167, 4, 50);
INSERT INTO `sys_role_menu` VALUES (4168, 4, 35);
INSERT INTO `sys_role_menu` VALUES (4169, 4, 124);
INSERT INTO `sys_role_menu` VALUES (4170, 4, 125);
INSERT INTO `sys_role_menu` VALUES (4171, 4, 78);
INSERT INTO `sys_role_menu` VALUES (4172, 4, 145);
INSERT INTO `sys_role_menu` VALUES (4173, 4, 146);
INSERT INTO `sys_role_menu` VALUES (4174, 4, 147);
INSERT INTO `sys_role_menu` VALUES (4175, 4, 148);
INSERT INTO `sys_role_menu` VALUES (4176, 4, 149);
INSERT INTO `sys_role_menu` VALUES (4177, 4, 150);
INSERT INTO `sys_role_menu` VALUES (4178, 4, 151);
INSERT INTO `sys_role_menu` VALUES (4179, 4, 153);
INSERT INTO `sys_role_menu` VALUES (4180, 4, 155);
INSERT INTO `sys_role_menu` VALUES (4181, 4, 156);
INSERT INTO `sys_role_menu` VALUES (4182, 4, 157);
INSERT INTO `sys_role_menu` VALUES (4183, 4, 158);
INSERT INTO `sys_role_menu` VALUES (4184, 4, 159);
INSERT INTO `sys_role_menu` VALUES (4185, 4, 160);
INSERT INTO `sys_role_menu` VALUES (4186, 4, 161);
INSERT INTO `sys_role_menu` VALUES (4187, 4, 113);
INSERT INTO `sys_role_menu` VALUES (4188, 4, 114);
INSERT INTO `sys_role_menu` VALUES (4189, 4, 115);
INSERT INTO `sys_role_menu` VALUES (4190, 4, 116);
INSERT INTO `sys_role_menu` VALUES (4191, 4, 117);
INSERT INTO `sys_role_menu` VALUES (4192, 4, 118);
INSERT INTO `sys_role_menu` VALUES (4193, 4, 119);
INSERT INTO `sys_role_menu` VALUES (4194, 4, 120);
INSERT INTO `sys_role_menu` VALUES (4195, 4, 121);
INSERT INTO `sys_role_menu` VALUES (4196, 4, 122);
INSERT INTO `sys_role_menu` VALUES (4197, 4, 123);
INSERT INTO `sys_role_menu` VALUES (4198, 4, 98);
INSERT INTO `sys_role_menu` VALUES (4199, 4, 100);
INSERT INTO `sys_role_menu` VALUES (4200, 4, 105);
INSERT INTO `sys_role_menu` VALUES (4201, 4, 106);
INSERT INTO `sys_role_menu` VALUES (4202, 4, 107);
INSERT INTO `sys_role_menu` VALUES (4203, 4, 108);
INSERT INTO `sys_role_menu` VALUES (4204, 4, 109);
INSERT INTO `sys_role_menu` VALUES (4205, 4, 110);
INSERT INTO `sys_role_menu` VALUES (4206, 4, 111);
INSERT INTO `sys_role_menu` VALUES (4207, 4, 112);
INSERT INTO `sys_role_menu` VALUES (4208, 4, 133);
INSERT INTO `sys_role_menu` VALUES (4209, 4, 134);
INSERT INTO `sys_role_menu` VALUES (4210, 4, 135);
INSERT INTO `sys_role_menu` VALUES (4211, 4, 136);
INSERT INTO `sys_role_menu` VALUES (4212, 4, 137);
INSERT INTO `sys_role_menu` VALUES (4213, 4, 82);
INSERT INTO `sys_role_menu` VALUES (4214, 4, 15);
INSERT INTO `sys_role_menu` VALUES (4215, 4, 19);
INSERT INTO `sys_role_menu` VALUES (4216, 4, 23);
INSERT INTO `sys_role_menu` VALUES (4217, 4, -666666);
INSERT INTO `sys_role_menu` VALUES (4218, 4, 32);
INSERT INTO `sys_role_menu` VALUES (4219, 4, 34);
INSERT INTO `sys_role_menu` VALUES (4220, 4, 38);
INSERT INTO `sys_role_menu` VALUES (4221, 4, 152);
INSERT INTO `sys_role_menu` VALUES (4222, 4, 39);
INSERT INTO `sys_role_menu` VALUES (4223, 4, 1);
INSERT INTO `sys_role_menu` VALUES (4224, 4, 2);
INSERT INTO `sys_role_menu` VALUES (4225, 4, 3);
INSERT INTO `sys_role_menu` VALUES (4226, 4, 4);
INSERT INTO `sys_role_menu` VALUES (4352, 9, 40);
INSERT INTO `sys_role_menu` VALUES (4353, 9, 46);
INSERT INTO `sys_role_menu` VALUES (4354, 9, 48);
INSERT INTO `sys_role_menu` VALUES (4355, 9, 50);
INSERT INTO `sys_role_menu` VALUES (4356, 9, 124);
INSERT INTO `sys_role_menu` VALUES (4357, 9, 78);
INSERT INTO `sys_role_menu` VALUES (4358, 9, 146);
INSERT INTO `sys_role_menu` VALUES (4359, 9, 147);
INSERT INTO `sys_role_menu` VALUES (4360, 9, 148);
INSERT INTO `sys_role_menu` VALUES (4361, 9, 150);
INSERT INTO `sys_role_menu` VALUES (4362, 9, 151);
INSERT INTO `sys_role_menu` VALUES (4363, 9, 153);
INSERT INTO `sys_role_menu` VALUES (4364, 9, 156);
INSERT INTO `sys_role_menu` VALUES (4365, 9, 157);
INSERT INTO `sys_role_menu` VALUES (4366, 9, 158);
INSERT INTO `sys_role_menu` VALUES (4367, 9, 160);
INSERT INTO `sys_role_menu` VALUES (4368, 9, 161);
INSERT INTO `sys_role_menu` VALUES (4369, 9, 115);
INSERT INTO `sys_role_menu` VALUES (4370, 9, 116);
INSERT INTO `sys_role_menu` VALUES (4371, 9, 117);
INSERT INTO `sys_role_menu` VALUES (4372, 9, 120);
INSERT INTO `sys_role_menu` VALUES (4373, 9, 121);
INSERT INTO `sys_role_menu` VALUES (4374, 9, 122);
INSERT INTO `sys_role_menu` VALUES (4375, 9, 105);
INSERT INTO `sys_role_menu` VALUES (4376, 9, 106);
INSERT INTO `sys_role_menu` VALUES (4377, 9, 107);
INSERT INTO `sys_role_menu` VALUES (4378, 9, 109);
INSERT INTO `sys_role_menu` VALUES (4379, 9, 110);
INSERT INTO `sys_role_menu` VALUES (4380, 9, 111);
INSERT INTO `sys_role_menu` VALUES (4381, 9, 112);
INSERT INTO `sys_role_menu` VALUES (4382, 9, 134);
INSERT INTO `sys_role_menu` VALUES (4383, 9, 135);
INSERT INTO `sys_role_menu` VALUES (4384, 9, 136);
INSERT INTO `sys_role_menu` VALUES (4385, 9, 82);
INSERT INTO `sys_role_menu` VALUES (4386, 9, 15);
INSERT INTO `sys_role_menu` VALUES (4387, 9, 19);
INSERT INTO `sys_role_menu` VALUES (4388, 9, 23);
INSERT INTO `sys_role_menu` VALUES (4389, 9, -666666);
INSERT INTO `sys_role_menu` VALUES (4390, 9, 32);
INSERT INTO `sys_role_menu` VALUES (4391, 9, 34);
INSERT INTO `sys_role_menu` VALUES (4392, 9, 35);
INSERT INTO `sys_role_menu` VALUES (4393, 9, 38);
INSERT INTO `sys_role_menu` VALUES (4394, 9, 145);
INSERT INTO `sys_role_menu` VALUES (4395, 9, 152);
INSERT INTO `sys_role_menu` VALUES (4396, 9, 155);
INSERT INTO `sys_role_menu` VALUES (4397, 9, 113);
INSERT INTO `sys_role_menu` VALUES (4398, 9, 114);
INSERT INTO `sys_role_menu` VALUES (4399, 9, 119);
INSERT INTO `sys_role_menu` VALUES (4400, 9, 98);
INSERT INTO `sys_role_menu` VALUES (4401, 9, 100);
INSERT INTO `sys_role_menu` VALUES (4402, 9, 133);
INSERT INTO `sys_role_menu` VALUES (4403, 9, 39);
INSERT INTO `sys_role_menu` VALUES (4404, 9, 1);
INSERT INTO `sys_role_menu` VALUES (4405, 9, 2);
INSERT INTO `sys_role_menu` VALUES (4406, 9, 3);
INSERT INTO `sys_role_menu` VALUES (4407, 9, 4);
INSERT INTO `sys_role_menu` VALUES (5010, 3, 40);
INSERT INTO `sys_role_menu` VALUES (5011, 3, 32);
INSERT INTO `sys_role_menu` VALUES (5012, 3, 46);
INSERT INTO `sys_role_menu` VALUES (5013, 3, 47);
INSERT INTO `sys_role_menu` VALUES (5014, 3, 34);
INSERT INTO `sys_role_menu` VALUES (5015, 3, 48);
INSERT INTO `sys_role_menu` VALUES (5016, 3, 49);
INSERT INTO `sys_role_menu` VALUES (5017, 3, 50);
INSERT INTO `sys_role_menu` VALUES (5018, 3, 51);
INSERT INTO `sys_role_menu` VALUES (5019, 3, 52);
INSERT INTO `sys_role_menu` VALUES (5020, 3, 53);
INSERT INTO `sys_role_menu` VALUES (5021, 3, 35);
INSERT INTO `sys_role_menu` VALUES (5022, 3, 124);
INSERT INTO `sys_role_menu` VALUES (5023, 3, 125);
INSERT INTO `sys_role_menu` VALUES (5024, 3, 38);
INSERT INTO `sys_role_menu` VALUES (5025, 3, 78);
INSERT INTO `sys_role_menu` VALUES (5026, 3, 79);
INSERT INTO `sys_role_menu` VALUES (5027, 3, 80);
INSERT INTO `sys_role_menu` VALUES (5028, 3, 81);
INSERT INTO `sys_role_menu` VALUES (5029, 3, 145);
INSERT INTO `sys_role_menu` VALUES (5030, 3, 146);
INSERT INTO `sys_role_menu` VALUES (5031, 3, 147);
INSERT INTO `sys_role_menu` VALUES (5032, 3, 148);
INSERT INTO `sys_role_menu` VALUES (5033, 3, 149);
INSERT INTO `sys_role_menu` VALUES (5034, 3, 150);
INSERT INTO `sys_role_menu` VALUES (5035, 3, 151);
INSERT INTO `sys_role_menu` VALUES (5036, 3, 152);
INSERT INTO `sys_role_menu` VALUES (5037, 3, 153);
INSERT INTO `sys_role_menu` VALUES (5038, 3, 154);
INSERT INTO `sys_role_menu` VALUES (5039, 3, 155);
INSERT INTO `sys_role_menu` VALUES (5040, 3, 156);
INSERT INTO `sys_role_menu` VALUES (5041, 3, 157);
INSERT INTO `sys_role_menu` VALUES (5042, 3, 158);
INSERT INTO `sys_role_menu` VALUES (5043, 3, 159);
INSERT INTO `sys_role_menu` VALUES (5044, 3, 160);
INSERT INTO `sys_role_menu` VALUES (5045, 3, 161);
INSERT INTO `sys_role_menu` VALUES (5046, 3, 191);
INSERT INTO `sys_role_menu` VALUES (5047, 3, 192);
INSERT INTO `sys_role_menu` VALUES (5048, 3, 193);
INSERT INTO `sys_role_menu` VALUES (5049, 3, 194);
INSERT INTO `sys_role_menu` VALUES (5050, 3, 195);
INSERT INTO `sys_role_menu` VALUES (5051, 3, 196);
INSERT INTO `sys_role_menu` VALUES (5052, 3, 197);
INSERT INTO `sys_role_menu` VALUES (5053, 3, 198);
INSERT INTO `sys_role_menu` VALUES (5054, 3, 199);
INSERT INTO `sys_role_menu` VALUES (5055, 3, 200);
INSERT INTO `sys_role_menu` VALUES (5056, 3, 201);
INSERT INTO `sys_role_menu` VALUES (5057, 3, 202);
INSERT INTO `sys_role_menu` VALUES (5058, 3, 203);
INSERT INTO `sys_role_menu` VALUES (5059, 3, 204);
INSERT INTO `sys_role_menu` VALUES (5060, 3, 205);
INSERT INTO `sys_role_menu` VALUES (5061, 3, 206);
INSERT INTO `sys_role_menu` VALUES (5062, 3, 207);
INSERT INTO `sys_role_menu` VALUES (5063, 3, 208);
INSERT INTO `sys_role_menu` VALUES (5064, 3, 210);
INSERT INTO `sys_role_menu` VALUES (5065, 3, 211);
INSERT INTO `sys_role_menu` VALUES (5066, 3, 212);
INSERT INTO `sys_role_menu` VALUES (5067, 3, 213);
INSERT INTO `sys_role_menu` VALUES (5068, 3, 214);
INSERT INTO `sys_role_menu` VALUES (5069, 3, 215);
INSERT INTO `sys_role_menu` VALUES (5070, 3, 216);
INSERT INTO `sys_role_menu` VALUES (5071, 3, 217);
INSERT INTO `sys_role_menu` VALUES (5072, 3, 218);
INSERT INTO `sys_role_menu` VALUES (5073, 3, 219);
INSERT INTO `sys_role_menu` VALUES (5074, 3, 220);
INSERT INTO `sys_role_menu` VALUES (5075, 3, 222);
INSERT INTO `sys_role_menu` VALUES (5076, 3, 223);
INSERT INTO `sys_role_menu` VALUES (5077, 3, 224);
INSERT INTO `sys_role_menu` VALUES (5078, 3, 225);
INSERT INTO `sys_role_menu` VALUES (5079, 3, 226);
INSERT INTO `sys_role_menu` VALUES (5080, 3, 227);
INSERT INTO `sys_role_menu` VALUES (5081, 3, 228);
INSERT INTO `sys_role_menu` VALUES (5082, 3, 98);
INSERT INTO `sys_role_menu` VALUES (5083, 3, 221);
INSERT INTO `sys_role_menu` VALUES (5084, 3, 100);
INSERT INTO `sys_role_menu` VALUES (5085, 3, 105);
INSERT INTO `sys_role_menu` VALUES (5086, 3, 106);
INSERT INTO `sys_role_menu` VALUES (5087, 3, 107);
INSERT INTO `sys_role_menu` VALUES (5088, 3, 108);
INSERT INTO `sys_role_menu` VALUES (5089, 3, 109);
INSERT INTO `sys_role_menu` VALUES (5090, 3, 110);
INSERT INTO `sys_role_menu` VALUES (5091, 3, 111);
INSERT INTO `sys_role_menu` VALUES (5092, 3, 112);
INSERT INTO `sys_role_menu` VALUES (5093, 3, 133);
INSERT INTO `sys_role_menu` VALUES (5094, 3, 134);
INSERT INTO `sys_role_menu` VALUES (5095, 3, 135);
INSERT INTO `sys_role_menu` VALUES (5096, 3, 136);
INSERT INTO `sys_role_menu` VALUES (5097, 3, 137);
INSERT INTO `sys_role_menu` VALUES (5098, 3, 39);
INSERT INTO `sys_role_menu` VALUES (5099, 3, 82);
INSERT INTO `sys_role_menu` VALUES (5100, 3, 83);
INSERT INTO `sys_role_menu` VALUES (5101, 3, 119);
INSERT INTO `sys_role_menu` VALUES (5102, 3, 120);
INSERT INTO `sys_role_menu` VALUES (5103, 3, 121);
INSERT INTO `sys_role_menu` VALUES (5104, 3, 122);
INSERT INTO `sys_role_menu` VALUES (5105, 3, 123);
INSERT INTO `sys_role_menu` VALUES (5106, 3, 1);
INSERT INTO `sys_role_menu` VALUES (5107, 3, 2);
INSERT INTO `sys_role_menu` VALUES (5108, 3, 15);
INSERT INTO `sys_role_menu` VALUES (5109, 3, 16);
INSERT INTO `sys_role_menu` VALUES (5110, 3, 17);
INSERT INTO `sys_role_menu` VALUES (5111, 3, 18);
INSERT INTO `sys_role_menu` VALUES (5112, 3, 3);
INSERT INTO `sys_role_menu` VALUES (5113, 3, 19);
INSERT INTO `sys_role_menu` VALUES (5114, 3, 20);
INSERT INTO `sys_role_menu` VALUES (5115, 3, 21);
INSERT INTO `sys_role_menu` VALUES (5116, 3, 22);
INSERT INTO `sys_role_menu` VALUES (5117, 3, 4);
INSERT INTO `sys_role_menu` VALUES (5118, 3, 23);
INSERT INTO `sys_role_menu` VALUES (5119, 3, 24);
INSERT INTO `sys_role_menu` VALUES (5120, 3, 25);
INSERT INTO `sys_role_menu` VALUES (5121, 3, 26);
INSERT INTO `sys_role_menu` VALUES (5122, 3, -666666);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'ed698cdd24c33647d1135f0cb261e6f99def7b37f48b1e048d5181df5d7ea2b3', 'YzcmCZNvbXocrsz9dm8e', '1212121254@qq.com', '13612345678', 1, 1, '2016-11-11 11:11:11');
-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------

INSERT INTO `sys_user_role` VALUES (43, 1, 3);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户Token' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `open_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公众号openid',
  `clientid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息推送clientid',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `member` int(11) NULL DEFAULT 1 COMMENT '会员 1非会员 2初 3中 4高',
  `invitation_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邀请码',
  `superior` int(11) NULL DEFAULT NULL COMMENT '上级用户id',
  `inviter_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邀请人邀请码',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `union_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信unionId',
  `image_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `gender` int(11) NULL DEFAULT 0 COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `zhifubao` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付宝账号',
  `zhifubao_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付宝名称',
  `open_uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT 1 COMMENT '状态 1正常',
  `is_task` int(11) NULL DEFAULT 1 COMMENT '是否可以发任务 1可以 2不可以',
  `is_sys_user` int(11) NULL DEFAULT 1 COMMENT '是否是系统用户 1不是 2是',
  `wx_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '小程序openId',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1006 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'mark', NULL, '3794d48907d35c355d57e4e09c1578d3', '18071085611', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 4, 'UKSLFX', 1, 'UKSLFX', '2017-03-23 22:37:41', NULL, 'https://www.gomyorder.cn/logo.png', 0, NULL, '张三', '18071085611', NULL, NULL, 1, 1, 1, NULL);

-- ----------------------------
-- Table structure for timed_task
-- ----------------------------
DROP TABLE IF EXISTS `timed_task`;
CREATE TABLE `timed_task`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '定时任务id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务名称',
  `object_id` int(11) NULL DEFAULT NULL COMMENT '对应id',
  `type` int(11) NULL DEFAULT 0 COMMENT '分类（1助力活动 ）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1258 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for user_integral
-- ----------------------------
DROP TABLE IF EXISTS `user_integral`;
CREATE TABLE `user_integral`  (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `integral_num` int(11) NULL DEFAULT NULL COMMENT '积分数量',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for user_integral_details
-- ----------------------------
DROP TABLE IF EXISTS `user_integral_details`;
CREATE TABLE `user_integral_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '积分详情id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `classify` int(255) NULL DEFAULT NULL COMMENT '获取类型 1签到',
  `type` int(255) NULL DEFAULT NULL COMMENT '分类 1增加 2减少',
  `num` int(11) NULL DEFAULT NULL COMMENT '数量',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1415 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for user_money
-- ----------------------------
DROP TABLE IF EXISTS `user_money`;
CREATE TABLE `user_money`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '钱包id',
  `cannot_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '钱包金额(不可提现金额)',
  `may_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '钱包金额(可提现金额)',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 839 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户钱包' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for user_money_details
-- ----------------------------
DROP TABLE IF EXISTS `user_money_details`;
CREATE TABLE `user_money_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '钱包详情id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `type` int(11) NULL DEFAULT 1 COMMENT '类别（0充值1收益2支出）',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `create_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10682 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;


SET FOREIGN_KEY_CHECKS = 1;
